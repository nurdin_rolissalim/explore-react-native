import React,{Component} from 'react';
import {StyleSheet, View, FlatList,Image} from 'react-native';
import { Container, Header, Content, Thumbnail, Text } from 'native-base';
import { Truncate } from '../../core/helper/GlobalFunction';

class Person extends Component{
  constructor(props){
    super(props);   
    this.state={
      data:[],
      analyze:props.analyze,
    } 
  }
  render(){
    const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";
    switch(this.state.analyze){
      case"home-person":
          this.state.data=[
            {key: 'Arta'},
            {key: 'Yayuk'},
            {key: 'Zul'},
            {key: 'Indra'},
            {key: 'Mey'},
            {key: 'Evi'},
            {key: 'Dewi'},
            {key: 'Jillian'},
            {key: 'Jimmy'},
            {key: 'Julie'},
            {key: 'Nurdin Rolissalim'},
          ]
        break;
      case"activity-person":
        this.state.data=[
          {key: 'Nurdin Rolissalim'},
          {key: 'Amin'},
          {key: 'Anwar'},
          {key: 'Dida'},
          {key: 'James'},
          {key: 'Joel'},
          {key: 'John'},
          {key: 'Jillian'},
          {key: 'Jimmy'},
          {key: 'Julie'},
        ]
        break;  
    }
    return (
        <View style={styles.boxSmall}>
          <FlatList
            style={[{paddingTop: 5}]}
            data={this.state.data}
            renderItem={({item}) => (
              // <View style={[styles.myButton]}>
                // {/* <Thumbnail style={{width: 60, height: 60}} source={{uri: uri}} /> */}
                // {/* <Image   style={{width: 70, height: 70}} source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}/> */}
                // {/* <Text styles="{s.item}">{item.key}</Text> */}
              // </View>
              <View style={{marginHorizontal:6}} > 
                <Thumbnail circle source={{uri: uri}} />
                 <Text style={{textAlign: 'center',}}>{Truncate(item.key,6,"...")}</Text> 
              </View>
            )}
            horizontal
            showsHorizontalScrollIndicator={false}></FlatList>
        </View>     

      //   <Container>
      //     <Content>         
      //       <Thumbnail large source={{uri: uri}} />
      //     </Content>
      // </Container>
    );
  }
};

const styles = StyleSheet.create({
  boxSmall: {
    // width: 200,
    height: 90,
    marginBottom: 5,
    marginRight: 5,
    marginTop: 5,
    marginLeft: 5,
    // backgroundColor: 'skyblue',
    // padding:1,
  },
  myButton: {
    padding: 10,
    marginLeft: 5,
    height: 75,
    width: 75, //The Width must be the same as the height
    borderRadius: 400, //Then Make the Border Radius twice the size of width or Height
    backgroundColor: 'rgb(195, 125, 198)',
  },
});

export default Person;
