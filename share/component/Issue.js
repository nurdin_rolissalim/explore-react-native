import React,{Component} from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import { Container, Header, Content, Button, Text } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Issue extends Component{
  constructor(props){
    super(props);   
    this.state={
      data:[],
      analyze:props.analyze,
    } 
  }
  render(){
    switch(this.state.analyze){
      case"home-issue":
          this.state.data=[
            {key: 'Banjir'},
            {key: 'Salah Anies ?'},
            {key: 'Zul'},
            {key: 'Indra'},
            {key: 'Mey'},
            {key: 'Evi'},
            {key: 'Dewi'},
            {key: 'Jillian'},
            {key: 'Jimmy'},
            {key: 'Julie'},
          ]
        break;
      case"activity-person":
        this.state.data=[
          {key: 'Kerja Sama 3 Provinsi'},
          {key: 'Amin'},
          {key: 'Anwar'},
          {key: 'Dida'},
          {key: 'James'},
          {key: 'Joel'},
          {key: 'John'},
          {key: 'Jillian'},
          {key: 'Jimmy'},
          {key: 'Julie'},
        ]
        break;
    }
    return (
      <View style={styles.boxSmall}>
        <FlatList
          style={[{paddingTop: 5}]}
          data={this.state.data}
          renderItem={props => <IssueItem key={props.item.key} {...props} />}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }
};

function IssueItem({item}) {
  return (
    <View style={[styles.myButton]}>
      <TouchableOpacity danger style={styles.button}>
        <Text style={styles.text}>{item.key}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  boxSmall: {
    // width: 200,
    height: 80,
    margin: 5,
    // backgroundColor: 'skyblue',
  },
  myButton: {
    padding: 5,
    // paddingHorizontal: 8,
    // marginLeft: 5,
    // height: 70,
    // width: 70, //The Width must be the same as the height
    // borderRadius: 400, //Then Make the Border Radius twice the size of width or Height
    // backgroundColor: 'rgb(195, 125, 198)',
  },
  button: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: 'rgba(250, 66, 59, 0.15)',
    borderColor: '#fa423b',
    borderRadius: 8,
  },
  text: {
    fontSize: 16,
    color: '#fa423b',
  },
});

export default Issue;
