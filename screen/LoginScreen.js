import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';

import {Item, Input, Icon, Form} from 'native-base';
import {ScrollView} from 'react-native-gesture-handler';

export default function LoginScreen({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [hidePassword, setShowPassword] = useState(true);

  const politicaIcon = require('../assets/politica-icon-text.png');

  const handleSubmit = () => {
    console.log('login', email, password);
    if (email === 'admin' && password === 'admin') {
      navigation.navigate('Home');
    } else {
      Alert.alert('Login error');
    }
  };

  const onToggle = () => {
    setShowPassword(!hidePassword);
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={require('../assets/politica-icon-text.png')}
        />
        <Form style={styles.form}>
          <Item regular style={styles.inputView}>
            <Icon
              style={styles.inputIcon}
              type="MaterialCommunityIcons"
              name="account"
              size={24}
              color="rgba(0, 0, 0, 0.1)"
            />
            <Input
              style={styles.inputText}
              placeholder="Username"
              onChangeText={text => setEmail(text)}
            />
          </Item>

          <Item regular style={styles.inputView}>
            <Icon
              style={styles.inputIcon}
              type="MaterialCommunityIcons"
              name="lock"
              size={24}
              color="rgba(0, 0, 0, 0.5)"
            />
            <Input
              secureTextEntry={hidePassword}
              style={styles.inputText}
              placeholder="Password"
              onChangeText={text => setPassword(text)}
            />
            <Icon
              style={[styles.inputIcon, styles.pullRight]}
              type="MaterialCommunityIcons"
              name={hidePassword ? 'eye' : 'eye-off'}
              size={24}
              color="rgba(0, 0, 0, 0.5)"
              onPress={onToggle}
            />
          </Item>
          <TouchableOpacity style={styles.loginBtn} onPress={handleSubmit}>
            <Text style={styles.loginText}>LOGIN</Text>
          </TouchableOpacity>
        </Form>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'rgba(255, 255, 255, 1)',
  },
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40,
  },
  logo: {
    resizeMode: 'center',
    marginBottom: -24,
    opacity: 0.65,
  },
  form: {
    width: '100%',
  },
  inputView: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    marginBottom: 10,
  },
  pullRight: {
    position: 'absolute',
    right: -10,
    paddingHorizontal: 0,
  },
  inputIcon: {
    width: 50,
    height: 50,
    paddingHorizontal: 10,
    paddingVertical: 12,
    color: 'rgba(0,0,0,0.5)',
  },
  inputText: {
    height: 50,
    color: '#222222',
    fontSize: 16,
  },
  inputViewError: {
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: 'rgba(250, 198, 55, 0.15)',
  },
  loginBtn: {
    width: '100%',
    backgroundColor: '#fb5b5a',
    borderRadius: 5,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginText: {
    fontSize: 15,
    color: '#ffffff',
    fontWeight: '600',
  },
});
