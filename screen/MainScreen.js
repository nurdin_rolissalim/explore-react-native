import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import HomeScreen from './HomeScreen';
import MyActivityScreen from './MyActivityScreen';
// import NewsScreen from './NewsScreen';
import Header from '../components/Header';

const Tab = createMaterialTopTabNavigator();

function MainScreen() {
  return (
    <>
      <Tab.Navigator
        lazy={true}
        tabBarOptions={{
          scrollEnabled: true,
          activeTintColor: '#fa423b',
          activeBackgroundColor: '#fa423b',
          inactiveTintColor: '#fa423b',
        }}
      >
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Activity" component={MyActivityScreen} />
        <Tab.Screen name="Universe" component={HomeScreen} />
        <Tab.Screen name="Audience" component={HomeScreen} />
        <Tab.Screen name="Volunteer" component={HomeScreen} />
        {/* <Tab.Screen name="detail-news" component={NewsScreen}/> */}
      </Tab.Navigator>
    </>
  );
}

export default MainScreen;
