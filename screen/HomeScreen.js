import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { Container, Content, Title } from 'native-base';
import Person from '../share/component/Person';
import Issue from '../share/component/Issue';
import News from '../share/component/News';
import FooterIcon from '../share/component/footer/FooterIcon';
import FooterNews from '../share/component/footer/FooterNews';
import NewsHeadline from '../components/NewsHeadline';
import NewsList from '../components/NewsList';

export default class HomeScreen extends Component {
  constructor(props) {
		super(props);
		this.state = {
			news: [
				{id: '1', title: 'Begini Virus Shut Out Klaim Usir COVID-19 dan Tanggapan dari Ahli' },
				{id: '2', title: 'Warga Tak Patuh, Iran Bersiap untuk Gelombang Kedua Wabah Corona' },
				{id: '3', title: 'Rumah Sakit di New York Rawat Pasien Corona dengan Vitamin C' }
			]
		}
  }

  render() {
    return (
      <Container>
        <Content>
          <Text style={styles.title}>TOP PERSON</Text>
          <Person analyze="home-person" />

          <Text style={styles.title}>TOP ISSUE</Text>
          <Issue analyze="home-issue" style={{ marginTop: 10 }} />

          <Text style={styles.title}>NEWS</Text>
          <View style={{ padding: 10 }}>
            <NewsHeadline />
						<NewsList news={this.state.news} />
          </View>
          {/* <News analyze="home-issue"></News>         */}
        </Content>
        {/* <FooterNews  navigation={this.props.navigation}/> */}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    padding: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
