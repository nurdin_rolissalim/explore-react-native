import React, {Component} from 'react';
import {Text, StyleSheet, View, FlatList, ScrollView} from 'react-native';
import HomeScreen from './HomeScreen';
import MyActivityScreen from './MyActivityScreen';

import ScrollableTabView, {
  DefaultTabBar,
  ScrollableTabBar,
} from 'react-native-scrollable-tab-view';

const TabScreen: () => React$Node = () => {
  return (
    <ScrollableTabView
      style={{marginTop: 0}}
      initialPage={0}
      renderTabBar={() => <ScrollableTabBar />}>
      <View tabLabel="HOME" style={styles.layout}>
        <HomeScreen />
      </View>
      <View tabLabel="MY ACTIVITIES" style={styles.layout}>
        <MyActivityScreen />
      </View>
      <ScrollView tabLabel="MY UNIVERSE" style={styles.tabView}>
        <View style={styles.card}>
          <Text>Messenger</Text>
        </View>
      </ScrollView>
      <ScrollView tabLabel="MY AUDIENCE" style={styles.tabView}>
        <View style={styles.card}>
          <Text>Notifications</Text>
        </View>
      </ScrollView>
      <ScrollView tabLabel="MY VOLUNTEER" style={styles.tabView}>
        <View style={styles.card}>
          <Text>Other nav</Text>
        </View>
      </ScrollView>
    </ScrollableTabView>
  );
};

const styles = StyleSheet.create({
  tabView: {
    flex: 1,
    padding: 5,
    backgroundColor: 'rgba(0,0,0,0.01)',
  },
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  layout: {
    flex: 1,
    backgroundColor: 'powderblue',
  },
  box: {
    padding: 25,
    backgroundColor: 'steelblue',
    margin: 5,
  },
});

export default TabScreen;
