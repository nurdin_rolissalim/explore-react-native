import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

import Person from '../components/Person';
import Issue from '../components/Issue';
import News from '../components/News';
import ModalComponent from '../components/Modal';
import { Chart } from '../components/Chart';
import { ScrollView } from 'react-native-gesture-handler';

const MyActivityScreen: () => React$Node = () => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <Person analyze="activity-person" />
        <View style={'margin: 10'}>
          <Chart />
        </View>
        {/* <ModalComponent
          // modalStyle={s.content__modal}
          alwaysOpen={60}
          handlePosition="inside"
          analyze="modal-home-news"
        /> */}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default MyActivityScreen;
