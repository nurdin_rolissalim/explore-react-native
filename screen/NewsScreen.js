import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Icon, Segment, Content, Text } from 'native-base';
import News from '../share/component/News';

export default class NewsScreen extends Component {
    constructor(props){
      console.log("NewsScreen",props)
        super(props);
    }
  render() {
    return (
      <Container>
        <Header hasSegment>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
              <Text>More News</Text>
          </Body>
        </Header>
        <Content padder>
            <News 
            analyze={this.props.route.params.analyze}            
            ></News>

        </Content>
      </Container>
    );
  }
}