import React from 'react';
import {
  Header as NBHeader,
  Left,
  Right,
  Button,
  Icon,
  Text,
} from 'native-base';
import { StyleSheet, Image } from 'react-native';

export default function Header() {
  return (
    <NBHeader>
      <Left>
        <Button transparent>
          <Icon name={'menu'} type={'MaterialCommunityIcons'} />
          <Text>Back</Text>
        </Button>
      </Left>
      <Image
        style={styles.logo}
        source={require('../assets/politica-icon-text.png')}
      />
      <Right>
        <Button transparent>
          <Icon name={'magnify'} type={'MaterialCommunityIcons'} size={28} />
        </Button>
      </Right>
    </NBHeader>
  );
}

const styles = StyleSheet.create({
  logoContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: 50,
  },
  logo: {
    height: 32,
    flex: 3,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignSelf: 'center',
    marginLeft: 24,
  },
});
