import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {WebView} from 'react-native-webview';
import HighchartsReactNative from '@highcharts/highcharts-react-native';

export function Chart() {
  const [options] = useState({
    series: [{name: 'Speed', data: [1, 10, 3]}],
    chart: {type: 'line'},
  });

  /**
   * List modules, fill as you needed only
   * e.g:
   *  const modules = ['highcharts-more','solid-gauge'];
   *
   * Other modules, see: node_modules/@highcharts/highcharts-files/modules/
   */
  const modules = [];

  return (
    <>
      <HighchartsReactNative
        useCDN={true}
        styles={styles.container}
        options={options}
        useSSL={true}
        modules={modules}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 200,
    // width: 200,
    backgroundColor: 'transparent',
    justifyContent: 'center',
  },
});
