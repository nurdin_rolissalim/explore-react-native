import React, { useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import NewsItem from './NewsItem';

export default function NewsList({ news }) {
	const [newsState] = useState(news);

	return (
		<FlatList
			style={{ paddingTop: 5 }}
			data={newsState}
			renderItem={item =>	<WrapperNewsItem key={item.id} {...item} />}
			horizontal
			showsHorizontalScrollIndicator={false}
		/>
	);
}

function WrapperNewsItem({ item }) {
	return (
		<View style={styles.container}>
			<NewsItem />
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		width: 200,
		height: 235,
		marginRight: 10,
	},
});
