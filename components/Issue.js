import React, { useState, useEffect } from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import { Container, Header, Content, Button, Text } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Issue(props) {
  const [data, setData] = useState([]);
  const [analyze, setAnalyze] = useState(props.analyze);

  useEffect(() => {
    switch (analyze) {
      case 'home-issue':
        setData([
          {key: 'Banjir'},
          {key: 'Salah Anies ?'},
          {key: 'Zul'},
          {key: 'Indra'},
          {key: 'Mey'},
          {key: 'Evi'},
          {key: 'Dewi'},
          {key: 'Jillian'},
          {key: 'Jimmy'},
          {key: 'Julie'},
        ]);
        break;

      case 'activity-person':
        setData([
          {key: 'Kerja Sama 3 Provinsi'},
          {key: 'Amin'},
          {key: 'Anwar'},
          {key: 'Dida'},
          {key: 'James'},
          {key: 'Joel'},
          {key: 'John'},
          {key: 'Jillian'},
          {key: 'Jimmy'},
          {key: 'Julie'},
        ]);
        break;
      default:
        break;
    }
  }, [analyze]);

  return (
    <View style={styles.container}>
      <FlatList
        style={{ paddingTop: 5 }}
        data={this.state.data}
        renderItem={items => <IssueItem key={items.item.key} {...items} />}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}

function IssueItem({ item }) {
  return (
    <View style={[styles.issuItemContainer]}>
      <TouchableOpacity danger style={styles.issuItemButton}>
        <Text style={styles.issuItemLabel}>{item.key}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // width: 200,
    height: 80,
    margin: 5,
  },
  issuItemContainer: {
    padding: 5,
  },
  issuItemButton: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: 'rgba(250, 66, 59, 0.15)',
    borderColor: '#fa423b',
    borderRadius: 8,
  },
  issuItemLabel: {
    fontSize: 16,
    color: '#fa423b',
  },
});
