import React from 'react';
import { Image, View, StyleSheet } from 'react-native';
import {
	Card,
	CardItem,
	Text,
	Body,
} from 'native-base';

export default function NewsItem() {
	return (
		<Card style={styles.card}>
			<CardItem cardBody>
				<Image
					source={require('../assets/politica-icon-text.png')}
					style={{ height: 65, width: null, flex: 1, resizeMode: 'contain' }}
				/>
			</CardItem>
			<CardItem>
				<Body>
					<Text style={styles.title}>
						Pesan untuk Anies Dibalik Keributan yang Berawal dari Lem Aibon
					</Text>
					<View style={styles.meta}>
						<Text style={styles.source}>Kompas.com</Text>
						<Text style={styles.datePub}>6 min ago</Text>
					</View>
				</Body>
			</CardItem>
		</Card>
	);
}

const styles = StyleSheet.create({
	card: {
		// borderRadius: 8,
		width: '100%',
		height: 200,
	},
	meta: {
		flexDirection: 'row',
		marginVertical: 5,
	},
	title: {
		fontSize: 16,
		fontWeight: 'bold',
	},
	source: {
		fontSize: 13,
		color: 'red',
		marginRight: 10,
	},
	datePub: {
		fontSize: 13,
	},
	summary: {
		fontSize: 13,
	},
});
