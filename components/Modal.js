import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import { Modalize } from 'react-native-modalize';
import faker from 'faker';

import { Button } from './Button';

class ModalComponent extends Component {
  constructor(props) {
    super();
    this.state = {
      handlePosition: props.handlePosition,
      alwaysOpen: props.alwaysOpen,
      analyze: props.analyze,
    };
  }
  modal = React.createRef();

  closeModal = dest => {
    if (this.modal.current) {
      this.modal.current.close(dest);
    }
  };

  renderContent = () => (
    <View style={styles.content}>
      <Text style={styles.content__subheading}>{'News'.toUpperCase()}</Text>
      <Text style={styles.content__heading}>Always open modal!</Text>
      <Text style={styles.content__description}>{faker.lorem.paragraph()}</Text>
      <Button
        name="Close to initial position"
        onPress={() => this.closeModal('alwaysOpen')}
      />
      <Button name="Close completely" onPress={this.closeModal} />
    </View>
  );
  render() {
    switch (this.state.analyze) {
      case 'modal-home-news':
        break;
      case 'modal':
        break;
    }
    return (
      <Modalize
        ref={this.modal}
        modalStyle={styles.content__modal}
        alwaysOpen={this.state.alwaysOpen}
        handlePosition={this.state.handlePosition}>
        {this.renderContent()}
      </Modalize>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  content__modal: {
    // shadowColor: '#000',
    // shadowOffset: {width: 0, height: 6},
    // shadowOpacity: 0.45,
    // shadowRadius: 16,
  },

  content__subheading: {
    marginBottom: 2,

    fontSize: 16,
    fontWeight: '600',
    color: '#ccc',
  },

  content__heading: {
    fontSize: 24,
    fontWeight: '600',
    color: '#333',
  },

  content__description: {
    paddingTop: 10,
    paddingBottom: 10,

    fontSize: 15,
    fontWeight: '200',
    lineHeight: 22,
    color: '#666',
  },
});

export default ModalComponent;
