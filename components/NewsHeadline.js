import React from 'react';
import { Image, View, StyleSheet } from 'react-native';
import {
	Card,
	CardItem,
	Text,
	Body,
} from 'native-base';

export default function NewsHeadline() {
	return (
		<Card style={styles.card}>
			<CardItem cardBody>
				<Image
					source={require('../assets/politica-icon-text.png')}
					style={{ height: 150, width: null, flex: 1, resizeMode: 'contain' }}
				/>
			</CardItem>
			<CardItem>
				<Body>
					<Text style={styles.title}>
						Pesan untuk Anies Dibalik Keributan yang Berawal dari Lem Aibon
					</Text>
					<View style={styles.meta}>
						<Text style={styles.source}>Kompas.com</Text>
						<Text style={styles.datePub}>6 min ago</Text>
					</View>
					<Text style={styles.summary}>
						Biasanya, rencana anggaran selalu diunggah Pemprov DKI ke situs
						apdb.jakarta.go.id setiap tahun. Dengan begitu, masyarakat bisa
						memantau alokasi anggaran setiap tahap perencanaan.
					</Text>
				</Body>
			</CardItem>
		</Card>
	);
}

const styles = StyleSheet.create({
	card: {
		borderRadius: 8,
	},
	meta: {
		flexDirection: 'row',
		marginVertical: 5,
	},
	title: {
		fontSize: 18,
		fontWeight: 'bold',
	},
	source: {
		fontSize: 13,
		color: 'red',
		marginRight: 10,
	},
	datePub: {
		fontSize: 13,
	},
	summary: {
		fontSize: 13,
		// lineHeight: 1.25,
	},
});
