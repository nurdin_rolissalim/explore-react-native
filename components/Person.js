import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

class Person extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			analyze: props.analyze,
		};
	}
	render() {
		switch (this.state.analyze) {
			case 'home-person':
				this.state.data = [
					{ key: 'Arta' },
					{ key: 'Yayuk' },
					{ key: 'Zul' },
					{ key: 'Indra' },
					{ key: 'Mey' },
					{ key: 'Evi' },
					{ key: 'Dewi' },
					{ key: 'Jillian' },
					{ key: 'Jimmy' },
					{ key: 'Julie' },
				];
				break;
			case 'activity-person':
				this.state.data = [
					{ key: 'Nurdin' },
					{ key: 'Amin' },
					{ key: 'Anwar' },
					{ key: 'Dida' },
					{ key: 'James' },
					{ key: 'Joel' },
					{ key: 'John' },
					{ key: 'Jillian' },
					{ key: 'Jimmy' },
					{ key: 'Julie' },
				];
				break;
		}
		return (
			<View style={styles.boxSmall}>
				<FlatList
					style={[{ paddingTop: 5 }]}
					data={this.state.data}
					renderItem={({ item }) => (
						<View style={[styles.myButton]}>
							<Text styles="{s.item}">{item.key}</Text>
						</View>
					)}
					horizontal
					showsHorizontalScrollIndicator={false}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	boxSmall: {
		// width: 200,
		height: 80,
		marginBottom: 10,
		// marginRight: 5,
		marginTop: 5,
		// marginLeft: 5,
		// backgroundColor: 'skyblue',
	},
	myButton: {
		padding: 10,
		marginLeft: 5,
		height: 70,
		width: 70, //The Width must be the same as the height
		borderRadius: 400, //Then Make the Border Radius twice the size of width or Height
		backgroundColor: 'rgb(195, 125, 198)',
	},
});

export default Person;
