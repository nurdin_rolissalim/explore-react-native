import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import {
  createDrawerNavigator,
  DrawerContent,
  DrawerItemList,
} from "@react-navigation/drawer";
import { addNavigationHelpers } from "@react-navigation/native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { Icon } from "native-base";
import { connect } from "react-redux";

import LoginScreen from "app/screens/auth/LoginScreen";
import Header from "app/shared/ui/Header";
import HomeScreen from "app/screens/home/HomeScreen";
import ActivityIndex from "app/screens/my-activities/ActivityIndex";
import UniverseDetail from "app/screens/universe/UniverseDetail";
import UniverseDetailSosmed from "app/screens/universe/UniverseDetailSosmed";
import UniverseIndex from "app/screens/universe/UniverseIndex";
import UserManagement from "app/screens/administrators/UserManagement";
import Audience from "app/screens/audience/Audience";
import UserManagementDetail from "app/screens/administrators/UserManagementDetail";

// single page headless
import SearchScreen from "app/screens/single-page-headless/SearchScreen";
import NewsScreen from "app/screens/single-page-headless/NewsScreen";
import VolunteerIndex from "app/screens/volunteer/VolunteerIndex";
import Sidebar from "app/screens/sidebar/Sidebar";
import MyProfileScreen from "app/screens/my-profile/MyProfileScreen";
import ChangeProfileScreen from "app/screens/my-profile/ChangeProfileScreen";
import ChangePasswordScreen from "app/screens/my-profile/ChangePasswordScreen";
import UserManagementEdit from "./screens/administrators/UserManagementEdit";
import FilterDate from "./screens/single-page-headless/FilterDate";
import Drawer from "./shared/ui/layouts/drawer/Drawer";
import Catalog from "./screens/catalog/Catalog";
import UserManagementAdd from "./screens/administrators/UserManagementAdd";
import CatalogAdd from "./screens/catalog/CatalogAdd";
import CatalogEdit from "./screens/catalog/CatalogEdit";

const MyProfileStack = createStackNavigator();
function MyProfileStackNavigation() {
  return (
    <MyProfileStack.Navigator>
      <MyProfileStack.Screen
        options={{ header: () => <Header /> }}
        name="my-profile"
        component={MyProfileScreen}
      />
      <MyProfileStack.Screen
        options={{ headerTitle: "Change profile" }}
        name="change-profile"
        component={ChangeProfileScreen}
      />
      <MyProfileStack.Screen
        options={{ headerTitle: "Change password" }}
        name="change-password"
        component={ChangePasswordScreen}
      />
    </MyProfileStack.Navigator>
  );
}

const CatalogStack = createStackNavigator();
function CatalogStackNavigation() {
  return (
    <CatalogStack.Navigator initialRouteName="catalog-index">
      <CatalogStack.Screen
        options={{ header: () => <Header /> }}
        name="catalog-index"
        component={Catalog}
      />
      <CatalogStack.Screen
        options={{ headerTitle: "Add catalog" }}
        name="catalog-add"
        component={CatalogAdd}
      />
      <CatalogStack.Screen
        options={{ headerTitle: "Edit catalog" }}
        name="catalog-edit"
        component={CatalogEdit}
      />
    </CatalogStack.Navigator>
  );
}

const AdministratorStack = createStackNavigator();
function AdministratorStackNavigation() {
  return (
    <AdministratorStack.Navigator header="false">
      <AdministratorStack.Screen
        options={{
          header: () => <Header />,
        }}
        name="user-management"
        component={UserManagement}
      />
      <AdministratorStack.Screen
        options={{ headerTitle: "User account detail" }}
        name="user-detail-management"
        component={UserManagementDetail}
      />
      <AdministratorStack.Screen
        options={{ headerTitle: "Edit user account" }}
        name="user-edit-management"
        component={UserManagementEdit}
      />
      <AdministratorStack.Screen
        options={{ headerTitle: "Add user account" }}
        name="user-add-management"
        component={UserManagementAdd}
      />
    </AdministratorStack.Navigator>
  );
}

const MainMenuTab = createMaterialTopTabNavigator();
function MainMenuStackNavigation() {
  return (
    <MainMenuTab.Navigator
      lazy
      tabBarOptions={{
        scrollEnabled: true,
        tabStyle: {
          backgroundColor: "#fa423b",
        },
        activeTintColor: "#fff",
        activeBackgroundColor: "#cc3c3b",
        inactiveTintColor: "rgba(250, 250, 250, 0.8)",
      }}
    >
      <MainMenuTab.Screen name="Home" component={HomeScreen} />
      <MainMenuTab.Screen name="Activity" component={ActivityIndex} />
      <MainMenuTab.Screen name="Universe" component={UniverseIndex} />
      <MainMenuTab.Screen name="Audience" component={Audience} />
      <MainMenuTab.Screen name="Volunteer" component={VolunteerIndex} />
    </MainMenuTab.Navigator>
  );
}

function WrapperMainMenu(props) {
  return (
    <>
      <Header />
      <MainMenuStackNavigation {...props} />
    </>
  );
}

const DrawerStack = createDrawerNavigator();
function DrawerStackNavigation() {
  return (
    <DrawerStack.Navigator
      headerMode={false}
      drawerContent={(props) => <Sidebar {...props} />}
    >
      <DrawerStack.Screen name="main-menu-stack" component={WrapperMainMenu} />
      <DrawerStack.Screen
        name="admin"
        component={AdministratorStackNavigation}
      />
      <DrawerStack.Screen name="catalog" component={CatalogStackNavigation} />
      <DrawerStack.Screen
        name="my-profile"
        component={MyProfileStackNavigation}
      />
    </DrawerStack.Navigator>
  );
}

const DashboardStack = createStackNavigator();
export function DashboardStackNavigation() {
  return (
    <DashboardStack.Navigator initialRouteName="drawer" headerMode={false}>
      <DashboardStack.Screen name="drawer" component={DrawerStackNavigation} />
      {/* put below for fullpage screens */}
      <DashboardStack.Screen name="search" component={SearchScreen} />
      <DashboardStack.Screen
        name="universe-detail"
        component={UniverseDetail}
      />
      <DashboardStack.Screen
        name="universe-detail-sosmed"
        component={UniverseDetailSosmed}
      />
      <DashboardStack.Screen name="filter-date" component={FilterDate} />
      <DashboardStack.Screen name="detail-news" component={NewsScreen} />
    </DashboardStack.Navigator>
  );
}

const AuthStack = createStackNavigator();
export function AuthStackNavigation() {
  return (
    <AuthStack.Navigator initialRouteName="login" headerMode={false}>
      <AuthStack.Screen name="login" component={LoginScreen} />
      {/* logout comp is here (?) */}
    </AuthStack.Navigator>
  );
}

const RootStack = createStackNavigator();
function RootStackNavigation() {
  return (
    <RootStack.Navigator initialRouteName="auth" headerMode={false}>
      <RootStack.Screen name="dashboard" component={DashboardStackNavigation} />
      <RootStack.Screen name="auth" component={AuthStackNavigation} />
    </RootStack.Navigator>
  );
}

const mapStateToProps = (state) => {
  const { userData, isSignedIn } = state.auth;

  return { userData, isSignedIn };
};

export default connect(mapStateToProps)(RootStackNavigation);
