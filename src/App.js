import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { Root } from "native-base";
import RNBootSplash from "react-native-bootsplash";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "./redux/store";
import RootStackNavigation from "./routes";
import { navigationRef, isMountedRef } from "./shared/helpers/router";

class App extends React.Component {
  componentDidMount() {
    RNBootSplash.hide({ duration: 300 });
    isMountedRef.current = true;
  }

  componentWillUnmount() {
    isMountedRef.current = false;
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Root>
            <NavigationContainer ref={navigationRef}>
              <RootStackNavigation />
            </NavigationContainer>
          </Root>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
