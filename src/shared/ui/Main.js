import React from "react";
import { Text, View } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import HomeScreen from "./HomeScreen";
import MyActivityScreen from "./MyActivityScreen";
// import NewsScreen from './NewsScreen';

const Tab = createMaterialTopTabNavigator();

function MainScreen() {
  return (
    <Tab.Navigator
      lazy
      tabBarOptions={{
        scrollEnabled: true,
        activeTintColor: "blue",
        inactiveTintColor: "gray",
      }}
    >
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Activity" component={MyActivityScreen} />
      <Tab.Screen name="Universe" component={HomeScreen} />
      <Tab.Screen name="Audience" component={HomeScreen} />
      <Tab.Screen name="Volunteer" component={HomeScreen} />
      {/* <Tab.Screen name="detail-news" component={NewsScreen}/> */}
    </Tab.Navigator>
  );
}

export default MainScreen;
