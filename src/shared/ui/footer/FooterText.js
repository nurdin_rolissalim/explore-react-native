import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Icon,
  Text,
} from "native-base";
import { StyleSheet } from "react-native";

export default class FooterText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [],
    };
  }

  render() {
    switch (this.props.analyze) {
      case "universe-news":
        this.state.menu = [
          {
            name: "person",
            navigate: "universe-news",
          },
          {
            name: "organization",
            navigate: "universe-facebook",
          },
          {
            name: "location",
            navigate: "universe-twitter",
          },
        ];
        break;
      case "universe":
        this.state.menu = [
          {
            name: "paper",
            navigate: "universe-news",
          },
          {
            name: "logo-facebook",
            navigate: "universe-facebook",
          },
          {
            name: "logo-twitter",
            navigate: "universe-twitter",
          },
          {
            name: "logo-instagram",
            navigate: "universe-instagram",
          },
        ];
        break;
      default:
        break;
    }

    return (
      <Footer>
        <FooterTab style={styles.footerContainer}>
          {this.state.menu.map((r) => (
            <Button
              vertical
              key={r.name}
              onPress={() =>
                this.props.navigation.navigate("detail-news", {
                  analyze: "home-news-more",
                })
              }
            >
              <Text>{r.name}</Text>
            </Button>
          ))}
        </FooterTab>
      </Footer>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    backgroundColor: "#fa423b",
  },
});
