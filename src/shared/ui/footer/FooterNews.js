import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Icon,
} from "native-base";

export default class FooterNews extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Footer>
        <FooterTab>
          <Button
            onPress={() =>
              this.props.navigation.navigate("detail-news", {
                analyze: "home-news-more",
              })
            }
          >
            <Icon name="paper" />
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}
