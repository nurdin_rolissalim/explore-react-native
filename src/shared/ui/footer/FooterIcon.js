import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Icon,
  Text,
} from "native-base";
import { StyleSheet } from "react-native";

export default class FooterIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [],
      type: props.type,
      typeFont: props.typeFont ? props.typeFont : "FontAwesome5",
    };
  }

  getMenu() {
    let menu = null;

    switch (this.props.analyze) {
      case "universe-news":
        menu = [
          {
            name: "user",
            navigate: "person",
          },
          {
            name: "building",
            navigate: "organization",
          },
          {
            name: "map-marker-alt",
            navigate: "location",
          },
        ];
        break;
      case "universe-activity":
        menu = [
          {
            name: "twitter",
            navigate: "twitter",
          },
          {
            name: "facebook",
            navigate: "facebook",
          },

          {
            name: "instagram",
            navigate: "instagram",
          },
        ];
        break;
      case "volunteer":
        menu = [
          {
            name: "user-tie",
            navigate: "recruitment",
          },
          {
            name: "calendar-day",
            navigate: "events",
          },
          {
            name: "calendar-alt",
            navigate: "calendar",
          },
          {
            name: "fire",
            navigate: "issue",
          },
        ];
        break;
      default:
        break;
    }
    return menu;
  }

  componentDidMount() {
    const menu = this.getMenu();
    this.setState({ menu });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.type !== this.state.type) {
      this.setState({ type: nextProps.type });
    }
  }

  UNSAFE_componentWillUnmount() {
    this.setState({ menu: null });
  }

  // UNSAFE_componentWillMount() {

  // }

  // UNSAFE_componentWillUpdate() {

  // }

  // UNSAFE_componentWillReceiveProps() {

  // }

  clickFootter(footer) {
    this.props.callback(footer);
  }

  render() {
    return (
      <Footer style={{ height: 50 }}>
        <FooterTab style={styles.footerContainer}>
          {this.state.menu.map((r) => (
            <Button
              style={[
                this.state.type === r.navigate
                  ? { backgroundColor: "#fff" }
                  : "",
              ]}
              fontSize={3}
              vertical
              key={r.name}
              title={r.navigate}
              onPress={() => this.clickFootter(r.navigate)}
            >
              <Icon
                name={r.name}
                type={this.state.typeFont}
                active={this.state.type === r.navigate}
                style={[
                  this.state.type === r.navigate ? { color: "#000" } : "#fff",
                ]}
              />
              <Text
                style={[
                  this.state.type === r.navigate
                    ? { color: "#000", fontSize: 8 }
                    : { color: "#fff", fontSize: 8 },
                ]}
              >
                {r.navigate}
              </Text>
            </Button>
          ))}
        </FooterTab>
      </Footer>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    backgroundColor: "#fa423b",
    // height:45
  },
  iconText: {
    fontSize: 12,
  },
});
