import React from "react";
import { Image, View, StyleSheet } from "react-native";
import { Card, CardItem, Text, Body } from "native-base";

export default function NewsItemRow() {
  return (
    <Card style={styles.card}>
      <CardItem cardBody style={{ height: 72 }}>
        <Image
          source={require("assets/images/politica-icon-text.png")}
          style={{
            height: 72,
            width: 72,
            resizeMode: "center",
            borderColor: "red",
            borderWidth: 1,
          }}
        />
        <Body style={{ marginLeft: 2, padding: 10 }}>
          <Text numberOfLines={2} style={(styles.title, { lineHeight: 18 })}>
            Pesan untuk Anies Dibalik Keributan yang Berawal dari Lem Aibon
          </Text>
          <View style={styles.meta}>
            <Text style={styles.source}>Kompas.com</Text>
            <Text style={styles.datePub}>6 min ago</Text>
          </View>
        </Body>
      </CardItem>
    </Card>
  );
}

const styles = StyleSheet.create({
  card: {
    width: "100%",
  },
  meta: {
    flexDirection: "row",
    marginVertical: 0,
  },
  title: {
    fontSize: 12,
    fontWeight: "bold",
    overflow: "hidden",
  },
  source: {
    fontSize: 13,
    color: "red",
    marginRight: 10,
  },
  datePub: {
    fontSize: 13,
  },
  summary: {
    fontSize: 13,
  },
});
