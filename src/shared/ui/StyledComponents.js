import React from "react";
import { View, Text } from "react-native";

export function WrapperContent({ children, ...props }) {
  return (
    <View style={{ flex: 1, padding: 16, marginBottom: 10 }} {...props}>
      {children}
    </View>
  );
}

export function TitleSection({ children, ...props }) {
  return (
    <View style={{ flexDirection: "column" }}>
      <Text
        style={{
          fontSize: 18,
          fontWeight: "bold",
          textTransform: "uppercase",
          letterSpacing: 1.275,
        }}
        {...props}
      >
        {children}
      </Text>
      <View
        style={{
          borderBottomColor: "#fb5b5a",
          borderBottomWidth: 4,
          width: 48,
          marginBottom: 5,
        }}
      />
    </View>
  );
}

export function SubtitleSection({ children, ...props }) {
  return (
    <Text
      style={{
        fontSize: 14,
        fontWeight: "bold",
        textTransform: "uppercase",
        letterSpacing: 0.85,
      }}
      {...props}
    >
      {children}
    </Text>
  );
}

export function TitleHeader({ children, ...props }) {
  return (
    <Text
      style={{
        fontSize: 14,
        fontWeight: "bold",
        textTransform: "uppercase",
        color: "#fff",
      }}
      {...props}
    >
      {children}
    </Text>
  );
}
