import React, { useState } from "react";
import {
  FlatList,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from "react-native";

const uri = require("../../assets/images/politica-icon.png");

export default function NewsList({ news, children, ...props }) {
  const [newsState] = useState(news);

  return (
    <FlatList
      data={newsState}
      keyExtractor={(item, index) => item.id + index}
      renderItem={(item, index) => (
        <View style={styles.container}>{children}</View>
      )}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
    />
  );
}

function WrapperNews(props) {
  return (
    <View style={styles.container}>
      <NewsItem />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 200,
    // height: 280,
    marginRight: 10,
  },
});
