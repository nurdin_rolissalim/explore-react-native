import React, { Component } from "react";
import { Fab, Footer, FooterTab, Button, Icon, Text } from "native-base";
import { StyleSheet } from "react-native";

export default class FabIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [],
      type: props.type,
      typeFont: props.typeFont ? props.typeFont : "FontAwesome5",
      active: false,
    };
  }

  getMenu() {
    let menu = null;
    let { analyze } = this.props
    switch (analyze) {
      case "universe-news":
        menu = [
          {
            name: "user",
            navigate: "person",
            color: "#fa423b"
          },
          {
            name: "building",
            navigate: "organization",
            color: "#fa423b"
          },
          {
            name: "map-marker-alt",
            navigate: "location",
            color: "#fa423b"
          },
        ];
        break;
      case "universe-activity":
      case "audience":
      case "activity":
        menu = [
          {
            name: "twitter",
            navigate: "twitter",
            color: "#5067FF",
          },
          {
            name: "facebook",
            navigate: "facebook",
            color: "#3B5998",
          },

          {
            name: "instagram",
            navigate: "instagram",
            color: "#b950d9",
          },
        ];
        break;
      case "activity-response":
        menu = [
          {
            name: "twitter",
            navigate: "twitter",
            color: "#5067FF",
          },
          {
            name: "facebook",
            navigate: "facebook",
            color: "#3B5998",
          },

          {
            name: "instagram",
            navigate: "instagram",
            color: "#b950d9",
          },
          {
            name: "globe-asia",
            navigate: "globe-asia",
            color: "#fa423b",
          },
        ];
        break;
      case "activity-news":
        menu = [
          {
            name: "list",
            navigate: "ActivityTimeline",
            color: "#fa423b",
          },
          {
            name: "newspaper",
            navigate: "ActivityNews",
            color: "#fa423b",
          },
        ];
        break;
      case "volunteer":
        menu = [
          {
            name: "user-tie",
            navigate: "recruitment",
            color: "#fa423b"
          },
          {
            name: "calendar-day",
            navigate: "event",
            color: "#fa423b"
          },
          {
            name: "calendar-alt",
            navigate: "calendar",
            color: "#fa423b"
          },
          {
            name: "fire",
            navigate: "issue",
            color: "#fa423b"
          },
        ];
        break;
      default:
        break;
    }
    return menu;
  }

  componentDidMount() {
    const menu = this.getMenu();
    this.setState({ menu });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.type !== this.state.type) {
      this.setState({ type: nextProps.type });
    }
  }


  UNSAFE_componentWillUnmount() {
    this.setState({ menu: null });
  }

  clickFootter(footer) {
    this.props.callback(footer);
  }

  updateActive(update) {
    this.setState({ active: update });
  }

  render() {
    let { active, typeFont, menu } = this.state
    let { color, type, analyze } = this.props

    return (
      <Fab
        active={active}
        direction="up"
        containerStyle={{}}
        style={{ backgroundColor: color }}
        position="bottomRight"
        onPress={() => this.updateActive(!active)}
        type={typeFont}
      >
        <Icon type={typeFont} name={type} />

        {menu.map((r) => (
          <Button
            style={{ backgroundColor: r.color }}
            onPress={() => this.clickFootter(r)}
            key={r.name}
          >
            <Icon name={r.name} type={typeFont} />
          </Button>
        ))}
      </Fab>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    backgroundColor: "#fa423b",
    // height:45
  },
  iconText: {
    fontSize: 12,
  },
});
