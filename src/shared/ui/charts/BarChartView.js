import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import { connect } from "react-redux";
import {
    objectToSeriesAndCategories,
    parseArryObjectSingleSeries,
    parseChartSeriesSentimentSumStacked,
    objectToSeriesAndCategoriesStacked
} from "app/helpers/ChartHelper";
import {
    parseObejctToArray,
    sortArrayDesc
} from "app/helpers/GlobalFunction";
import styles from "app/shared/styles";

const options = {
    chart: {
        type: "bar"
    },
    title: "",
    xAxis: {
        categories: [
            "sadness",
            "anger",
            "anticipation",
            "surprise",
            "disgust",
            "neutral",
            "joy",
            "trust",
        ],
        title: "",
        labels: {
            enabled: true
        }
    },
    yAxis: {
        title: "",
    },
    legend: {
        enabled: false,
        layout: "horizontal",
        align: "center",
        vertical: "middle",
        itemStyle: {

        }
    },
    credits: { enabled: false },
    plotOptions: {
        series: {
            stacking: false,
            dataLabels: {
                enabled: false
            }
        }
    },
    series: [{ name: "", data: [1, 10, 3, 4, 7, 11, 2, 5] }],
};

class BarChartView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: null,
            modules: [],
            loading: true,
            containerHeight: "container"
        };
    }

    viewData() {
        let dataChart;
        let chartData;
        let option;
        let temp;
        let { data, analyze } = this.props;

        switch (analyze) {
            case "universe-news-media-share":
            case "activity-news-media-share":
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );
                chartData = objectToSeriesAndCategories(
                    dataChart,
                    "Media",
                    "default"
                );
                option = options;
                option.xAxis.categories = chartData.categories;
                option.xAxis.labels.enabled = true
                option.series = chartData.series;
                option.plotOptions.series.dataLabels.enabled = false;
                this.setState({ options: option });
                break;
            case "universe-news-sentiment":
            case "activity-news-sentiment":
                chartData = parseChartSeriesSentimentSumStacked(
                    data
                );

                option = options;
                option.xAxis.categories = chartData.categories;
                option.series = chartData.series;
                option.plotOptions.series.stacking = 'normal';
                option.plotOptions.series.dataLabels.enabled = true;

                // option.legend.enabled = true;
                // option.legend.layout = "vertical";
                // option.legend.align = "right";
                // option.legend.verticalAlign = "top";
                // option.legend.itemStyle = {
                //     fontSize: '12px',
                //     color: "#818389"
                // }
                option.tooltip = {
                    formatter: function (e) {
                        return '<b>' + this.series.name + '</b> : <b>' + this.y + '</b>';
                    }
                };
                option.xAxis.labels.enabled = false;
                this.setState({ options: option, containerHeight: "container100" });
                break;
            case "universe-sentiment-activity":
            case "universe-sentiment-response":
            case "activity-sentiment-profile":
            case "activity-sentiment-response":
                chartData = parseChartSeriesSentimentSumStacked(
                    data
                );

                option = options;
                option.xAxis.categories = chartData.categories;
                option.series = chartData.series;
                option.plotOptions.series.stacking = 'normal';
                option.plotOptions.series.dataLabels.enabled = true;
                option.tooltip = {
                    formatter: function (e) {
                        return '<b>' + this.series.name + '</b> : <b>' + this.y + '</b>';
                    }
                };
                option.xAxis.labels.enabled = false;
                this.setState({ options: option, containerHeight: "container100" });
                break;
            case "universe-activity-issue":
                break;
            case "volunteer-recruitment-education":
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "name",
                    "total"
                );
                chartData = objectToSeriesAndCategoriesStacked(
                    dataChart,
                    "Education",
                    "education_level"
                )
                option = options;
                option.xAxis.categories = chartData.categories;
                option.series = chartData.series;
                option.plotOptions.series.stacking = 'normal';
                option.plotOptions.series.dataLabels.enabled = true;

                option.legend.enabled = true;
                option.legend.layout = "horizontal";
                // option.legend.align = "right";
                // option.legend.verticalAlign = "top";
                // option.legend.itemStyle = {
                //     fontSize: '12px',
                //     color: "#818389"
                // }
                option.tooltip = {
                    formatter: function (e) {
                        return '<b>' + this.series.name + '</b> : <b>' + this.y + '</b>';
                    }
                };
                option.xAxis.labels.enabled = false;
                this.setState({ options: option, containerHeight: "container130" });
                break;
            case "volunteer-recruitment-campaign":
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "category",
                    "total"
                );
                chartData = objectToSeriesAndCategories(
                    dataChart,
                    "Media",
                    "default"
                );
                option = options;
                option.xAxis.categories = chartData.categories;
                option.xAxis.labels.enabled = false
                option.series = chartData.series;
                option.plotOptions.series.dataLabels.enabled = false;
                this.setState({ options: option });
                break;
            case "volunteer-event":
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "name",
                    "total"
                );
                chartData = objectToSeriesAndCategories(
                    dataChart,
                    "Events",
                    "default"
                );
                option = options;
                option.xAxis.categories = chartData.categories;
                option.xAxis.labels.enabled = true
                option.series = chartData.series;
                option.plotOptions.series.dataLabels.enabled = false;
                option.legend.enabled = false;
                this.setState({ options: option });
                break;
            case "audience-map":
                let temp = parseObejctToArray(data);
                temp = sortArrayDesc(temp)
                dataChart = parseArryObjectSingleSeries(
                    temp,
                    "key",
                    "value"
                );
                chartData = objectToSeriesAndCategories(
                    dataChart,
                    "Post",
                    "default"
                );
                option = options;
                option.xAxis.categories = chartData.categories;
                option.xAxis.labels.enabled = true
                option.series = chartData.series;
                option.plotOptions.series.dataLabels.enabled = false;
                option.legend.enabled = false;
                this.setState({ options: option });
                break;
            default:
                this.setState({ options: options });
                break;
        }
    }

    UNSAFE_componentWillMount() {
        this.viewData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // if (this.state.options != nextState.options) {
        //     return true;
        // }

        return true;
    }

    componentWillUnmount() {
        this.setState({ options: null });
    }

    viewChart(modules, options, container) {
        return (
            // [styles.text, touched && invalid ? styles.textinvalid : styles.textvalid]
            // <View style={styles.charts.container}>
            < View style={styles.charts[container]} >
                <HighchartsReactNative
                    useCDN
                    webviewStyles={styles.charts.webviewStyles}
                    styles={styles.charts.container}
                    options={options}
                    modules={modules}
                    useSSL
                />
            </View >
        );
    }

    render() {

        const { modules, options, containerHeight } = this.state;
        const { data, analyze } = this.props;

        if (options != null && data != null) {
            return this.viewChart(modules, options, containerHeight)
        } else {
            return (
                <>
                </>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity } = state;

    return {
        user,
        filter,
        entity,
    };
};

export default connect(mapStateToProps)(BarChartView);

