import React, { useState, useEffect, Component } from "react";
import { StyleSheet, View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import { Toast } from "native-base";
import * as chartService from "app/service/ChartService";
import {
  parseAreaChartSeries,
  parseArryObjectSingleSeries,
} from "app/helpers/ChartHelper";
import styles from "../../styles";

const sampleLineChartOptions = {
  chart: {
    backgroundColor: "transparent",
    type: "spline",
  },
  title: {
    text: "",
  },

  subtitle: {
    text: "",
  },

  yAxis: {
    title: {
      text: "",
    },
  },

  xAxis: {
    accessibility: {
      rangeDescription: "Range: 2010 to 2017",
    },
  },

  legend: {
    enabled: false,
    // layout: 'vertical',
    // align: 'right',
    // verticalAlign: 'middle'
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: true,
      },
      pointStart: 2010,
    },
    spline: {
      lineWidth: 4,
      states: {
        hover: {
          lineWidth: 5,
        },
      },
      marker: {
        enabled: true,
      },
      // pointInterval: 3600000, // one hour
      // pointStart: Date.UTC(2018, 1, 13, 0, 0, 0)
    },
  },

  credits: {
    enabled: false,
  },

  series: [
    {
      name: "Installation",
      data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175],
    },
    {
      name: "Manufacturing",
      data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434],
    },
    {
      name: "Sales & Distribution",
      data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387],
    },
    {
      name: "Project Development",
      data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227],
    },
    {
      name: "Other",
      data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111],
    },
  ],
};

export default class LineChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: null,
      loading: false,
      modules: [],
    };
  }

  getExposureNews() {
    const params = {
      end_date: "20200428",
      index: "news",
      interval: "hour",
      start_date: "20200428",
      person: "joko widodo",
    };
    chartService
      .exposure(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          let option = sampleLineChartOptions;
          let data = parseArryObjectSingleSeries(response.data, "key", "value");
          let chartData = parseAreaChartSeries(data, "Exposure", params);
          option.chart.type = "areaspline";
          option.xAxis.categories = chartData.categories;
          option.series = chartData.series;

          this.setState({
            options: option,
          });
        } else {
          Toast.show({
            text: "Asdasd",
            position: "top",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  getData() {
    switch (this.props.analyze) {
      case "universe-news-exposure":
        this.getExposureNews();
        break;
      default:
        this.setState({ options: sampleLineChartOptions });
        break;
    }
  }

  UNSAFE_componentWillUpdate() {
  }

  UNSAFE_componentWillMount() {
    this.getData();
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (this.state.options != nextState.options) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    this.setState({ options: null });
  }

  render() {
    const { modules } = this.state;
    return (
      <View style={styles.charts.wrapper}>
        <HighchartsReactNative
          useCDN
          webviewStyles={styles.charts.webviewStyles}
          styles={styles.charts.container}
          options={this.state.options}
          modules={modules}
          useSSL
        />
      </View>
    );
  }
}
