import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import {
    parseAreaChartSeries,
    parseArryObjectSingleSeries,
} from "app/helpers/ChartHelper";
import styles from "app/shared/styles";
import { connect } from "react-redux";

const options = {
    chart: {
        backgroundColor: "transparent",
        type: "spline",
    },
    title: {
        text: "",
    },

    subtitle: {
        text: "",
    },

    yAxis: {
        title: {
            text: "",
        },
    },

    xAxis: {
        categories: []
    },

    legend: {
        enabled: false,
        // layout: 'vertical',
        // align: 'right',
        // verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                style: {
                    fontWeight: 'normal',
                    fontSize: '9px',
                    textShadow: '.5px .5px #fff',
                }
            }
        },
        areaspline: {
            events: {
                click: item => {
                    // if (this.props.drillDown == true) {
                    //     if (this.month === "") {
                    //         this.onClickMonth(item.point.index + 1);
                    //     } else {
                    //         this.onClickDay(item.point.index + 1);
                    //     }
                    // }
                }
            },
            fillColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1, x3: 0, y3: 1 },
                stops: [[0, '#ff0000'], [1, '#0000ff'], [2, '#ffffff']]
            }
        },
    },


    credits: {
        enabled: false,
    },

    series: [
        {
            name: "Installation",
            data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175],
        },
        {
            name: "Manufacturing",
            data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434],
        },
        {
            name: "Sales & Distribution",
            data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387],
        },
        {
            name: "Project Development",
            data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227],
        },
        {
            name: "Other",
            data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111],
        },
    ],
};

class HighMapChartView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: null,
            modules: [],
            loading: true,
            containerHeight: "container"
        };
    }

    viewData() {
        let dataChart;
        let chartData;
        let option;
        let container = this.state.containerHeight;

        let { data, analyze, filter } = this.props;

        switch (analyze) {
            case "universe-news-sentiment":
                dataChart = parseAreaChartSeries(
                    data,
                    "key",
                    "value"
                )
                // chartData = parseAreaChartSeries(
                //     this.props.data
                // );

                option.tooltip = {
                    formatter: function (e) {
                        return '<b>' + this.series.name + '</b> : <b>' + this.y + '</b>';
                    }
                };
                option.xAxis.labels.enabled = false;
                break;
            case "universe-news-exposure":
                option = options;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );

                chartData = parseAreaChartSeries(
                    dataChart,
                    "Exposure",
                    {
                        start_date: filter.startDate,
                        end_date: filter.endDate,
                    }
                );

                option.chart.type = "areaspline";
                option.xAxis.categories = chartData.categories;

                option.series = chartData.series;
                break;
            case "universe-performance-activity":
            case "universe-performance-response":
                option = options;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );
                chartData = parseAreaChartSeries(
                    dataChart,
                    "Exposure",
                    {
                        start_date: filter.startDate,
                        end_date: filter.endDate,
                    }
                );
                option.chart.type = "areaspline";
                option.xAxis.categories = chartData.categories;

                option.series = chartData.series;
                break;
            default:
                break;
        }
        this.setState({ options: options, containerHeight: container });

    }

    UNSAFE_componentWillMount() {
        this.viewData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // if (this.state.options != nextState.options) {
        //     return true;
        // }

        return true;
    }

    componentWillUnmount() {
        this.setState({ options: null });
    }

    LightenDarkenColor(col, amt) {
        // return this.theme == 'dark' ? this.calculateDark(col, amt) : this.calculateLight(col, amt);
        return this.calculateLight(col, amt);
    }

    calculateDark(col, amt) {
        var usePound = false;
        if (col[0] == '#') {
            col = col.slice(1);
            usePound = true;
        }

        var num = parseInt(col, 16);
        var r = (num >> 16) - amt;
        if (r > 255) r = 255;
        else if (r < 0) r = 0;
        var b = (num >> 8) & 0x00ff;
        if (b > 255) b = 255;
        else if (b < 0) b = 0;
        var g = num & 0x0000ff;
        if (g > 255) g = 255;
        else if (g < 0) g = 0;
        return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
    }

    calculateLight(col, amt) {
        var usePound = false;
        if (col[0] == '#') {
            col = col.slice(1);
            usePound = true;
        }

        var num = parseInt(col, 16);
        var r = (num >> 16) + amt;
        if (r > 255) r = 255;
        else if (r < 0) r = 0;
        var b = ((num >> 8) & 0x00ff) + amt;
        if (b > 255) b = 255;
        else if (b < 0) b = 0;
        var g = (num & 0x0000ff) + amt;
        if (g > 255) g = 255;
        else if (g < 0) g = 0;
        return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
    }

    renderChart(data) {
        var __this = this;
        let seriesData = [];
        let colors = [];
        let minColor = '#EEEEEE';
        // let minColor = this.theme == 'dark' ? '#424040' : '#EEEEEE';
        let interval = 0;
        let totalData = [];
        let districts = [];

        data.sort(function (a, b) {
            return b.data[0].value - a.data[0].value;
        });

        Object.keys(data).forEach(element => {
            totalData.push(data[element].data[0].value);
        });

        let i = 0;
        let color = "";
        let maxColor = "#CC2121";//560303,7F0000,AD0000,CC2121
        Object.keys(data).forEach(element => {
            i += 1;
            colors.push(data[element].data[0].value > 0 ? this.LightenDarkenColor(maxColor, interval) : minColor)
            color = data[element].data[0].value > 0 ? this.LightenDarkenColor(maxColor, interval) : minColor;

            seriesData.push({
                name: data[element].name,
                data: data[element].data,
                percent: this.percent(data[element].data[0].value, this.sumInArray(totalData)).replace("NaN", "0.00"),
                color: color,
                // color: data[element].data[0].value > 0 ? this.LightenDarkenColor('#CC2121', interval) : minColor,        
                borderColor: '#FFFFFF',
                // borderColor: this.theme == 'dark' ? '#212121' : '#FFFFFF',
                states: {
                    hover: {
                        color: '#EA2727',
                    },
                },
                dataLabels: {
                    enabled: false,
                    style: {
                        textShadow: '',
                    },
                    format: '<span style="color:yelow">{point.name}</span>',
                },
                joinBy: ['hc_key'],
            });
            interval += 5;
        });

        let highcharts = Highcharts;
        let analyze = (this.analyze) ? this.analyze : ''
    }

    percent(args, total, fixed = 2) {
        let result = (args / total) * 100;

        return result.toFixed(fixed);
    }

    sumInArray(arrayData = []) {
        var sum = arrayData.reduce(this.sumArray, 0);
        return sum;
    }

    sumArray(a, b) {
        return a + b;
    }

    viewChart(modules, options, container) {
        return (
            // [styles.text, touched && invalid ? styles.textinvalid : styles.textvalid]
            // <View style={styles.charts.container}>
            < View style={styles.charts[container]} >
                <HighchartsReactNative
                    useCDN
                    webviewStyles={styles.charts.webviewStyles}
                    styles={styles.charts.container}
                    options={options}
                    modules={modules}
                    useSSL
                />
            </View >
        );
    }

    render() {

        const { modules, options, containerHeight } = this.state;
        const { data } = this.props;

        if (options != null && data != null) {
            return this.viewChart(modules, options, containerHeight)
        } else {
            return (
                <>
                </>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity } = state;

    return {
        user,
        filter,
        entity,
    };
};

export default connect(mapStateToProps)(HighMapChartView);

