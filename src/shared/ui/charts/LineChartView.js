import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import {
    parseAreaChartSeries,
    parseArryObjectSingleSeries,
} from "app/helpers/ChartHelper";
import styles from "app/shared/styles";
import { connect } from "react-redux";

const options = {
    chart: {
        backgroundColor: "transparent",
        type: "spline",
    },
    title: {
        text: "",
    },

    subtitle: {
        text: "",
    },

    yAxis: {
        title: {
            text: "",
        },
    },

    xAxis: {
        categories: []
    },

    legend: {
        enabled: false,
        // layout: 'vertical',
        // align: 'right',
        // verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                style: {
                    fontWeight: 'normal',
                    fontSize: '9px',
                    textShadow: '.5px .5px #fff',
                }
            }
        },
        areaspline: {
            events: {
                click: item => {
                    // if (this.props.drillDown == true) {
                    //     if (this.month === "") {
                    //         this.onClickMonth(item.point.index + 1);
                    //     } else {
                    //         this.onClickDay(item.point.index + 1);
                    //     }
                    // }
                }
            },
            fillColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1, x3: 0, y3: 1 },
                stops: [[0, '#ff0000'], [1, '#0000ff'], [2, '#ffffff']]
            }
        },
    },


    credits: {
        enabled: false,
    },

    series: [
        {
            name: "Installation",
            data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175],
        },
        {
            name: "Manufacturing",
            data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434],
        },
        {
            name: "Sales & Distribution",
            data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387],
        },
        {
            name: "Project Development",
            data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227],
        },
        {
            name: "Other",
            data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111],
        },
    ],
};

class LineChartView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: null,
            modules: [],
            loading: true,
            containerHeight: "container"
        };
    }

    viewData() {
        let dataChart;
        let chartData;
        let option;
        let container = this.state.containerHeight;

        let { data, analyze, filter } = this.props;

        switch (analyze) {
            case "universe-news-sentiment":
                dataChart = parseAreaChartSeries(
                    data,
                    "key",
                    "value"
                )
                option.tooltip = {
                    formatter: function (e) {
                        return '<b>' + this.series.name + '</b> : <b>' + this.y + '</b>';
                    }
                };
                option.xAxis.labels.enabled = false;
                break;
            case "universe-news-exposure":
            case "activity-news-exposure":
                option = options;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );

                chartData = parseAreaChartSeries(
                    dataChart,
                    "Exposure",
                    {
                        start_date: filter.startDate,
                        end_date: filter.endDate,
                    }
                );

                option.chart.type = "areaspline";
                option.xAxis.categories = chartData.categories;

                option.series = chartData.series;
                break;
            case "universe-performance-activity":
            case "universe-performance-response":
            case "activity-performance-profile":
            case "activity-performance-response":
                option = options;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );
                chartData = parseAreaChartSeries(
                    dataChart,
                    "Exposure",
                    {
                        start_date: filter.startDate,
                        end_date: filter.endDate,
                    }
                );
                option.chart.type = "areaspline";
                option.xAxis.categories = chartData.categories;

                option.series = chartData.series;
                break;
            default:
                break;
        }
        this.setState({ options: options, containerHeight: container });

    }

    UNSAFE_componentWillMount() {
        this.viewData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // if (this.state.options != nextState.options) {
        //     return true;
        // }

        return true;
    }

    componentWillUnmount() {
        this.setState({ options: null });
    }

    viewChart(modules, options, container) {
        return (
            // [styles.text, touched && invalid ? styles.textinvalid : styles.textvalid]
            // <View style={styles.charts.container}>
            < View style={styles.charts[container]} >
                <HighchartsReactNative
                    useCDN
                    webviewStyles={styles.charts.webviewStyles}
                    styles={styles.charts.container}
                    options={options}
                    modules={modules}
                    useSSL
                />
            </View >
        );
    }

    render() {

        const { modules, options, containerHeight } = this.state;
        const { data } = this.props;

        if (options != null && data != null) {
            return this.viewChart(modules, options, containerHeight)
        } else {
            return (
                <>
                </>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity } = state;

    return {
        user,
        filter,
        entity,
    };
};

export default connect(mapStateToProps)(LineChartView);

