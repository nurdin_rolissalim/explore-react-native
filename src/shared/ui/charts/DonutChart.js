import React, { useState, Component } from "react";
import { StyleSheet, View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import * as issueService from "app/service/IssueService";
import * as chartService from "app/service/ChartService";
import { Toast } from "native-base";
import {
  objectToSeriesPie,
  parseArryObjectSingleSeries,
} from "app/helpers/ChartHelper";
import styles from "../../styles";

const sampleDonutOptions = {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: 0,
    plotShadow: false,
    backgroundColor: "transparent",
    type: "pie",
  },
  title: {
    text: "",
    align: "center",
    verticalAlign: "middle",
    // y: 60
  },
  tooltip: {
    pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
  },
  accessibility: {
    point: {
      valueSuffix: "%",
    },
  },
  plotOptions: {
    pie: {
      // allowPointSelect: true,
      // cursor: 'pointer',
      dataLabels: {
        enabled: false,
        // distance: -50,
        style: {
          fontWeight: "bold",
          color: "white",
        },
      },
      showInLegend: true,
      // startAngle: -90,
      // endAngle: 90,
      // center: ['50%', '75%'],
      // size: '110%'
    },
  },
  credits: { enabled: false },
  series: [
    {
      // colorByPoint: true,
      name: "",
      innerSize: "50%",
      data: [
        ["Chrome", 58.9],
        ["Firefox", 13.29],
        ["Internet Explorer", 13],
        ["Edge", 3.78],
        ["Safari", 3.42],
        {
          name: "Other",
          y: 7.61,
          dataLabels: {
            enabled: false,
          },
        },
      ],
    },
  ],
};

const sampleHalfDonutOptions = {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: 0,
    plotShadow: false,
    backgroundColor: "transparent",
    type: "pie",
  },
  title: {
    text: "",
    align: "center",
    verticalAlign: "middle",
    y: 60,
  },
  tooltip: {
    pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
  },
  accessibility: {
    point: {
      valueSuffix: "%",
    },
  },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: true,
        distance: -50,
        style: {
          fontWeight: "bold",
          color: "white",
        },
      },
      startAngle: -90,
      endAngle: 90,
      center: ["50%", "75%"],
      size: "110%",
    },
  },
  credits: { enabled: false },
  series: [
    {
      name: "",
      innerSize: "50%",
      data: [
        ["Chrome", 58.9],
        ["Firefox", 13.29],
        ["Internet Explorer", 13],
        ["Edge", 3.78],
        ["Safari", 3.42],
        {
          name: "Other",
          y: 7.61,
          dataLabels: {
            enabled: false,
          },
        },
      ],
    },
  ],
};

export default class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: null,
      loading: true,
      modules: [],
    };
  }

  getissueTop() {
    const params = {
      order: "DESC",
      size: 10,
      interval: 11,
      criteria: ["joko widodo"],
      start_date: "20200328",
      end_date: "20200428",
    };
    issueService
      .issueTop(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          const option = sampleDonutOptions;
          const data = parseArryObjectSingleSeries(
            response.data,
            "id",
            "count"
          );
          const chartData = objectToSeriesPie(data, "Issue");
          option.series[0].data = chartData.series;
          option.plotOptions.pie.dataLabels.enabled = true;
          option.plotOptions.pie.dataLabels.distance = -40;
          option.plotOptions.pie.showInLegend = false;
          option.plotOptions.pie.size = "100%";

          this.setState({
            options: option,
          });
        } else {
          Toast.show({
            text: "Asdasd",
            position: "top",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }
  getSocmedIssue() {
    const params = {
      person: "joko widodo",
      profiling: true,
      platform: "twitter",
      start_date: "20200504",
      end_date: "20200504",
      size: 10
    };;
    issueService
      .socmedIssue(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          const option = sampleDonutOptions;
          const data = parseArryObjectSingleSeries(
            response.data,
            "key",
            "value"
          );
          const chartData = objectToSeriesPie(data, "Issue");
          option.series[0].data = chartData.series;
          option.plotOptions.pie.dataLabels.enabled = true;
          option.plotOptions.pie.dataLabels.distance = -40;
          option.plotOptions.pie.showInLegend = false;
          option.plotOptions.pie.size = "100%";

          this.setState({
            options: option,
          });
        } else {
          Toast.show({
            text: "Asdasd",
            position: "top",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  getSentiment() {
    const params = {
      end_date: "20200430",
      start_date: "20200430",
      criteria: "joko widodo",
      timezone: "Asia/Jakarta"
    };
    chartService
      .sentiment(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          const option = sampleDonutOptions;
          const data = parseArryObjectSingleSeries(
            response.data,
            "id",
            "count"
          );
          const chartData = objectToSeriesPie(data, "Issue");
          option.series[0].data = chartData.series;
          option.plotOptions.pie.dataLabels.enabled = true;
          option.plotOptions.pie.dataLabels.distance = -40;
          option.plotOptions.pie.showInLegend = false;
          option.plotOptions.pie.size = "100%";

          this.setState({
            options: option,
          });
        } else {
          Toast.show({
            text: "Asdasd",
            position: "top",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  getData() {
    switch (this.props.analyze) {
      case "universe-news-issue":
        this.getissueTop();
        break;
      case "universe-activity-issue":
        this.getSocmedIssue();
        break;
      default:
        this.setState({ options: sampleHalfDonutOptions });
        break;
    }
  }

  UNSAFE_componentWillUpdate() {
  }

  UNSAFE_componentWillMount() {
    this.getData();
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (this.state.options != nextState.options) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    this.setState({ options: null });
  }

  render() {
    const { modules } = this.state;
    return (
      <View style={styles.charts.wrapper}>
        <HighchartsReactNative
          useCDN
          webviewStyles={styles.charts.webviewStyles}
          styles={styles.charts.container}
          options={this.state.options}
          useSSL
          modules={modules}
        />
      </View>
    );
  }
}
