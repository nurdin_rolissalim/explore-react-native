import React, { Component } from "react";
import { View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import {
    objectToSeriesPie,
    parseArryObjectSingleSeries,
} from "app/helpers/ChartHelper";
import styles from "../../styles";

const sampleDonutOptions = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        backgroundColor: "transparent",
        type: "pie",
    },
    title: {
        text: "",
        align: "center",
        verticalAlign: "middle",
        // y: 60
    },
    tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
    },
    accessibility: {
        point: {
            valueSuffix: "%",
        },
    },
    plotOptions: {
        pie: {
            // allowPointSelect: true,
            // cursor: 'pointer',
            dataLabels: {
                enabled: false,
                // distance: -50,
                style: {
                    fontWeight: "bold",
                    color: "white",
                },
            },
            showInLegend: true,
            // startAngle: -90,
            // endAngle: 90,
            // center: ['50%', '75%'],
            // size: '110%'
        },
    },
    credits: { enabled: false },
    series: [
        {
            // colorByPoint: true,
            name: "",
            innerSize: "50%",
            data: [
                ["Chrome", 58.9],
                ["Firefox", 13.29],
                ["Internet Explorer", 13],
                ["Edge", 3.78],
                ["Safari", 3.42],
                {
                    name: "Other",
                    y: 7.61,
                    dataLabels: {
                        enabled: false,
                    },
                },
            ],
        },
    ],
};

const sampleHalfDonutOptions = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        backgroundColor: "transparent",
        type: "pie",
    },
    title: {
        text: "",
        align: "center",
        verticalAlign: "middle",
        y: 60,
    },
    tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
    },
    accessibility: {
        point: {
            valueSuffix: "%",
        },
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: "bold",
                    color: "white",
                },
            },
            startAngle: -90,
            endAngle: 90,
            center: ["50%", "75%"],
            size: "110%",
        },
    },
    credits: { enabled: false },
    series: [
        {
            name: "",
            innerSize: "50%",
            data: [
                ["Chrome", 58.9],
                ["Firefox", 13.29],
                ["Internet Explorer", 13],
                ["Edge", 3.78],
                ["Safari", 3.42],
                {
                    name: "Other",
                    y: 7.61,
                    dataLabels: {
                        enabled: false,
                    },
                },
            ],
        },
    ],
};

export default class DonutChartView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: null,
            loading: true,
            modules: [],
        };
    }

    viewData() {
        let option;
        let dataChart;
        let chartData;
        let { analyze, data } = this.props

        switch (analyze) {
            case "universe-news-issue":
            case "activity-news-issue":
                option = sampleDonutOptions;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "id",
                    "count"
                );
                chartData = objectToSeriesPie(dataChart, "Issue");
                option.series[0].data = chartData.series;
                option.plotOptions.pie.dataLabels.enabled = true;
                option.plotOptions.pie.dataLabels.distance = -40;
                option.plotOptions.pie.showInLegend = false;
                option.plotOptions.pie.size = "100%";
                this.setState({ options: option });
                break;
            case "activity-profile-issue":
            case "activity-response-issue":
                option = sampleDonutOptions;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "id",
                    "value"
                );
                chartData = objectToSeriesPie(dataChart, "Issue");
                option.series[0].data = chartData.series;
                option.plotOptions.pie.dataLabels.enabled = true;
                option.plotOptions.pie.dataLabels.distance = -40;
                option.plotOptions.pie.showInLegend = false;
                option.plotOptions.pie.size = "100%";
                this.setState({ options: option });
                break;
            case "volunteer-recruitment-gender":
                option = sampleDonutOptions;
                // data = [{ "id": 1, "sex": "Laki-laki", "total": 12 }, {
                //     "id": 2, "sex":
                //         "Perempuan", "total": 10
                // }]
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "sex",
                    "total"
                );
                chartData = objectToSeriesPie(dataChart, "Gender", "gender");

                option.series[0].data = chartData.series;
                option.series[0].innerSize = "0";
                option.plotOptions.pie.dataLabels.enabled = false;
                option.plotOptions.pie.dataLabels.distance = -40;
                option.plotOptions.pie.showInLegend = true;
                option.plotOptions.pie.size = "100%";
                this.setState({ options: option });
                break;
            case "volunteer-recruitment-religion":
                option = sampleDonutOptions;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "name",
                    "total"
                );
                chartData = objectToSeriesPie(dataChart, "Religion", "religion");

                option.series[0].data = chartData.series;
                option.series[0].innerSize = "0";
                option.plotOptions.pie.dataLabels.enabled = false;
                option.plotOptions.pie.dataLabels.distance = -40;
                option.plotOptions.pie.showInLegend = true;
                option.plotOptions.pie.size = "100%";
                this.setState({ options: option });
                break;
            case "universe-activity":
            case "universe-response":
                option = sampleDonutOptions;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );
                chartData = objectToSeriesPie(dataChart, "Issue");
                option.series[0].data = chartData.series;
                option.plotOptions.pie.dataLabels.enabled = true;
                option.plotOptions.pie.dataLabels.distance = -40;
                option.plotOptions.pie.showInLegend = false;
                option.plotOptions.pie.size = "100%";
                this.setState({ options: option });
                break;
            case "universe-post-type-activity":
            case "universe-post-type-response":
            case "activity-post-type-profile":
            case "activity-post-type-response":
                option = sampleDonutOptions;
                // dataChart = parseArryObjectSingleSeries(
                //     data,
                //     "key",
                //     "value"
                // );
                // chartData = objectToSeriesPie(dataChart, "Issue");
                // option.series[0].data = chartData.series;
                // option.plotOptions.pie.dataLabels.enabled = true;
                // option.plotOptions.pie.dataLabels.distance = -40;
                // option.plotOptions.pie.showInLegend = false;
                option.plotOptions.pie.size = "100%";
                this.setState({ options: option });
                break;
            default:
                this.setState({ options: sampleHalfDonutOptions });
                break;
        }
    }

    onClikcChart(event) {

    }

    UNSAFE_componentWillMount() {
        this.viewData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // if (this.state.options != nextState.options) {
        //     return true;
        // }

        return true;
    }

    UNSAFE_componentWillUpdate() {
        // this.viewData();
    }


    componentDidUpdate() {
        // this.viewChart();
    }

    componentWillUnmount() {
        this.setState({ options: null });
    }

    viewChart(modules, options) {
        return (
            <View style={styles.charts.wrapper}>
                <HighchartsReactNative
                    useCDN
                    webviewStyles={styles.charts.webviewStyles}
                    styles={styles.charts.container}
                    options={options}
                    useSSL
                    modules={modules}
                    callback={this.onClikcChart()}
                />
            </View>
        );
    }

    render() {
        const { modules, options } = this.state;
        const { data, analyze } = this.props;

        if (options != null) {
            return this.viewChart(modules, options)
        } else {
            return (
                <>
                </>
            )
        }

    }
}
