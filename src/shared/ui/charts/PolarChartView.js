import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import {
    parseAreaChartSeries,
    parseArryObjectSingleSeries,
    objectToSeriesAndCategoriesEmotions
} from "app/helpers/ChartHelper";
import styles from "app/shared/styles";

const options = {
    credits: {
        enabled: false
    },
    chart: {
        polar: true,
        type: 'line'
    },

    title: {

        text: '',
        // x: -80
    },

    pane: {
        size: '80%'
    },

    xAxis: {
        categories: ['Sales', 'Marketing', 'Development', 'Customer Support',
            'Information Technology', 'Administration'],
        tickmarkPlacement: 'on',
        lineWidth: 0
    },

    yAxis: {
        gridLineInterpolation: 'polygon',
        lineWidth: 0,
        min: 0
    },

    tooltip: {
        shared: false,
        // pointFormat: '<span style="color:{point.color}">{series.name}: <b>{point.y:,.0f}</b><br/>',
        // valueSuffix: ' cm',
        formatter: function () {
            var tooltipMarkup = '';
            tooltipMarkup += '<span style="color:' + this.color + '">' + this.x + '</span>: <b>' + Highcharts.numberFormat(this.y, 0) + "" + '</b><br/>';
            return tooltipMarkup;
        }
    },

    legend: {
        enabled: false,
        align: 'right',
        verticalAlign: 'middle',
        layout: 'vertical'
    },

    series: [{
        name: 'Allocated Budget',
        data: [43000, 19000, 60000, 35000, 17000, 10000],
        pointPlacement: 'on'
    }],

    responsive: {
        rules: [{
            // condition: {
            //     maxWidth: 500
            // },
            chartOptions: {
                legend: {
                    enabled: false,
                    align: 'center',
                    verticalAlign: 'bottom',
                    layout: 'horizontal'
                },
                pane: {

                    size: '70%'
                }
            }
        }]
    }

};

class PolarChartView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: null,
            modules: [],
            loading: true,
            containerHeight: "container"
        };
    }

    viewData() {
        let dataChart;
        let chartData;
        let option = options;
        let container = this.state.containerHeight;
        let { data, analyze, filter } = this.props
        switch (analyze) {
            case "universe-news-sentiment":

                dataChart = parseAreaChartSeries(
                    data,
                    "key",
                    "value"
                )
                // chartData = parseAreaChartSeries(
                //     this.props.data
                // );

                option.tooltip = {
                    formatter: function (e) {
                        return '<b>' + this.series.name + '</b> : <b>' + this.y + '</b>';
                    }
                };
                option.xAxis.labels.enabled = false;
                break;
            case "universe-news-exposure":
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );
                chartData = parseAreaChartSeries(
                    dataChart,
                    "Exposure",
                    {
                        start_date: filter.startDate,
                        end_date: filter.endDate,
                    }
                );
                option.chart.type = "areaspline";
                option.xAxis.categories = chartData.categories;

                option.series = chartData.series;
                break;
            case "universe-emotion-activity":
            case "universe-emotion-response":
            case "activity-emotion-profile":
            case "activity-emotion-response":
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );
                chartData = objectToSeriesAndCategoriesEmotions(
                    dataChart,
                    "Emotions",
                    "emotion"
                )
                option.xAxis.categories = chartData.categories;
                option.series = chartData.series;

                break;
            default:
                break;
        }
        this.setState({ options: option, containerHeight: container });

    }

    UNSAFE_componentWillMount() {
        this.viewData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // if (this.state.options != nextState.options) {
        //     return true;
        // }

        return true;
    }

    componentWillUnmount() {
        this.setState({ options: null });
    }

    viewChart(modules, options, container) {
        return (
            // [styles.text, touched && invalid ? styles.textinvalid : styles.textvalid]
            // <View style={styles.charts.container}>
            < View style={styles.charts[container]} >
                <HighchartsReactNative
                    useCDN
                    webviewStyles={styles.charts.webviewStyles}
                    styles={styles.charts.container}
                    options={options}
                    modules={modules}
                    useSSL
                />
            </View >
        );
    }

    render() {

        const { modules, options, containerHeight } = this.state;
        const { data } = this.props;

        if (options != null && data != null) {
            return this.viewChart(modules, options, containerHeight)
        } else {
            return (
                <>
                </>
            )
        }
    }
}



export default PolarChartView;
