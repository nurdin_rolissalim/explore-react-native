import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import * as ChartService from "app/service/ChartService";
import { Toast } from "native-base";
import {
  objectToSeriesAndCategories,
  parseArryObjectSingleSeries,
} from "app/helpers/ChartHelper";
import styles from "../../styles";

const options = {
  chart: { type: "bar" },
  title: "",
  xAxis: {
    categories: [
      "sadness",
      "anger",
      "anticipation",
      "surprise",
      "disgust",
      "neutral",
      "joy",
      "trust",
    ],
    title: "",
  },
  yAxis: {
    title: "",
  },
  legend: {
    enabled: false,
  },
  credits: { enabled: false },
  series: [{ name: "", data: [1, 10, 3, 4, 7, 11, 2, 5] }],
};

class BarChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: null,
      modules: [],
      loading: true,
    };
  }

  getMediaShare() {
    const params = {
      aggregation_by: "MEDIA",
      size: 5,
      order: "DESC",
      start_date: "20200328",
      end_date: "20200428",
      person: "joko widodo",
    };

    ChartService.mediaShare(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          const data = parseArryObjectSingleSeries(
            response.data,
            "key",
            "value"
          );
          const chartData = objectToSeriesAndCategories(
            data,
            "Media",
            "default"
          );
          const option = options;
          option.xAxis.categories = chartData.categories;
          option.series = chartData.series;
          this.setState({
            options: option,
          });
        } else {
          Toast.show({
            text: "Asdasd",
            position: "top",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  getDefault() {
    const option = options;
    this.setState({
      options: option,
    });
  }

  getData() {
    switch (this.props.analyze) {
      case "universe-news-media-share":
        const chartData = this.getMediaShare();
        break;
      default:
        this.getDefault();
        break;
    }
  }

  UNSAFE_componentWillUpdate() {
  }

  UNSAFE_componentWillMount() {
    this.getData();
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (this.state.options != nextState.options) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    this.setState({ options: null });
  }

  render() {
    const { options, modules } = this.state;
    return (
      <View style={styles.charts.wrapper}>
        <HighchartsReactNative
          useCDN
          webviewStyles={styles.charts.webviewStyles}
          styles={styles.charts.container}
          options={this.state.options}
          modules={this.state.modules}
          useSSL
        />
      </View>
    );
  }
}

export default BarChart;
