import React, { Component } from "react";
import { View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import {
    objectToSeriesPie,
    parseArryObjectSingleSeries,
} from "app/helpers/ChartHelper";
import styles from "../../styles";

const sampleWordcloudOptions = {
    chart: {
        backgroundColor: "transparent",
        plotBorderWidth: 0,
        animation: true,
    },
    title: {
        enabled: false,
        text: null,
    },
    credits: {
        enabled: false,
    },
    series: [
        {
            type: "wordcloud",
            rotation: {
                from: 0,
                to: 0,
            },
            spiral: "rectangular",
            placementStrategy: "center",
            data: [
                ["#istiqomah", 7],
                ["#allahu_akbar", 7],
                ["#tolakdaruratsipil", 1],
            ],
        },
    ],
    tooltip: {
        formatter(e) {
            return `${this.key} : ${this.point.value}`;
        },
    },
};


export default class WordCloudChartView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: null,
            loading: true,
            modules: ["wordcloud"],
        };
    }

    viewData() {
        let option;
        let dataChart;
        let chartData;
        let { analyze, data } = this.props

        switch (analyze) {
            case "universe-news-issue":
                option = sampleWordcloudOptions;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "id",
                    "count"
                );
                chartData = objectToSeriesPie(dataChart, "Issue");
                option.series[0].data = chartData.series;
                this.setState({ options: option });
                break;

            case "universe-activity":
            case "universe-response":
            case "universe-wordcloud-activity":
            case "universe-wordcloud-response":
            case "activity-wordcloud-profile":
            case "activity-wordcloud-response":
            case "audience-wordcloud":
                option = sampleWordcloudOptions;
                dataChart = parseArryObjectSingleSeries(
                    data,
                    "key",
                    "value"
                );
                chartData = objectToSeriesPie(dataChart, "Issue");
                option.series[0].data = chartData.series;
                this.setState({ options: option });
                break;
            default:
                this.setState({ options: sampleWordcloudOptions });
                break;
        }
    }

    onClikcChart(event) {

    }
    onMessage(event) {

    }

    UNSAFE_componentWillMount() {
        this.viewData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        // if (this.state.options != nextState.options) {
        //     return true;
        // }

        return true;
    }

    UNSAFE_componentWillUpdate() {
        // this.viewData();
    }


    componentDidUpdate() {
        // this.viewChart();
    }

    componentWillUnmount() {
        this.setState({ options: null });
    }

    viewChart(modules, options) {
        return (
            <View style={styles.charts.wrapper}>
                <HighchartsReactNative
                    useCDN
                    webviewStyles={styles.charts.webviewStyles}
                    styles={styles.charts.container}
                    options={options}
                    useSSL
                    modules={modules}
                    callback={this.onClikcChart()}
                    onMessage={message => this.onMessage(message)}
                />
            </View>
        );
    }

    render() {
        const { modules, options } = this.state;
        const { data } = this.props;

        if (options != null) {
            return this.viewChart(modules, options)
        } else {
            return (
                <>
                </>
            )
        }

    }
}
