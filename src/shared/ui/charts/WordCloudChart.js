import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import HighchartsReactNative from "@highcharts/highcharts-react-native";
import styles from "../../styles";

const sampleWordcloudOptions = {
  chart: {
    backgroundColor: "transparent",
    plotBorderWidth: 0,
    animation: true,
  },
  title: {
    enabled: false,
    text: null,
  },
  credits: {
    enabled: false,
  },
  series: [
    {
      type: "wordcloud",
      rotation: {
        from: 0,
        to: 0,
      },
      spiral: "rectangular",
      placementStrategy: "center",
      data: [
        ["#istiqomah", 7],
        ["#allahu_akbar", 7],
        ["#tolakdaruratsipil", 1],
      ],
    },
  ],
  tooltip: {
    formatter(e) {
      return `${this.key} : ${this.point.value}`;
    },
  },
};

export default function WordCloudChart(props) {
  const [options] = useState({});

  /**
   * List modules, fill as you needed only
   * e.g:
   *  const modules = ['highcharts-more','solid-gauge'];
   *
   * Other modules, see: node_modules/@highcharts/highcharts-files/modules/
   */
  const modules = ["wordcloud"];

  return (
    <View style={styles.charts.wrapper}>
      <HighchartsReactNative
        useCDN
        webviewStyles={styles.charts.webviewStyles}
        styles={styles.charts.container}
        options={sampleWordcloudOptions}
        useSSL
        modules={modules}
      />
    </View>
  );
}
