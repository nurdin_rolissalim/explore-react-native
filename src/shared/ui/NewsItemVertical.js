import React from "react";
import { Image, View, StyleSheet } from "react-native";
import { Card, CardItem, Text, Body } from "native-base";
import * as propTypes from "prop-types";

function NewsItemVertical({ images, title, source, datePub }) {
  return (
    <Card style={styles.card}>
      <CardItem cardBody>
        <Image
          source={{ uri: images }}
          style={{ height: 90, width: null, flex: 1, resizeMode: "cover" }}
        />
      </CardItem>
      <CardItem>
        <Body>
          <Text numberOfLines={2} style={styles.title}>
            {title}
          </Text>
          <View style={styles.meta}>
            <Text style={styles.source}>{source}</Text>
            <Text numberOfLines={1} style={styles.datePub}>
              {datePub}
            </Text>
          </View>
        </Body>
      </CardItem>
    </Card>
  );
}

const styles = StyleSheet.create({
  card: {
    width: "100%",
  },
  meta: {
    flexDirection: "row",
    marginVertical: 5,
    overflow: "hidden",
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    overflow: "hidden",
  },
  source: {
    fontSize: 13,
    color: "red",
    marginRight: 10,
  },
  datePub: {
    fontSize: 13,
  },
  summary: {
    fontSize: 13,
  },
});

NewsItemVertical.propTypes = {
  images: propTypes.string,
  title: propTypes.string.isRequired,
  source: propTypes.string.isRequired,
  datePub: propTypes.string.isRequired,
};

NewsItemVertical.defaultProps = {
  images:
    "https://images.unsplash.com/photo-1559589689-577aabd1db4f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
};

export default NewsItemVertical;
