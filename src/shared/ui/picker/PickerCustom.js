import React, { Component } from "react";
import { StyleSheet } from "react-native";
import {
    Icon,
    Item,
    Picker,
    Form,
    View,
    Text
} from "native-base";
import { connect } from "react-redux";
import { get_categories } from 'app/service/IssueVolunteerService'
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';

class PickerCustom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            // type: props.type,
            // typeFont: props.typeFont ? props.typeFont : "FontAwesome5",
            active: false,
            selected: undefined
        };
        this.updateActive = this.updateActive.bind(this)
    }
    onSelected(value) {
        this.setState({
            selected: value
        });
    }

    getData() {
        let { analyze, user } = this.props;
        let params;
        let type_location = "1";
        let location = user.location;

        this.updateLoader(true, null, false);

        switch (analyze) {
            case "volunteer-issue":
                if (user.type_location === "city") {
                    type_location = "2";
                    if (location.indexOf("kab. ") > -1) {
                        location = "kabupaten " + location.replace("kab. ", "")
                    }
                }

                params = new Array(
                    "region=" + encodeURIComponent(location),
                    "type_location=" + type_location,
                    "start_date=",
                    "end_date=",
                    "code_workspace=" + user.workspace
                )

                this.getCategories(params)
                break;
        }
    }

    getCategories(params) {
        let data = null;

        get_categories(params)
            .then((response) => {
                if (response.data.length > 0) {
                    data = response.data
                    this.updateActive(data[0].id)

                }
                this.updateLoader(false, data)

            })
            .catch((error) => {

                this.updateLoader(false, data, error)
            });
    }

    updateLoader(loading, data, error = null) {
        this.setState({ loading: loading, data: data, error: error })
    }


    getPickers() {
        let data = null;
        let { analyze } = this.props
        switch (analyze) {
            case "universe-news":
                data = [
                    {
                        name: "user",
                        navigate: "person",
                        color: "#fa423b"
                    },
                    {
                        name: "building",
                        navigate: "organization",
                        color: "#fa423b"
                    },
                    {
                        name: "map-marker-alt",
                        navigate: "location",
                        color: "#fa423b"
                    },
                ];
                break;
            default:
                break;
        }
        return data;
    }

    UNSAFE_componentWillMount() {
        // this.getData();
    }

    componentDidMount() {
        const data = this.getData();
    }

    // UNSAFE_componentWillReceiveProps(nextProps) {
    //     if (nextProps.type !== this.state.type) {
    //         this.setState({ type: nextProps.type });
    //     }
    // }


    UNSAFE_componentWillUnmount() {
        this.setState({ data: null });
    }


    updateActive(selected) {
        this.props.callback(selected);
        this.setState({ selected: selected });
    }

    render() {
        let { selected, data, loading } = this.state
        let { type, analyze } = this.props

        if (loading === false && data != null) {
            return (
                <>
                    <Form>
                        <Item picker>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                                style={{ width: undefined }}
                                placeholder="Select your categories"
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                selectedValue={selected}
                                onValueChange={this.updateActive}
                            >
                                {data.map((item, index) => (
                                    <Picker.Item label={item.name} value={item.id} key={index} />
                                ))}
                            </Picker>
                        </Item>
                    </Form>
                </>
            );
        } else {
            return (
                <PlaceHolder loading={loading} data={data} />
            )
        }


    }
}

const styles = StyleSheet.create({
    footerContainer: {
        backgroundColor: "#fa423b",
        // height:45
    },
    iconText: {
        fontSize: 12,
    },
});


const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity, issue } = state;

    return {
        user,
        filter,
        entity,
        issue,
    };
};

export default connect(mapStateToProps)(PickerCustom);