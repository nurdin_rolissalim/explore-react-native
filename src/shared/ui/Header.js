import React, { useEffect, useState } from "react";
import {
  Header as NBHeader,
  Left,
  Right,
  Button,
  Icon,
  Text,
  Body,
} from "native-base";
import { StyleSheet, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";

import { connect } from "react-redux";
import { getPage } from "app/redux/ducks";

function Header(props) {
  const navigation = useNavigation();
  const [pageActive, setPageActive] = useState("login");
  const [filter, setFilter] = useState(false);
  useEffect(() => {
    let showFilter = false;
    switch (props.page.name) {
      case "activity-news":
      case "activity-timeline":
      case "activity-profile":
      case "activity-response":
      case "universe-news":
      case "universe-activity":
      case "universe-response":
      case "audience":
      case "volunteer-recruitment":
      case "volunteer-event":
      case "volunteer-calendar":
      case "volunteer-issue":
        showFilter = true;
        break;
      default:
        break;

    }
    setFilter(showFilter)

  });
  return (

    <NBHeader style={{ backgroundColor: "rgba(250, 66, 59, 1)" }} {...props}>
      <Left>
        <Button transparent onPress={() => navigation.openDrawer()}>
          <Icon name="menu" type="MaterialCommunityIcons" />
          <Text>Back</Text>
        </Button>
      </Left>
      <Body>
        <Image
          style={styles.logo}
          source={require("assets/images/politica-icon-text-light.png")}
        />
      </Body>
      <Right>
        {filter === true ?
          <Button transparent onPress={() => navigation.navigate("filter-date")}>
            <Icon name="filter" type="MaterialCommunityIcons" size={28} />
          </Button>
          : null
        }

        <Button transparent onPress={() => navigation.navigate("search")}>
          <Icon name="magnify" type="MaterialCommunityIcons" size={28} />
        </Button>
      </Right>
    </NBHeader>
  );
}

const styles = StyleSheet.create({
  logoContainer: {
    flex: 3,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    height: 50,
  },
  logo: {
    height: 32,
    resizeMode: "contain",
    justifyContent: "center",
    alignSelf: "center",
    marginLeft: 64,
  },
});


const mapStateToProps = (state) => {
  const user = state.auth.userData;
  const { filter, page, issue } = state;

  return { user, page, issue };
};

const mapDispatchToProps = {
  getPage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
