import React, { Component } from 'react'
import {
    Text,
    View
} from "native-base";
import { Card, Icon } from "native-base";

class CustomLabel extends Component {
    constructor(props) {
        super(props);

    }

    shouldComponentUpdate() {
        return true;

    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    viewPost() {
        let { type, data } = this.props
        return (
            <View style={{ flexDirection: "row", marginTop: 5 }}>
                {data.map((item) => (
                    < View
                        style={{ backgroundColor: item.color, marginRight: 3 }}
                        key={item.key}
                    >
                        {item.key == "platform" ?
                            <Icon
                                style={{ padding: 2, color: "#fff" }}
                                type="MaterialCommunityIcons"
                                name={item.value}
                                fontSize={12}
                            />
                            :

                            <Text
                                style={{
                                    fontSize: 16,
                                    paddingHorizontal: 10,
                                    paddingVertical: 5,
                                    color: "#fff",
                                    // color: "hsl(0, 0%, 55%)",
                                }}
                            >
                                {item.value}
                            </Text>
                        }

                    </View>
                ))}
            </View >
        )

    }

    render() {
        let { type, data } = this.props
        switch (type) {
            case "post":
                return this.viewPost()
                break;

            default:
                return (
                    <></>
                )
                break
        }
    }
}

export default CustomLabel;