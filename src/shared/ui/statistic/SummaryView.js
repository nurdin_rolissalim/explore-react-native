import React, { Component } from "react";
import { StyleSheet, View, FlatList, Image } from "react-native";
import {
    Card,
    CardItem,
    Text,
    Body,
} from "native-base";
import { NumberConvert } from 'app/helpers/GlobalFunction';

class SummaryView extends Component {
    constructor(props) {
        super(props);
    }
    /*
        digunakan di universe detail 
    */
    cardTotal(data) {
        return (
            <>
                {data.map((item) => (
                    <Card key={item.key}>
                        <CardItem>
                            <Body style={styles.container}>
                                <View>
                                    <Text style={styles.paragraph}>{NumberConvert(item.value)} {item.suffix}</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>
                ))
                }
            </>
        );
    }
    cardTotalString(data) {
        return (
            <>
                {data.map((item) => (
                    <Card key={item.key}>
                        <CardItem>
                            <Body style={styles.container}>
                                <View>
                                    <Text style={styles.paragraph}>{item.value} {item.suffix}</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>
                ))
                }
            </>
        );
    }

    render() {
        const { analyze, data } = this.props;

        switch (analyze) {
            case "volunteer-recruitment-monthly-growth":
                return this.cardTotalString(data);
                break;
            default:
                return this.cardTotal(data);
                break;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'center',
    },

    paragraph: {
        textAlign: 'center',
        // fontSize: 12,
        // fontWeight: 'bold'
    },
});

export default SummaryView;
