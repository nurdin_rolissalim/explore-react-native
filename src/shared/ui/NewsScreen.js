import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Segment,
  Content,
  Text,
} from "native-base";
import News from "app/shared/ui/News";

export default class NewsScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        <Header hasSegment>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Text>More News</Text>
          </Body>
        </Header>
        <Content padder>
          <News analyze={this.props.route.params.analyze} />
        </Content>
      </Container>
    );
  }
}
