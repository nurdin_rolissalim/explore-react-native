import React, { Component } from 'react';
import {
    Input,
    Header,
    Body,
    Item,
    Icon,
} from 'native-base';

import { StyleSheet } from "react-native";

class SearchView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: "",
            loading: false,
            typing: false,
            typingTimeout: 0
        }
        this.onChangeKeyword = this.onChangeKeyword.bind(this);
    }

    onChangeKeyword = (event) => {
        let self = this;
        let keyword = event.nativeEvent.text;

        if (this.state.typingTimeout) {
            clearTimeout(this.state.typingTimeout);
        }


        this.setState({
            keyword: keyword,
            typing: false,
            typingTimeout: setTimeout(function () {
                self.callbackSearch(keyword);
            }, 500)
        });
    }

    callbackSearch(keyword) {
        this.props.callback(keyword);
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.type != prevProps.type) {
            this.setState({ keyword: "" })
        }
    }


    componentWillUnmount() {
        clearTimeout(this.state.typingTimeout);
    }

    viewSearch() {
        return (
            <>
                <Header style={styles.headerContainer}>
                    <Body>
                        <Item>
                            <Icon name="ios-search" />
                            <Input placeholder="Search" type="text" value={this.state.keyword} onChange={evt => this.onChangeKeyword(evt)} />
                            {/* <Icon name="ios-people" /> */}
                        </Item>
                    </Body>
                </Header>
            </>
        )
    }

    render() {
        const { loading, keyword } = this.state;
        const { analyze } = this.props;

        switch (analyze) {
            case "universe":
                return this.viewSearch()
                break;
            default:
                return (
                    <>
                    </>
                )
                break;
        }

    }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#fff",
        borderColor: "#fff",
        borderTopWidth: 0,
        elevation: 0,
    },
    segmentContainer: {
        backgroundColor: "#fff",
    },
});

export default SearchView;