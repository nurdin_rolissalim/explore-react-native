import React, { Component } from "react";
import { StyleSheet, Image } from "react-native";
import {
    Thumbnail,
} from "native-base";
import { Truncate, CapitalizeFirstLetter, getImageEntity } from "app/helpers/GlobalFunction";
import { FlatList } from 'react-native-gesture-handler';
import { checkImageUrl } from "../../../service/EntityService";
const uri = require("assets/images/politica-icon.png");

export default class ThumbnailView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null,
        };
    }
    componentDidMount() {
        this.getImage()
    }

    getImage() {
        let { data, type } = this.props
        checkImageUrl(data, type)
            .then((response) => {
                this.setState({ image: response });
            })
            .catch((error) => {
                this.setState({ image: error });
            });
    }


    shouldComponentUpdate(nextProps, nextState, nextContext) {

        if (this.state.image != nextState.image) {
            return true;

        }

        return false;
    }


    UNSAFE_componentWillUnmount() {
        this.setState({ image: null });
    }

    ViewCircle() {
        let { image } = this.state
        return (
            <Thumbnail
                source={{ uri: image }}
            />
        )

    }

    viewSquare() {
        let { data, type } = this.props
        let { image } = this.state
        return (
            <Thumbnail
                source={{ uri: image }}
            />
        )
    }

    render() {
        const { analyze } = this.props;
        let { image } = this.state;

        if (image != null) {
            switch (analyze) {
                case "home-person":
                case "universe-news":
                case "universe-sosmed-profile":
                case "universe-sosmed-response":
                    return this.ViewCircle();
                case "universe-news":
                case "universe-sosmed-profile":
                case "universe-sosmed-response":
                    return this.viewSquare();
                    break;
                default:
                    return (
                        <>
                        </>
                    )
                    break;
            }
        } else {
            return (
                <>
                </>
            )
        }

    }
}

