import React from 'react';
import { Text, View, Dimensions, StyleSheet } from 'react-native';

import MapView, {
    Marker, Callout,
    MAP_TYPES,
    PROVIDER_DEFAULT,
    ProviderPropType,
    UrlTile,
    Circle,
} from 'react-native-maps';
import flagImg from 'app/assets/images/flag-blue.png';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = -6.90389;
const LONGITUDE = 107.61861;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

class MapsView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            type: ""
        };
    }

    UNSAFE_componentWillMount() {
        this.getMapType()
    }

    getMapType() {
        // MapKit does not support 'none' as a base map
        console.log("PROVIDER_DEFAULT", PROVIDER_DEFAULT);
        let type = MAP_TYPES.STANDARD;
        if (this.props.provider === PROVIDER_DEFAULT) {
            type = MAP_TYPES.STANDARD;
        }

        this.setState({ type: type })
        // return this.props.provider === PROVIDER_DEFAULT
        //     ? MAP_TYPES.STANDARD
        //     : MAP_TYPES.NONE;
    }

    ViewMap() {
        return (
            <View style={styles.container}>
                <MapView
                    provider={this.props.provider}
                    style={styles.map}
                    initialRegion={this.state.region}
                    onPress={this.onMapPress}
                    // mapType={type}
                    loadingEnabled
                    loadingIndicatorColor="#666666"
                    loadingBackgroundColor="#eeeeee"
                >
                    <Marker
                        coordinate={{
                            latitude: LATITUDE + SPACE,
                            longitude: LONGITUDE + SPACE,
                        }}
                        centerOffset={{ x: -18, y: -60 }}
                        anchor={{ x: 0.69, y: 1 }}
                        image={flagImg}
                    />
                    <Marker
                        coordinate={{
                            latitude: LATITUDE - SPACE,
                            longitude: LONGITUDE - SPACE,
                        }}
                        centerOffset={{ x: -42, y: -60 }}
                        anchor={{ x: 0.84, y: 1 }}
                    >
                        <Callout>
                            <View>
                                <Text>This is a plain view</Text>
                            </View>
                        </Callout>
                    </Marker>
                </MapView>
                <View style={styles.buttonContainer}>
                    <View style={styles.bubble}>
                        <Text>Map with Loading</Text>
                    </View>
                </View>
            </View>
        );
    }
    ViewCircle() {
        return (
            <View style={styles.container}>
                <MapView
                    provider={this.props.provider}
                    style={styles.map}
                    initialRegion={this.state.region}
                // onPress={this.onMapPress}
                // onRegionChangeComplete={this.onRegionChangeComplete.bind(this)}
                >
                    <MapView.Circle
                        // key={(this.state.currentLongitude + this.state.currentLongitude).toString()}
                        center={{
                            latitude: LATITUDE + SPACE,
                            longitude: LONGITUDE + SPACE,
                        }}
                        radius={1000}
                        strokeWidth={1}
                        strokeColor={'#1a66ff'}
                        fillColor={'#B61515'}
                    // fillColor={'rgba(230,238,255,0.5)'}
                    // onRegionChangeComplete={this.onRegionChangeComplete.bind(this)}
                    >
                        <Callout>
                            <Text>asdasd</Text>

                        </Callout>
                    </MapView.Circle>
                </MapView>
                <View style={styles.buttonContainer}>
                    <View style={styles.bubble}>
                        <Text>Map with Loading</Text>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        let { type } = this.state;
        return this.ViewCircle()
    }
}

// const LoadingMap.propTypes = {
//     provider: ProviderPropType,
// };

const styles = StyleSheet.create({
    container: {
        // ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: 300
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
});

export default MapsView;