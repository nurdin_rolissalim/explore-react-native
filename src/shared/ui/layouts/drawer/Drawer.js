import React from "react";
import { ScrollView, SafeAreaView } from "react-native";
import { DrawerItemList } from "@react-navigation/drawer";

function Drawer(props) {
  return (
    <ScrollView>
      <SafeAreaView style={{}}>
        <DrawerItemList {...props} />
      </SafeAreaView>
    </ScrollView>
  );
}

export default Drawer;
