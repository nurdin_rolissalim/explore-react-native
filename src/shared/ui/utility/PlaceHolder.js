import React, { Component } from 'react'
import { View, Text, Spinner } from 'native-base';

export default class PlaceHolder extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { loading, data } = this.props
        if (loading === true) {
            return (
                <View>
                    <Spinner color='red' />
                </View>
            )
        } else if (loading === false && data === false) {
            return (
                <>
                </>
            )
        } else {
            return (
                <View style={{ flex: 1, justifyContent: "center", alignContent: "center" }}>
                    <Text> No Result </Text>
                </View>
            )
        }
    }

}