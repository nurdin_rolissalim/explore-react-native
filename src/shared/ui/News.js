import React, { Component } from "react";
import { StyleSheet, View, FlatList, Image } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
} from "native-base";

const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../assets/images/politica-icon.png");

class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { key: "Banjir" },
        { key: "Salah Anies ?" },
        { key: "Zul" },
        { key: "Indra" },
        { key: "Mey" },
        { key: "Evi" },
        { key: "Dewi" },
        { key: "Jillian" },
        { key: "Jimmy" },
        { key: "Julie" },
      ],
      analyze: props.analyze,
    };
  }

  viewPostImage() {
    return (
      <>
        {this.state.data.map((r) => (
          <Card key={r.key}>
            <CardItem>
              <Left>
                <Thumbnail source={{ uri: img }} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image
                source={{ uri: img }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Body>
                <Text>NativeBase</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  NativeBase is a free and open source framework that enable
                  developers to build high-quality mobile apps using React
                  Native iOS and Android apps with a fusion of ES6.
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
        ))}
      </>
    );
  }

  viewPostList() {
    if (this.state.data.length > 0) {
      return (
        <>
          <List>
            {this.state.data.map((r) => (
              <ListItem thumbnail key={r.key}>
                <Left>
                  <Thumbnail square source={uri} />
                </Left>
                <Body>
                  <Text>NativeBase</Text>
                  <Text note numberOfLines={1}>
                    NativeBase is a free and open source framework that enable
                    developers to build high-quality mobile apps using React
                    Native iOS and Android apps with a fusion of ES6.
                  </Text>
                  <Text note numberOfLines={1}>
                    Media
                  </Text>
                  <Text note numberOfLines={1}>
                    Time
                  </Text>
                </Body>
                <Right>
                  <Button transparent>
                    <Text>View</Text>
                  </Button>
                </Right>
              </ListItem>
            ))}
          </List>
        </>
      );
    }
    return <Text>No Result</Text>;
  }

  render() {
    switch (this.state.analyze) {
      case "home-issue":
        return this.viewPostImage();
        break;
      case "activity-person":
        return this.viewPostImage();
        break;
      case "home-news-more":
        return this.viewPostList();
        break;
      default:
        return this.viewPostImage();
        break;
    }
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  container: {
    shadowRadius: 16,
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default News;
