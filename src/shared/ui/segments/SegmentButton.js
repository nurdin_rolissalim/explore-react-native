import React, { Component } from 'react';
import { StyleSheet } from "react-native";
import {
    Text,
    Button,
    Segment,
} from "native-base";

class SegmentButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            segment: null,
        }
    }

    getSegment() {
        let segment = null;
        switch (this.props.analyze) {
            case "universe":
                segment = [
                    {
                        type: "first",
                        name: "news",
                        navigate: "UniverseNews",
                    },
                    {
                        type: "",
                        name: "activity",
                        navigate: "UniverseActivity",
                    },
                    {
                        type: "last",
                        name: "response",
                        navigate: "UniverseResponse",
                    },
                ];
                break;
            case "activity":
                segment = [
                    {
                        type: "first",
                        name: "news",
                        navigate: "ActivityNews",
                    },
                    {
                        type: "",
                        name: "activity",
                        navigate: "ActivityProfile",
                    },
                    {
                        type: "last",
                        name: "response",
                        navigate: "ActivityResponse",
                    },
                ];
                break;
            default:
                break;
        }
        return segment;
    }

    componentDidMount() {
        const segment = this.getSegment();
        this.setState({ segment });
    }

    UNSAFE_componentWillUnmount() {
        this.setState({ segment: null });
    }

    clickSegment(segment) {
        this.props.callback(segment);
    }

    render() {
        const { segment } = this.state;
        if (segment == null) {
            return (
                <>
                </>
            )
        } else {
            return (
                <Segment style={styles.segmentContainer}>
                    {segment.map((item) => (
                        <Button
                            {...item.type}
                            {...(item.name == this.props.typeSegment ? { danger: true } : {})}
                            {...(item.name != this.props.typeSegment ? { dark: true } : {})}
                            // onPress={() => navigation.navigate(item.navigate)}
                            onPress={() => this.clickSegment(item.navigate)}
                            key={item.name}
                        >
                            <Text>{item.name}</Text>
                        </Button>

                    ))}
                </Segment>
            )
        }
    }
}


const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#fff",
        borderColor: "#fff",
        borderTopWidth: 0,
        elevation: 0,
    },
    segmentContainer: {
        backgroundColor: "#fff",
    },
});

export default SegmentButton;