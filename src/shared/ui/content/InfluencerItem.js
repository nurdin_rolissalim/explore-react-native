import React from "react";
import { View, Image, Text } from "native-base";

export default function InfluencerItem(props) {
  return (
    <View style={{ flexDirection: "row" }}>
      <Image
        resizeMode="center"
        style={{ width: 32, height: 32 }}
        source={require("../../../assets/images/politica-icon.png")}
      />
      <View style={{ marginLeft: 2, padding: 2 }}>
        <Text style={{ fontSize: 24 }}>Probowo Subianto</Text>
        <Text style={{ fontSize: 12 }}>Kompas 20/04/2020, 10:00</Text>
        <Text style={{ fontSize: 14 }}>
          Biasanya, rencana anggaran selalu diunggah Pemprov DKI ke dalam situs
          apbd.jakarta.go.id setiap tahun ...
        </Text>
      </View>
    </View>
  );
}
