import React, { Component } from "react";
import { StyleSheet } from "react-native";

import ListAvatar from './ListAvatar';
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';
import * as ContentService from "app/service/ContentService";
import { Truncate } from "app/helpers/GlobalFunction";
import { Toast } from "native-base";
const img =
    "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

export default class Post extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            analyze: props.analyze,
            loading: true,

        };
    }

    getDummyNews() {
        let data = [
            {
                "user_image": "http://pbs.twimg.com/profile_images/1185747474012442625/SWdGwudP_400x400.jpg",
                "user_full_name": "Joko Widodo",
                "username": "jokowi",
                "ann_clean_text": "saya meminta para kepala daerah fleksibel mencari solusi bagi warga miskin yang belum menerima bansos. data penerima agar transparan siapa, kriterianya apa, jenis bantuannya, dll. ini agar lebih jelas dan tidak menimbulkan kecurigaan. kita juga bisa mengoreksi di lapangan",
                "ann_sentiment": "positive",
                "ann_emotion": "neutral",
                "ann_info_class": "bias",
                "created_at": "2020-05-04 06:35:32",
                "media": null,
                "link": "https://twitter.com/jokowi/status/1257197150393360386",
                "platform": "twitter"
            },
            {
                "user_image": "http://pbs.twimg.com/profile_images/1185747474012442625/SWdGwudP_400x400.jpg",
                "user_full_name": "Joko Widodo",
                "username": "jokowi",
                "ann_clean_text": "semua program jaring pengaman sosial, pkh, sembako, bansos, blt, hingga dana desa, telah berjalan. saya minta menteri sosial, para gubernur, bupati, wali kota, camat, hingga kepala desa turun dan memastikan seluruh program sampai di tangan keluarga penerima pada pekan ini.",
                "ann_sentiment": "positive",
                "ann_emotion": "neutral",
                "ann_info_class": "neutral",
                "created_at": "2020-05-04 06:35:06",
                "media": [
                    {
                        "id": "537cf17b49e91bbcfeea33ff36f21423",
                        "index": "1257197040112558080-0",
                        "type": "photo",
                        "url": "http://pbs.twimg.com/media/EXJ2CGOUYAIB5QW.jpg"
                    }
                ],
                "link": "https://twitter.com/jokowi/status/1257197040112558080",
                "platform": "twitter"
            }
        ]
        this.setState({
            data: data
        })
    }

    getLatestPost(params) {

        ContentService.latestPost(params)
            .then((response) => {
                if (response.status === true && response.total > 0) {
                    // setTimeout(() => {
                    this.setState({
                        data: response.data,
                        loading: false
                    });

                    // }, 1000)

                } else {
                    Toast.show({
                        text: "Asdasd",
                        position: "top",
                        buttonText: "Okay",
                        duration: 2000,
                    });
                }
            })
            .catch((error) => {
                Toast.show({
                    text: "Check yout connection network",
                    position: "top",
                    buttonText: "Okay",
                    duration: 2000,
                });
            });
    }

    getData() {
        let params;
        this.setState({ loading: true, data: null });
        switch (this.props.analyze) {
            case "universe-news-issue":
                this.getNewsIssue();
                break;
            case "universe-sosmed-activity":
                params = {
                    index: this.props.type,
                    person: "joko widodo",
                    start_date: "20200504",
                    end_date: "20200504",
                    profiling: true,
                    order: "desc",
                    order_by: "latest",
                    page: 1,
                    size: 10
                }
                this.getLatestPost(params);
                // this.getDummyNews();

                break;
            default:
                this.getDummyNews();
                break;
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.type != this.props.type) {
            const data = this.getData();
        }
    }

    componentDidMount() {
        this.getData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    componentWillUnmount() {
        this.setState({ data: null });
    }


    viewPostImage(analyze, type, data) {
        return (
            <>
                <ListAvatar data={data} type={this.props.type} analyze={this.state.analyze} />
            </>
        );
    }

    render() {
        const { data, loading } = this.state
        const { type, analyze } = this.props

        if (loading === true) {
            return (
                <PlaceHolder loading={loading} data={data} />
            )
        } else {
            switch (analyze) {
                case "home-issue":
                    return this.viewPostImage(analyze, type, data);
                    break;
                case "activity-person":
                    return this.viewPostImage(analyze, type, data);
                    break;
                case "home-news":
                    return this.viewPostImage(analyze, type, data);
                    break;
                default:
                    return this.viewPostImage(analyze, type, data);
                    break;
            }
        }


    }
}

const styles = StyleSheet.create({
    content: {
        padding: 20,
    },
    container: {
        shadowRadius: 16,
    },
    image: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});

