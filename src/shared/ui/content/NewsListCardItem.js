import React, { Component } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Image,
} from "native-base";

const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class NewsListCardItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { key: "Banjir" },
        { key: "Salah Anies ?" },
        { key: "Zul" },
        { key: "Indra" },
        { key: "Mey" },
        { key: "Evi" },
        { key: "Dewi" },
        { key: "Jillian" },
        { key: "Jimmy" },
        { key: "Julie" },
      ],
      analyze: props.analyze,
    };
  }

  viewPost() {
    return (
      <>
        {this.state.data.map((r) => (
          <Card style={{ flex: 0 }} key={r.key}>
            <CardItem>
              <Left>
                <Thumbnail source={uri} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>April 15, 2016</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Text>//Your text here</Text>
              </Body>
            </CardItem>
            {/* <CardItem>
                            <Left>
                                <Button transparent textStyle={{ color: '#87838B' }}>
                                    <Icon name="logo-github" />
                                    <Text>1,926 stars</Text>
                                </Button>
                            </Left>
                        </CardItem> */}
          </Card>
        ))}
      </>
    );
  }

  render() {
    switch (this.state.analyze) {
      case "home-issue":
        return this.viewPost();
        break;
      case "activity-person":
        return this.viewPost();
        break;
      case "home-news":
        return this.viewPost();
        break;
      default:
        return this.viewPost();
        break;
    }
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  container: {
    shadowRadius: 16,
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default NewsListCardItem;
