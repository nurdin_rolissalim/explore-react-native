import React, { Component } from "react";
import { StyleSheet, FlatList, Image } from "react-native";
import {
  Container,
  Header,
  Content,
  Thumbnail,
  Text,
  List,
  ListItem,
  Body,
  Left,
  Right,
  View,
  Toast,
} from "native-base";
import { Truncate } from "app/helpers/GlobalFunction";
import * as EntityService from "app/service/EntityService";

const uri = require("assets/images/politica-icon.png");

export default class Entity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      keyword: null,
    };
  }

  selectedEntity(entity) {
    this.props.callback(entity);
  }

  getData() {
    let data = null;
    switch (this.props.analyze) {
      case "universe-news":
        data = this.getUniverseNews();
        break;
      case "universe-sosmed-profile":
      case "universe-sosmed-response":
        data = this.getUniverseSosmed();
        break;
      default:
        data = this.getPerson();
        break;
    }
    return data;
  }

  getUniverseNews() {
    let data = null;
    EntityService.getSubscription({
      page: 1,
      size: 100,
      type: this.props.type,
      keyword: "",
      start_date: "20200324",
      end_date: "20200424",
      index: "news",
      id: "40",
    })
      .then((response) => {
        if (response.status === true) {
          data = response.data;
        } else {
          data = null;
        }
        this.setState({ data });
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  getUniverseSosmed() {
    let data = null;
    EntityService.getSubscription({
      page: 1,
      size: 100,
      type: "person",
      keyword: "",
      start_date: "20200324",
      end_date: "20200424",
      index: this.props.type,
      id: "40",
    })
      .then((response) => {
        if (response.status === true) {
          data = response.data;
        } else {
          data = null;
        }
        this.setState({ data });
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  getPerson() {
    const data = [
      { key: "Anwar", count: 90, image: uri },
      { key: "Dida", count: 1203, image: uri },
      { key: "James", count: 1203, image: uri },
      { key: "Arta", count: 1203, image: uri },
      { key: "Imam", count: 1203, image: uri },
      { key: "Dewi", count: 1203, image: uri },
      { key: "Mey", count: 1203, image: uri },
      { key: "Yayuk", count: 1203, image: uri },
      { key: "Indra", count: 1203, image: uri },
      { key: "Nurdin Rolissalim", count: 1203, image: uri },
    ];
    this.setState({ data });
    return data;
  }

  componentDidMount() {
    const data = this.getData();
    // this.setState({ data: data })
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return true;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.type != this.props.type) {
      const data = this.getData();
      // this.setState({ data: data })
    }
  }

  UNSAFE_componentWillUnmount() {
    this.setState({ data: null });
  }

  verticalLayout() {
    if (this.state.data != null && this.state.data.length > 0) {
      return (
        <>
          <Header style={styles.headerContainer}>
            <Body>
              <Item>
                <Icon name="ios-search" />
                <Input placeholder="Search" />
                {/* <Icon name="ios-people" /> */}
              </Item>
            </Body>
          </Header>
          <List>
            {this.state.data.map((item) => (
              <ListItem
                avatar
                key={item.key}
                onPress={() => this.selectedEntity(item.key)}
              >
                <Left>
                  <Thumbnail source={uri} />
                </Left>
                <Body>
                  <Text>{item.key}</Text>
                  <Text note>{item.count}</Text>
                </Body>
              </ListItem>
            ))}
          </List>
        </>
      );
    }
    return <Text>No Result</Text>;
  }

  slideLayout() {
    return (
      <View style={styles.boxSmall}>
        <FlatList
          style={[{ paddingTop: 5 }]}
          keyExtractor={(_, index) => index.toString()}
          data={this.state.data}
          renderItem={({ item }) => (
            <View style={{ marginHorizontal: 8 }} key={item.key}>
              <Thumbnail circle source={uri} />
              <Text style={{ textAlign: "center", fontSize: 16, marginTop: 3 }}>
                {Truncate(item.key, 6, "...")}
              </Text>
            </View>
          )}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }

  viewSearch() {
    <Header style={styles.headerContainer}>
      <Body>
        <Item>
          <Icon name="ios-search" />
          <Input placeholder="Search" />
          {/* <Icon name="ios-people" /> */}
        </Item>
      </Body>
    </Header>
  }

  componentWillUnmount() {
    this.setState({ data: null });
  }

  render() {
    if (this.state.data != null && this.state.data.length > 0) {
      switch (this.props.analyze) {
        case "home-person":
        case "activity-person":
          return this.slideLayout();
        case "universe-news":
        case "universe-sosmed-profile":
        case "universe-sosmed-response":
          return this.verticalLayout();
        default:
          break;
      }
    } else {
      return (
        <View>
          <Text>No result</Text>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  boxSmall: {
    // width: 200,
    height: 90,
    marginBottom: 5,
    marginRight: 5,
    marginTop: 5,
    marginLeft: 5,
    // backgroundColor: 'rgba(0, 0, 0, 0.02)',
    // padding:1,
  },
  myButton: {
    padding: 10,
    marginLeft: 5,
    height: 75,
    width: 75, // The Width must be the same as the height
    borderRadius: 400, // Then Make the Border Radius twice the size of width or Height
    backgroundColor: "rgb(195, 125, 198)",
  },
});
