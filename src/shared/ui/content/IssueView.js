import React from 'react';
import { StyleSheet, FlatList, ScrollView } from "react-native";
import { Text, View } from "native-base";
import { TouchableOpacity } from "react-native-gesture-handler";

class Issue extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            analyze: props.analyze,
            selected: {
                id: null,
                key: null
            }
        };

        this.onSelectedIssue = this.onSelectedIssue.bind(this);
    }

    onSelectedIssue(issue) {
        this.setState({ selected: issue })
        this.props.callback(issue)
    }

    UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
        // update selected issue
        if (this.state.selected.id != prevState.selected.id) {
            switch (this.state.analyze) {
                case "universe-news-issue":
                case "universe-activity":
                case "universe-response":
                    return this.viewSlideHorizontal(this.state.data);
                    break;
                default:
                    return this.viewSlideHorizontal(this.state.data);
                    break;
            }
        }
    }

    shouldComponentUpdate() {
        return true;
    }

    componentDidMount() {
        // switch (this.state.analyze) {
        //     case "home-issue":
        //         this.getHomeIssue();
        //         break;

        //     case "activity-person":
        //         this.setState({
        //             data: [
        //                 { key: "Kerja Sama 3 Provinsi" },
        //                 { key: "Amin" },
        //                 { key: "Anwar" },
        //                 { key: "Dida" },
        //                 { key: "James" },
        //                 { key: "Joel" },
        //                 { key: "John" },
        //                 { key: "Jillian" },
        //                 { key: "Jimmy" },
        //                 { key: "Julie" },
        //             ],
        //         });
        //         break;
        //     default:
        //         break;
        // }
    }
    UNSAFE_componentWillMount() {
        this.onSelectedIssue(this.props.data[0])
        switch (this.state.analyze) {
            case "home-issue":
                // this.getHomeIssue();
                break;

            case "activity-person":
                this.setState({
                    data: [
                        { key: "Kerja Sama 3 Provinsi" },
                        { key: "Amin" },
                        { key: "Anwar" },
                        { key: "Dida" },
                        { key: "James" },
                        { key: "Joel" },
                        { key: "John" },
                        { key: "Jillian" },
                        { key: "Jimmy" },
                        { key: "Julie" },
                    ],
                });
                break;
            default:
                break;
        }
    }

    IssueItem = ({ item, index }) => {
        let { selected } = this.state;
        return (
            <View style={selected.id == item.id ? [styles.issuItemContainer] : [styles.issuItemContainer]} key={item.id}>
                <TouchableOpacity
                    danger
                    style={selected.id == item.id ? [styles.issuItemButtonActive] : [styles.issuItemButton]}
                    onPress={() => this.onSelectedIssue(item)}
                >
                    <Text style={styles.issuItemLabel}>{item.label}</Text>
                </TouchableOpacity>
            </View>
        );
    }
    IssueItemSosmed = ({ item, index }) => {
        let { selected } = this.state;
        return (
            <View style={selected.key == item.key ? [styles.issuItemContainer] : [styles.issuItemContainer]} key={item.key}>
                <TouchableOpacity
                    danger
                    style={selected.key == item.key ? [styles.issuItemButtonActive] : [styles.issuItemButton]}
                    onPress={() => this.onSelectedIssue(item)}
                >
                    <Text style={styles.issuItemLabel}>{item.key}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    viewSlideHorizontal(data) {
        return (
            <View style={styles.container}>
                {/* <ScrollView horizontal={true} locked={false}> */}
                <FlatList
                    style={{ paddingTop: 5 }}
                    data={data}
                    renderItem={(item) => this.IssueItem(item)}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                />
                {/* </ScrollView> */}
            </View>
        );
    }

    viewSlideHorizontalSosmed(data) {
        return (
            <View style={styles.container}>
                {/* <ScrollView horizontal={true} locked={false}> */}
                <FlatList
                    style={{ paddingTop: 5 }}
                    data={data}
                    renderItem={(item) => this.IssueItemSosmed(item)}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                />
                {/* </ScrollView> */}
            </View>
        );
    }


    render() {
        const { analyze, data } = this.props;
        switch (analyze) {
            case "universe-news-issue":
            case "home-issue":
                return this.viewSlideHorizontal(data);
                break;
            case "universe-activity":
            case "universe-response":
                return this.viewSlideHorizontalSosmed(data);
                break;
            default:
                return this.viewSlideHorizontal(data);
                break;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        // width: 200,
        height: 80,
        margin: 5,
    },
    issuItemContainer: {
        padding: 5,
    },
    issuItemButton: {
        paddingVertical: 10,
        paddingHorizontal: 16,
        backgroundColor: "#fff",
        // backgroundColor: "rgba(250, 66, 59, 0.15)",
        borderColor: "#fa423b",
        borderRadius: 8,
        borderWidth: 0.2
    },
    issuItemButtonActive: {
        paddingVertical: 10,
        paddingHorizontal: 16,
        backgroundColor: "rgba(250, 66, 59, 0.15)",
        borderColor: "#fa423b",
        borderRadius: 8,
        borderWidth: 0.2
    },
    issuItemLabel: {
        fontSize: 16,
        color: "#fa423b",
    },
});

export default Issue;
