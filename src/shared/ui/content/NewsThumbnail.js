import React, { Component } from "react";
import { StyleSheet, FlatList, Image } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Badge,
  Toast,
  View,
} from "native-base";

import * as IssueService from "app/service/IssueService";
import { Truncate } from "app/helpers/GlobalFunction";

const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class NewsThumbnail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      loading: true,
    };
  }

  getDummyNews() {
    const data = [
      {
        created_at: "2020-05-02 00:37:36",
        desc:
          "Sorotan tajam tengah mengarah pada jajaran staf khusus milenial Presiden Joko Widodo. Belva Devara selaku chief executive officer (CEO) Ruangguru diketahui mengundurkan diri dari jabatan staf khusus presiden karena ikut menyediakan pelatihan Kartu Prakerja dalam platform digital berbayarnya tersebut. Tidak hanya Belva yang mengundurkan diri sebagai staf khusus milenial presiden, Andi Taufan Garuda Putra selaku CEO PT Amartha juga ikut mengirimkan surat pengunduruan dirinya. Dia mengatakan, masalah yang terjadi pada jajaran staf khusus presiden bisa dimasukkan dalam kategori konflik kepentingan.",
        id_hash: "40218d0b3125ca3d5c618e9f220d7ee6",
        images: [
          "https://www.seputarnkri.com/wp-content/uploads/2020/05/penyelesaian-kisruh-staf-milenial-presiden-tak-cukup-dengan-mundur-uio.jpg",
        ],
        link:
          "https://www.seputarnkri.com/69103/penyelesaian-kisruh-staf-milenial-presiden-tak-cukup-dengan-mundur.html",
        source: "seputar nkri",
        title:
          "Penyelesaian Kisruh Staf Milenial Presiden Tak Cukup dengan Mundur",
      },
      {
        created_at: "2020-05-01 23:48:27",
        desc:
          'Usai mendapat promosi jabatan sebagai Kabaintelkam Polri, Kapolda Jateng Irjen Pol Rycko Amelza Dahniel langsung berziarah ke makam ibunda Presiden Republik Indonesia Joko Widodo, Hj Sujdiatmi Notomiharjo di makam Keluarga Mundu, Gondangrejo Karanganyar, Jumat (1/5) "Ini sudah menjadi kebiasaan saya, setiap mendapatkan amanah baru, saya selalu berziarah ke makam orangtua saya. Surat TR ini telah diterima oleh seluruh anggota Polri jajaran, termasuk Kapolda Jateng dengan Nomor ST/1377/V/KEP./2020, tertanggal Jumat 1 Mei 2020. Nantinya, jabatan Rykco sebagai Kapolda Jateng akan digantikan oleh wakilnya, Brigjen Pol Ahmad Luthfi. Berdasarkan informasi yang dihimpun, orang nomor satu di Kepolisian Daerah (Polda) Jateng ini, juga otomatis mendapat promosi kenaikan pangkat sebagai jenderal bintang 3 atau Komjen Pol dalam jabatan barunya.',
        id_hash: "b3194011b63a373544402d9020e7f349",
        images: [
          "https://bhinnekanusantara.id/wp-content/uploads/2020/05/IMG_20200501_202716-650x430.jpg",
        ],
        link:
          "https://bhinnekanusantara.id/jadi-bintang-tiga-kapolda-jateng-ziarahi-makam-ibu-jokowi/",
        source: "bhinneka nusantara",
        title: "Jadi Bintang Tiga, Kapolda Jateng Ziarahi Makam Ibu Jokowi",
      },
      {
        created_at: "2020-05-01 23:52:17",
        desc:
          'Pemerintah melalui PT Perusahaan Listrik Negara (Persero) (PT PLN) tengah menyiapkan mekanisme teknis membebaskan tagihan listrik bagi pelanggan Bisnis Skala Kecil (BI) dan Industri Skala Kecil (I1) yang berdaya 450 VA. "Tentu sangat jelas dan tergambar nyata kebijakan pak presiden bagaimana merasakan penderitaan dan kesulitan masyarakat akibat Covid-19 ini. Maka, PLN langsung menyiapkan langkah-langkah teknis pembebasan tagihan listrik bagi pelanggan Bisnis Kecil dan Industri Kecil," kata Direktur Utama PLN, Zulkifli Zaini, kemarin (1/5). Pihak PLN telah merealisasikan kebijakan Presiden Joko Widodo (Jokowi) tersebut.',
        id_hash: "4e0a985a86b42d35fe4e28cd53946396",
        images: [
          "https://radartegal.com/wp-content/uploads/2020/05/sistem-pembayaran-feature.jpg",
        ],
        link:
          "https://radartegal.com/headline/giliran-pelanggan-listrik-bisnis-dan-industri-kecil-digratiskan.html",
        source: "radar tegal",
        title:
          "Giliran Pelanggan Listrik Bisnis dan Industri Kecil Digratiskan",
      },
      {
        created_at: "2020-05-01 23:45:19",
        desc:
          "Mabes Polri melakukan mutasi dan promosi sejumlah pejabat tinggi, salah satu yang dirotasi jabatan Kepala Polrestabes Medan Kombes Jhonny Eddizon Isir dimutasi menjadi kepala Polrestabes Surabaya, Polda Jawa Timur. Jhonny merupakan alumnus terbaik Akademi Kepolisian (Akpol) 1996, yang pernah menjadi ajudan Presiden Joko Widodo (Jokowi). Sedangkan pengganti Jhonny dipercayakan kepada Kombes Riko Sunarko yang sebelumnya menjabat sebagai Analis Kebijakan Madya Bidang Paminal Divpropram Polri. Mutasi itu tertuang dalam Surat Telegram nomor ST/1377/V/KEP.2020 dan ST/1378/V/KEP.2020 pada tanggal 1 Mei 2020.",
        id_hash: "3b3d89919745351bb4a9795d47b9e33c",
        images: [
          "https://static.republika.co.id/uploads/images/inpicture_slide/kombes-johnny-eddizon-isir-yang-kini-dimutasi-menjadi-kepala_200502064159-625.jpg",
        ],
        link:
          "https://republika.co.id/berita/q9of7x484/eks-ajudan-jokowi-jabat-kepala-polrestabes-surabaya",
        source: "republika",
        title: "Eks Ajudan Jokowi Jabat Kepala Polrestabes Surabaya",
      },
      {
        created_at: "2020-05-02 03:22:51",
        desc:
          "Mantan Wakil Ketua DPR, Fahri Hamzah terang-terangan mengritik pengetahuan Presiden Joko Widodo (Jokowi) soal pemberantasan korupsi. Dilansir TribunWow.com, Fahri Hamzah secara gamblang menyebut Jokowi dan jajarannya belum cukup pengetahuan untuk menegakkan pemberantasan korupsi di Indonesia. Pernyataan Fahri Hamzah itu pun lantas memancing tawa Pakar Hukum Tata Negara, Refly Harun. Melalui tayangan YouTube Refly Harun, Jumat (30/4/2020), mulanya Fahri Hamzah menyebut KPK periode lalu bekerja tanpa menggunakan pikiran.",
        id_hash: "3c1f0c9a3e1d6474b11c6fbfff433151",
        images: [
          "https://cdn2.tstatic.net/tribunnews/foto/bank/images/mantan-wakil-ketua-dpr-ri-fahri-hamzah-dalam-kanal-youtube-refly-h.jpg",
        ],
        link:
          "https://www.tribunnews.com/nasional/2020/05/02/ngeri-dengar-kritik-fahri-hamzah-soal-jokowi-refly-harun-mohon-maaf-yang-ngomong-bukan-saya",
        source: "tribun news",
        title:
          "'Ngeri' Dengar Kritik Fahri Hamzah soal Jokowi, Refly Harun: Mohon Maaf, yang Ngomong Bukan Saya",
      },
    ];
    this.setState({
      data,
    });
  }

  getNewsIssue() {
    const params = {
      cluster_id: "presiden joko widodo",
      size: 3,
      start_date: "20200502",
      end_date: "20200502",
      order: "desc",
      page: 1,
      person: "joko widodo",
    };
    IssueService.issueNews(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          this.setState({
            data: response.data,
          });
        } else {
          Toast.show({
            text: "Asdasd",
            position: "top",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  getData() {
    switch (this.props.analyze) {
      case "universe-news-issue":
        this.getNewsIssue();
        break;
      default:
        this.getDummyNews();
        break;
    }
  }

  UNSAFE_componentWillUpdate() {
  }

  UNSAFE_componentWillMount() {

    this.getData();
  }

  componentDidMount() {

    // this.getData();
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (
      nextProps.analyze != this.props.analyze ||
      this.state.data != nextState.data
    ) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    this.setState({ data: null });
  }

  viewPostImage() {
    return (
      <>
        {this.state.data.map((item) => (
          <Card key={item.link}>
            <CardItem cardBody>
              <Image
                source={uri}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Text>{item.source} </Text>
              </Left>
              <Right>
                <Text>{item.created_at}</Text>
              </Right>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{item.title}</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{Truncate(item.desc, 100)}</Text>
              </Body>
            </CardItem>
          </Card>
        ))}
      </>
    );
  }

  viewPostPlatform() {
    return (
      <>
        {this.state.data.map((r) => (
          <Card key={r.key}>
            <CardItem>
              <Left>
                <Thumbnail source={uri} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>11 hours</Text>
                </Body>
              </Left>
              <Right>
                <Icon name="facebook" type="FontAwesome5" />
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Image
                source={uri}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  NativeBase is a free and open source framework that enable
                  developers to build high-quality mobile apps using React
                  Native iOS and Android apps with a fusion of ES6.
                </Text>
              </Body>
            </CardItem>

            <CardItem>
              <Badge red>
                <Text>negative</Text>
              </Badge>
              {/* <Button small ><Text>neutral</Text></Button> */}
            </CardItem>
          </Card>
        ))}
      </>
    );
  }

  noResult() {
    return (
      <>
        <View>
          <Text> No Result</Text>
        </View>
      </>
    );
  }

  render() {
    if (this.state.data != null) {
      switch (this.props.analyze) {
        case "universe-news-issue":
          return this.viewPostImage();
          break;
        case "audience":
          return this.viewPostPlatform();
          break;
        case "home-issue":
          return this.viewPostPlatform();
          break;
        case "activity-person":
          return this.viewPostImage();
          break;
        case "home-news":
          return this.viewPostImage();
          break;
        case "universe-sosmed":
          return this.viewPostPlatform();
          break;
        default:
          return this.viewPostImage();
          break;
      }
    } else {
      return this.noResult();
    }
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  container: {
    shadowRadius: 16,
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default NewsThumbnail;
