import React, { Component } from "react";
import { StyleSheet, View, FlatList, Image } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
} from "native-base";

const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class NewsListCardItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { key: "Banjir" },
        { key: "Salah Anies ?" },
        { key: "Zul" },
        { key: "Indra" },
        { key: "Mey" },
        { key: "Evi" },
        { key: "Dewi" },
        { key: "Jillian" },
        { key: "Jimmy" },
        { key: "Julie" },
      ],
      analyze: props.analyze,
    };
  }

  viewPostImage() {
    return (
      <>
        <List>
          {this.state.data.map((r) => (
            <ListItem avatar key={r.key}>
              <Left>
                <Thumbnail source={uri} />
              </Left>
              <Body>
                <Text>Kumar Pratik</Text>
                <Text note>
                  Doing what you like will always keep you happy . .
                </Text>
              </Body>
              <Right>
                <Text note>3:43 pm</Text>
              </Right>
            </ListItem>
          ))}
        </List>
      </>
    );
  }

  render() {
    switch (this.state.analyze) {
      case "home-issue":
        return this.viewPostImage();
        break;
      case "activity-person":
        return this.viewPostImage();
        break;
      case "home-news":
        return this.viewPostImage();
        break;
      default:
        return this.viewPostImage();
        break;
    }
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  container: {
    shadowRadius: 16,
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default NewsListCardItem;
