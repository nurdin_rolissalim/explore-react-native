import React, { Component } from "react";
import { StyleSheet, Image } from "react-native";
import {
  Card,
  CardItem,
  Thumbnail,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Badge,
} from "native-base";

import { Truncate } from "app/helpers/GlobalFunction";
import { TimeAgoMoment } from "app/helpers/DateHelper";

const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class ListThumbnail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
    };
  }

  componentDidMount() {
    // this.getData();
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return false;
  }

  componentDidUpdate() {
    if (
      this.props.data != nextProps.data &&
      (this.props.analyze === nextProps.analyze)
    ) {
      this.setState({ data: this.props.data })
    }
  }

  componentWillUnmount() {
    this.setState({ data: null });
  }

  openLink(item) {
    // const OpenURLButton = ({ url, children }) => {
    //   const handlePress = useCallback(async () => {
    //     const supported = await Linking.canOpenURL(url);

    //     if (supported) {
    //       await Linking.openURL(url);
    //     } else {
    //       Alert.alert(`Don't know how to open this URL: ${url}`);
    //     }
    //   }, [url]);

    //   return <Button title={children} onPress={handlePress} />;
    // };
  }

  viewPostNews(data) {
    return (
      <>
        {data.map((item) => (
          <Card key={item.link}>
            <CardItem cardBody>
              <Image
                source={uri}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Text>{item.source} </Text>
              </Left>
              <Right>
                <Text>{item.created_at}</Text>
              </Right>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{item.title}</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{Truncate(item.desc, 100)}</Text>
              </Body>
            </CardItem>
          </Card>
        ))}
      </>
    );
  }

  viewNews(data) {
    return (
      <>
        {data.map((item) => (
          <Card key={item.link}>
            <CardItem cardBody>
              <Image
                // source={uri}
                source={item.images[0] != null ? { uri: item.images[0] } : { uri }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Text>{item.source} </Text>
              </Left>
              <Right>
                <Text>{TimeAgoMoment(item.created_at, "DD MMMM YYYY")}</Text>
              </Right>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{item.title}</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{Truncate(item.desc, 100)}</Text>
              </Body>
            </CardItem>
          </Card>
        ))}
      </>
    );
  }

  viewPostPlatform(data) {
    return (
      <>
        {data.map((r) => (
          <Card key={r.key}>
            <CardItem>
              <Left>
                <Thumbnail source={uri} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>11 hours</Text>
                </Body>
              </Left>
              <Right>
                <Icon name="facebook" type="FontAwesome5" />
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Image
                source={uri}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  NativeBase is a free and open source framework that enable
                  developers to build high-quality mobile apps using React
                  Native iOS and Android apps with a fusion of ES6.
                </Text>
              </Body>
            </CardItem>

            <CardItem>
              <Badge red>
                <Text>negative</Text>
              </Badge>
              {/* <Button small ><Text>neutral</Text></Button> */}
            </CardItem>
          </Card>
        ))}
      </>
    );
  }

  noResult() {
    return (
      <>
        {/* <View>
                    <Text> No Result</Text>
                </View> */}
      </>
    );
  }

  render() {
    const { data, analyze } = this.props;
    if (data != null) {
      switch (analyze) {
        case "news":
          return this.viewNews(data);
          break;
        case "audience":
          return this.viewPostPlatform(data);
          break;
        case "home-issue":
          return this.viewPostPlatform(data);
          break;
        case "activity-person":
          return this.viewNews(data);
          break;
        case "home-news":
          return this.viewNews(data);
          break;
        case "universe-sosmed":
          return this.viewPostPlatform(data);
          break;
        default:
          return this.viewNews(data);
          break;
      }
    } else {
      return this.noResult();
    }
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  container: {
    shadowRadius: 16,
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default ListThumbnail;
