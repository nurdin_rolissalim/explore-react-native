import React, { Component } from "react";
import { StyleSheet, FlatList, Image } from "react-native";
import { Container, Header, View, DeckSwiper, Card, CardItem, Thumbnail, Text, Left, Body, Icon, Button } from 'native-base';
import {
    Truncate,
    CapitalizeFirstLetter,
    getImageEntity,
    checkImageEntity
} from "app/helpers/GlobalFunction";
import ListCardItem from 'app/shared/ui/content/ListCardItem';
const img =
    "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

export default class DeckSwiperView extends Component {
    constructor(props) {
        super(props);
    }

    viewCardItem(data) {
        let { analyze, type, platform } = this.props;
        return (

            <ListCardItem
                analyze={analyze}
                type={type}
                data={data}
            />
        )
    }

    viewDeckerSwiper() {
        let { analyze, data, type, platform } = this.props;
        const cards = [
            {
                text: 'Card One',
                name: 'One',

            },
        ];

        return (
            <Container style={{ height: 500 }}>
                <View style={styles.bodyText}>
                    <DeckSwiper
                        // ref={(c) => this._deckSwiper = c}
                        dataSource={data}
                        renderEmpty={() =>
                            <View style={{ alignSelf: "center" }}>
                                <Text>Over</Text>
                            </View>
                        }
                        renderItem={(items) => this.viewCardItem(data)}
                    />
                </View>
                {/* <View style={{ flexDirection: "row", flex: 1, position: "absolute", bottom: 50, left: 0, right: 0, justifyContent: 'space-between', padding: 15 }}>
                <Button iconLeft onPress={() => this._deckSwiper._root.swipeLeft()}>
                    <Icon name="arrow-back" />
                    <Text>Swipe Left</Text>
                </Button>
                <Button iconRight onPress={() => this._deckSwiper._root.swipeRight()}>
                    <Text>Swipe Right</Text>
                    <Icon name="arrow-forward" />
                </Button>
            </View> */}
            </Container>

        )
    }


    componentDidMount() {

    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    render() {
        const { analyze, type, platform, data } = this.props
        const cards = [
            {
                decription: 'Card One',
                first_name: 'One',
                caption: 'One',
                location: 'One',
                issue_photo: uri
            },
            {
                decription: 'Card Two',
                first_name: 'Two',
                caption: 'Two',
                location: 'Two',
                issue_photo: uri
            },
            {
                decription: 'Card Three',
                first_name: 'Three',
                caption: "Three",
                location: "Three",
                issue_photo: uri
            },
            {
                decription: 'Card Four',
                first_name: 'Four',
                caption: 'Four',
                location: 'Four',
                issue_photo: ""
            },
        ];
        switch (analyze) {
            case "home-issue":
                return this.viewDeckerSwiper();
                break;
            default:
                return this.viewDeckerSwiper();
                break;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 5,
    },
    title: {
        padding: 10,
        fontSize: 16,
        fontWeight: "bold",
    },
    right: {
        position: "absolute",
        right: 0,
    },
    containerWrapper: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    bodyText: {
        paddingHorizontal: 5
    }
});


