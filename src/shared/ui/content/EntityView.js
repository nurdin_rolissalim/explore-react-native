import React, { Component } from "react";
import { StyleSheet, Image } from "react-native";
import {
    Thumbnail,
    Text,
    List,
    ListItem,
    Body,
    Left,
    View,
    ScrollView
} from "native-base";
import { Truncate, CapitalizeFirstLetter, getImageEntity } from "app/helpers/GlobalFunction";
import { FlatList } from 'react-native-gesture-handler';

import ThumbnailView from "app/shared/ui/thumbnail/ThumbnailView";

const uri = require("assets/images/politica-icon.png");

export default class EntityView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            keyword: null,
            loading: true,
        };
    }

    selectedEntity = (entity) => {
        this.props.callback(entity)
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.props.data != nextProps.data) {
            return true;
        }

        return false;
    }
    UNSAFE_componentWillUnmount() {
        this.setState({ data: null });
    }
    onErrorImage() {
        console.log("on erorro");

    }

    /* digunakan di menu uinverse list entity */
    verticalLayout() {
        let { type, data, analyze } = this.props;
        if (data != null && data.length > 0) {
            return (
                <>

                    <List>
                        {data.map((item) => (
                            < ListItem
                                avatar
                                key={item.key}
                                onPress={() => this.selectedEntity(item)}
                            >
                                <Left>
                                    <ThumbnailView
                                        data={item.key}
                                        type={type}
                                        analyze={analyze}
                                    />
                                </Left>
                                <Body>
                                    <Text>{CapitalizeFirstLetter(item.key)}</Text>
                                    <Text note>{item.count}</Text>
                                </Body>
                            </ListItem>
                        ))}
                    </List>
                </>
            );
        }
    }

    /* digunakan di menu home,activity list entity */
    slideLayout() {
        let { data, type, analyze } = this.props
        return (
            <View style={{ height: 90, margin: 5 }}>
                <FlatList
                    style={[{ paddingTop: 5 }]}
                    keyExtractor={(_, index) => index.toString()}
                    data={data}
                    renderItem={({ item }) => (
                        <View
                            style={{ marginHorizontal: 8 }}
                            key={item.key}
                            onTouchEnd={() => this.selectedEntity(item)}
                        >
                            <ThumbnailView
                                data={item.key}
                                type={type}
                                analyze={analyze}
                            />
                            <Text style={{ textAlign: "center", fontSize: 16, marginTop: 3 }}>
                                {Truncate(item.key, 6, "...")}
                            </Text>
                        </View>
                    )}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        );
    }

    componentWillUnmount() {
        this.setState({ data: null });
    }

    render() {
        const { analyze, type, data } = this.props;

        switch (analyze) {
            case "home-person":
            case "activity-person":
            case "activity-news-influencer":
                return this.slideLayout();
            case "universe-news":
            case "universe-sosmed-profile":
            case "universe-sosmed-response":
                return this.verticalLayout();
                break;
            default:
                return (
                    <>
                    </>
                )
                break;
        }
    }
}

const styles = StyleSheet.create({
    boxSmall: {
        // width: 200,
        height: 90,
        marginBottom: 5,
        marginRight: 5,
        marginTop: 5,
        marginLeft: 5,
        // backgroundColor: 'rgba(0, 0, 0, 0.02)',
        // padding:1,
    },
    myButton: {
        padding: 10,
        marginLeft: 5,
        height: 75,
        width: 75, // The Width must be the same as the height
        borderRadius: 400, // Then Make the Border Radius twice the size of width or Height
        backgroundColor: "rgb(195, 125, 198)",
    },
});
