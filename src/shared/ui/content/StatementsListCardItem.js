import React, { Component } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import {
  Card,
  CardItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Toast
} from "native-base";
import * as IssueService from "app/service/IssueService";
const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class StatementsListCardItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      loading: true
    };
  }

  getDummy() {
    let data = [
      {
        "influencer": "Joko Widodo",
        "opinion": "punya bukti virus corona (Covid-19) berasal dari laboratorium Wuhan di China.",
        "sentiment": "neutral"
      },
      {
        "influencer": "Joko Widodo",
        "opinion": "\"Ya, saya punya.\"",
        "sentiment": "neutral"
      },
      {
        "influencer": "Joko Widodo",
        "opinion": "Saya tidak diizinkan untuk memberitahukannya kepada Anda,\"",
        "sentiment": "negative"
      },
    ]
    this.setState({ data: data })
  }

  getUniverseStatements() {
    let params = {
      cluster_id: "virus corona",
      person: "Joko Widodo",
      size: 5,
      page: 1,
      order: "DESC",
      "timezone": "Asia/Jakarta"
    }

    IssueService.issueStatements(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          this.setState({
            data: response.data,
          });
        } else {
          Toast.show({
            text: "Asdasd",
            position: "top",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "top",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  getData() {
    switch (this.props.analyze) {
      case "universe-news-statements":
        this.getUniverseStatements();
        break;
      default:
        this.getDummy();

        break;
    }
  }

  UNSAFE_componentWillUpdate() {
  }


  UNSAFE_componentWillMount() {

    this.getData();
  }

  componentDidMount() {

    // this.getData();
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if ((nextProps.analyze != this.props.analyze) || this.state.data != nextState.data) {
      return true;
    }

    return false;
  }

  componentWillUnmount() {
    this.setState({ data: null });
  }

  viewPost() {
    return (
      <>
        {this.state.data.map((item) => (
          <Card style={{ flex: 0 }} key={item.opinion}>
            <CardItem>
              <Left>
                <Thumbnail source={uri} />
                <Body>
                  <Text>{item.influencer}</Text>
                  <Text note>{item.sentiment}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{item.opinion}</Text>
              </Body>
            </CardItem>
            {/* <CardItem>
                            <Left>
                                <Button transparent textStyle={{ color: '#87838B' }}>
                                    <Icon name="logo-github" />
                                    <Text>1,926 stars</Text>
                                </Button>
                            </Left>
                        </CardItem> */}
          </Card>
        ))}
      </>
    );
  }

  noResult() {
    return (
      <>
        <View>
          <Text> No Result</Text>
        </View>
      </>
    )
  }

  render() {
    if (this.state.data != null) {
      switch (this.props.analyze) {
        case "universe-news-statements":
          return this.viewPost();
          break;
        case "home-issue":
          return this.viewPost();
          break;
        case "activity-person":
          return this.viewPost();
          break;
        case "home-news":
          return this.viewPost();
          break;
        default:
          return this.viewPost();
          break;
      }
    } else {
      return this.noResult();
    }

  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  container: {
    shadowRadius: 16,
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default StatementsListCardItem;
