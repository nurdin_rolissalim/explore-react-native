import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    Linking
} from 'react-native';
import Timeline from 'react-native-timeline-flatlist'
const uri = require("../../../assets/images/politica-icon.png");
import { Truncate } from "app/helpers/GlobalFunction";

export default class TimelineView extends Component {
    constructor() {
        super()
        this.onEventPress = this.onEventPress.bind(this)
        this.renderSelected = this.renderSelected.bind(this)
        this.renderDetail = this.renderDetail.bind(this)

        this.data = [
            {
                time: '2020-06-25 13:43:40',
                title: 'Archery Training',
                description: 'The Beginner Archery and Beginner Crossbow course does not require you to bring any equipment, since everything you need will be provided for the course. ',
                lineColor: '#009688',
                icon: uri,
                imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240340/c0f96b3a-0fe3-11e7-8964-fe66e4d9be7a.jpg'
            },
            {
                time: '10:45',
                title: 'Play Badminton',
                description: 'Badminton is a racquet sport played using racquets to hit a shuttlecock across a net.',
                icon: uri,
                imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240405/0ba41234-0fe4-11e7-919b-c3f88ced349c.jpg'
            },
            {
                time: '12:00',
                title: 'Lunch',
                icon: uri,
            },
            {
                time: '14:00',
                title: 'Watch Soccer',
                description: 'Team sport played between two teams of eleven players with a spherical ball. ',
                lineColor: '#009688',
                icon: uri,
                imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg'
            },
            {
                time: '16:30',
                title: 'Go to Fitness center',
                description: 'Look out for the Best Gym & Fitness Centers around me :)',
                icon: uri,
                imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240422/20d84f6c-0fe4-11e7-8f1d-9dbc594d0cfa.jpg'
            }
        ]
        this.state = { selected: null }
    }

    onEventPress(data) {
        this.setState({ selected: data })
    }

    renderSelected() {
        if (this.state.selected)
            return <Text style={{ marginTop: 10 }}>Selected event: {this.state.selected.title} at {this.state.selected.time}</Text>
    }

    renderDetail(rowData, sectionID, rowID) {
        let title = <Text style={[styles.title]}>{rowData.title}</Text>
        var desc = null

        if (rowData.desc && rowData.images && rowData.images[0])
            desc = (
                <View style={styles.descriptionContainer}>
                    <Image source={{ uri: rowData.images[0] }} style={styles.image} />
                    <Text style={[styles.textDescription]}>{Truncate(rowData.desc, 100)}</Text>
                </View>
            )

        return (
            <View style={{ flex: 1 }}>
                {title}
                {desc}
            </View>
        )
    }

    viewTimelineNews() {
        let { data } = this.props

        return (
            <View style={styles.container}>
                {this.renderSelected()}
                <Timeline
                    style={styles.list}
                    data={data}
                    circleSize={20}
                    circleColor='rgba(0,0,0,0)'
                    lineColor='rgb(45,156,219)'
                    timeContainerStyle={{ minWidth: 52, marginTop: 5 }}
                    timeStyle={{ textAlign: 'center', backgroundColor: '#ff9797', color: 'white', padding: 5, borderRadius: 13 }}
                    descriptionStyle={{ color: 'gray' }}
                    options={{
                        style: { paddingTop: 0 }
                    }}
                    innerCircle={'icon'}
                    // onEventPress={this.onEventPress}
                    onEventPress={() => Linking.openURL('https://reactnativecode.com')}
                    renderDetail={this.renderDetail}
                // columnFormat='two-column'
                />
            </View>
        )
    }

    render() {
        return (
            <ScrollView>
                {this.viewTimelineNews()}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        // paddingTop: 10,
        backgroundColor: 'white',
    },
    list: {
        flex: 1,
        marginTop: 20,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    descriptionContainer: {
        flexDirection: 'row',
        paddingRight: 50
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 25
    },
    textDescription: {
        marginLeft: 10,
        color: 'gray'
    }
});