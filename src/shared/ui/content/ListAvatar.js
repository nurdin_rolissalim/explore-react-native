import React, { Component } from "react";
import { StyleSheet } from "react-native";
import {
    Thumbnail,
    Text,
    Left,
    Body,
    Right,
    List,
    ListItem,
    View
} from "native-base";
import { Truncate } from "app/helpers/GlobalFunction";
import { ThemeProvider } from "@react-navigation/native";
import CustomLabel from 'app/shared/ui/label/CustomLabel';
import { _color_pallete } from "app/helpers/ColorPallete";

const img =
    "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class ListAvatar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    viewStatements(data) {
        return (
            <>
                <List>
                    {data.map((r) => (
                        <ListItem avatar key={r.key}>
                            <Left>
                                <Thumbnail source={uri} />
                            </Left>
                            <Body>
                                <Text>Kumar Pratik</Text>
                                <Text note>
                                    Doing what you like will always keep you happy . .
                                </Text>
                            </Body>
                            <Right>
                                <Text note>3:43 pm</Text>
                            </Right>
                        </ListItem>
                    ))}
                </List>
            </>
        );
    }

    viewNews(data) {
        return (
            <>
                <List>
                    {data.map((item) => (
                        <ListItem avatar key={item.key}>
                            <Left>
                                <Thumbnail source={{ uri: item.images }} />
                            </Left>
                            <Body>
                                <Text>{item.title}</Text>
                                <Text note>
                                    {item.desc}
                                </Text>
                            </Body>
                            <Right>
                                <Text note>{item.created_at}</Text>
                            </Right>
                        </ListItem>
                    ))}
                </List>
            </>
        );
    }

    viewLabel(sentiment, emotion, info, platform) {
        let labels = [
        ]
        let color = "#DBDBDB";
        if (sentiment && sentiment != undefined) {
            color = _color_pallete.sentiment;
            labels.push(
                { key: "sentiment", value: sentiment, color: color[sentiment] }
            )
        }
        if (info && info != undefined && info != "neutral") {
            color = "#877A7A";
            labels.push(
                { key: "info", value: info, color: color }
            )
        }
        if (emotion && emotion != "neutral" && emotion != undefined) {
            color = _color_pallete.emotion;
            labels.push(
                { key: "emotion", value: emotion, color: color[emotion] }
            )
        }
        if (platform && platform != undefined) {
            color = _color_pallete.platform;
            labels.push(
                { key: "platform", value: platform, color: color[platform] }
            )
        }

        if (labels.length > 0) {
            return (
                < CustomLabel
                    data={labels}
                    type={"post"}
                />
            )
        }
        return (
            <></>
        )


    }

    viewSosmed(data) {
        let label = [];
        return (
            <>
                <List>
                    {data.map((item) => (
                        <ListItem avatar key={item.link}>
                            <Left>
                                <Thumbnail source={{ uri: item.user_image }} />
                            </Left>
                            <Body>
                                <Text>{item.username} - {item.user_full_name} </Text>
                                <Text note>{item.created_at}</Text>
                                <Text note>
                                    {Truncate(item.ann_clean_text, 100)}
                                </Text>
                                {item.ann_sentiment || item.ann_emotions || item.ann_info_class ?
                                    this.viewLabel(item.ann_sentiment, item.ann_emotion, item.ann_info_class, item.platform)
                                    : null
                                }

                            </Body>
                            {/* <Right>
                                <Text note>{item.created_at}</Text>
                            </Right> */}
                        </ListItem>
                    ))}
                </List>
            </>
        );
    }

    componentDidUpdate() {
        this.viewData();
    }

    viewData(analyze, type, data) {
        if (data != null && data.length > 0) {
            switch (analyze) {
                case "universe-news-issue":
                    return this.viewNews(data)
                    break;
                case "statements":
                    return this.viewStatements(data)
                    break;
                default:
                    return this.viewSosmed(data)
                    break;
            }
        } else {
            return (
                <>
                </>
            )
        }


    }
    getNoResult() {
        return (
            <>
                <Text>No result</Text>
            </>
        )
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            prevProps.platform != this.props.platform ||
            this.props.keyword != prevProps.keyword
        ) {
            // const data = this.getData();
        }
    }



    componentWillUnmount() {
        this.setState({ data: null });
    }

    render() {
        const { data, type, analyze } = this.props

        if (data != null && data.length > 0) {
            return this.viewData(analyze, type, data)
        } else {
            return this.getNoResult()
        }
    }
}

const styles = StyleSheet.create({
    content: {
        padding: 20,
    },
    container: {
        shadowRadius: 16,
    },
    image: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});

export default ListAvatar;
