import React, { Component } from "react";
import { StyleSheet, View, FlatList ,Image} from "react-native";
import {
  Card,
  CardItem,
  Thumbnail,
  Text,
  Left,
  Body,
} from "native-base";
import { Truncate, CapitalizeFirstLetter, getImageEntity } from "app/helpers/GlobalFunction";
import ThumbnailView from "app/shared/ui/thumbnail/ThumbnailView";
const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

export default class ListCardItem extends Component {
  constructor(props) {
    super(props);    
  }

  viewPost(data) {
    return (
      <>
        {data.map((r) => (
          <Card style={{ flex: 0 }} key={r.key}>
            <CardItem>
              <Left>
                <Thumbnail source={uri} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>April 15, 2016</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Text>//Your text here</Text>
              </Body>
            </CardItem>
            {/* <CardItem>
                            <Left>
                                <Button transparent textStyle={{ color: '#87838B' }}>
                                    <Icon name="logo-github" />
                                    <Text>1,926 stars</Text>
                                </Button>
                            </Left>
                        </CardItem> */}
          </Card>
        ))}
      </>
    );
  }

  viewPostIssueVolunteer() {
    let {data}=this.props;
    return (
      <>
        {data.map((item,index) => (
          <Card key={index}>
          <CardItem>
              <Left>
                  <Thumbnail source={uri} />
                  <Body>
                      <Text>{item.first_name}</Text>
                      <Text note>{item.created_time}</Text>
                  </Body>
              </Left>
          </CardItem>
          <CardItem cardBody>
              <Image style={{ height: 200, flex: 1 }} source={uri} />
          </CardItem>
          <CardItem cardBody style={styles.bodyText}>
              <Text>{item.caption}</Text>
          </CardItem>
          <CardItem cardBody style={styles.bodyText}>
              {/* <Icon name="map-marker-alt" type="FontAwesome5" style={{ color: '#ED4A6A' }} /> */}
              <Text>{item.location}</Text>
          </CardItem >
          <CardItem cardBody style={styles.bodyText}>
              <Text>{item.description}</Text>
          </CardItem>
          <CardItem>
          </CardItem>
      </Card>
        ))}
      </>
    );
  }

  viewStatemens() {
    let {data,type,analyze}=this.props
    return (
      <>
        {data.map((item,index) => (
          <Card style={{ flex: 0 }} key={index}>
            <CardItem>
              <Left>                
                <ThumbnailView
                                data={item.key}
                                type={type}
                                analyze={analyze}
                            />
                <Body>
                  <Text>{item.influencer}</Text>
                  <Text note>{item.sentiment}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Text>{item.opinion}</Text>
              </Body>
            </CardItem>
            {/* <CardItem>
                            <Left>
                                <Button transparent textStyle={{ color: '#87838B' }}>
                                    <Icon name="logo-github" />
                                    <Text>1,926 stars</Text>
                                </Button>
                            </Left>
                        </CardItem> */}
          </Card>
        ))}
      </>
    );
  }

  render() {
    const{analyze, type, platform,data} =this.props

    switch (analyze) {
      case "home-issue":
        return this.viewPost(data);
        break;
      case "activity-person":
        return this.viewPost(data);
        break;
      case "home-news":
        return this.viewPost(data);
        break;
        case "statements":
        case "activity-news-statements":
        return this.viewStatemens();
        break;
      case "volunteer-issue-detail":
        return this.viewPostIssueVolunteer()
        break;  
      default:
        return this.viewPost(data);
        break;
    }
  }
}


const styles = StyleSheet.create({
  container: {
      flex: 1,
      padding: 5,
  },
  title: {
      padding: 10,
      fontSize: 16,
      fontWeight: "bold",
  },
  right: {
      position: "absolute",
      right: 0,
  },
  containerWrapper: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between",
  },
  bodyText: {
      paddingHorizontal: 5
  }
});