import React from "react";
import { Card, Icon } from "native-base";
import { View, Image, Text } from "react-native";

export default function PostItem(props) {
  return (
    <>
      <View style={{ flex: 1, flexDirection: "row", marginVertical: 10 }}>
        <View style={{ flex: 1, marginRight: 10 }}>
          <Image
            style={{ width: 48, height: 48 }}
            source={require("../../../assets/images/politica-icon.png")}
          />
        </View>
        <View style={{ flex: 6 }}>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text style={{ fontSize: 18, color: "#111" }}>Joko Widodo</Text>
            <Text style={{ fontSize: 16, color: "#525252" }}>1h ago</Text>
          </View>
          <View style={{ marginTop: 2 }}>
            <Text style={{ fontSize: 16, color: "#525252" }}>
              Kasus rasialisme guru terhadap murid Papua di Wamen,
              kronologisnya...
            </Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 5 }}>
            <View
              style={{ backgroundColor: "hsl(0, 0%, 87%)", marginRight: 3 }}
            >
              <Text
                style={{
                  fontSize: 16,
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  color: "hsl(0, 0%, 55%)",
                }}
              >
                Neutral
              </Text>
            </View>
            <View
              style={{ backgroundColor: "hsl(64, 100%, 75%)", marginRight: 3 }}
            >
              <Text
                style={{
                  fontSize: 16,
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  color: "hsl(64, 100%, 35%)",
                }}
              >
                Joy
              </Text>
            </View>
            <View
              style={{ backgroundColor: "hsl(191, 51%, 65%)", marginRight: 3 }}
            >
              <Text
                style={{
                  fontSize: 16,
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                  color: "hsl(191, 51%, 30%)",
                }}
              >
                Bias
              </Text>
            </View>
            <View style={{ backgroundColor: "#2196F3", marginRight: 3 }}>
              <Icon
                style={{ padding: 2, color: "#fff" }}
                type="MaterialCommunityIcons"
                name="twitter"
                fontSize={12}
              />
            </View>
          </View>
        </View>
      </View>
    </>
  );
}
