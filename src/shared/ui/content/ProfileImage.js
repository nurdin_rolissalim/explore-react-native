import React, { Component } from 'react'
import { Container, Thumbnail, Text, View } from 'native-base';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import {
    Truncate,
    CapitalizeFirstLetter,
    getImageEntity,
    checkImageEntity
} from "app/helpers/GlobalFunction";
import ThumbnailView from "app/shared/ui/thumbnail/ThumbnailView";
const img =
    "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class ProfileImage extends Component {
    constructor(props) {
        super(props);
        this.state = { lable: "News" }
    }

    render() {
        const { data, type, analyze } = this.props;
        return (
            <>
                <View style={styles.container}>
                    <Text style={styles.paragraph}>
                        {CapitalizeFirstLetter(data.name)}
                    </Text>
                    <ThumbnailView
                        data={data.name}
                        type={type}
                        analyze={analyze}
                    />
                    {/* <Thumbnail style={styles.image} large source={{ uri: getImageEntity(data.name, type) }} /> */}
                    {/* <Text style={styles.paragraph}>{data.value} News</Text> */}
                </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'center',
        // borderRightWidth: 1,
        // borderLeftWidth: 1
    },
    image: {
        width: 100,
        height: 100,
        alignSelf: 'center',
    },
    paragraph: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
});

export default ProfileImage;