import React, { useState, useEffect } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Container, Header, Content, Button, Text, Toast } from "native-base";
import { TouchableOpacity } from "react-native-gesture-handler";
import moment from "moment";
import * as IssueService from "app/service/IssueService";

class Issue extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      analyze: props.analyze,
    };
  }

  componentDidMount() {
    switch (this.state.analyze) {
      case "home-issue":
        this.getHomeIssue();
        break;

      case "activity-person":
        this.setState({
          data: [
            { key: "Kerja Sama 3 Provinsi" },
            { key: "Amin" },
            { key: "Anwar" },
            { key: "Dida" },
            { key: "James" },
            { key: "Joel" },
            { key: "John" },
            { key: "Jillian" },
            { key: "Jimmy" },
            { key: "Julie" },
          ],
        });
        break;
      default:
        break;
    }
  }

  getHomeIssue() {
    const params = {
      order: "DESC",
      size: 10,
      timezone: "Asia/Jakarta",
      start_date: moment(new Date()).format("YYYYMMDD").toString(),
      end_date: moment(new Date()).format("YYYYMMDD").toString(),
      interval: parseInt(moment(new Date()).format("HH")),
    };

    IssueService.issueTop(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          this.setState({
            data: response.data,
          });
        } else {
          Toast.show({
            text: "No data",
            position: "bottom",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "bottom",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={{ paddingTop: 5 }}
          data={this.state.data}
          renderItem={(items) => <IssueItem key={items.item.id} {...items} />}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }
}

function IssueItem({ item }) {
  return (
    <View style={[styles.issuItemContainer]}>
      <TouchableOpacity danger style={styles.issuItemButton}>
        <Text style={styles.issuItemLabel}>{item.label}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // width: 200,
    height: 80,
    margin: 5,
  },
  issuItemContainer: {
    padding: 5,
  },
  issuItemButton: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "rgba(250, 66, 59, 0.15)",
    borderColor: "#fa423b",
    borderRadius: 8,
  },
  issuItemLabel: {
    fontSize: 16,
    color: "#fa423b",
  },
});

export default Issue;
