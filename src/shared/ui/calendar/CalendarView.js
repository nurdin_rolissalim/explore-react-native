
import React, { Component } from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import {
    View,
    Text,

} from 'native-base'

import { Calendar } from 'react-native-calendars';
import moment from 'moment';
import _ from 'lodash';

const marker = {
    color: '#72A7D8',
    textColor: 'white',
    marked: true,
    // dotColor: '#72A7D8'
}

class CalendarView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        }

    }


    onDayPress = (e) => {
        console.log(e);

    }

    renderCalendarWithPeriodMarkingAndSpinner() {
        return (
            <View>
                {/* <Text style={styles.text}>Calendar with period marking and spinner</Text> */}
                <Calendar
                    // style={styles.calendar}
                    current={'2020-07-03'}
                    // minDate={'2020-07-10'}
                    // minDate={'2020-07-03'}
                    displayLoadingIndicator={false}
                    markingType={'period'}
                    // hideExtraDays={false}
                    onDayPress={this.onDayPress}
                    theme={{
                        // calendarBackground: '#333248',
                        // textSectionTitleColor: 'black',
                        // textSectionTitleDisabledColor: 'gray',
                        // dayTextColor: 'black',
                        // todayTextColor: 'white',
                        // selectedDayTextColor: 'white',
                        // monthTextColor: 'white',
                        // indicatorColor: 'white',
                        // selectedDayBackgroundColor: '#333248',
                        // arrowColor: 'white',
                        // textDisabledColor: 'red',
                        'stylesheet.calendar.header': {
                            week: {
                                marginTop: 5,
                                flexDirection: 'row',
                                justifyContent: 'space-between'
                            }
                        }
                    }}
                    markedDates={{
                        '2020-07-17': { disabled: true },
                        '2020-07-08': {},
                        '2020-07-09': {},
                        '2020-07-14': { startingDay: true, color: '#72A7D8', endingDay: true, textColor: '#fff' },
                        '2020-07-21': { startingDay: true, color: '#72A7D8', textColor: '#fff' },
                        '2020-07-22': { endingDay: true, color: '#72A7D8', textColor: '#fff' },
                        '2020-07-24': { startingDay: true, color: '#72A7D8', textColor: '#fff' },
                        '2020-07-25': { color: '#72A7D8', textColor: '#fff' },
                        '2020-07-26': { endingDay: true, color: '#72A7D8', textColor: '#fff' }
                    }}
                />
            </View>
        );
    };



    renderCalendarWithMultiPeriodMarking() {
        return (
            <>
                <Text style={styles.text}>Calendar with multi-period marking</Text>
                <Calendar
                    style={styles.calendar}
                    current={'2012-05-16'}
                    markingType={'multi-period'}
                    markedDates={{
                        '2012-05-16': {
                            periods: [
                                { startingDay: true, endingDay: false, color: 'green' },
                                { startingDay: true, endingDay: false, color: 'orange' }
                            ]
                        },
                        '2012-05-17': {
                            periods: [
                                { startingDay: false, endingDay: true, color: 'green' },
                                { startingDay: false, endingDay: true, color: 'orange' },
                                { startingDay: true, endingDay: false, color: 'pink' }
                            ]
                        },
                        '2012-05-18': {
                            periods: [
                                { startingDay: true, endingDay: true, color: 'orange' },
                                { color: 'transparent' },
                                { startingDay: false, endingDay: false, color: 'pink' }
                            ]
                        }
                    }}
                />
            </>
        );
    };

    renderCalendarWithMultiDotMarking() {
        return (
            <>
                <Text style={styles.text}>Calendar with multi-dot marking</Text>
                <Calendar
                    handleOnlyMarkedDays={true}
                    hideExtraDays={false}
                    onDayPress={this.onDayPress}
                    style={styles.calendar}
                    current={'2012-05-16'}
                    markingType={'multi-dot'}
                    markedDates={{
                        '2012-05-08': {
                            selected: true,
                            dots: [
                                { key: 'vacation', color: 'blue', selectedDotColor: 'red' },
                                { key: 'massage', color: 'red', selectedDotColor: 'white' }
                            ]
                        },
                        '2012-05-09': {
                            disabled: true,
                            dots: [
                                { key: 'vacation', color: 'green', selectedDotColor: 'red' },
                                { key: 'massage', color: 'red', selectedDotColor: 'green' }
                            ]
                        }
                    }}
                />
            </>
        );
    };

    getDisabledDates = (startDate, endDate, daysToDisable) => {
        const disabledDates = {};
        const start = moment(startDate);
        const end = moment(endDate);
        for (let m = moment(start); m.diff(end, 'days') <= 0; m.add(1, 'days')) {
            if (_.includes(daysToDisable, m.weekday())) {
                disabledDates[m.format('YYYY-MM-DD')] = { disabled: true };
            }
        }
        return disabledDates;
    };

    renderCalendarWithPeriodMarkingAndDotMarking() {
        return (
            <>
                {/* <Text style={styles.text}>Calendar with period marking and dot marking</Text> */}
                <Calendar
                    current={'2012-05-16'}
                    minDate={'2012-05-01'}
                    disabledDaysIndexes={[0, 6]}
                    markingType={'period'}
                    hideExtraDays={false}
                    onDayPress={this.onDayPress}
                    markedDates={{
                        '2012-05-15': { ...marker, startingDay: true },
                        '2012-05-16': { marked: true, dotColor: '#50cebb' },
                        '2012-05-15': { startingDay: true, color: '#72A7D8', textColor: 'white', dotColor: '#white' },
                        '2012-05-22': {
                            color: '#70d7c7',
                            customTextStyle: {
                                color: '#FFFAAA',
                                fontWeight: '700'
                            }
                        },
                        '2012-05-23': { color: '#72A7D8', textColor: 'white', marked: true, dotColor: 'white' },
                        '2012-05-24': { color: '#72A7D8', textColor: 'white' },
                        '2012-05-25': {
                            endingDay: true, color: '#50cebb', textColor: 'white',
                            customContainerStyle: {
                                borderTopRightRadius: 5,
                                borderBottomRightRadius: 5
                            }
                        },
                        // ...this.getDisabledDates('2012-05-01', '2012-05-30', [0, 6])
                    }}
                />
            </>
        );
    };

    render() {
        // const { analyze, type, data } = this.props;

        return this.renderCalendarWithPeriodMarkingAndSpinner()
    }
}


export default CalendarView

const styles = StyleSheet.create({
    container: {
        marginTop: 10
    },
    calendar: {
        marginBottom: 10
    },
    text: {
        textAlign: 'center',
        padding: 10,
        backgroundColor: 'lightgrey',
        fontSize: 16
    },

});