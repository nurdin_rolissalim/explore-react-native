import React, { Component } from "react";
import { StyleSheet, FlatList, Image } from "react-native";
import {
  Container,
  Header,
  Content,
  Thumbnail,
  Text,
  List,
  ListItem,
  Body,
  Left,
  Right,
  View,
} from "native-base";
import { Truncate } from "app/helpers/GlobalFunction";
import * as ChartService from "app/service/ChartService";
import moment from "moment";

const uri = require("../../assets/images/politica-icon.png");

class Person extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          key: "Amin",
          count: 1113,
          image: require("../../assets/images/politica-icon.png"),
        },
        {
          key: "Anwar",
          count: 90,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "Dida",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "James",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "Arta",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "Imam",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "Dewi",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "Mey",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "Yayuk",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "Indra",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
        {
          key: "Nurdin Rolissalim",
          count: 1203,
          image: "../../assets/images/politica-icon.png",
        },
      ],
      analyze: props.analyze,
      navigate: props.navigate,
      type: props.type,
    };
  }

  // componentWillReceiveProps(){

  // }

  componentDidMount() {
    switch (this.state.analyze) {
      case "home-person":
        return this.getHomePerson();
      default:
    }
  }

  getHomePerson() {
    const params = {
      aggregation_by: "PERSON",
      size: 5,
      start_date: moment(new Date()).format("YYYYMMDD"),
      end_date: moment(new Date()).format("YYYYMMDD"),
      order: "DESC",
      index: "news",
      timezone: "Asia/Jakarta",
    };

    ChartService.trending(params)
      .then((response) => {
        if (response.status === true && response.total > 0) {
          this.setState({
            data: response.data,
          });
        } else {
          Toast.show({
            text: "No data",
            position: "bottom",
            buttonText: "Okay",
            duration: 2000,
          });
        }
      })
      .catch((error) => {
        Toast.show({
          text: "Check yout connection network",
          position: "bottom",
          buttonText: "Okay",
          duration: 2000,
        });
      });
  }

  verticalLayout() {
    if (this.state.data.length > 0) {
      return (
        <>
          <List>
            {this.state.data.map((item) => (
              <ListItem
                avatar
                key={item.key}
                onPress={() =>
                  this.props.navigation.navigate(this.state.navigate, {
                    analyze: this.analyze,
                    name: item.key,
                  })
                }
              >
                <Left>
                  <Thumbnail source={uri} />
                </Left>
                <Body>
                  <Text>{item.key}</Text>
                  <Text note>{item.count}</Text>
                </Body>

                {/* <Right>
              <Text note>3:43 pm</Text>
            </Right> */}
              </ListItem>
            ))}
          </List>
        </>
      );
    }
    return <Text>No Result</Text>;
  }

  slideLayout() {
    return (
      <View style={styles.boxSmall}>
        <FlatList
          style={[{ paddingTop: 5 }]}
          keyExtractor={(_, index) => index.toString()}
          data={this.state.data}
          renderItem={({ item }) => (
            <View style={{ marginHorizontal: 8 }} key={item.key}>
              <Thumbnail circle source={uri} />
              <Text style={{ textAlign: "center", fontSize: 16, marginTop: 3 }}>
                {Truncate(item.key, 6, "...")}
              </Text>
            </View>
          )}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }

  render() {

    switch (this.state.analyze) {
      case "home-person":
        return this.slideLayout();
        break;
      case "activity-person":
        return this.slideLayout();
        break;
      case "universe-news":
        return this.verticalLayout();
        break;
      case "universe":
        return this.verticalLayout();
        break;
    }
  }
}

const styles = StyleSheet.create({
  boxSmall: {
    // width: 200,
    height: 90,
    marginBottom: 5,
    marginRight: 5,
    marginTop: 5,
    marginLeft: 5,
    // backgroundColor: 'rgba(0, 0, 0, 0.02)',
    // padding:1,
  },
  myButton: {
    padding: 10,
    marginLeft: 5,
    height: 75,
    width: 75, // The Width must be the same as the height
    borderRadius: 400, // Then Make the Border Radius twice the size of width or Height
    backgroundColor: "rgb(195, 125, 198)",
  },
});

export default Person;
