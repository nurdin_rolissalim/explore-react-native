/**
 * ============= HIGHCHARTS ==================
 *
 * */

export function getHighchartTheme(theme_mode: string) {
  const is_dark = theme_mode == "dark";
  const labels_color = is_dark ? "#fff" : "#636363";
  const legend_color_hover = is_dark ? "#ffffff" : "#989797";
  const labels_color_axis = is_dark ? "#DEDEDE" : "#636363";
  const legend_color = is_dark ? "#c9c9ca" : "#636363";
  const grid_color = is_dark ? "#222" : "#f3f3f3";
  const tooltip_background = is_dark ? "rgba(0, 0, 0, 0.85)" : "#fbfbfb";
  const tooltip_color = is_dark ? "white" : "#636363";

  const legend_border_color = is_dark ? "#313131" : "#f3f3f3";
  const legend_navigation_active = is_dark ? "#cccccc" : "#f3f3f3";
  const legend_navigation_inactive = is_dark ? "#737373" : "#f3f3f3";
  const data_label_color = is_dark ? "#c9c9ca" : "#262626";

  const labels_border_color = is_dark ? "#1a1a20" : "#f9f9f9";
  const crosshair_color = is_dark ? "#1a1a20" : "#f9f9f9";

  const _chart_theme = {
    colors: [
      "#d35400",
      "#FF9F00",
      "#DF3016",
      "#EF0055",
      "#A700AF",
      "#7700B8",
      "#5500B7",
      "#2c3e50",
      "#009C8A",
      "#05d2f3",
      "#327770",
      "#0c4744",
      "#00a1a1",
      "#a10000",
      "#8c525e",
      "#474700",
      "#470047",
      "#f6546a",
      "#008080",
      "#008000",
      "#65eee1",
      "#003366",
      "#c5dd62",
      "#ff00e7",
      "#102658",
      "#E0033C",
      "#373737",
      "#333333",
      "#1F1A17",
      "#0160C9",
      "#1CA3DE",
      "#FF9F00",
      "#DF3016",
      "#EF0055",
      "#1B307D",
      "#C40233",
      "#936B4C",
      "#55777B",
      "#615D5C",
      "#2c3e50",
      "#3ACBE8",
      "#FF9F00",
      "#DF3016",
      "#EF0055",
      "#2E62B6",
      "#FE0343",
      "#112D50",
      "#A1836E",
      "#58A8C4",
      "#A9A9A9",
    ],
    chart: {
      backgroundColor: "transparent",
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      style: {
        fontFamily: "-apple-system, 'Roboto', sans-serif",
      },
    },
    title: {
      style: {
        color: "#525252",
        textTransform: "uppercase",
        fontSize: "12px",
      },
    },
    subtitle: {
      style: {
        color: grid_color,
        textTransform: "none",
      },
    },
    xAxis: {
      gridLineColor: grid_color,
      labels: {
        style: {
          color: labels_color_axis,
        },
      },
      lineColor: grid_color,
      minorGridLineColor: "pink",
      tickColor: grid_color,
      title: {
        style: {
          color: labels_color_axis,
        },
      },
    },
    yAxis: {
      gridLineColor: grid_color,
      labels: {
        style: {
          color: labels_color_axis,
        },
      },
      lineColor: "blue",
      minorGridLineColor: "yellow",
      tickColor: grid_color,
      tickWidth: 1,
      title: {
        style: {
          color: labels_color_axis,
        },
      },
      min: 0,
    },
    tooltip: {
      backgroundColor: tooltip_background,
      style: {
        color: tooltip_color,
      },
    },
    plotOptions: {
      areaspline: {
        dataLabels: {
          enabled: true,
        },
        fillOpacity: 0.5,
        cursor: "pointer",
        marker: {
          enabled: true,
          symbol: "circle",
        },
        events: {
          click: (item) => {},
        },
        fillColor: {
          linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1, x3: 0, y3: 1 },
          stops: [
            [0, "#ff0000"],
            [1, "#0000ff"],
            [2, "#00ffff"],
          ],
        },
      },
      pie: {
        borderWidth: 0,
        dataLabels: {
          color: "#fff",
        },
      },
      map: {
        dataLabels: {
          enabled: true,
          color: data_label_color,
          style: {
            fontWeight: "bold",
          },
        },
      },
      series: {
        borderWidth: 0,
        dataLabels: {
          color: labels_color,
          style: {
            textShadow: true,
            textOutline: "1",
          },
        },
        marker: {
          lineColor: "#333",
        },
        states: {
          hover: {
            lineWidth: 3,
          },
        },
      },
      boxplot: {
        fillColor: "#505053",
      },
      candlestick: {
        lineColor: labels_color_axis,
      },
      errorbar: {
        color: labels_color_axis,
      },
    },
    legend: {
      itemStyle: {
        color: legend_color,
      },
      itemHoverStyle: {
        color: legend_color_hover,
      },
      itemHiddenStyle: {
        color: "#606063",
      },
      useHTML: true,
      enabled: true,
      borderColor: legend_border_color,
      navigation: {
        activeColor: legend_navigation_active,
        inactiveColor: legend_navigation_inactive,
        style: {
          color: "#4a4a4a",
          fontSize: "12px",
        },
      },
    },
    credits: {
      enabled: false,
    },
    labels: {
      style: {
        color: "#707073",
      },
    },

    drilldown: {
      // for axis label
      activeAxisLabelStyle: {
        textDecoration: "none",
        color: "#E0E0E3",
        outlineColor: "#000",
      },
      // for datalabel
      activeDataLabelStyle: {
        textDecoration: "none",
        color: "#E0E0E3",
        outlineColor: "#000",
      },
    },

    navigation: {
      buttonOptions: {
        symbolStroke: "#DDDDDD",
        theme: {
          fill: "#505053",
        },
      },
    },

    navigator: {
      handles: {
        backgroundColor: "#262626",
        borderColor: "#5C5C5C",
      },
      outlineColor: "#383838",
      maskFill: "rgba(255,255,255,0.1)",
      series: {
        color: labels_color_axis,
        lineColor: "none",
      },
      xAxis: {
        gridLineColor: "#383838",
      },
    },

    scrollbar: {
      barBackgroundColor: "#808083",
      barBorderColor: "#808083",
      buttonArrowColor: "#CCC",
      buttonBackgroundColor: "#606063",
      buttonBorderColor: "#606063",
      rifleColor: "#FFF",
      trackBackgroundColor: "#404043",
      trackBorderColor: "#404043",
    },

    // special colors for some of the
    legendBackgroundColor: "rgba(0, 0, 0, 0.5)",
    background2: "#505053",
    dataLabelsColor: "#B0B0B3",
    textColor: "blue",
    contrastTextColor: "#F0F0F3",
    maskColor: "rgba(255,255,255,0.3)",
  };

  return _chart_theme;
}
