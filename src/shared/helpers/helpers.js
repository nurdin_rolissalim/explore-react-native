export function getAsset(path) {
  if (typeof path !== "string") return "";

  const remoteAssetUrl = "https://politica.blackeye.id/assets";
  const pathArray = path.split("/");
  const { length } = pathArray;
  const filename = pathArray[length - 1];
  const dirname = pathArray[length - 2];
  let typeOfAsset = "";

  if (dirname == "photo-user") {
    typeOfAsset = "user-profile-image";
  } else {
    // other type of assets
    typeOfAsset = "user-profile-image";
  }
  return `${remoteAssetUrl}/${typeOfAsset}/${filename}`;
}

export function objectParamToStringParam(obj) {
  const params = [];
  Object.keys(obj).forEach((key) => params.push(`${key}=${obj[key]}`));
  return params.join("&");
}
