/**
 *  Service Params Generator
 *  ==============
 *  Ex. Params Object = {"start":"20190101","end":"20190131","mode":"adv"}
 *  Ex. Params Url = ....?workspace_id=1&sort=desc&skip=2&limit=2
 */
export function serviceParamsFilter(params, params_rule, output = "object") {
  if (output === "object") {
    const _params = {};

    Object.keys(params_rule).forEach((key) => {
      if (params_rule[key].hasOwnProperty("alias")) {
        const { alias } = params_rule[key];
        if (params.hasOwnProperty(alias)) {
          _params[key] = params[alias];
        }
      } else if (params.hasOwnProperty(key)) {
        _params[key] = params[key];
      } else {
        _params[key] = params_rule[key].default;
      }
    });

    return _params;
  }
  if (output === "string") {
    const _params = [];

    Object.keys(params_rule).forEach((key) => {
      if (params_rule[key].hasOwnProperty("alias")) {
        const { alias } = params_rule[key];
        if (params.hasOwnProperty(alias)) {
          _params.push(`${key}=${params[alias]}`);
        }
      } else if (params.hasOwnProperty(key)) {
        _params.push(`${key}=${params[key]}`);
      } else {
        _params.push(`${key}=${params_rule[key].default}`);
      }
    });

    return _params.join("&");
  }
}

/**
 *  Reorder array index
 *  Ex:
 *    [1,2,3,4,5] => moveItem(0, 2)
 *  @returns {Array}  [2,3,1,4,5]
 */
export function reOrderArrayIndex(data, { from, to }) {
  // remove `from` item and store it
  const f = data.splice(from, 1)[0];
  // insert stored item into position `to`
  data.splice(to, 0, f);

  return data;
}

/**
 * Search object array by key value
 */
export function search(array, { key, value }) {
  const result = array.find((x) => x[key] === value);

  return result;
}
