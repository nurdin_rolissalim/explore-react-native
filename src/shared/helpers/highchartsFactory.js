import { _color_pallete } from "app/shared/configs/colorPallete";
import * as moment from "moment";
import { convertionDate, dateYearAndMonth } from "./date";

export function dataChartResultObjectToSeries(data) {}

/**
 *
 * @param data
 * @param name
 * @param pallete (default | key pallete | #hexColorCode|series | default) * default used for series data
 */
export function objectToSeriesAndCategories(
  data,
  name = "data",
  pallete = "default"
) {
  const chartData = { categories: [], series: [] };
  const palleteArr = pallete.split("|");
  const isPalleteForSeries: boolean = !!(
    palleteArr.length > 1 && palleteArr[1] == "series"
  );

  const cPallete = _color_pallete.hasOwnProperty(pallete)
    ? _color_pallete[pallete]
    : _color_pallete.default;

  Object.entries(data).forEach(([key, value], index) => {
    const cat = key
      .toLowerCase()
      .split(" ")
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ");

    if (key != "undefined" && key != "null" && key != "") {
      const seriesData = {
        y: data[key],
        color: pallete == "default" ? cPallete[index] : cPallete[key],
      };
      chartData.categories.push(cat);
      chartData.series.push(seriesData);
    }
  });
  Object.keys(data).forEach((key) => {});

  chartData.series = [
    {
      name,
      data: chartData.series,
      color: isPalleteForSeries ? palleteArr[0] : undefined,
    },
  ];

  return chartData;
}

/* digunakan untuk education chart */
export function objectToSeriesAndCategoriesStacked(
  data,
  name = "data",
  pallete = "default"
) {
  const chartData = { categories: [], series: [] };
  const palleteArr = pallete.split("|");
  const isPalleteForSeries: boolean = !!(
    palleteArr.length > 1 && palleteArr[1] == "series"
  );

  const cPallete = _color_pallete.hasOwnProperty(pallete)
    ? _color_pallete[pallete]
    : _color_pallete.default;
  Object.entries(data).forEach(([key, value], index) => {
    const cat = key
      .toLowerCase()
      .split(" ")
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ");

    if (key != "undefined" && key != "null" && key != "") {
      const seriesData = {
        name: key,
        data: [+data[key]],
        color: pallete == "default" ? cPallete[index] : cPallete[key],
      };
      chartData.categories.push(cat);
      chartData.series.push(seriesData);
    }
  });
  Object.keys(data).forEach((key) => {});
  return chartData;
}

/* digunakan untuk  chart  pie */
export function objectToSeriesPie(data, name = "data", pallete = "default") {
  const chartData = { series: [] };
  const palleteArr = pallete.split("|");
  const isPalleteForSeries: boolean = !!(
    palleteArr.length > 1 && palleteArr[1] == "series"
  );

  const cPallete = _color_pallete.hasOwnProperty(pallete)
    ? _color_pallete[pallete]
    : _color_pallete.default;
  Object.entries(data).forEach(([key, value], index) => {
    const cat = key
      .toLowerCase()
      .split(" ")
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ");

    if (key != "undefined" && key != "null" && key != "") {
      const seriesData = {
        name: key,
        y: +data[key],
        color: pallete == "default" ? cPallete[index] : cPallete[key],
      };
      chartData.series.push(seriesData);
    }
  });
  Object.keys(data).forEach((key) => {});
  return chartData;
}

export function getHighchartLegendConfig({ enabled, options }) {
  /* vertical right middle 250 */
  const opt = options.split(" ");
  const layout = opt.length > 0 ? opt[0] : "horizontal";
  const align = opt.length > 1 ? opt[1] : "left";
  const valign = opt.length > 2 ? opt[2] : "bottom";
  const width = opt.length > 3 ? opt[3] : 0;

  const legend = {
    enabled,
    layout,
    align,
    verticalAlign: valign,
    x: 0,
    y: 0,
    left: 0,
    right: 0,
  };

  if (width > 0) {
    legend.width = width;
  }

  return legend;
}

export function getHighchartsTimeFormat() {
  return {
    type: "datetime",
    align: "center",
    dateTimeLabelFormats: {
      second: "%H:%M:%S",
      minute: "%H:%M",
      hour: "%H:%M",
      day: "%e/%b",
      week: "%e/%b",
      month: "%b '%y",
      year: "%Y",
    },
  };
}

export function calculateSizeWord(
  dataArray,
  min = 25,
  max = 140,
  dataSource = ""
) {
  const data = [];
  let item;
  const colorSourceObj = {
    "online-news": "#888888",
    twitter: "#00aced",
    "facebook-post": "#3b5998",
    "instagram-post": "#cd2898",
    "youtube-post": "#f00",
  };

  // find min max data
  const arrTotal = [];
  Object.keys(dataArray).forEach((element) => {
    arrTotal.push(dataArray[element][1]);
  });

  // calculate
  Object.keys(dataArray).forEach((element) => {
    item = {
      color: dataSource ? colorSourceObj[dataSource] : "",
      name: dataArray[element][0],
      value: dataArray[element][1],
      weight: minMaxNorm(dataArray[element][1], arrTotal, min, max),
      percentage: percent(dataArray[element][1], sumInArray(arrTotal)),
    };

    data.push(item);
  });

  return data;
}

export function minMaxNorm(dataValue, totalAllData, min = 0, max = 0) {
  const minData = Math.min.apply(null, totalAllData);
  const maxData = Math.max.apply(null, totalAllData);

  const minFont = min;
  const maxFont = max;

  const result =
    ((dataValue - minData) / (maxData - minData)) * (maxFont - minFont) +
    minFont;

  return result;
}

export function percent(args, total, fixed = 2) {
  const result = (args / total) * 100;

  return result.toFixed(fixed);
}

export function sumInArray(arrayData = []) {
  const sum = arrayData.reduce(sumArray, 0);
  return sum;
}

export function sumArray(a, b) {
  return a + b;
}

/* Array obeject to object  */
export function parseArryObjectSingleSeries(series, keyField, keyValue) {
  // series.reduce((obj, item) => {
  //   obj[item[keyField]] = item[keyValue]
  //   return obj
  // })
  const arrayToObject = (series, keyField, keyValue) =>
    series.reduce((obj, item) => {
      obj[item[keyField]] = item[keyValue];
      return obj;
    }, {});
  const data = arrayToObject(series, keyField, keyValue);
  return data;
}

/* parsing for area chart */
export function parseAreaChartSeriesSentiment(
  series,
  titleChart = "",
  state = "",
  colorChart = ""
) {
  const start = moment(state.start_date, "YYYYMMDD");
  const end = moment(state.end_date, "YYYYMMDD");
  const diffDays = moment.duration(end.diff(start)).asDays();
  const tmpCatagories = [];
  const value = [];
  const negative = [];
  const neutral = [];
  const positive = [];
  let interval;

  if (diffDays > 28) {
    // M
    interval = "dm-short";
  } else if (diffDays > 5) {
    // W
    interval = "day";
  } else if (diffDays === 0) {
    // T
    interval = "hour";
  } else {
    // T
    interval = "hour";
  }

  Object.keys(series).map((key) => {
    let convCat = new Date(
      moment(series[key].date, "YYYYMMDDHH").toDate()
    ).getTime();
    convCat = convertionDate(convCat, interval);
    tmpCatagories.push(convCat);
    negative.push([series[key].date, series[key].negative]);
    neutral.push([series[key].date, series[key].neutral]);
    positive.push([series[key].date, series[key].positive]);
  });
  const dataChart = {
    categories: tmpCatagories,
    series: [
      {
        name: "Negative",
        data: negative,
        color: _color_pallete.sentiment.negative,
      },
      {
        name: "Neutral",
        data: neutral,
        color: _color_pallete.sentiment.neutral,
      },
      {
        name: "Positive",
        data: positive,
        color: _color_pallete.sentiment.positive,
      },
    ],
  };
  return dataChart;
}

/* parsing for area chart */
export function parseAreaChartSeriesReaction(
  series,
  titleChart = "",
  state = "",
  colorChart = ""
) {
  const start = moment(state.start_date, "YYYYMMDD");
  const end = moment(state.end_date, "YYYYMMDD");
  const diffDays = moment.duration(end.diff(start)).asDays();
  const tmpCatagories = [];
  const comment = [];
  const like = [];
  const share = [];
  let interval;

  if (diffDays > 28) {
    // M
    interval = "dm-short";
  } else if (diffDays > 5) {
    // W
    interval = "day";
  } else if (diffDays === 0) {
    // T
    interval = "hour";
  } else {
    // T
    interval = "hour";
  }

  Object.keys(series).map((key) => {
    let convCat = new Date(
      moment(series[key].key, "YYYYMMDDHH").toDate()
    ).getTime();
    convCat = convertionDate(convCat, interval);
    tmpCatagories.push(convCat);
    like.push(series[key].likes_count);
    comment.push(series[key].comments_count);
    share.push(series[key].shares_count);
  });
  const dataChart = {
    categories: tmpCatagories,
    series: [
      {
        name: "Comments",
        data: comment,
        color: _color_pallete.activity.comments_count,
      },
      {
        name: "Shares",
        data: share,
        color: _color_pallete.activity.shares_count,
      },
      {
        name: "Likes",
        data: like,
        color: _color_pallete.activity.likes_count,
      },
    ],
  };
  return dataChart;
}

/* parsing for area chart */
export function parseAreaChartSeriesPost(
  series,
  titleChart = "",
  state = "",
  colorChart = ""
) {
  const start = moment(state.start_date, "YYYYMMDD");
  const end = moment(state.end_date, "YYYYMMDD");
  const diffDays = moment.duration(end.diff(start)).asDays();
  const tmpCatagories = [];
  const post = [];
  const comment = [];
  const video = [];
  const link = [];
  let interval;

  if (diffDays > 28) {
    // M
    interval = "dm-short";
  } else if (diffDays > 5) {
    // W
    interval = "day";
  } else if (diffDays === 0) {
    // T
    interval = "hour";
  } else {
    // T
    interval = "hour";
  }

  Object.keys(series).map((key) => {
    let convCat = new Date(
      moment(series[key].date, "YYYYMMDDHH").toDate()
    ).getTime();
    convCat = convertionDate(convCat, interval);
    tmpCatagories.push(convCat);
    post.push([series[key].date, series[key].post]);
    comment.push([series[key].date, series[key].comment]);
    link.push([series[key].date, series[key].link]);
    video.push([series[key].date, series[key].video]);
  });
  const dataChart = {
    categories: tmpCatagories,
    series: [
      { name: "Post", data: post, color: _color_pallete.post.post },
      { name: "Comment", data: comment, color: _color_pallete.post.comment },
      { name: "Video", data: video, color: _color_pallete.post.video },
      { name: "link", data: link, color: _color_pallete.post.link },
    ],
  };
  return dataChart;
}

/* parsing for area chart */
export function parseAreaChartSeries(
  series,
  titleChart = "",
  state = "",
  colorChart = ""
) {
  const start = moment(state.start_date, "YYYYMMDD");
  const end = moment(state.end_date, "YYYYMMDD");
  const diffDays = moment.duration(end.diff(start)).asDays();
  const tmpCatagories = [];
  const value = [];
  let interval;

  if (diffDays > 28) {
    // M
    interval = "dm-short";
  } else if (diffDays > 5) {
    // W
    interval = "day";
  } else if (diffDays === 0) {
    // T
    interval = "hour";
  } else {
    // T
    interval = "hour";
  }

  Object.keys(series).map((key) => {
    let convCat = new Date(
      moment(series[key].key, "YYYYMMDDHH").toDate()
    ).getTime();
    convCat = convertionDate(convCat, interval);
    tmpCatagories.push(convCat);
    value.push([series[key].key, series[key].value]);
  });

  const dataChart = {
    categories: tmpCatagories,
    series: [
      {
        name: titleChart,
        data: value,
        color: colorChart,
      },
    ],
  };
  return dataChart;
}

/* parsing for area stacked chart */
export function parseAreaChartSeriesActivity(
  series,
  titleChart = "",
  state = "",
  colorChart = ""
) {
  const start = moment(state.start_date, "YYYYMMDD");
  const end = moment(state.end_date, "YYYYMMDD");
  const diffDays = moment.duration(end.diff(start)).asDays();
  const tmpCatagories = [];
  const comment = [];
  const like = [];
  const share = [];
  let interval;

  if (diffDays > 28) {
    // M
    interval = "dm-short";
  } else if (diffDays > 5) {
    // W
    interval = "day";
  } else if (diffDays === 0) {
    // T
    interval = "hour";
  } else {
    // T
    interval = "hour";
  }

  Object.keys(series).map((key) => {
    let convCat = new Date(
      moment(series[key].key, "YYYYMMDDHH").toDate()
    ).getTime();
    convCat = convertionDate(convCat, interval);
    tmpCatagories.push(convCat);
    like.push(series[key].likes_count);
    comment.push(series[key].comments_count);
    share.push(series[key].shares_count);
  });
  const dataChart = {
    categories: tmpCatagories,
    series: [
      {
        name: "Comments",
        data: comment,
        color: _color_pallete.activity.comments_count,
      },
      {
        name: "Shares",
        data: share,
        color: _color_pallete.activity.shares_count,
      },
      {
        name: "Likes",
        data: like,
        color: _color_pallete.activity.likes_count,
      },
    ],
  };
  return dataChart;
}

/* parsing for area stacked chart dsicover */
export function parseAreaChartSeriesYearMonth(
  series,
  year,
  month = "",
  formater = "YYYYMMDD",
  format_match = "YYYYMM",
  titleChart = "",
  colorChart = ""
) {
  const daterange = dateYearAndMonth(year, month, formater, format_match);
  const categories = [];
  const value = [];
  for (let i = 0; i < daterange.max; i++) {
    const cat = moment(daterange.start_date)
      .add(i, daterange.interval)
      .format(daterange.format_category);
    const dt = moment(daterange.start_date)
      .add(i, daterange.interval)
      .format(daterange.format_match);
    categories.push(cat);
    value[i] = 0;
    if (series[dt]) {
      value[i] = series[dt];
    }
  }

  const dataChart = {
    categories,
    series: [
      {
        name: titleChart,
        data: value,
        color: colorChart,
      },
    ],
  };

  return dataChart;
}

export function parseHighmapsSeries(series, cities, selected = []) {
  const chartSeries = [];
  let city;
  cities.forEach((element) => {
    element.value = series[element.label] ? series[element.label] : 0;
    city = {
      name: element.label,
      data: new Array(element),
    };
    chartSeries.push(city);
  });

  return chartSeries;
}

export function parseCitiesOfProvince(data) {
  const cities = [];
  data.forEach((element) => {
    cities.push({
      label: element.properties.NAME_2,
      hc_key: element.properties.hc_key,
    });
  });
  return cities;
}

export function formatCitiesApi(data) {
  const city = [];
  if (data !== "-" && data.length > 0) {
    data.forEach((element) => {
      let temp = element.toLowerCase();
      if (temp.indexOf("kota") < 0) {
        temp = `kab. ${temp}`;
      }
      // city.push('"' + temp + '"')
      city.push(temp);
    });
  }
  return city;
}
