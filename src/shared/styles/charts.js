import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 320,
    width: "100%",
    justifyContent: "center",
    alignSelf: "center",
    // borderWidth: 1,
    // borderColor: 'green',
  },
  container100: {
    flex: 1,
    height: 100,
    width: "100%",
    justifyContent: "center",
    alignSelf: "center",
  },
  container130: {
    flex: 1,
    height: 130,
    width: "100%",
    justifyContent: "center",
    alignSelf: "center",
  },
  container150: {
    flex: 1,
    height: 150,
    width: "100%",
    justifyContent: "center",
    alignSelf: "center",
  },
  container200: {
    flex: 1,
    height: 200,
    width: "100%",
    justifyContent: "center",
    alignSelf: "center",
  },
  wrapper: {
    paddingHorizontal: 0,
    // flex: 1,
    // borderWidth: 1,
    // borderColor: 'blue',
  },
  webviewStyles: {
    backgroundColor: "transparent",
    // margin: 20,
    // borderWidth: 2,
    // borderColor: 'red',
  },
});
