import { styles as charts } from "./charts";

const styles = {
  charts,
  form: {
    paddingHorizontal: 18,
    marginVertical: 10,
  },
  inputGroup: {
    marginVertical: 5,
  },
  formAction: {
    marginVertical: 10,
  },
  inputLabel: {
    fontSize: 16,
    marginBottom: 5,
    fontWeight: "bold",
  },
  pullRight: {
    position: "absolute",
    right: -10,
    paddingHorizontal: 0,
  },
  inputIcon: {
    width: 50,
    height: 50,
    paddingHorizontal: 10,
    paddingVertical: 12,
    color: "rgba(0,0,0,0.5)",
  },
  inputText: {
    height: 50,
    color: "#222222",
    fontSize: 16,
    backgroundColor: "rgba(0, 0, 0, 0.05)",
  },
  inputViewError: {
    borderWidth: 1,
    borderColor: "red",
    backgroundColor: "rgba(250, 198, 55, 0.15)",
  },
};

export default styles;
