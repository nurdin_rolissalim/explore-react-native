import React, { Component } from 'react';
import SummaryView from 'app/shared/ui/statistic/SummaryView';
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';
import { connect } from 'react-redux';
import { getIssue } from "app/redux/ducks";
import moment from 'moment'
// import { contentNews, issueNews } from "app/service";
import {
    latestPost,
    contentNews
} from "app/service/ContentService";
import {
    issueNews,
} from "app/service/IssueService";
import {
    totalPostSocmed,
} from "app/service/StatisticService";

import {
    totalVolunteers,
    monthlyGrowth
} from "app/service/RecruitmentVolunteerService";

class SummaryController extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            loading: true,
            suffix: " News"
        }
    }

    getData() {
        let { filter, entity, analyze, platform, user, issue } = this.props
        let data;
        let params;
        let response;
        let type_location = "1";
        let location = user.location;
        this.updateLoader(true, null)

        switch (analyze) {
            case "universe-person":
            case "universe-location":
            case "universe-organization":
                params = {
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    page: 1,
                    size: 1,
                }
                this.getNews(params);
                break;
            case "universe-response":
                params = {
                    index: platform,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    page: 1,
                    size: 1,
                    order_by: "latest",
                }

                this.getLatestPost(params)
                break;
            case "universe-activity":
                params = {
                    index: platform,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    page: 1,
                    size: 1,
                    profiling: true,
                    order_by: "latest",
                    keyword: "",
                    hashtag: "",
                    sentiment: "",
                    emotion: ""
                }
                this.getLatestPost(params)
                break;
            case "activity-news-total":
                params = {
                    cluster_id: issue.key ? issue.key : "",
                    user: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    size: 1,
                    page: 1,
                    order: "desc"
                };
                this.getIssueNews(params);
                break;
            case "activity-profile-total":
                params = {
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    profiling: true,
                    index: platform
                };
                this.getTotalPostSocmed(params);
                break;
            case "activity-response-total":
                params = {
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    profiling: true,
                    index: (platform == "global-asia") ? "all" : platform,
                };
                this.getTotalPostSocmed(params);
                break;
            case "volunteer-recruitment-total-volunteers":
                if (user.type_location === "city") {
                    type_location = "2";
                    if (location.indexOf("kab. ") > -1) {
                        location = "kabupaten " + location.replace("kab. ", "")
                    }
                }

                params = new Array(
                    "region=" + encodeURIComponent(location),
                    "type_location=" + type_location,
                    "start_date=",
                    "end_date=",
                    "code_workspace=" + user.workspace
                )
                this.getTotalVolunteers(params);
                break;
            case "volunteer-recruitment-monthly-growth":
                if (user.type_location === "city") {
                    type_location = "2";
                    if (location.indexOf("kab. ") > -1) {
                        location = "kabupaten " + location.replace("kab. ", "")
                    }
                }

                params = new Array(
                    "region=" + encodeURIComponent(location),
                    "type_location=" + type_location,
                    "month=" + moment().format("MM"),
                    "year=" + moment().format("YYYY"),
                    "code_workspace=" + user.workspace
                )
                this.getMonthlyGrowth(params);
                break;
            default:
                data = [{
                    key: "jook wi a",
                    value: 1200,
                    suffix: " News"
                }]
                this.setState({
                    data: data,
                    loading: false
                })
                break;
        }
    }

    updateLoader(loading, data, error = null) {
        this.setState({ loading: loading, data: data, error: error })
    }

    getIssueNews(params) {
        let { analyze, user } = this.props

        issueNews(params)
            .then((response) => {
                let data = 0;

                if (response.status === true && response.total > 0) {
                    data = response.total

                }
                data = [{
                    key: user.keyword,
                    value: data,
                    suffix: " News"
                }]

                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(true, null, error)
            });
    }

    getLatestPost(params) {
        let data = null;
        latestPost(params)
            .then((response) => {
                data = [{
                    key: params.person,
                    value: response.total,
                    suffix: " Posts",
                }];

                if (params.index == "twitter") {
                    data[0].suffix = " Tweets"
                }

                this.setState({
                    data: data,
                    loading: false
                });
            })
            .catch((error) => {
                this.setState({
                    data: null,
                    loading: false,
                    error: error
                })
            });
    }

    getNews(params) {
        let data = null;
        contentNews(params)
            .then((response) => {
                data = [{
                    key: params.person,
                    value: response.total,
                    suffix: " News",
                }];

                this.setState({
                    data: data,
                    loading: false
                })
            })
            .catch((error) => {
                this.setState({
                    data: null,
                    loading: false,
                    error: error
                })
            });
    }

    getTotalPostSocmed(params) {
        let data = null;
        let suffix = " Posts"
        totalPostSocmed(params)
            .then((response) => {
                response.data.forEach(element => {
                    suffix = " Posts";
                    if (params.index != "all") {
                        if (element.key === params.index) {
                            if (params.index == "twitter") {
                                suffix = " Tweets"
                            }
                            data = [{
                                key: params.person,
                                value: element.value,
                                suffix: suffix,
                            }];
                        }
                    } else {
                        data = [{
                            key: params.person,
                            value: element.value,
                            suffix: suffix,
                        }];
                        data.push({
                            key: element.key,
                            value: element.value,
                            suffix: suffix,
                        });
                    }

                });

                this.setState({
                    data: data,
                    loading: false
                })
            })
            .catch((error) => {
                this.setState({
                    data: null,
                    loading: false,
                    error: error
                })
            });
    }
    getMonthlyGrowth(params) {
        let data = null;
        let suffix = ""
        monthlyGrowth(params)
            .then((response) => {
                data = [{
                    key: "recruitment",
                    value: response.data,
                    suffix: suffix,
                }];

                this.setState({
                    data: data,
                    loading: false
                })
            })
            .catch((error) => {
                this.setState({
                    data: null,
                    loading: false,
                    error: error
                })
            });
    }
    getTotalVolunteers(params) {
        let data = null;
        let suffix = ""
        totalVolunteers(params)
            .then((response) => {

                data = [{
                    key: "recruitment",
                    value: response.data[0].total,
                    suffix: suffix,
                }];

                this.setState({
                    data: data,
                    loading: false
                })
            })
            .catch((error) => {
                this.setState({
                    data: null,
                    loading: false,
                    error: error
                })
            });
    }


    componentDidMount() {
        this.getData();
    }

    shouldComponentUpdate() {
        return true;
    }

    UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            prevProps.platform != this.props.platform ||
            this.props.keyword != prevProps.keyword
            ||
            (
                // kondisi update filter 
                (
                    prevProps.filter.startDate != this.props.filter.startDate
                    ||
                    prevProps.filter.endDate != this.props.filter.endDate
                    ||
                    prevProps.filter.locations != this.props.filter.locations
                    ||
                    prevProps.filter.location != this.props.filter.location
                    ||
                    prevProps.filter.timeframe != this.props.filter.timeframe
                )
                &&
                (
                    this.props.cPage == this.props.filter.page.name
                )
            )
            ||
            (
                // kondisi update issue 
                prevProps.issue.key != this.props.issue.key
                &&
                this.props.cPage == this.props.issue.page
            )

        ) {

            const data = this.getData();
        }
    }


    viewTotal(analyze, type, data) {
        return (
            <SummaryView analyze={analyze} data={data} />
        )
    }

    render() {
        const { analyze, type, filter } = this.props
        const { data, loading } = this.state;

        if (loading === false && data != null) {
            switch (analyze) {
                case "universe-news":
                case "universe-sosmed":
                case "activity-news-total":
                case "activity-profile-total":
                case "activity-response-total":
                    return this.viewTotal(
                        analyze,
                        type,
                        data,
                    );
                    break;
                default:
                    return this.viewTotal(
                        analyze,
                        type,
                        data
                    );
                    break;
            }
        } else {
            return (
                <PlaceHolder loading={loading} data={data} />
            )
        }
    }

}


const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity, issue } = state;

    return {
        user,
        filter,
        entity,
        issue
    };
}

const mapDispatchToProps = {
    getIssue
};


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SummaryController);