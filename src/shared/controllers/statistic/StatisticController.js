import React from "react";

import {
    totalPostType,
    map,
    mapTotalData
} from "app/service/StatisticService";
import { connect } from "react-redux";

import DonutChartView from 'app/shared/ui/charts/DonutChartView';
import BarChartView from 'app/shared/ui/charts/BarChartView';
import LineChartView from 'app/shared/ui/charts/LineChartView';
import PolarChartView from 'app/shared/ui/charts/PolarChartView';
import WordCloudChartView from 'app/shared/ui/charts/WordCloudChartView';
import HighMapChartView from 'app/shared/ui/charts/HighMapChartView';
import MapsView from 'app/shared/ui/maps/MapsView';
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';
import {
    Text,
} from "native-base";
class StatisticController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            analyze: props.analyze,
            poligon: []
        };
    }

    UNSAFE_componentWillMount() {
        let { analyze } = this.props;
        switch (analyze) {
            case "audience-map":
                this.getMap()
                break;
            default:
                break;
        }
    }

    componentDidMount() {
        this.getData()
    }

    shouldComponentUpdate() {
        return true;
    }

    UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            prevProps.platform != this.props.platform ||
            this.props.keyword != prevProps.keyword
            ||
            (
                // kondisi update filter 
                (
                    prevProps.filter.startDate != this.props.filter.startDate
                    ||
                    prevProps.filter.endDate != this.props.filter.endDate
                    ||
                    prevProps.filter.locations != this.props.filter.locations
                    ||
                    prevProps.filter.location != this.props.filter.location
                    ||
                    prevProps.filter.timeframe != this.props.filter.timeframe
                )
                &&
                (
                    this.props.cPage == this.props.filter.page.name
                )
            )
            ||
            (
                // kondisi update issue 
                prevProps.issue.key != this.props.issue.key
                &&
                this.props.cPage == this.props.issue.page
            )

        ) {

            const data = this.getData();
        }
    }

    getData() {
        let paramsMap
        let params
        let date;
        let data = null;
        let { analyze, filter, entity, platform, user } = this.props
        this.updateLoader(true, null, false)
        switch (analyze) {
            case "universe-post-type-activity":
                params = {
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    source: platform,
                    profiling: true
                }
                this.getTotalPostType(params);
                break;
            case "universe-post-type-response":
                params = {
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    source: platform,
                }
                this.getTotalPostType(params);
                break;
            case "activity-post-type-profile":
                params = {
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    source: platform,
                    profiling: true
                }
                this.getTotalPostType(params);
                break;
            case "activity-post-type-response":
                params = {
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    source: platform,
                }
                this.getTotalPostType(params);
                break;
            case "audience-map":
                params = {
                    location: user.location,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                }
                this.getMapTotalData(params);
                break;
            case "":
                break;
            default:
                break;
        }
    }

    updateLoader(loading, data, error = null) {
        this.setState({ loading: loading, data: data, error: error })
    }


    getTotalPostType(params) {
        let data = null;
        console.log("prams", params);

        totalPostType(params)
            .then((response) => {
                if (response.status === true) {
                    data = response.data;
                }

                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, null, error)
            });
    }
    getMap() {
        let data = null;
        let { user } = this.props;
        let params = {
            location: user.location
        }
        map(params)
            .then((response) => {
                if (response.status === true) {
                    data = response.data;
                }

                this.setState({ poligon: data })
            })
            .catch((error) => {
            });
    }
    getMapTotalData(params) {
        let data = null;

        mapTotalData(params)
            .then((response) => {
                if (response.status === true) {
                    data = response.data;
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, null, error)
            });
    }

    viewDonut() {
        let { data, analyze } = this.props;
        return (
            <DonutChartView analyze={analyze} data={data} />
        );
    }
    viewBar() {
        let { analyze } = this.props;
        let { data } = this.state;
        return (
            <BarChartView analyze={analyze} data={data} />
        );
    }
    viewLine() {
        let { data, analyze } = this.props;
        return (
            <LineChartView analyze={analyze} data={data} />
        );
    }
    viewWordCloud() {
        let { data, analyze } = this.props;
        return (
            <WordCloudChartView analyze={analyze} data={data} />
        );
    }
    viewPolar(analyze, type, data) {
        return (
            <PolarChartView analyze={analyze} data={data} />
        );
    }
    viewHighMap(analyze, type, data, poligon) {
        return (
            <HighMapChartView analyze={analyze} data={data} poligon={poligon} />
        );
    }
    viewMaps() {
        let { data, analyze } = this.props;
        return (
            <MapsView analyze={analyze} data={data} />
        );
    }

    render() {
        let { analyze, type, filter } = this.props
        let { data, loading, poligon } = this.state;

        console.log("data", data);
        if (loading === false && data != null) {
            switch (analyze) {
                case "universe-post-type-activity":
                case "universe-post-type-response":
                case "activity-post-type-profile":
                case "activity-post-type-response":
                    return this.viewDonut();
                    // return (
                    //     <Text>Text</Text>
                    // )
                    break;
                case "audience-map":
                    return this.viewBar();
                    // return this.viewMaps();
                    break;
                default:
                    return this.viewWordCloud();
                    break;
            }
        } else {
            return (
                <PlaceHolder loading={loading} data={data} />
            )
        }
    }
}


const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity, issue } = state;

    return {
        user,
        filter,
        entity,
        issue
    };
};

export default connect(mapStateToProps)(StatisticController);
