import React, { Component } from "react";
import { StyleSheet, FlatList, Image, Platform } from "react-native";
import {
    Toast,
} from "native-base";
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';
import ProfileImage from 'app/shared/ui/content/ProfileImage';
import { connect } from "react-redux";

const uri = require("assets/images/politica-icon.png");

class ProfileImageController extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            keyword: null,
            loading: true,
        };
    }

    selectedEntity(entity) {
        // this.props.setEntity(entity)
        // this.props.callback(entity);
    }

    getData() {
        let data = null;
        this.setState({ loading: true, data: null })
        switch (this.props.analyze) {
            case "universe-news":
            case "universe-sosmed":
                data = this.getUniverse();
                break;

            default:
                data = this.getUniverse();
                break;
        }
        return data;
    }

    getUniverse() {
        let data = null;
        data = {
            image: this.props.entity.key,
            value: this.props.entity.value,
            name: this.props.entity.key,
        }
        this.setState({ data: data, loading: false, type: "news" })
    }



    componentDidMount() {
        const data = this.getData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.analyze != this.props.analyze) {
            const data = this.getData();
        }
    }

    UNSAFE_componentWillUnmount() {
        this.setState({ data: null });
    }

    componentWillUnmount() {
        this.setState({ data: null });
    }
    profileImageUniverse(analyze, type, data, platform) {
        return (
            <ProfileImage analyze={analyze} data={data} type={type} platform={platform} />
        )
    }

    render() {
        const { data, loading, } = this.state;
        let { analyze, platform, type } = this.props;

        if (loading == false && data != null) {
            switch (analyze) {
                case "universe-person":
                case "universe-location":
                case "universe-organization":
                case "universe-activity":
                case "universe-response":
                    return this.profileImageUniverse(analyze, type, data, platform);
                    break;
                default:
                    return (
                        <>
                        </>
                    )
                    break;
            }
        } else {
            return (
                <PlaceHolder loading={loading} data={data} />
            )
        }
    }
}

const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity } = state;

    return { user, filter, entity };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     onLogin: (userData) => dispatch(signIn(userData)),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Entity);

export default connect(mapStateToProps, null)(ProfileImageController);