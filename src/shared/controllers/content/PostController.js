import React, { Component } from "react";

import {
    latestPost,
    postAudience
} from "app/service/ContentService";
import {
    getIssuesVolunteer
} from "app/service/IssueVolunteerService";
import { connect } from "react-redux";
import {
    getFilter,
    getIssue,
    getEntity
} from 'app/redux/ducks';

import ListAvatar from 'app/shared/ui/content/ListAvatar';
import DeckSwiperView from 'app/shared/ui/content/DeckSwiperView';
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';

class PostController extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            loading: true,
            paging: 1

        };
    }

    getDummyNews() {
        let data = [
            {
                "user_image": "http://pbs.twimg.com/profile_images/1185747474012442625/SWdGwudP_400x400.jpg",
                "user_full_name": "Joko Widodo",
                "username": "jokowi",
                "ann_clean_text": "saya meminta para kepala daerah fleksibel mencari solusi bagi warga miskin yang belum menerima bansos. data penerima agar transparan siapa, kriterianya apa, jenis bantuannya, dll. ini agar lebih jelas dan tidak menimbulkan kecurigaan. kita juga bisa mengoreksi di lapangan",
                "ann_sentiment": "positive",
                "ann_emotion": "neutral",
                "ann_info_class": "bias",
                "created_at": "2020-05-04 06:35:32",
                "media": null,
                "link": "https://twitter.com/jokowi/status/1257197150393360386",
                "platform": "twitter"
            },
            {
                "user_image": "http://pbs.twimg.com/profile_images/1185747474012442625/SWdGwudP_400x400.jpg",
                "user_full_name": "Joko Widodo",
                "username": "jokowi",
                "ann_clean_text": "semua program jaring pengaman sosial, pkh, sembako, bansos, blt, hingga dana desa, telah berjalan. saya minta menteri sosial, para gubernur, bupati, wali kota, camat, hingga kepala desa turun dan memastikan seluruh program sampai di tangan keluarga penerima pada pekan ini.",
                "ann_sentiment": "positive",
                "ann_emotion": "neutral",
                "ann_info_class": "neutral",
                "created_at": "2020-05-04 06:35:06",
                "media": [
                    {
                        "id": "537cf17b49e91bbcfeea33ff36f21423",
                        "index": "1257197040112558080-0",
                        "type": "photo",
                        "url": "http://pbs.twimg.com/media/EXJ2CGOUYAIB5QW.jpg"
                    }
                ],
                "link": "https://twitter.com/jokowi/status/1257197040112558080",
                "platform": "twitter"
            }
        ]
        this.setState({
            data: data
        })
    }

    getLatestPost(params) {
        let data = null;
        latestPost(params)
            .then((response) => {
                let emotion;
                let sentiment;
                let info;
                if (response.status === true && response.total > 0) {
                    data = response.data;
                    response.data.forEach(element => {
                        element.label = [{

                        }]
                    });
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, null, error)
            });
    }
    getPostAudience(params) {
        let data = null;
        postAudience(params)
            .then((response) => {
                if (response.status === true && response.total > 0) {
                    data = response.data;

                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, null, error)
            });
    }
    getIssuesVolunteer(params) {
        let data = null;
        getIssuesVolunteer(params)
            .then((response) => {
                if (response.status === true && response.data.length > 0) {
                    data = response.data;

                }

                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, null, error)
            });
    }

    getData() {
        let params;
        this.setState({ loading: true, data: null });
        let { analyze, type, entity, filter, platform, user, paging } = this.props
        let type_location = "1";
        let location = user.location;

        switch (analyze) {
            case "universe-news-issue":
                this.getNewsIssue();
                break;
            case "universe-activity":
                params = {
                    index: platform,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    profiling: true,
                    order: "desc",
                    order_by: "latest",
                    page: paging,
                    size: 5
                }
                this.getLatestPost(params);

                break;
            case "universe-response":
                params = {
                    index: platform,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    order_by: "latest",
                    page: paging,
                    size: 5
                }
                this.getLatestPost(params);

                break;
            case "audience-post-issue":
                params = {
                    index: platform,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    order_by: "latest",
                    page: paging,
                    size: 12,
                    keyword: (issue.key) ? issue.key : "-",
                }
                this.getPostAudience(params);
                break;
            case "audience-post-location":
                params = {
                    index: platform,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    order_by: "latest",
                    page: paging,
                    size: 12,
                    location: user.location
                }

                // city = formatParamsLocation(
                //     user.type_location,
                //     user.location,
                // districts,
                // this.global.location_audience,
                // )

                // if (this.global.changed == "location_audience" && this.global.location_audience !== "-") {
                //     this.params.locations = city.location_audience;
                // } else if (city.locations != "-") {
                //     this.params.locations = city.locations
                // } else {
                //     this.params.location = city.location
                // }

                this.getPostAudience(params);

                break;
            case "activity-profile-post":
                params = {
                    index: platform,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    order_by: "latest",
                    page: paging,
                    size: 3,
                    person: user.keyword
                }
                this.getLatestPost(params);
                break;
            case "activity-profile-post":
                params = {
                    index: platform,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    order_by: "latest",
                    page: paging,
                    size: 3,
                    person: user.keyword,
                    profiling: true
                }
                this.getLatestPost(params);
                break;
            case "activity-response-post":
                params = {
                    index: (platform == "global-asia") ? "all" : platform,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "desc",
                    order_by: "latest",
                    page: paging,
                    size: 3,
                    person: user.keyword
                }
                this.getLatestPost(params);
                break;
            case "volunteer-issue-detail":
                if (user.type_location === "city") {
                    type_location = "2";
                    if (location.indexOf("kab. ") > -1) {
                        location = "kabupaten " + location.replace("kab. ", "")
                    }
                }

                params = new Array(
                    "region=" + encodeURIComponent(location),
                    "type_location=" + type_location,
                    "start_date=",
                    "end_date=",
                    "code_workspace=" + user.workspace,
                    "id_category=" + platform
                )
                this.getIssuesVolunteer(params);
                break;
            default:
                this.getDummyNews();
                break;
        }
    }


    UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            prevProps.platform != this.props.platform ||
            this.props.keyword != prevProps.keyword
            ||
            (
                // kondisi update filter 
                (
                    prevProps.filter.startDate != this.props.filter.startDate
                    ||
                    prevProps.filter.endDate != this.props.filter.endDate
                    ||
                    prevProps.filter.locations != this.props.filter.locations
                    ||
                    prevProps.filter.location != this.props.filter.location
                    ||
                    prevProps.filter.timeframe != this.props.filter.timeframe
                )
                &&
                (
                    this.props.cPage == this.props.filter.page.name
                )
            )
            ||
            (
                // kondisi update issue 
                prevProps.issue.key != this.props.issue.key
                &&
                this.props.cPage == this.props.issue.page
            )

        ) {
            // const data = this.getData();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            prevProps.platform != this.props.platform ||
            this.props.keyword != prevProps.keyword
            ||
            (
                // kondisi update filter 
                (
                    prevProps.filter.startDate != this.props.filter.startDate
                    ||
                    prevProps.filter.endDate != this.props.filter.endDate
                    ||
                    prevProps.filter.locations != this.props.filter.locations
                    ||
                    prevProps.filter.location != this.props.filter.location
                    ||
                    prevProps.filter.timeframe != this.props.filter.timeframe
                )
                &&
                (
                    this.props.cPage == this.props.filter.page.name
                )
            )
            ||
            (
                // kondisi update issue 
                prevProps.issue.key != this.props.issue.key
                &&
                this.props.cPage == this.props.issue.page
            )

        ) {
            const data = this.getData();
        }
    }

    componentDidMount() {
        this.getData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    componentWillUnmount() {
        this.setState({ data: null, loading: false, error: false });
    }

    updateLoader(loading, data, error = null) {
        this.setState({ loading: loading, data: data, error: error })
    }

    viewPostImage(analyze, type, data) {
        return (
            <>
                <ListAvatar data={data} type={type} analyze={analyze} />
            </>
        );
    }
    viewDickerSwipe(analyze, type, data) {
        return (
            <>
                <DeckSwiperView data={data} type={type} analyze={analyze} />
            </>
        );
    }

    render() {
        let { data, loading } = this.state
        let { type, analyze, platform } = this.props

        if (loading === false && data != null) {
            switch (analyze) {
                case "home-issue":
                    return this.viewPostImage(analyze, type, data);
                    break;
                case "activity-person":
                    return this.viewPostImage(analyze, type, data);
                    break;
                case "home-news":
                    return this.viewPostImage(analyze, type, data);
                    break;
                case "universe-activity":
                case "universe-response":
                case "audience-post":
                case "audience-post-issue":
                case "audience-post-location":
                    return this.viewPostImage(analyze, type, data);
                    break;
                case "volunteer-issue-detail":
                    return this.viewDickerSwipe(analyze, type, data);
                    break;
                default:
                    return this.viewPostImage(analyze, type, data);
                    break;
            }
        } else {
            return (
                <PlaceHolder loading={loading} data={data} />
            )

        }


    }
}

const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity, issue } = state;
    return {
        user,
        filter,
        entity,
        issue,
    };
};
const mapDispatchToProps = {
    getFilter,
    getIssue,
    getEntity
};

export default connect(mapStateToProps, mapDispatchToProps)(PostController);

