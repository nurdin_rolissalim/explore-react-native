import React, { Component } from "react";
import { StyleSheet } from "react-native";

import { connect } from "react-redux";
import { getFilter, getIssue, getEntity } from 'app/redux/ducks';

import { contentNews } from "app/service/ContentService";
import { issueNews } from "app/service/IssueService";

import ListAvatar from "app/shared/ui/content/ListAvatar";
import ListThumbnail from "app/shared/ui/content/ListThumbnail";
import TimelineView from "app/shared/ui/content/TimelineView";
import PlaceHolder from "app/shared/ui/utility/PlaceHolder";
import moment from 'moment';
const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class NewsController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      analyze: props.analyze,
      loading: true,
      layout: "news",
      timer: ""
    };
  }

  getDummyNews() {
    const data = [
      {
        user_image:
          "http://pbs.twimg.com/profile_images/1185747474012442625/SWdGwudP_400x400.jpg",
        user_full_name: "Joko Widodo",
        username: "jokowi",
        ann_clean_text:
          "saya meminta para kepala daerah fleksibel mencari solusi bagi warga miskin yang belum menerima bansos. data penerima agar transparan siapa, kriterianya apa, jenis bantuannya, dll. ini agar lebih jelas dan tidak menimbulkan kecurigaan. kita juga bisa mengoreksi di lapangan",
        ann_sentiment: "positive",
        ann_emotion: "neutral",
        ann_info_class: "bias",
        created_at: "2020-05-04 06:35:32",
        media: null,
        link: "https://twitter.com/jokowi/status/1257197150393360386",
        platform: "twitter",
      },
      {
        user_image:
          "http://pbs.twimg.com/profile_images/1185747474012442625/SWdGwudP_400x400.jpg",
        user_full_name: "Joko Widodo",
        username: "jokowi",
        ann_clean_text:
          "semua program jaring pengaman sosial, pkh, sembako, bansos, blt, hingga dana desa, telah berjalan. saya minta menteri sosial, para gubernur, bupati, wali kota, camat, hingga kepala desa turun dan memastikan seluruh program sampai di tangan keluarga penerima pada pekan ini.",
        ann_sentiment: "positive",
        ann_emotion: "neutral",
        ann_info_class: "neutral",
        created_at: "2020-05-04 06:35:06",
        media: [
          {
            id: "537cf17b49e91bbcfeea33ff36f21423",
            index: "1257197040112558080-0",
            type: "photo",
            url: "http://pbs.twimg.com/media/EXJ2CGOUYAIB5QW.jpg",
          },
        ],
        link: "https://twitter.com/jokowi/status/1257197040112558080",
        platform: "twitter",
      },
    ];
    this.setState({
      data,
    });
  }

  getNewsIssue(params) {
    let data = null;

    issueNews(params)
      .then((response) => {
        if (response.status === true) {
          data = response.data;
        }
        this.updateLoader(false, data)
      })
      .catch((error) => {
        this.updateLoader(false, null, error)
      });
  }
  getContentNews(params) {
    let data = null;

    contentNews(params)
      .then((response) => {
        if (response.status === true) {
          data = new Array();
          response.data.forEach(element => {
            element.time = moment(element.created_at).format("DD MMM YYYY");
            data.push(element)
          });

          // data = response.data;
        }
        this.updateLoader(false, data)
      })
      .catch((error) => {
        this.updateLoader(false, null, error)
      });
  }

  getData() {
    let params;
    let { analyze, issue, entity, filter, user } = this.props;
    this.updateLoader(true, null, false)

    switch (analyze) {
      case "universe-news-issue":
        params = {
          cluster_id: issue.key,
          size: 3,
          start_date: filter.startDate,
          end_date: filter.endDate,
          order: "desc",
          page: 1,
          person: entity.key,
        };

        return this.getNewsIssue(params);
        break;
      case "activity-news-issue":
        params = {
          cluster_id: (issue.key) ? issue.key : "",
          size: 3,
          start_date: filter.startDate,
          end_date: filter.endDate,
          order: "desc",
          page: 1,
          user: user.keyword
        };

        return this.getNewsIssue(params);
        break;
      case "activity-timeline":
        params = {
          size: 10,
          start_date: filter.startDate,
          end_date: filter.endDate,
          order: "desc",
          page: 1,
          user: user.keyword
        };

        return this.getContentNews(params);
        break;
      case "home-person":
        params = {
          size: 30,
          start_date: filter.startDate,
          end_date: filter.endDate,
          order: "desc",
          person: "joko widodo",
          time_zone: "Asia/Jakarta"
        }
        console.log("filter", filter);
        return this.getContentNews(params);
        break
      default:
        this.getDummyNews();
        break;
    }
  }

  updateLoader(loading, data, error = null) {
    this.setState({ loading: loading, data: data, error: error })
  }

  UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.type != this.props.type ||
      this.props.keyword != prevProps.keyword
      ||
      (
        // kondisi update filter 
        (
          prevProps.filter.startDate != this.props.filter.startDate
          ||
          prevProps.filter.endDate != this.props.filter.endDate
          ||
          prevProps.filter.locations != this.props.filter.locations
          ||
          prevProps.filter.location != this.props.filter.location
          ||
          prevProps.filter.timeframe != this.props.filter.timeframe
        )
        &&
        (
          this.props.cPage == this.props.filter.page.name
        )
      )
      ||
      (
        // kondisi update issue 
        prevProps.issue.key != this.props.issue.key
        &&
        this.props.cPage == this.props.issue.page
      )

    ) {

      const data = this.getData();
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // if (prevProps.type != this.props.type) {
    //   const data = this.getData();
    // }




    if (
      prevProps.type != this.props.type ||
      this.props.keyword != prevProps.keyword
      ||
      (
        // kondisi update filter 
        (
          prevProps.filter.startDate != this.props.filter.startDate
          ||
          prevProps.filter.endDate != this.props.filter.endDate
          ||
          prevProps.filter.locations != this.props.filter.locations
          ||
          prevProps.filter.location != this.props.filter.location
          ||
          prevProps.filter.timeframe != this.props.filter.timeframe
        )
        &&
        (
          prevProps.cPage == this.props.filter.page.name
        )
      )
      ||
      (
        // kondisi update issue 
        prevProps.issue.key != this.props.issue.key
        &&
        prevProps.cPage == this.props.issue.page
      )

    ) {
      // const data = this.getData();
    }
  }

  componentDidMount() {
    this.getData();
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return true;
  }

  componentWillUnmount() {
    clearTimeout(this.state.timer);
    // this.setState({ data: null });
  }

  viewThumbnail(analyze, type, data) {

    return (
      <>
        <ListThumbnail data={data} type={type} analyze={analyze} />
      </>
    );
  }

  viewListAvatar(analyze, type, data) {
    return (
      <>
        <ListAvatar data={data} type={type} analyze={analyze} />
      </>
    );
  }
  viewTimelineView(analyze, type, data) {
    return (
      <>
        <TimelineView data={data} type={type} analyze={analyze} />
      </>
    );
  }

  render() {
    const { data, loading, layout } = this.state;
    const { type, analyze } = this.props;

    if (loading === false && data != null) {
      switch (analyze) {
        case "universe-news-issue":
          return this.viewThumbnail(layout, type, data);
          break;
        case "home-issue":
          return this.viewListAvatar(layout, type, data);
          break;
        case "activity-person":
          return this.viewListAvatar(layout, type, data);
          break;
        case "home-news":
          return this.viewListAvatar(layout, type, data);
          break;
        case "activity-timeline":
          return this.viewTimelineView(layout, type, data);
          break;
        default:
          return this.viewThumbnail(layout, type, data);
          break;
      }
    } else {
      return <PlaceHolder loading={loading} data={data} />;
    }
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 20,
  },
  container: {
    shadowRadius: 16,
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

const mapStateToProps = (state) => {
  const user = state.auth.userData;
  const { filter, entity, issue } = state;
  return {
    user,
    filter,
    entity,
    issue
  };
};

const mapDispatchToProps = {
  getFilter,
  getIssue,
  getEntity
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsController);
