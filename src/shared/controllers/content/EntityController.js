import React, { Component } from "react";
import { getSubscription } from "app/service/EntityService";
import PlaceHolder from "app/shared/ui/utility/PlaceHolder";
import EntityView from "app/shared/ui/content/EntityView";
import { connect } from "react-redux";
import { getFilter, setEntity } from "app/redux/ducks";
import {
  issueInfluencer,
} from "app/service/IssueService";
import {
  sentiment,
  mediaShare,
  exposure,
  emotions,
  exposureSocmed,
  sentimentSocmed,
  trending
} from "app/service/ChartService";
const uri = require("assets/images/politica-icon.png");

class EntityController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      loading: true,
      error: false,
    };
  }

  selectedEntity(entity) {
    let { platform, type, analyze, cPage, page } = this.props;

    let data = {
      key: entity.key,
      value: entity.value,
      platform: platform,
      type: type,
      page: cPage,
      analyze: analyze
    }

    this.props.setEntity(data);
    switch (analyze) {
      case "home-person":
      case "universe-news":
      case "universe-sosmed-profile":
      case "universe-sosmed-response":
        this.props.callback(data);
        break;
      default:
        break;
    }
  }

  search(keyword) {
    this.setState({ keyword });
  }

  getData() {
    let data = null;
    this.updateLoader(true, null, false)
    const keyword = this.props.keyword !== undefined ? this.props.keyword : "";
    let params;
    let { filter, user, analyze, type, platform, issue } = this.props;
    switch (analyze) {
      case "universe-news":
        params = {
          page: 1,
          size: 100,
          type: type,
          keyword: keyword,
          start_date: filter.startDate,
          end_date: filter.endDate,
          index: platform,
          id: user.id_user,
        }

        data = this.getSubscription(params);
        break;
      case "universe-sosmed-profile":
      case "universe-sosmed-response":

        params = {
          page: 1,
          size: 100,
          type: "person",
          keyword: keyword,
          start_date: filter.startDate,
          end_date: filter.endDate,
          index: "twitter",
          id: user.id_user,
        }
        data = this.getSubscription(params);
        break;
      case "activity-news-influencer":
        params = {
          cluster_id: (issue.key) ? issue.key : "",
          ORDER: "DESC",
          size: 10,
          user: user.keyword,
          start_date: filter.startDate,
          end_date: filter.endDate,
        };
        this.getIssueInfluencer(params);
        break;
      case "home-person":
        params = {
          aggregation_by: "PERSON",
          order: "desc",
          index: "news",
          size: 10,
          start_date: filter.startDate,
          end_date: filter.endDate,
        }
        this.getTrending(params);

        break;
      default:
        data = this.getPerson();
        break;
    }
    return data;
  }

  getSubscription(params) {
    let data = null;
    getSubscription(params)
      .then((response) => {
        if (response.status === true) {
          data = response.data;
        } else {
          data = null;
        }

        this.updateLoader(false, data)
      })
      .catch((error) => {
        this.updateLoader(false, null, error)
      });
  }


  getTrending(params) {
    let data = null;
    trending(params)
      .then((response) => {
        if (response.status === true) {
          data = response.data;
        }

        this.updateLoader(false, data)
      })
      .catch((error) => {

        this.updateLoader(false, null, error)
      });
  }

  getIssueInfluencer(params) {

    issueInfluencer(params)
      .then((response) => {
        let data = null;
        if (response.status === true && response.total > 0) {
          data = response.data
          this.selectedEntity(data[0]);
        }
        this.updateLoader(false, data)
      })
      .catch((error) => {
        this.updateLoader(true, null, error)
      });
  }

  updateLoader(loading, data, error = null) {
    this.setState({ loading: loading, data: data, error: error })
  }

  getPerson() {
    const data = [
      { key: "Anwar", count: 90, image: uri },
      { key: "Dida", count: 1203, image: uri },
      { key: "James", count: 1203, image: uri },
      { key: "Arta", count: 1203, image: uri },
      { key: "Imam", count: 1203, image: uri },
      { key: "Dewi", count: 1203, image: uri },
      { key: "Mey", count: 1203, image: uri },
      { key: "Yayuk", count: 1203, image: uri },
      { key: "Indra", count: 1203, image: uri },
      { key: "Nurdin Rolissalim", count: 1203, image: uri },
    ];
    this.setState({ data, loading: false });
    return data;
  }

  componentDidMount() {
    // this.didFocusListener = this.props.navigation.addListener(
    //   'didFocus',
    //   () => {},
    // );

    const data = this.getData();
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return true;
  }

  // UNSAFE_componentWillUpdate() {
  // }

  // UNSAFE_componentWillReceiveProps(nextProps) {
  // }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.type != this.props.type ||
      prevProps.platform != this.props.platform ||
      this.props.keyword != prevProps.keyword
      ||
      (
        (
          prevProps.filter.startDate != this.props.filter.startDate
          ||
          prevProps.filter.endDate != this.props.filter.endDate
          ||
          prevProps.filter.locations != this.props.filter.locations
          ||
          prevProps.filter.location != this.props.filter.location
          ||
          prevProps.filter.timeframe != this.props.filter.timeframe
        )
        &&
        (
          this.props.cPage == this.props.filter.page.name
        )
      )
    ) {

      const data = this.getData();
    }
  }

  UNSAFE_componentWillUnmount() {
    this.setState({ data: null });
  }

  componentWillUnmount() {
    // this.didFocusListener.remove();
    this.setState({ data: null });
  }

  viewData(analyze, type, data) {
    return (
      <>
        <EntityView
          analyze={analyze}
          callback={(entity) => this.selectedEntity(entity)}
          data={data}
          type={type}
        />
      </>
    );
  }

  viewPlaceHolder(loading, data) {
    return <PlaceHolder loading={loading} data={data} />;
  }

  render() {
    const { data, loading } = this.state;
    const { analyze, type } = this.props;

    if (loading == false && data != null && data.length > 0) {

      switch (analyze) {
        case "home-person":
        case "universe-news":
        case "universe-sosmed-profile":
        case "universe-sosmed-response":
        case "activity-news-influencer":
          return this.viewData(analyze, type, data);
          break;
        default:
          return (
            <>
            </>
          )

          break;
      }
    }
    return <PlaceHolder loading={loading} data={data} />;
  }
}

const mapStateToProps = (state) => {
  const user = state.auth.userData;
  const { filter, page, issue } = state;

  return { user, filter, page, issue };
};

const mapDispatchToProps = {
  getFilter,
  setEntity
};

export default connect(mapStateToProps, mapDispatchToProps)(EntityController);
