import React from "react";
import moment from "moment";

import {
    issueTop,
    socmedIssue,
    issueNews,
    issueInfluencer,
} from "app/service/IssueService";
import { connect } from "react-redux";
import {
    getFilter,
    setIssue,
    setEntity,
    getEntity
} from "app/redux/ducks";

import IssueVIew from 'app/shared/ui/content/IssueView';
import DonutChartView from 'app/shared/ui/charts/DonutChartView';
import SummaryView from 'app/shared/ui/statistic/SummaryView';
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';

class IssueController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            analyze: props.analyze,
        };
    }

    componentDidMount() {
        this.getData()
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            prevProps.platform != this.props.platform ||
            this.props.keyword != prevProps.keyword
            ||
            (
                (
                    prevProps.filter.startDate != this.props.filter.startDate
                    ||
                    prevProps.filter.endDate != this.props.filter.endDate
                    ||
                    prevProps.filter.locations != this.props.filter.locations
                    ||
                    prevProps.filter.location != this.props.filter.location
                    ||
                    prevProps.filter.timeframe != this.props.filter.timeframe
                )
                &&
                (
                    this.props.cPage == this.props.filter.page.name
                )
            )
        ) {
            const data = this.getData();
        }
    }

    getData() {
        let params;
        let { analyze, entity, type, filter, page, platform, user, issue } = this.props;
        this.updateLoader(true, null)
        switch (analyze) {
            case "universe-person":
            case "universe-location":
            case "universe-person":
                params = {
                    order: "DESC",
                    size: 10,
                    criteria: [entity.key],
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    interval: parseInt(moment(new Date()).format("HH")),
                };
                this.getIssueTop(params);
                break;
            case "home-issue":
                params = {
                    order: "DESC",
                    size: 10,
                    timezone: "Asia/Jakarta",
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    interval: parseInt(moment(new Date()).format("HH")),
                };
                this.getIssueTop(params);
                break;
            case "universe-activity":
                params = {
                    person: entity.key,
                    profiling: true,
                    platform: platform,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    size: 10
                };
                this.getSocmedIssue(params);
                break;
            case "universe-response":
                params = {
                    person: entity.key,
                    profiling: true,
                    platform: platform,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    size: 10
                };
                this.getSocmedIssue(params);
                break;
            case "activity-news-issue":
                params = {
                    order: "DESC",
                    size: 10,
                    user: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    interval: parseInt(moment(new Date()).format("HH")),
                };
                this.getIssueTop(params);
                break;
            case "activity-profile-issue":
                params = {
                    size: 10,
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    platform: platform,
                    profiling: true
                };
                this.getSocmedIssue(params);
                break;
            case "activity-response-issue":
                params = {
                    size: 10,
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    platform: (platform == "global-asia") ? "all" : platform,
                };
                this.getSocmedIssue(params);
                break;
            default:
                break;
        }
    }

    updateLoader(loading, data, error = null) {
        this.setState({ loading: loading, data: data, error: error })
    }

    selectedIssue(issue) {
        let { platform, type, analyze, cPage, page } = this.props;
        let data = {
            key: issue.id,
            value: issue.count,
            platform: platform,
            type: type,
            page: page,
            analyze: analyze
        }

        this.props.setIssue(data);
    }

    getIssueTop(params) {
        issueTop(params)
            .then((response) => {
                let data = null;
                if (response.status === true && response.total > 0) {
                    data = response.data
                    this.selectedIssue(data[0])
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(true, null, error)
            });
    }

    getSocmedIssue(params) {
        socmedIssue(params)
            .then((response) => {
                let data = null;
                if (response.status === true && response.total > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(true, null, error)
            });
    }
    getSocmedIssue(params) {
        socmedIssue(params)
            .then((response) => {
                let data = null;
                if (response.status === true && response.total > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(true, null, error)
            });
    }
    getIssueNews(params) {
        let { analyze, user } = this.props

        issueNews(params)
            .then((response) => {
                let data = null;
                if (response.status === true && response.total > 0) {
                    data = response.data
                }
                if (analyze === "activity-news-total") {

                    data = [{
                        key: user.keyword,
                        value: response.total,
                        suffix: " News"
                    }]

                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(true, null, error)
            });
    }

    viewText() {
        let { data, loading } = this.state;
        let { analyze, type, cPage } = this.props;
        return (
            <IssueVIew
                data={data}
                analyze={analyze}
                callback={(issue) => this.selectedIssue(issue)}
            />
        );
    }

    viewDonut() {
        let { data, loading } = this.state;
        let { analyze, type, cPage } = this.props;

        return (
            <DonutChartView analyze={analyze} data={data} />
        );
    }
    viewSummaryNews() {
        let { data, loading } = this.state;
        let { analyze, type, cPage } = this.props;
        return (
            <SummaryView analyze={analyze} data={data} />
        );
    }

    componentWillUnmount() {
        this.setState({
            data: null,

        })
    }

    render() {
        let { cPage, analyze } = this.props
        let { data, loading } = this.state;

        if (loading === false && data != null) {
            switch (analyze) {
                case "universe-news-detail":
                    return this.viewText();
                    break;
                case "universe-activity":
                case "universe-response":
                case "activity-news-issue":
                case "activity-profile-issue":
                case "activity-response-issue":
                    return this.viewDonut();
                    break;
                case "activity-news-total":
                    return this.viewSummaryNews()
                    break;
                default:
                    return this.viewText();
                    break;
            }
        } else {
            return (
                <PlaceHolder loading={loading} data={data} />
            )
        }
    }
}


const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity, issue, page } = state;

    return {
        user,
        filter,
        entity,
        issue,
        page
    };
};


const mapDispatchToProps = {
    getFilter,
    setIssue,
    setEntity,
    getEntity
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IssueController);
