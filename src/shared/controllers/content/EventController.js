import React, { Component } from "react";

import { connect } from "react-redux";
import { getFilter, setEntity } from "app/redux/ducks";

import { eventsCalendar } from "app/service/EventVolunteerService";

import PlaceHolder from "app/shared/ui/utility/PlaceHolder";
import CalendarView from "app/shared/ui/calendar/CalendarView";
import Calendars from "app/shared/ui/calendar/Calendars";

class EventController extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            loading: true,
            error: false,
        };
    }

    selectedEntity(entity) {
        let { platform, type, analyze, cPage, page } = this.props;

        let data = {
            key: entity.key,
            value: entity.value,
            platform: platform,
            type: type,
            page: cPage,
            analyze: analyze
        }

        this.props.setEntity(data);
        switch (analyze) {
            case "universe-news":
            case "universe-sosmed-profile":
            case "universe-sosmed-response":
                this.props.callback(data);
                break;
            default:
                break;
        }
    }

    search(keyword) {
        this.setState({ keyword });
    }

    getData() {
        let data = null;
        const keyword = this.props.keyword !== undefined ? this.props.keyword : "";
        let params;
        let { filter, user, analyze, type, platform, issue } = this.props;
        let location = user.location;
        let type_location = "";

        this.updateLoader(true, null, false);

        switch (analyze) {
            case "volunteer-calendar":
                if (user.type_location === "city") {
                    type_location = "2";
                    if (location.indexOf("kab. ") > -1) {
                        location = "kabupaten " + location.replace("kab. ", "")
                    }
                }

                params = new Array(
                    "region=" + encodeURIComponent(location),
                    "type_location=" + type_location,
                    "start_date=",
                    "end_date=",
                    "code_workspace=" + user.workspace
                )
                data = this.getEventsCalendar(params);
                break;

            default:
                break;
        }
        return data;
    }

    getEventsCalendar(params) {
        let data = null;
        eventsCalendar(params)
            .then((response) => {
                console.log("response", response);

                if (response.status === true) {
                    data = response.data;
                } else {
                    data = null;
                }
                this.setState({ data: data, loading: false });
            })
            .catch((error) => {
                this.setState({ data: null, loading: false, error: error });
            });
    }

    updateLoader(loading, data, error = null) {
        this.setState({ loading: loading, data: data, error: error })
    }

    componentDidMount() {
        // this.didFocusListener = this.props.navigation.addListener(
        //   'didFocus',
        //   () => {},
        // );

        const data = this.getData();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    // UNSAFE_componentWillUpdate() {
    // }

    // UNSAFE_componentWillReceiveProps(nextProps) {
    // }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            prevProps.platform != this.props.platform ||
            this.props.keyword != prevProps.keyword
            ||
            (
                (
                    prevProps.filter.startDate != this.props.filter.startDate
                    ||
                    prevProps.filter.endDate != this.props.filter.endDate
                    ||
                    prevProps.filter.locations != this.props.filter.locations
                    ||
                    prevProps.filter.location != this.props.filter.location
                    ||
                    prevProps.filter.timeframe != this.props.filter.timeframe
                )
                &&
                (
                    this.props.cPage == this.props.filter.page.name
                )
            )
        ) {

            const data = this.getData();
        }
    }

    UNSAFE_componentWillUnmount() {
        this.setState({ data: null });
    }

    componentWillUnmount() {
        // this.didFocusListener.remove();
        this.setState({ data: null });
    }

    viewData() {
        let { data } = this.state;
        let { analyze, type } = this.props;
        return (
            <>
                <CalendarView
                    // { <Calendars }
                    analyze={analyze}
                    // callback={(entity) => this.selectedEntity(entity)}
                    data={data}
                    type={type}
                />
            </>
        );
    }

    viewPlaceHolder(loading, data) {
        return <PlaceHolder loading={loading} data={data} />;
    }

    render() {
        const { data, loading } = this.state;
        const { analyze, type } = this.props;

        if (loading == false) {
            // if (loading == false && data != null && data.length > 0) {

            switch (analyze) {
                case "volunteer-calendar":
                    return this.viewData();
                    break;
                default:
                    return (
                        <>
                        </>
                    )

                    break;
            }
        }
        return <PlaceHolder loading={loading} data={data} />;
    }
}

const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, page, issue } = state;

    return { user, filter, page, issue };
};

const mapDispatchToProps = {
    getFilter,
    setEntity
};

export default connect(mapStateToProps, mapDispatchToProps)(EventController);
