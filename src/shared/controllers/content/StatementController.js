import React, { Component } from "react";
import { issueStatements } from "app/service/IssueService";
import ListCardItem from "app/shared/ui/content/ListCardItem";
import PlaceHolder from "app/shared/ui/utility/PlaceHolder";
import { connect } from "react-redux";
import { getFilter, getIssue, getEntity, setEntity } from 'app/redux/ducks';

const img =
  "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjUxMTY3fQ&dpr=1&auto=format&fit=crop&w=416&h=312&q=60r";
const uri = require("../../../assets/images/politica-icon.png");

class StatemenController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      loading: true,
      layout: "statements",
      error: null
    };
  }

  getDummy() {
    const data = [
      {
        influencer: "Joko Widodo",
        opinion:
          "punya bukti virus corona (Covid-19) berasal dari laboratorium Wuhan di China.",
        sentiment: "neutral",
      },
      {
        influencer: "Joko Widodo",
        opinion: '"Ya, saya punya."',
        sentiment: "neutral",
      },
      {
        influencer: "Joko Widodo",
        opinion: 'Saya tidak diizinkan untuk memberitahukannya kepada Anda,"',
        sentiment: "negative",
      },
    ];
    this.updateLoader(false, data)
  }

  /*
    digunakand di menu universe detail 
  */

  getIssueStatements(params) {
    issueStatements(params)
      .then((response) => {

        let data = null;
        if (response.status === true && response.total > 0) {
          data = response.data;
        }
        this.updateLoader(false, data)
      })
      .catch((error) => {
        this.updateLoader(false, null, error)
      });
  }

  getData() {
    this.updateLoader(true, null)
    let { analyze, entity, issue, user } = this.props;
    let params;

    switch (analyze) {
      case "universe-news-statements":
        params = {
          cluster_id: (issue.key) ? issue.key : "",
          person: entity.key,
          size: 5,
          page: 1,
          order: "DESC",
        };

        this.getIssueStatements(params);
        break;
      case "activity-news-statements":
        params = {
          cluster_id: (issue.key) ? issue.key : "",
          person: entity.key,
          // person: user.keyword,
          size: 5,
          page: 1,
          order: "DESC",
        };

        this.getIssueStatements(params);
        break;
      default:
        this.getDummy();

        break;
    }
  }

  updateLoader(loading, data, error = null) {
    this.setState({ loading: loading, data: data, error: error })
  }

  componentDidMount() {
    this.getData();
  }

  UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.type != this.props.type ||
      this.props.keyword != prevProps.keyword
      ||
      (
        // kondisi update filter 
        (
          prevProps.filter.startDate != this.props.filter.startDate
          ||
          prevProps.filter.endDate != this.props.filter.endDate
          ||
          prevProps.filter.locations != this.props.filter.locations
          ||
          prevProps.filter.location != this.props.filter.location
          ||
          prevProps.filter.timeframe != this.props.filter.timeframe
        )
        &&
        (
          this.props.cPage == this.props.filter.page.name
        )
      )
      ||
      (
        // kondisi update issue 
        prevProps.issue.key != this.props.issue.key
        && this.props.issue.hasOwnProperty('key')
        &&
        this.props.cPage == this.props.issue.page
      )
      ||
      (
        // kondisi update issue 
        prevProps.entity.key != this.props.entity.key
        && this.props.entity.hasOwnProperty('key')
        &&
        this.props.cPage == this.props.entity.page
      )

    ) {

      // const data = this.getData();
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.type != this.props.type ||
      this.props.keyword != prevProps.keyword
      ||
      (
        // kondisi update filter 
        (
          prevProps.filter.startDate != this.props.filter.startDate
          ||
          prevProps.filter.endDate != this.props.filter.endDate
          ||
          prevProps.filter.locations != this.props.filter.locations
          ||
          prevProps.filter.location != this.props.filter.location
          ||
          prevProps.filter.timeframe != this.props.filter.timeframe
        )
        &&
        (
          this.props.cPage == this.props.filter.page.name
        )
      )
      ||
      (
        // kondisi update issue 
        prevProps.issue.key != this.props.issue.key
        && this.props.issue.hasOwnProperty('key')
        &&
        this.props.cPage == this.props.issue.page
      )
      ||
      (
        // kondisi update issue 
        prevProps.entity.key != this.props.entity.key
        && this.props.entity.hasOwnProperty('key')
        &&
        this.props.cPage == this.props.entity.page
      )

    ) {

      const data = this.getData();
    }
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return true;
  }

  componentWillUnmount() {
    this.updateLoader(false, null, null)
  }

  viewPostImage(analyze, type, data) {
    return (
      <>
        <ListCardItem data={data} type={"person"} analyze={analyze} />
      </>
    );
  }

  render() {
    const { data, loading, layout } = this.state;
    const { analyze, type } = this.props;

    if (loading == false && data != null) {
      switch (analyze) {
        case "activity-news-statements":
          return this.viewPostImage(layout, type, data);
          break;
        default:
          return this.viewPostImage(layout, type, data);
          break;
      }
    }
    return <PlaceHolder loading={loading} data={data} />;
  }
}

const mapStateToProps = (state) => {
  const user = state.auth.userData;
  const { filter, entity, issue } = state;

  return {
    user,
    filter,
    entity,
    issue
  };
};

const mapDispatchToProps = {
  getFilter,
  getIssue,
  getEntity,
};


export default connect(mapStateToProps, mapDispatchToProps)(StatemenController);
