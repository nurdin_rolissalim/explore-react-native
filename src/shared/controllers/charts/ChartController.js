import React from "react";
import DonutChartView from 'app/shared/ui/charts/DonutChartView';
import BarChartView from 'app/shared/ui/charts/BarChartView';
import LineChartView from 'app/shared/ui/charts/LineChartView';
import PolarChartView from 'app/shared/ui/charts/PolarChartView';
import WordCloudChartView from 'app/shared/ui/charts/WordCloudChartView';
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';
import {
    sentiment,
    mediaShare,
    exposure,
    emotions,
    exposureSocmed,
    sentimentSocmed,
    trending
} from "app/service/ChartService";

import { issueChart, issueSentiment } from "app/service/IssueService";
import { connect } from "react-redux";
import { getDateTimeRangeByType } from 'app/helpers/DateHelper'

class ChartController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            analyze: props.analyze,
        };
    }

    componentDidMount() {
        this.getData()
    }

    getData() {
        let params
        let date;
        let data = null;
        let { analyze, filter, entity, platform, issue, user } = this.props
        this.updateLoader(true, null, false)
        switch (analyze) {
            case "universe-news-media-share":
                params = {
                    cluster_id: issue.key,
                    aggregation_by: "MEDIA",
                    size: 10,
                    order: "DESC",
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    person: entity.key
                };
                this.getMediaChart(params);
                break;
            case "universe-news-sentiment":
                params = {
                    index: "news",
                    interval: "year",
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    person: entity.key
                };
                this.getSentiment(params);
                break;
            case "activity-news-sentiment":
                params = {
                    cluster_id: (issue.key) ? issue.key : "",
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    size: 10,
                    user: user.keyword,
                }
                this.getIssueSentiment(params);
                break;
            case "universe-news-exposure":
                date = getDateTimeRangeByType(
                    filter.timeframe,
                    filter.startDate,
                    filter.endDate
                )
                params = {
                    index: "news",
                    interval: date.interval,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    person: entity.key
                };

                this.getExposureNews(params);
                break;
            case "activity-news-exposure":
                date = getDateTimeRangeByType(
                    filter.timeframe,
                    filter.startDate,
                    filter.endDate
                )
                params = {
                    index: "news",
                    interval: date.interval,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    size: 10,
                    user: user.keyword,
                }
                this.getExposureNews(params);
                break;
            case "universe-emotion-activity":
                params = {
                    page: 1,
                    size: 10,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                    profiling: true,
                }
                this.getEmotions(params);
                break;
            case "universe-emotion-response":
                params = {
                    page: 1,
                    size: 10,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                }
                this.getEmotions(params);
                break;
            case "universe-performance-activity":
                date = getDateTimeRangeByType(
                    filter.timeframe,
                    filter.startDate,
                    filter.endDate
                )
                params = {
                    interval: date.interval,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                    profiling: true,
                }
                this.getExposureSocmed(params);
                break;
            case "activity-emotion-profile":
                params = {
                    page: 1,
                    size: 10,
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                    profiling: true,
                }
                this.getEmotions(params);
                break;
            case "activity-emotion-response":
                params = {
                    page: 1,
                    size: 10,
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: (platform == "global-asia") ? "all" : platform,
                }
                this.getEmotions(params);
                break;
            case "universe-performance-activity":
                date = getDateTimeRangeByType(
                    filter.timeframe,
                    filter.startDate,
                    filter.endDate
                )
                params = {
                    interval: date.interval,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                    profiling: true,
                }
                this.getExposureSocmed(params);
                break;
            case "universe-performance-response":
                date = getDateTimeRangeByType(
                    filter.timeframe,
                    filter.startDate,
                    filter.endDate
                )
                params = {
                    interval: date.interval,
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: (platform == "global-asia") ? "all" : platform,
                }
                this.getExposureSocmed(params);
                break;
            case "activity-performance-profile":
                date = getDateTimeRangeByType(
                    filter.timeframe,
                    filter.startDate,
                    filter.endDate
                )
                params = {
                    interval: date.interval,
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                    profiling: true,
                }
                this.getExposureSocmed(params);
                break;
            case "activity-performance-response":
                date = getDateTimeRangeByType(
                    filter.timeframe,
                    filter.startDate,
                    filter.endDate
                )
                params = {
                    interval: date.interval,
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: (platform == "global-asia") ? "all" : platform,
                }
                this.getExposureSocmed(params);
                break;
            case "universe-sentiment-activity":
                params = {
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    platform: platform,
                    profiling: true
                }
                this.getSentimentSocmed(params);
                break;
            case "universe-sentiment-response":
                params = {
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    platform: platform,
                }
                this.getSentimentSocmed(params);
                break;
            case "activity-sentiment-profile":
                params = {
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    platform: platform,
                    profiling: true
                }
                this.getSentimentSocmed(params);
                break;
            case "activity-sentiment-response":
                params = {
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    platform: (platform == "global-asia") ? "all" : platform,
                }
                this.getSentimentSocmed(params);
                break;
            case "universe-wordcloud-activity":
                params = {
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                    profiling: true,
                    size: 10,
                    aggregation_by: "hashtag"
                }
                this.getTrending(params);
                break;
            case "universe-wordcloud-response":
                params = {
                    person: entity.key,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                    size: 10,
                    aggregation_by: "hashtag"
                }
                this.getTrending(params);
                break;
            case "activity-wordcloud-profile":
                params = {
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: platform,
                    profiling: true,
                    size: 10,
                    aggregation_by: "hashtag"
                }
                this.getTrending(params);
                break;
            case "activity-wordcloud-response":
                params = {
                    person: user.keyword,
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: (platform == "global-asia") ? "all" : platform,
                    size: 10,
                    aggregation_by: "hashtag"
                }
                this.getTrending(params);
                break;
            case "audience-wordcloud":
                params = {
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    index: "smm",
                    size: 10,
                    aggregation_by: "keyword",
                    location: user.location
                }
                this.getTrending(params);
                break;
            case "activity-news-media-share":
                params = {
                    cluster_id: (issue.key) ? issue.key : "",
                    start_date: filter.startDate,
                    end_date: filter.endDate,
                    order: "DESC",
                    size: 10,
                    aggregation_by: "MEDIA",
                    user: user.keyword,
                }
                this.getIssueChart(params);
                break;
            default:
                break;
        }
    }

    updateLoader(loading, data, error = null) {
        this.setState({ loading: loading, data: data, error: error })
    }

    shouldComponentUpdate() {
        return true;
    }

    UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            prevProps.platform != this.props.platform ||
            this.props.keyword != prevProps.keyword
            ||
            (
                // kondisi update filter 
                (
                    prevProps.filter.startDate != this.props.filter.startDate
                    ||
                    prevProps.filter.endDate != this.props.filter.endDate
                    ||
                    prevProps.filter.locations != this.props.filter.locations
                    ||
                    prevProps.filter.location != this.props.filter.location
                    ||
                    prevProps.filter.timeframe != this.props.filter.timeframe
                )
                &&
                (
                    this.props.cPage == this.props.filter.page.name
                )
            )
            ||
            (
                // kondisi update issue 
                prevProps.issue.key != this.props.issue.key
                &&
                this.props.cPage == this.props.issue.page
            )

        ) {

            const data = this.getData();
        }
    }

    getSentiment(params) {
        let data = null;

        sentiment(params)
            .then((response) => {
                if (response.status === true && response.total > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, data, error)
            });
    }
    getMediaChart(params) {
        let data = null;

        mediaShare(params)
            .then((response) => {
                if (response.status === true && response.total > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, data, error)
            });
    }
    getExposureNews(params) {
        let data = null;
        exposure(params)
            .then((response) => {
                if (response.status === true && response.total > 0) {
                    data = response.data;
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, data, error)
            });
    }
    getEmotions(params) {
        let data = null;
        emotions(params)
            .then((response) => {
                if (response.status === true) {
                    data = response.data;
                }

                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, null, error)
            });
    }
    getExposureSocmed(params) {
        let data = null;
        exposureSocmed(params)
            .then((response) => {
                if (response.status === true) {
                    data = response.data;
                }

                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, null, error)
            });
    }
    getSentimentSocmed(params) {
        let data = null;

        sentimentSocmed(params)
            .then((response) => {

                if (response.status === true) {
                    data = new Array()
                    data[0] = response.data
                }

                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, null, error)
            });
    }
    getTrending(params) {
        let data = null;
        trending(params)
            .then((response) => {
                if (response.status === true) {
                    data = response.data;
                }

                this.updateLoader(false, data)
            })
            .catch((error) => {

                this.updateLoader(false, null, error)
            });
    }
    getIssueChart(params) {
        let data = null;
        issueChart(params)
            .then((response) => {
                if (response.status === true) {
                    data = response.data;
                }

                this.updateLoader(false, data)
            })
            .catch((error) => {

                this.updateLoader(false, null, error)
            });
    }
    getIssueSentiment(params) {
        let data = null;
        issueSentiment(params)
            .then((response) => {
                if (response.status === true) {
                    data = [
                        {
                            negative: (response.data.negative) ? response.data.negative : 0,
                            neutral: (response.data.neutral) ? response.data.neutral : 0,
                            positive: (response.data.positive) ? response.data.positive : 0,
                        }
                    ]
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {

                this.updateLoader(false, null, error)
            });
    }

    viewDonut(analyze, type, data) {
        return (
            <DonutChartView analyze={analyze} data={data} />
        );
    }
    viewBar(analyze, type, data) {
        return (
            <BarChartView analyze={analyze} data={data} />
        );
    }
    viewLine(analyze, type, data) {
        return (
            <LineChartView analyze={analyze} data={data} />
        );
    }
    viewWordCloud(analyze, type, data) {
        return (
            <WordCloudChartView analyze={analyze} data={data} />
        );
    }
    viewPolar(analyze, type, data) {
        return (
            <PolarChartView analyze={analyze} data={data} />
        );
    }

    render() {
        let { analyze, type, filter } = this.props
        let { data, loading } = this.state;

        if (loading === false && data != null) {
            switch (analyze) {
                case "universe-news-media-share":
                case "universe-news-sentiment":
                case "universe-sentiment-activity":
                case "universe-sentiment-response":
                case "activity-news-media-share":
                case "activity-news-sentiment":
                case "activity-sentiment-profile":
                case "activity-sentiment-response":
                    return this.viewBar(analyze, type, data);
                    break;
                case "activity-news-exposure":
                case "universe-news-exposure":
                case "universe-performance-activity":
                case "universe-performance-response":
                case "activity-performance-profile":
                case "activity-performance-response":
                    return this.viewLine(analyze, type, data);
                    break;
                case "universe-emotion-activity":
                case "universe-emotion-response":
                case "activity-emotion-profile":
                case "activity-emotion-response":
                    return this.viewPolar(analyze, type, data);
                    break;
                default:
                    return this.viewWordCloud(analyze, type, data);
                    break;
            }
        } else {
            return (
                <PlaceHolder loading={loading} data={data} />
            )
        }
    }
}


const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity, issue } = state;

    return {
        user,
        filter,
        entity,
        issue,
    };
};

export default connect(mapStateToProps)(ChartController);
