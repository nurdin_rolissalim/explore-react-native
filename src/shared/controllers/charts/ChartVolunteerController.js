import React from "react";
import DonutChartView from 'app/shared/ui/charts/DonutChartView';
import BarChartView from 'app/shared/ui/charts/BarChartView';
import LineChartView from 'app/shared/ui/charts/LineChartView';
import PolarChartView from 'app/shared/ui/charts/PolarChartView';
import WordCloudChartView from 'app/shared/ui/charts/WordCloudChartView';
import PlaceHolder from 'app/shared/ui/utility/PlaceHolder';
import MapsView from 'app/shared/ui/maps/MapsView';

import {
    byGender,
    byReligion,
    byEducation,
} from "app/service/RecruitmentVolunteerService";
import {
    count,
} from "app/service/EventVolunteerService";

import {
    countCategory
} from "app/service/CampaignVolunteerService";

import { connect } from "react-redux";
import { getDateTimeRangeByType } from 'app/helpers/DateHelper'

class ChartVolunteerController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            analyze: props.analyze,
        };
    }

    componentDidMount() {
        this.getData()
    }

    getData() {
        let { analyze, filter, user } = this.props

        let params
        let date;
        let data = null;
        let type_location = "1";
        let location = user.location;

        this.updateLoader(true, null, false);
        if (user.type_location === "city") {
            type_location = "2";
            if (location.indexOf("kab. ") > -1) {
                location = "kabupaten " + location.replace("kab. ", "")
            }
        }

        params = new Array(
            "region=" + encodeURIComponent(location),
            "type_location=" + type_location,
            "start_date=",
            "end_date=",
            "code_workspace=" + user.workspace
        )
        switch (analyze) {
            case "volunteer-recruitment-gender":
                this.getGender(params)
                break;
            case "volunteer-recruitment-religion":
                this.getReligion(params)
                break;
            case "volunteer-recruitment-education":
                this.getEducation(params)
                break;
            case "volunteer-recruitment-campaign":
                this.getCampaign(params)
                break;
            case "volunteer-event":
                this.getEvents(params)
                break;
            default:
                this.getEvents(params)
                break;
        }
    }

    updateLoader(loading, data, error = null) {
        this.setState({ loading: loading, data: data, error: error })
    }

    shouldComponentUpdate() {
        return true;
    }

    UNSAFE_componentWillUpdate(prevProps, prevState, snapshot) {
        if (
            prevProps.type != this.props.type ||
            (
                // kondisi update filter 
                (
                    prevProps.filter.startDate != this.props.filter.startDate
                    ||
                    prevProps.filter.endDate != this.props.filter.endDate
                    ||
                    prevProps.filter.locations != this.props.filter.locations
                    ||
                    prevProps.filter.location != this.props.filter.location
                    ||
                    prevProps.filter.timeframe != this.props.filter.timeframe
                )
                &&
                (
                    this.props.cPage == this.props.filter.page.name
                )
            )
            ||
            (
                // kondisi update issue 
                prevProps.issue.key != this.props.issue.key
                &&
                this.props.cPage == this.props.issue.page
            )

        ) {

            const data = this.getData();
        }
    }

    getGender(params) {
        let data = null;

        byGender(params)
            .then((response) => {
                if (response.data.length > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, data, error)
            });
    }
    getReligion(params) {
        let data = null;

        byReligion(params)
            .then((response) => {
                if (response.data.length > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, data, error)
            });
    }

    getEducation(params) {
        let data = null;

        byEducation(params)
            .then((response) => {
                if (response.data.length > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, data, error)
            });
    }
    getCampaign(params) {
        let data = null;

        countCategory(params)
            .then((response) => {
                if (response.data.length > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, data, error)
            });
    }
    getEvents(params) {
        let data = null;

        count(params)
            .then((response) => {
                if (response.data.length > 0) {
                    data = response.data
                }
                this.updateLoader(false, data)
            })
            .catch((error) => {
                this.updateLoader(false, data, error)
            });
    }

    componentWillUnmount() {
        this.updateLoader(false, null, false)
    }

    viewDonut() {
        let { analyze } = this.props;
        let { data } = this.state
        return (
            <DonutChartView analyze={analyze} data={data} />
        );
    }
    viewBar() {
        let { analyze } = this.props;
        let { data } = this.state
        return (
            <BarChartView analyze={analyze} data={data} />
        );
    }
    viewLine() {
        let { analyze } = this.props;
        let { data } = this.state
        return (
            <LineChartView analyze={analyze} data={data} />
        );
    }
    viewWordCloud() {
        let { analyze } = this.props;
        let { data } = this.state
        return (
            <WordCloudChartView analyze={analyze} data={data} />
        );
    }
    viewPolar() {
        let { analyze } = this.props;
        let { data } = this.state
        return (
            <PolarChartView analyze={analyze} data={data} />
        );
    }
    viewMaps() {
        let { analyze } = this.props;
        let { data } = this.state
        return (
            <MapsView analyze={analyze} data={data} />
        );
    }


    render() {
        let { analyze, type, filter } = this.props
        let { data, loading } = this.state;

        if (loading === false && data != null) {
            switch (analyze) {
                case "volunteer-recruitment-gender":
                case "volunteer-recruitment-religion":
                    return this.viewDonut();
                    break;
                case "volunteer-recruitment-education":
                case "volunteer-recruitment-campaign":
                case "volunteer-event":
                    return this.viewBar();
                    break;
                case "activity-news-exposure":
                case "activity-performance-response":
                    return this.viewLine();
                    break;
                case "universe-emotion-activity":
                    return this.viewPolar();
                    break;
                case "volunteer-recruitment-map":
                case "volunteer-event-map":
                    return this.viewMaps();
                    break;
                default:
                    return this.viewWordCloud();
                    break;
            }
        } else {
            return (
                <PlaceHolder loading={loading} data={data} />
            )
        }
    }
}


const mapStateToProps = (state) => {
    const user = state.auth.userData;
    const { filter, entity, issue } = state;

    return {
        user,
        filter,
        entity,
        issue,
    };
};

export default connect(mapStateToProps)(ChartVolunteerController);
