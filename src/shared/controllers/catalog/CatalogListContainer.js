import React from "react";
import { connect } from "react-redux";
import { FlatList, View } from "react-native";
import {
  ListItem,
  Left,
  Thumbnail,
  Body,
  Right,
  Icon,
  Text,
} from "native-base";
import { fetchCatalog, selectCatalog } from "../../../redux/ducks";
import { WrapperContent, TitleSection } from "../../ui/StyledComponents";
import PlaceHolder from "../../ui/utility/PlaceHolder";
import { getAsset } from "../../helpers/helpers";
import { directoryImage } from "../../../config";

const uri = require("../../../assets/images/politica-icon.png");

// const uri = { uri: `${directoryImage}person/donald+trump.jpg` };

class CatalogListContainer extends React.Component {
  constructor(props) {
    super(props);

    // this.handlerClickItem = this.handlerClickItem.bind(this);
  }

  componentDidMount() {
    this.props.fetchCatalog({
      keyword: "",
      order: "desc",
      order_by: "popular",
      page: 1,
      size: 15,
      type: "person",
    });
  }

  handlerClickItem(e) {
    // this.props.selectCatalog(e);
  }

  renderItem({ item }, self) {
    return (
      <ListItem avatar onPress={() => self.handlerClickItem(item)}>
        <Left>
          <Thumbnail source={uri} />
        </Left>
        <Body>
          <Text>{item.name}</Text>
          <Text note>{item.count}</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  }

  render() {
    const { catalogs, isFetching } = this.props.catalog;
    if (isFetching) {
      return <PlaceHolder loading={isFetching} data={catalogs} />;
    }

    return (
      <>
        <WrapperContent>
          <TitleSection>CATALOG</TitleSection>
        </WrapperContent>
        <FlatList
          data={catalogs}
          renderItem={(items) => this.renderItem(items, this)}
          keyExtractor={(user) => user.id.toString()}
        />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const { catalog } = state;
  return { catalog };
};

const mapDispatchToProps = {
  fetchCatalog,
  selectCatalog,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CatalogListContainer);
