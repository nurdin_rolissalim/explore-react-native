import React from "react";
import { connect } from "react-redux";
import { FlatList, View } from "react-native";
import {
  ListItem,
  Left,
  Thumbnail,
  Body,
  Right,
  Icon,
  Text,
} from "native-base";
import {
  fetchManagementUser,
  fetchManagementRole,
  selectManagementUser,
} from "../../../redux/ducks";
import { WrapperContent, TitleSection } from "../../ui/StyledComponents";
import PlaceHolder from "../../ui/utility/PlaceHolder";
import { getAsset } from "../../helpers/helpers";

class AccountManagementContainer extends React.Component {
  constructor(props) {
    super(props);

    // this.handlerClickItem = this.handlerClickItem.bind(this);
  }

  componentDidMount() {
    this.props.fetchManagementUser({
      page: 1,
      limit: 16,
      sortby: "full_name",
    });
    this.props.fetchManagementRole();
  }

  handlerClickItem(e) {
    this.props.selectManagementUser(e);
    this.props.navigation.navigate("user-detail-management");
  }

  renderItem({ item }, self) {
    return (
      <ListItem avatar onPress={() => self.handlerClickItem(item)}>
        <Left>
          <Thumbnail source={{ uri: getAsset(`${item.images}`) }} />
        </Left>
        <Body>
          <Text>{item.full_name}</Text>
          <Text note>{item.email}</Text>
        </Body>
        <Right>
          <Icon name="arrow-forward" />
        </Right>
      </ListItem>
    );
  }

  render() {
    const { users, isFetching } = this.props.user;
    if (isFetching) {
      return <PlaceHolder loading={isFetching} data={users} />;
    }

    return (
      <>
        <WrapperContent>
          <TitleSection>USER MANAGEMENT</TitleSection>
        </WrapperContent>
        <FlatList
          data={users}
          renderItem={(items) => this.renderItem(items, this)}
          keyExtractor={(user) => user.id_user.toString()}
        />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const { management_user: user, management_role: role } = state;
  return { user, role };
};

const mapDispatchToProps = {
  fetchManagementUser,
  fetchManagementRole,
  selectManagementUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AccountManagementContainer);
