import React, { Component } from 'react';
import SearchView from 'app/shared/ui/search/SearchView';

class SearchController extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: "",
            loading: false,
            typing: false,
            typingTimeout: 0
        }
    }

    changeKeyword = (keyword) => {
        this.props.callback(keyword)
    }

    viewSearch() {
        return (
            <>
                <SearchView
                    analyze={this.props.analyze}
                    value={this.state.keyword}
                    callback={(keyword) => this.changeKeyword(keyword)}
                    type={this.props.type}
                />
            </>
        )
    }

    resetKeyword() {
        this.setState({ keyword: this.props.keyword })
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.keyword != prevState.keyword) {
            this.setState({ keyword: this.props.keyword })
        }
    }

    componentWillUnmount() {
        this.setState({ keyword: "" })
    }

    render() {
        const { loading, keyword } = this.state;
        const { analyze } = this.props;

        switch (analyze) {
            case "universe":
                return this.viewSearch()
                break;
            default:
                return (
                    <>
                    </>
                )
                break;
        }

    }
}

export default SearchController;