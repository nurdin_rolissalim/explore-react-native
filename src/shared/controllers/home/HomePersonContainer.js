import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Thumbnail, Text, ListItem, View } from "native-base";
import { fetchPerson, selectPerson, fetchContentNews } from "app/redux/ducks";
import PlaceHolder from "app/shared/ui/utility/PlaceHolder";
import { Truncate } from "app/helpers/GlobalFunction";
import { FlatList } from "react-native";
import { directoryImage } from "../../../config";

const uri = require("../../../assets/images/politica-icon.png");

class HomePersonContainer extends React.Component {
  constructor(props) {
    super(props);

    this.onPressItem = this.onPressItem.bind(this);
  }

  componentDidMount() {
    this.props.fetchPerson();
  }

  onPressItem(e) {
    this.props.selectPerson(e);
    this.props.fetchContentNews(e);
  }

  getPersonAsset(item) {
    const filename = item.key.replace(/ /g, "+");
    const result = `${directoryImage}person/${filename}.jpg`;

    return result;
  }

  render() {
    const { persons, isFetching } = this.props;
    console.log("persons ", persons);

    if (isFetching) {
      return <PlaceHolder loading={isFetching} data={persons} />;
    }

    return (
      <View style={{ height: 90, margin: 5 }}>
        <FlatList
          style={[{ paddingTop: 5 }]}
          keyExtractor={(_, index) => index.toString()}
          data={persons}
          renderItem={({ item }) => (
            <View
              style={{ marginHorizontal: 8 }}
              key={item.key}
              onTouchEnd={() => this.onPressItem(item)}
            >
              <Thumbnail
                circle
                source={{
                  uri: this.getPersonAsset(item),
                }}
              />
              <Text style={{ textAlign: "center", fontSize: 16, marginTop: 3 }}>
                {Truncate(item.key, 6, "...")}
              </Text>
            </View>
          )}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }
}

HomePersonContainer.propTypes = {
  persons: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  fetchPerson: PropTypes.func.isRequired,
  selectPerson: PropTypes.func.isRequired,
  fetchContentNews: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const { persons, isFetching } = state.home_person;
  return { persons, isFetching };
};

const mapDispatchToProps = { fetchPerson, selectPerson, fetchContentNews };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePersonContainer);
