import React from "react";
import { connect } from "react-redux";
import {
  FlatList,
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
} from "react-native";
import PropTypes from "prop-types";
import { fetchIssue, selectIssue, fetchIssueNews } from "../../../redux/ducks";
import PlaceHolder from "../../ui/utility/PlaceHolder";

class HomeIssueContainer extends React.Component {
  constructor(props) {
    super(props);

    this.issueItemClicked = this.issueItemClicked.bind(this);
  }

  componentDidMount() {
    this.props.fetchIssue();
  }

  issueItemClicked(e) {
    this.props.selectIssue(e);
    this.props.fetchIssueNews(e);
  }

  render() {
    const { issues, isFetching } = this.props;

    if (isFetching) {
      return <PlaceHolder loading={isFetching} data={issues} />;
    }

    return (
      <View style={styles.container}>
        <FlatList
          style={{ paddingTop: 5 }}
          data={issues}
          renderItem={({ item }) => (
            <IssueItem
              key={item.id}
              issue={item}
              itemClicked={this.issueItemClicked}
            />
          )}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }
}

function IssueItem({ issue, itemClicked }) {
  const { label } = issue;

  return (
    <View style={[styles.issuItemContainer]}>
      <TouchableOpacity
        danger
        style={styles.issuItemButton}
        onPress={() => itemClicked(issue)}
      >
        <Text style={styles.issuItemLabel}>{label}</Text>
      </TouchableOpacity>
    </View>
  );
}

IssueItem.propTypes = {
  issue: PropTypes.object.isRequired,
  itemClicked: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    // width: 200,
    height: 80,
    margin: 5,
  },
  issuItemContainer: {
    padding: 5,
  },
  issuItemButton: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: "rgba(250, 66, 59, 0.15)",
    borderColor: "#fa423b",
    borderRadius: 100,
  },
  issuItemLabel: {
    fontSize: 16,
    color: "#fa423b",
  },
});

const mapStateToProps = (state) => {
  const { issues, isFetching, isInvalidate, selectedIssue } = state.home_issue;
  return { issues, isFetching, isInvalidate, selectedIssue };
};

const mapDispatchToProps = { fetchIssue, selectIssue, fetchIssueNews };

export default connect(mapStateToProps, mapDispatchToProps)(HomeIssueContainer);
