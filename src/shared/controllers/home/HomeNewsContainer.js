import React from "react";
import { connect } from "react-redux";

import {
  WrapperContent,
  TitleSection,
  SubtitleSection,
} from "app/shared/ui/StyledComponents";
import NewsItemVertical from "app/shared/ui/NewsItemVertical";
import NewsHeadline from "app/shared/ui/NewsHeadline";
import { View, FlatList } from "react-native";
import PlaceHolder from "app/shared/ui/utility/PlaceHolder";
import { fetchContentNews } from "app/redux/ducks";

class HomeNewsContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchContentNews();
  }

  render() {
    const { news, isFetching } = this.props;

    if (isFetching) {
      return <PlaceHolder loading={isFetching} data={news} />;
    }

    return (
      <WrapperContent>
        {/* <TitleSection>NEWS</TitleSection> */}
        {/* <NewsHeadline
          title={news[0].title}
          source={news[0].source}
          datePub={news[0].created_at}
          images={news[0].images[0]}
        /> */}
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <TitleSection>NEWS</TitleSection>
          <SubtitleSection
            onPress={() =>
              this.props.navigation.navigate("detail-news", {
                title: "Other news",
              })
            }
            style={{ position: "absolute", right: 0 }}
          >
            See other news
          </SubtitleSection>
        </View>
        <FlatList
          data={news}
          keyExtractor={(item) => item.id_hash}
          renderItem={(item) => <NewsItem item={item} />}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </WrapperContent>
    );
  }
}

function NewsItem({ item }) {
  return (
    <View
      style={{
        width: 200,
        marginRight: 5,
        overflow: "hidden",
        padding: 5,
      }}
    >
      <NewsItemVertical
        title={item.item.title}
        source={item.item.source}
        datePub={item.item.created_at}
        images={item.item.images[0]}
      />
    </View>
  );
}

const mapStateToProps = (state) => {
  const { news, isFetching } = state.home_news;
  return { news, isFetching };
};

const mapDispatchToProps = { fetchContentNews };

export default connect(mapStateToProps, mapDispatchToProps)(HomeNewsContainer);
