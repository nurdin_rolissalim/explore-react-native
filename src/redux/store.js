import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-community/async-storage";
import thunk from "redux-thunk";
import {
  authReducer,
  filterReducer,
  entityReducer,
  issueReducer,
  pageReducer,
  typeReducer,
  homeIssueReducer,
  homeNewsReducer,
  homePersonReducer,
  managementUserReducer,
  managementRoleReducer,
  catalogReducer,
} from "./ducks";

const rootReducer = combineReducers({
  auth: authReducer,
  filter: filterReducer,
  entity: entityReducer,
  page: pageReducer,
  type: typeReducer,
  issue: issueReducer,
  home_issue: homeIssueReducer,
  home_news: homeNewsReducer,
  home_person: homePersonReducer,
  management_user: managementUserReducer,
  management_role: managementRoleReducer,
  catalog: catalogReducer,
});

const middlewares = [thunk];

let composeEnhancers = compose;

if (__DEV__) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const persistConfig = {
  key: "politica",
  storage: AsyncStorage,
  whitelist: ["auth", "filter", "issue", "entity"], // only auth and filter state will be persisted
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(...middlewares)),
);

export const persistor = persistStore(store);
