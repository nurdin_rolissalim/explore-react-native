/**
 * Creators
 */
const SET_PAGE = "politica/page/SET_PAGE";
const GET_PAGE = "politica/page/GET_PAGE";
const DELETE_PAGE = "politica/page/DELETE_PAGE";

/**
 * Initial state
 */
const INITIAL_STATE = {
  name: "home",
  type: "person",
  platform: "news"
};

/**
 * Reducer
 */
export function pageReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_PAGE: {
      const { name, type, platform } = action.page;
      return {
        ...state,
        name: name,
        type: type,
        platform: platform,
      };
    }

    case DELETE_PAGE: {
      return {
        ...state,
        INITIAL_STATE,
      };
    }
    case GET_PAGE: {
      return state;
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const setPage = (page) => {
  return {
    type: SET_PAGE,
    page,
  };
};

export const deletePage = () => ({
  type: "DELETE_PAGE",
});

export const getPage = () => {
  return {
    type: GET_PAGE,
  };
};

/**
 * Side effects aka async actions
 */
