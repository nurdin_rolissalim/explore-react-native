import moment from "moment";
import { issueTop } from "../../../service";

/**
 * Creators
 */
const REQUEST_ISSUE = "politica/auth/REQUEST_ISSUE";
const RECEIVE_ISSUE = "politica/auth/RECEIVE_ISSUE";
const INVALIDATE_ISSUE = "politica/auth/INVALIDATE_ISSUE";
const SELECT_ISSUE = "politica/auth/SELECT_ISSUE";

/**
 * Initial state
 */
const initialState = {
  issues: [],
  isFetching: false,
  isInvalidate: false,
  selectedIssue: "",
};

/**
 * Reducer
 */
export function reducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_ISSUE: {
      return {
        ...state,
        selectedIssue: action.selectedIssue,
      };
    }

    case REQUEST_ISSUE: {
      return {
        ...state,
        isFetching: true,
        isInvalidate: false,
      };
    }

    case RECEIVE_ISSUE: {
      return {
        ...state,
        issues: action.issues,
        isFetching: false,
        isInvalidate: false,
        selectedIssue: action.issues[0],
      };
    }

    case INVALIDATE_ISSUE: {
      return {
        ...state,
        isFetching: false,
        isInvalidate: true,
      };
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const selectIssue = (selectedIssue) => {
  return {
    type: SELECT_ISSUE,
    selectedIssue,
  };
};

export const requestIssue = () => {
  return {
    type: REQUEST_ISSUE,
  };
};

export const receiveIssue = (issues) => {
  return {
    type: RECEIVE_ISSUE,
    issues,
  };
};

export const invalidateIssue = () => {
  return {
    type: INVALIDATE_ISSUE,
  };
};

/**
 * Side effects aka async actions
 */
export const fetchIssue = () => {
  const params = {
    order: "DESC",
    size: 10,
    timezone: "Asia/Jakarta",
    start_date: moment(new Date()).format("YYYYMMDD").toString(),
    end_date: moment(new Date()).format("YYYYMMDD").toString(),
    interval: parseInt(moment(new Date()).format("HH"), 10),
  };
  return (dispatch) => {
    dispatch(requestIssue());
    issueTop(params)
      .then((res) => {
        dispatch(receiveIssue(res.data));
      })
      .catch((err) => {
        dispatch(invalidateIssue());
      });
  };
};
