import moment from "moment";
import { trending } from "../../../service";

/**
 * Creators
 */
const REQUEST_PERSON = "REQUEST_PERSON";
const RECEIVE_PERSON = "RECEIVE_PERSON";
const INVALIDATE_PERSON = "INVALIDATE_PERSON";
const SELECT_PERSON = "SELECT_PERSON";

/**
 * Initial state
 */
const initialState = {
  persons: [],
  isFetching: false,
  isInvalidate: false,
  selectedPerson: "",
};

/**
 * Reducer
 */
export function reducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_PERSON: {
      return {
        ...state,
        selectedPerson: action.selectedPerson,
      };
    }

    case REQUEST_PERSON: {
      return {
        ...state,
        isFetching: true,
        isInvalidate: false,
      };
    }

    case RECEIVE_PERSON: {
      return {
        ...state,
        persons: action.persons,
        isFetching: false,
        isInvalidate: false,
        selectedPerson: action.persons[0],
      };
    }

    case INVALIDATE_PERSON: {
      return {
        ...state,
        isInvalidate: true,
      };
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const selectPerson = (selectedPerson) => {
  return {
    type: SELECT_PERSON,
    selectedPerson,
  };
};

export const requestPerson = () => {
  return {
    type: REQUEST_PERSON,
  };
};

export const receivePerson = (persons) => {
  return {
    type: RECEIVE_PERSON,
    persons,
  };
};

export const invalidatePerson = () => {
  return {
    type: INVALIDATE_PERSON,
  };
};

/**
 * Side effects aka async actions
 */
export const fetchPerson = () => {
  const params = {
    aggregation_by: "PERSON",
    order: "desc",
    index: "news",
    timezone: "Asia/Jakarta",
    size: 10,
    start_date: moment(new Date()).format("YYYYMMDD").toString(),
    end_date: moment(new Date()).format("YYYYMMDD").toString(),
  };

  return (dispatch) => {
    dispatch(requestPerson());
    trending(params)
      .then((res) => {
        dispatch(receivePerson(res.data));
      })
      .catch((err) => {
        dispatch(invalidatePerson());
      });
  };
};
