import moment from "moment";
import { contentNews, issueNews } from "../../../service";

/**
 * Creators
 */
const REQUEST_NEWS = "politica/home/REQUEST_NEWS";
const RECEIVE_NEWS = "politica/home/RECEIVE_NEWS";
const INVALIDATE_NEWS = "politica/home/INVALIDATE_NEWS";

/**
 * Initial state
 */
const initialState = {
  news: [],
  isFetching: false,
  isInvalidate: false,
};

/**
 * Reducer
 */
export function reducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_NEWS: {
      return {
        ...state,
        isFetching: true,
        isInvalidate: false,
      };
    }

    case RECEIVE_NEWS: {
      return {
        ...state,
        news: action.news,
        isFetching: false,
        isInvalidate: false,
      };
    }

    case INVALIDATE_NEWS: {
      return {
        ...state,
        isInvalidate: true,
      };
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const requestNews = () => {
  return {
    type: REQUEST_NEWS,
  };
};

export const receiveNews = (news) => {
  return {
    type: RECEIVE_NEWS,
    news,
  };
};

export const invalidateNews = () => {
  return {
    type: INVALIDATE_NEWS,
  };
};

/**
 * Side effects aka async actions
 */
export const fetchContentNews = (person = {}) => {
  const params = {
    size: 30,
    start_date: moment(new Date()).format("YYYYMMDD").toString(),
    end_date: moment(new Date()).format("YYYYMMDD").toString(),
    order: "desc",
    page: 1,
    time_zone: "Asia/Jakarta",
    person: person.key,
  };

  return (dispatch) => {
    dispatch(requestNews());
    contentNews(params)
      .then((res) => {
        dispatch(receiveNews(res.data));
      })
      .catch((err) => {
        dispatch(invalidateNews());
      });
  };
};

export const fetchIssueNews = (issue = {}) => {
  const params = {
    size: 30,
    start_date: moment(new Date()).format("YYYYMMDD").toString(),
    end_date: moment(new Date()).format("YYYYMMDD").toString(),
    order: "desc",
    page: 1,
    time_zone: "Asia/Jakarta",
    cluster_id: issue.id,
  };

  return (dispatch) => {
    dispatch(requestNews());
    issueNews(params)
      .then((res) => {
        dispatch(receiveNews(res.data));
      })
      .catch((err) => {
        dispatch(invalidateNews());
      });
  };
};
