import { login } from "../../service/AuthService";
import * as router from "../../shared/helpers/router";

/**
 * Creators
 */
const SIGN_IN_SUCCESS = "politica/auth/SIGN_IN_SUCCESS";
// const SIGN_IN = "politica/auth/SIGN_IN";
// const SIGN_IN_FAILURE = "politica/auth/SIGN_IN_FAILURE";

const SIGN_OUT_SUCCESS = "politica/auth/SIGN_OUT_SUCCESS";

/**
 * Initial state
 */
const INITIAL_STATE = {
  userData: {},
  isSignedIn: false,
};

/**
 * Reducer
 */
export function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SIGN_IN_SUCCESS: {
      const { userData } = action.payload;
      return {
        ...state,
        userData: {
          ...state.userData,
          id_user: userData.id_user,
          username: userData.username,
          fullname: userData.full_name,
          email: userData.email,
          avatar: userData.images,
          keyword: userData.keyword,
          location: userData.location,
          workspace: userData.workspace,
          period: userData.period,
          role_id: userData.role_id,
          type_location: userData.type_location,
        },
        isSignedIn: true,
      };
    }

    case SIGN_OUT_SUCCESS: {
      return {
        ...state,
        userData: {},
        isSignedIn: false,
      };
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const signInSuccess = (userData) => ({
  type: SIGN_IN_SUCCESS,
  payload: {
    userData,
  },
});

export const signOutSuccess = () => ({
  type: SIGN_OUT_SUCCESS,
});

/**
 * Side effects aka async actions
 */
export const signIn = (userData) => {
  const { username, password } = userData;
  console.log("login", userData);

  return (dispatch) => {
    login({ username, password })
      .then((response) => {
        console.log("response", response);

        if (response.status === true) {
          dispatch(signInSuccess(response.data));
          router.navigate("dashboard");
        } else {
          // throw new Error('failed to login');
          console.log("responsefailedr");
        }
      })
      .catch((error) => {
        console.log("response error", error);
        // Toast.show({
        //   text: "Failed to log in, check again username/password",
        //   position: "top",
        //   buttonText: "Okay",
        //   duration: 2000,
        // });
      });
  };
};

export const signOut = () => {
  return (dispatch) => {
    dispatch(signOutSuccess());
    router.navigate("login");
  };
};
