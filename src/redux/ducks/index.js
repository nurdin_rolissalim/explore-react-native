export { reducer as authReducer, signInSuccess, signIn, signOut } from "./auth";

export {
  entityReducer,
  getEntity,
  setEntity,
  deleteEntity,
} from "./entity";

export { filterReducer, setFilter, getFilter } from "./filter";

export { pageReducer, setPage, deletePage, getPage } from "./page";

export {
  issueReducer,
  setIssue,
  deleteIssue,
  getIssue
} from "./issue";

export {
  reducer as typeReducer,
  setType,
  deleteType
} from "./type";

export {
  reducer as homeIssueReducer,
  fetchIssue,
  invalidateIssue,
  receiveIssue,
  requestIssue,
  selectIssue,
} from "./home/homeIssue";

export {
  reducer as homeNewsReducer,
  fetchContentNews,
  fetchIssueNews,
  invalidateNews,
  receiveNews,
  requestNews,
} from "./home/homeNews";

export {
  reducer as homePersonReducer,
  fetchPerson,
  invalidatePerson,
  receivePerson,
  requestPerson,
  selectPerson,
} from "./home/homePerson";

export {
  reducer as managementRoleReducer,
  fetchManagementRole,
  invalidateManagementRole,
  receiveManagementRole,
  requestManagementRole,
  selectManagementRole,
} from "./management/managementRole";

export {
  reducer as managementUserReducer,
  fetchManagementUser,
  invalidateManagementUser,
  receiveManagementUser,
  requestManagementUser,
  selectManagementUser,
} from "./management/managementUser";

export {
  reducer as catalogReducer,
  fetchCatalog,
  invalidateCatalog,
  receiveCatalog,
  requestCatalog,
  selectCatalog,
} from "./catalog/catalogList";
