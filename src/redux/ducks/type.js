/**
 * Creators
 */
const SET_TYPE = "politica/type/SET_TYPE";
const DELETE_TYPE = "politica/type/DELETE_TYPE";

/**
 * Initial state
 */
const INITIAL_STATE = {
  name: "news",
  color: "",
};

/**
 * Reducer
 */
export function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_TYPE: {
      const { name, color } = action.payload;
      return {
        ...state,
        name,
        color,
      };
    }
    case DELETE_TYPE: {
      return {
        ...state,
        INITIAL_STATE,
      };
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const setType = ({ name, color }) => ({
  type: "DELETE_TYPE",
  payload: {
    name,
    color,
  },
});

export const deleteType = () => ({
  type: DELETE_TYPE,
});

/**
 * Side effects aka async actions
 */
