/**
 * digunakan untuk store person, organizayion, locaiton, facebook, twitter,instagarm
 */

/**
 * Creators
 */
const SET_ENTITY = "politica/entity/SET_ENTITY";
const GET_ENTITY = "politica/entity/GET_ENTITY";
const DELETE_ENTITY = "politica/entity/DELETE_ENTITY";

/**
 * Initial state
 */
const INITIAL_STATE = {
  entity: {
    key: "",
    value: "",
    analyze: "",
    page: "home",
    type: "person",
    platform: "news"
  },
};

/**
 * Reducer
 */
export function entityReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_ENTITY: {
      const { key, value, analyze, page, type, platform } = action.entity;
      return {
        key: key,
        value: value,
        analyze: analyze,
        page: page,
        type: type,
        platform: platform
      };
    }

    case DELETE_ENTITY: {
      return {
        INITIAL_STATE,
      };
    }
    case GET_ENTITY: {
      return state;
    }


    default:
      return state;
  }
}

/**
 * Action creators
 */
export const setEntity = (entity) => {
  return {
    type: SET_ENTITY,
    entity,
  };
};

export const getEntity = () => {
  return {
    type: GET_ENTITY,
  };
};

export const deleteEntity = () => {
  return {
    type: DELETE_ENTITY,
  }
};

/**
 * Side effects aka async actions
 */
