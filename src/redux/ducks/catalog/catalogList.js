import { catalog } from "../../../service";

/**
 * Creators
 */
const REQUEST_CATALOG_LIST = "politica/catalog/REQUEST_CATALOG_LIST";
const RECEIVE_CATALOG_LIST = "politica/catalog/RECEIVE_CATALOG_LIST";
const INVALIDATE_CATALOG_LIST = "politica/catalog/INVALIDATE_CATALOG_LIST";
const SELECT_CATALOG_LIST = "politica/catalog/SELECT_CATALOG_LIST";

/**
 * Initial state
 */
const initialState = {
  catalogs: [],
  isFetching: false,
  isInvalidate: false,
  selectedCatalog: "",
};

/**
 * Reducer
 */
export function reducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_CATALOG_LIST: {
      return {
        ...state,
        selectedCatalog: action.selectedCatalog,
      };
    }

    case REQUEST_CATALOG_LIST: {
      return {
        ...state,
        isFetching: true,
        isInvalidate: false,
      };
    }

    case RECEIVE_CATALOG_LIST: {
      return {
        ...state,
        catalogs: action.catalogs,
        isFetching: false,
        isInvalidate: false,
        selectedCatalog: action.catalogs[0],
      };
    }

    case INVALIDATE_CATALOG_LIST: {
      return {
        ...state,
        isFetching: false,
        isInvalidate: true,
      };
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const selectCatalog = (selectedCatalog) => {
  return {
    type: SELECT_CATALOG_LIST,
    selectedCatalog,
  };
};

export const requestCatalog = () => {
  return {
    type: REQUEST_CATALOG_LIST,
  };
};

export const receiveCatalog = (catalogs) => {
  return {
    type: RECEIVE_CATALOG_LIST,
    catalogs,
  };
};

export const invalidateCatalog = () => {
  return {
    type: INVALIDATE_CATALOG_LIST,
  };
};

/**
 * Side effects aka async actions
 */
export const fetchCatalog = (params) => {
  return (dispatch) => {
    dispatch(requestCatalog());
    catalog(params)
      .then((res) => {
        dispatch(receiveCatalog(res.data));
      })
      .catch((err) => {
        dispatch(invalidateCatalog());
      });
  };
};
