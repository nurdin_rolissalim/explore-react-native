/**
 * digunakan untuk store person, organizayion, locaiton, facebook, twitter,instagarm
 */

/**
 * Creators
 */
const SET_ISSUE = "politica/issue/SET_ISSUE";
const GET_ISSUE = "politica/issue/GET_ISSUE";
const DELETE_ISSUE = "politica/issue/DELETE_ISSUE";

/**
 * Initial state
 */
const INITIAL_STATE = {
    issue: {
        key: "",
        value: "",
        analyze: "",
        page: "home",
        type: "person",
        platform: "news"
    },
};

/**
 * Reducer
 */
export function issueReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_ISSUE: {
            const { key, value, analyze, page, type, platform } = action.issue;
            return {
                // ...state,
                key: key,
                value: value,
                analyze: analyze,
                page: page.name,
                type: type,
                platform: platform
            };
        }

        case DELETE_ISSUE: {
            return {
                // ...state,
                INITIAL_STATE,
            };
        }
        case GET_ISSUE: {
            return state;
        }

        default:
            return state;
    }
}

/**
 * Action creators
 */
export const setIssue = (issue) => {
    return {
        type: SET_ISSUE,
        issue,
    };
};

export const getIssue = () => {
    return {
        type: GET_ISSUE,
    };
};

export const deleteIssue = () => {
    return {
        type: DELETE_ISSUE,
    }
};

/**
 * Side effects aka async actions
 */
