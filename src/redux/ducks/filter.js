import moment from "moment";

/**
 * Creators
 */
const SET_FILTER = "politica/filter/SET_FILTER";
const GET_FILTER = "politica/filter/GET_FILTER";

/**
 * Initial state
 */
const INITIAL_STATE = {
  timeframe: "",
  startDate: "",
  endDate: "",
  location: [],
  locations: [],
  page: {
    name: "",
    platform: "",
    type: ""
  }
};

/**
 * Reducer
 */
export function filterReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_FILTER: {
      const { timeframe, startDate, endDate, locations, location, page } = action.filter;
      return {
        // ...state,
        startDate: startDate,
        endDate: endDate,
        location: location,
        locations: locations,
        timeframe: timeframe,
        // startDate: moment(new Date()).subtract(1, "month").format("YYYYMMDD"),
        // endDate: moment(new Date()).format("YYYYMMDD"),
        page: page
      };
    }
    case GET_FILTER: {
      return state;
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const setFilter = (filter) => {
  return {
    type: SET_FILTER,
    filter,
  };
};
// export const setFilter = (filter) => (
//   {
//   type: "SET_FILTER",
//   payload: {
//     filter
//   },
// });
export const getFilter = () => {
  return {
    type: GET_FILTER,
  };
};

/**
 * Side effects aka async actions
 */
