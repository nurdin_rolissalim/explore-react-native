import { getUsers } from "../../../service/ManagementService";

/**
 * Creators
 */
const REQUEST_MANAGEMENT_USER = "politica/management/REQUEST_MANAGEMENT_USER";
const RECEIVE_MANAGEMENT_USER = "politica/management/RECEIVE_MANAGEMENT_USER";
const INVALIDATE_MANAGEMENT_USER =
  "politica/management/INVALIDATE_MANAGEMENT_USER";
const SELECT_MANAGEMENT_USER = "politica/management/SELECT_MANAGEMENT_USER";

/**
 * Initial state
 */
const initialState = {
  users: [],
  isFetching: false,
  isInvalidate: false,
  selectedUser: "",
};

/**
 * Reducer
 */
export function reducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_MANAGEMENT_USER: {
      return {
        ...state,
        selectedUser: action.selectedUser,
      };
    }

    case REQUEST_MANAGEMENT_USER: {
      return {
        ...state,
        isFetching: true,
        isInvalidate: false,
      };
    }

    case RECEIVE_MANAGEMENT_USER: {
      return {
        ...state,
        users: action.users,
        isFetching: false,
        isInvalidate: false,
        selectedUser: action.users[0],
      };
    }

    case INVALIDATE_MANAGEMENT_USER: {
      return {
        ...state,
        isFetching: false,
        isInvalidate: true,
      };
    }

    default:
      return state;
  }
}

/**
 * Action creators
 */
export const selectManagementUser = (selectedUser) => {
  return {
    type: SELECT_MANAGEMENT_USER,
    selectedUser,
  };
};

export const requestManagementUser = () => {
  return {
    type: REQUEST_MANAGEMENT_USER,
  };
};

export const receiveManagementUser = (users) => {
  return {
    type: RECEIVE_MANAGEMENT_USER,
    users,
  };
};

export const invalidateManagementUser = () => {
  return {
    type: INVALIDATE_MANAGEMENT_USER,
  };
};

/**
 * Side effects aka async actions
 */
export const fetchManagementUser = (params) => {
  return (dispatch) => {
    dispatch(requestManagementUser());
    getUsers(params)
      .then((res) => {
        dispatch(receiveManagementUser(res.data));
      })
      .catch((err) => {
        dispatch(invalidateManagementUser());
      });
  };
};
