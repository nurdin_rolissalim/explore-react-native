import { getRoles } from "../../../service/ManagementService";

const REQUEST_MANAGEMENT_ROLE = "politica/management/REQUEST_MANAGEMENT_ROLE";
const RECEIVE_MANAGEMENT_ROLE = "politica/management/RECEIVE_MANAGEMENT_ROLE";
const INVALIDATE_MANAGEMENT_ROLE =
  "politica/management/INVALIDATE_MANAGEMENT_ROLE";
const SELECT_MANAGEMENT_ROLE = "politica/management/SELECT_MANAGEMENT_ROLE";

/**
 * Initial state
 */
const initialState = {
  roles: [],
  isFetching: false,
  isInvalidate: false,
  selectedRole: "",
};

/**
 * Reducer
 */
export function reducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_MANAGEMENT_ROLE: {
      return {
        ...state,
        selectedRole: action.selectedRole,
      };
    }
    case REQUEST_MANAGEMENT_ROLE: {
      return {
        ...state,
        isFetching: true,
        isInvalidate: false,
      };
    }
    case RECEIVE_MANAGEMENT_ROLE: {
      return {
        ...state,
        roles: action.roles,
        isFetching: false,
        isInvalidate: false,
        selectedRole: action.roles[0],
      };
    }
    case INVALIDATE_MANAGEMENT_ROLE: {
      return {
        ...state,
        isFetching: false,
        isInvalidate: true,
      };
    }
    default:
      return state;
  }
}

/**
 * Action creators
 */
export const selectManagementRole = (selectedRole) => {
  return {
    type: SELECT_MANAGEMENT_ROLE,
    selectedRole,
  };
};

export const requestManagementRole = () => {
  return {
    type: REQUEST_MANAGEMENT_ROLE,
  };
};

export const receiveManagementRole = (users) => {
  return {
    type: RECEIVE_MANAGEMENT_ROLE,
    users,
  };
};

export const invalidateManagementRole = () => {
  return {
    type: INVALIDATE_MANAGEMENT_ROLE,
  };
};

/**
 * Side effects aka async actions
 */
export const fetchManagementRole = () => {
  return (dispatch) => {
    dispatch(requestManagementRole());
    getRoles()
      .then((res) => {
        dispatch(receiveManagementRole(res.data));
      })
      .catch((err) => {
        dispatch(invalidateManagementRole());
      });
  };
};
