import moment from 'moment';
import 'moment/min/moment-with-locales'
import { MaskedViewComponent } from 'react-native';


/* get by time range T | W | M | Q | Y
 * @params
 * formatter = can empty'' or with format ex YYYYMMDD etc
 */
export function getDateTimeRangeByType(rangeType = "", start_date = "", end_date = "", formater = "YYYY-MM-DD") {
  let daterange;
  let substract;
  let interval = "hour";
  let diffDays;
  let s_date
  let e_date
  rangeType = rangeType.toLocaleUpperCase();


  if (rangeType.indexOf("&") > -1 || rangeType === "ADV") {
    start = moment(start_date, "YYYYMMDD");
    end = moment(end_date, "YYYYMMDD");
    diffDays = moment.duration(end.diff(start)).asDays()
    if (diffDays > 300) {
      substract.interval = 'month'
    }

    else if (diffDays > 40) {
      substract.interval = 'month'
    }

    else if (diffDays > 28) {
      substract.interval = 'day'
    }
    else if (diffDays > 5) {
      substract.interval = 'day'
    }
    else if (diffDays == 0) {
      substract.interval = 'hour'
    }
    else {
      substract.interval = 'hour'
    }
    daterange = {
      start_date: start_date,
      end_date: end_date,
      startTimeStamp: moment(start_date + " 00:00:00").valueOf(),
      endTimeStamp: moment(end_date + " 23:59:59").valueOf(),
      interval: substract.interval,
    };
  } else {
    switch (rangeType) {
      case "T":
      case "TODAY":
        substract = { type: "days", subs: 0, interval: "hour" };
        break;
      case "W":
      case "WEEK":
        substract = { type: "days", subs: 6, interval: "day" };
        break;
      case "M":
      case "MONTH":
        substract = { type: "months", subs: 1, interval: "day" };
        break;
      case "Q":
      case "QUARTER":
        substract = { type: "months", subs: 4, interval: "month" };
        break;
      case "Y":
      case "YEAR":
        substract = { type: "years", subs: 1, interval: "month" };
        break;

      default:
        substract = 0;
        break;
    }
    s_date = moment().subtract(substract.subs, substract.type);
    s_date = formater != "" ? s_date.format(formater) : s_date;
    e_date = moment();
    e_date = formater != "" ? e_date.format(formater) : e_date;
    daterange = {
      start_date: s_date,
      end_date: e_date,
      startTimeStamp: moment(s_date + " 00:00:00").valueOf(),
      endTimeStamp: moment(e_date + " 23:59:59").valueOf(),
      interval: substract.interval,
    };
  }


  return daterange;
}

export function convertionDate(data, set, type = "") {
  const date = new Date(data);

  if (set === "hour") {
    return (
      moment(date).format("HH:mm")
    )
  }
  if (set === "day") {
    return date.toLocaleString("en", { weekday: "long" });
  }
  if (set === "month") {
    return (
      date.toLocaleString("en", { month: "short" }) +
      " " +
      date.toLocaleString("en", { year: "numeric" })
    );
  }
  if (set === "all") {
    return (
      date.toLocaleString("en", { day: "2-digit" }) +
      " " +
      date.toLocaleString("en", { month: "short", year: "numeric" })
    );
  }
  if (set === "h-num") {
    return date.toLocaleString("id", { hour: "2-digit", minute: "2-digit" });
  }
  if (set === "d-num") {
    return date.toLocaleString("en", { day: "2-digit" });
  }
  if (set === "m-short") {
    return date.toLocaleString("en", { month: "short" });
  }
  if (set === "m-long") {
    return date.toLocaleString("en", { month: "long" });
  }
  if (set === "y-num") {
    return date.toLocaleString("en", { year: "numeric" });
  }
  if (set === "dm-num") {
    return date.toLocaleString("en", { day: "2-digit", month: "2-digit" });
  }
  if (set === "dm-short") {
    return (
      moment(date).format("DD MMM")
    );
  }
  if (set === "dm-long") {
    return (
      date.toLocaleString("en", { day: "2-digit" }) +
      " " +
      date.toLocaleString("en", { month: "long" })
    );
  }
  if (set === "my-num") {
    return date.toLocaleString("en", { month: "2-digit", year: "numeric" });
  }
  if (set === "my-short") {
    return date.toLocaleString("en", { month: "short", year: "numeric" });
  }
  if (set === "my-long") {
    return date.toLocaleString("en", { month: "long", year: "numeric" });
  }
  if (set === "dmy-num") {
    return date.toLocaleString("en", {
      day: "2-digit",
      month: "2-digit",
      year: "numeric",
    });
  }
  if (set === "dmy-short") {
    return (
      date.toLocaleString("en", { day: "2-digit" }) +
      " " +
      date.toLocaleString("en", { month: "short", year: "numeric" })
    );
  }
  if (set === "dmy-long") {
    return (
      date.toLocaleString("en", { day: "2-digit" }) +
      " " +
      date.toLocaleString("en", { month: "long", year: "numeric" })
    );
  }
  if (set === "dmyh-num") {
    return (
      date.toLocaleString("en", {
        day: "2-digit",
        month: "2-digit",
        year: "numeric",
      }) +
      " " +
      date.toLocaleString("id", { hour: "2-digit", minute: "2-digit" })
    );
  }
  if (set === "dmyh-short") {
    return (
      date.toLocaleString("en", { day: "2-digit" }) +
      " " +
      date.toLocaleString("en", { month: "short", year: "numeric" }) +
      " " +
      date.toLocaleString("id", { hour: "2-digit", minute: "2-digit" })
    );
  }
  if (set === "dmyh-long") {
    return (
      date.toLocaleString("en", { day: "2-digit" }) +
      " " +
      date.toLocaleString("en", { month: "long", year: "numeric" }) +
      " " +
      date.toLocaleString("id", { hour: "2-digit", minute: "2-digit" })
    );
  } else {
    return this.isValidTimestamp(data, type);
  }
}

export function isValidTimestamp(value, type = "") {
  const valid = new Date(value).getTime();
  const keyphrase = value.toString().length;
  if (valid > 0 && keyphrase > 10 && type === "") {
    const date = new Date(value);
    return (
      date.toLocaleString("en", { month: "short" }) +
      " " +
      date.toLocaleString("en", { day: "2-digit" }) +
      ", " +
      date.toLocaleString("en", { year: "numeric" })
    );
  } else if (valid > 0 && keyphrase > 10 && type !== "") {
    const date = new Date(value);
    return (
      date.toLocaleString("en", { day: "2-digit" }) +
      " " +
      date.toLocaleString("en", { month: "long", year: "numeric" }) +
      " " +
      date.toLocaleString("id", { hour: "2-digit", minute: "2-digit" })
    );
  } else {
    return value;
  }
}


export function TimeAgoMoment(value, format_to = "DD MMMM YYYY HH:mm:ss", format_from = "YYYY-MM-DD HH:mm:ss") {
  let result = value;
  if (value != '' && value != null) {
    if (value.indexOf(' ') > 0) {
      result = result.replace('.000+0700', '');
      result = result.replace('T', ' ');
      const testDateUtc = moment.utc(result);
      result = moment(testDateUtc).local().format(format_to);
    } else {
      const w = parseInt(value, 10);
      result = moment.unix(w / 1000).format(format_to);
    }
  }

  return result;
}