import moment from "moment";
import { _color_pallete } from "./ColorPallete";
import { convertionDate } from "./DateHelper";

export function objectToSeriesAndCategories(data, name, pallete = "default") {
  const chartData = { categories: [], series: [] };
  const palleteArr = pallete.split("|");
  const isPalleteForSeries = !!(
    palleteArr.length > 1 && palleteArr[1] == "series"
  );

  const cPallete = _color_pallete.hasOwnProperty(pallete)
    ? _color_pallete[pallete]
    : _color_pallete.default;
  Object.entries(data).forEach(([key, value], index) => {
    const cat = key
      .toLowerCase()
      .split(" ")
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ");
    if (key != "undefined" && key != "null" && key != "") {
      const seriesData = {
        y: data[key],
        color: pallete == "default" ? cPallete[index] : cPallete[key],
      };
      chartData.categories.push(cat);
      chartData.series.push(seriesData);
    }
  });
  Object.keys(data).forEach((key) => { });

  chartData.series = [
    {
      name,
      data: chartData.series,
      color: isPalleteForSeries ? palleteArr[0] : undefined,
    },
  ];

  return chartData;
}

export function parseArryObjectSingleSeries(series, keyField, keyValue) {
  const arrayToObject = (series, keyField, keyValue) =>
    series.reduce((obj, item) => {
      obj[item[keyField]] = item[keyValue];
      return obj;
    }, {});
  const data = arrayToObject(series, keyField, keyValue);
  return data;
}

export function objectToSeriesPie(data, name = "data", pallete = "default") {
  const chartData = { series: [] };
  const palleteArr = pallete.split("|");
  const isPalleteForSeries = !!(
    palleteArr.length > 1 && palleteArr[1] == "series"
  );

  const cPallete = _color_pallete.hasOwnProperty(pallete)
    ? _color_pallete[pallete]
    : _color_pallete.default;

  Object.entries(data).forEach(([key, value], index) => {
    const labelArray = key.toLowerCase().split("_");
    let label = key.toLowerCase();
    if (labelArray.length > 1) {
      label = labelArray[1];
    }

    if (key != "undefined" && key != "null" && key != "") {
      const seriesData = {
        id: key.toLowerCase(),
        name: label,
        y: +data[key],
        color: pallete == "default" ? cPallete[index] : cPallete[key],
      };

      chartData.series.push(seriesData);
    }
  });
  Object.keys(data).forEach((key) => { });
  return chartData;
}
export function parseAreaChartSeries(series, titleChart = '', state = '', colorChart = '') {
  const start = moment(state.start_date, 'YYYYMMDD');
  const end = moment(state.end_date, 'YYYYMMDD');
  const diffDays = moment.duration(end.diff(start)).asDays();
  const tmpCatagories = [];
  const value = [];
  let interval;

  if (diffDays > 28) { // M
    interval = 'dm-short';
  } else if (diffDays > 5) { // W
    interval = 'day';
  } else if (diffDays === 0) { // T
    interval = 'hour';
  } else { // T
    interval = 'hour';
  }

  Object.keys(series).map(key => {
    let convCat = new Date(moment(key, 'YYYYMMDDHH').toDate()).getTime();
    convCat = convertionDate(
      convCat,
      interval
    );
    tmpCatagories.push(convCat);
    let temp;
    value.push([key, Number(series[key])]);
  });

  const dataChart = {
    categories: tmpCatagories,
    series: [{
      name: titleChart,
      data: value,
      color: colorChart
    }]
  };
  return dataChart;
}

/* parsing for area chart */
export function parseAreaChartSeriesSentiment(series) {
  const start = moment(state.start_date, 'YYYYMMDD');
  const end = moment(state.end_date, 'YYYYMMDD');
  const diffDays = moment.duration(end.diff(start)).asDays();
  const tmpCatagories = [];
  const value = [];
  const negative = [];
  const neutral = [];
  const positive = [];
  let interval;

  if (diffDays > 28) { // M
    interval = 'dm-short';
  } else if (diffDays > 5) { // W
    interval = 'day';
  } else if (diffDays === 0) { // T
    interval = 'hour';
  } else { // T
    interval = 'hour';
  }

  Object.keys(series).map(key => {
    let convCat = new Date(moment(series[key].date, 'YYYYMMDDHH').toDate()).getTime();
    convCat = convertionDate(convCat, interval);
    tmpCatagories.push(convCat);
    negative.push([series[key].date, series[key].negative]);
    neutral.push([series[key].date, series[key].neutral]);
    positive.push([series[key].date, series[key].positive]);
  });
  const dataChart = {
    categories: tmpCatagories,
    series: [
      { name: 'Negative', data: negative, color: _color_pallete.sentiment.negative },
      { name: 'Neutral', data: neutral, color: _color_pallete.sentiment.neutral },
      { name: 'Positive', data: positive, color: _color_pallete.sentiment.positive },
    ]
  };
  return dataChart;
}

/* parsing for chart stacked sum */
export function parseChartSeriesSentimentSumStacked(series) {
  let negative = 0;
  let neutral = 0;
  let positive = 0;

  Object.keys(series).map(key => {
    negative += series[key].negative;
    neutral += series[key].neutral;
    positive += series[key].positive;
  });

  const dataChart = {
    categories: ["Negative", "Neutral", "Positive"],
    series: [
      { name: 'Negative', data: [negative], color: _color_pallete.sentiment.negative },
      { name: 'Neutral', data: [neutral], color: _color_pallete.sentiment.neutral },
      { name: 'Positive', data: [positive], color: _color_pallete.sentiment.positive },
    ]
  };
  return dataChart;
}

export function objectToSeriesAndCategoriesStacked(data, name = 'data', pallete = 'default') {
  const chartData = { categories: [], series: [] };
  const palleteArr = pallete.split('|');
  const isPalleteForSeries = (palleteArr.length > 1 && palleteArr[1] == 'series') ? true : false;

  const cPallete = _color_pallete.hasOwnProperty(pallete) ? _color_pallete[pallete] : _color_pallete.default;
  Object.entries(data).forEach(([key, value], index) => {
    const cat = key.toLowerCase()
      .split(' ')
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(' ');

    if (key != 'undefined' && key != 'null' && key != '') {
      const seriesData = { name: key, data: [+data[key]], color: (pallete == 'default') ? cPallete[index] : cPallete[key] };
      chartData.categories.push(cat);
      chartData.series.push(seriesData);
    }
  });
  // Object.keys(data).forEach(key => { });
  return chartData;
}


export function objectToSeriesAndCategoriesEmotions(data, name = 'data', pallete = 'default') {
  const chartData = { categories: [], series: [] };
  const palleteArr = pallete.split('|');
  const isPalleteForSeries = (palleteArr.length > 1 && palleteArr[1] == 'series') ? true : false;

  const cPallete = _color_pallete.hasOwnProperty(pallete) ? _color_pallete[pallete] : _color_pallete.default;

  Object.entries(cPallete).forEach(([key, value], index) => {
    const cat = key.toLowerCase()
      .split(' ')
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(' ');
    let values = 0;


    if (data[key] != 'undefined' && data[key] != 'null' && data[key] != '' && data[key] != undefined) {
      values = data[key];
    }
    const seriesData = { y: values, color: value };
    chartData.categories.push(cat);
    chartData.series.push(seriesData);
  });

  chartData.series = [{
    name,
    data: chartData.series,
  }];

  return chartData;
}

export function parseHighmapsSeries(series, cities, selected = []) {
  let chartSeries = [];
  let city;
  cities.forEach(element => {
    element.value = (series[element.label]) ? series[element.label] : 0;
    city = {
      name: element.label,
      data: new Array(element)
    }
    chartSeries.push(city)
  });

  return chartSeries;
}