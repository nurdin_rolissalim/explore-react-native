import { directoryImage } from "app/config";
import { checkImageUrl } from "app/service/EntityService";
import { array } from "prop-types";

export function Truncate(value, lenght = 100, replace = " ...") {
    if (value) {
        return value.length > lenght ? value.substring(0, lenght) + replace : value;
    }
    return value;
}

export function TestFunc2() { }

export function CapitalizeFirstLetter(value) {
    if (value) {
        const seleksi = value.split(' ');
        let element = '';

        for (let index = 0; index < seleksi.length; index++) {
            if (index != 1) {
                element += seleksi[index].charAt(0).toLocaleUpperCase() + seleksi[index].slice(1) + ' ';
            } else {
                element += seleksi[index].charAt(0).toUpperCase() + seleksi[index].slice(1) + ' ';
            }
        }
        return element;
    } else {
        return value;
    }
}

export function NumberConvert(value) {
    return Math.abs(Number(value)) >= 1.0e+9
        ? (Math.abs(Number(vvalueal)) / 1.0e+9).toFixed(1) + ' B'
        : Math.abs(Number(value)) >= 1.0e+6
            ? (Math.abs(Number(value)) / 1.0e+6).toFixed(1) + ' M'
            : Math.abs(Number(value)) >= 1.0e+3
                ? (Math.abs(Number(value)) / 1.0e+3).toFixed(1) + ' K'
                : Math.abs(Number(value));

}
export function getImageEntity(value, type = "person") {
    if (type == "influencer") {
        type = "person"
    }
    const filename = value.replace(/ /g, "+").toLowerCase();

    let file = `${directoryImage}` + type + `/${filename}.jpg`;

    return file;
}
export async function checkImageEntity(value, type = "person") {

    const filename = value.replace(/ /g, "+").toLowerCase();
    const result = {
        file: `${directoryImage}` + type + `/${filename}.jpg`,
        default: "https://politica.blackeye.id/assets/images/person.png"
    }
    let data;

    switch (type) {
        case "organization":
            result.default = "https://politica.blackeye.id/assets/images/organization.png";
            break;
        case "location":
            result.default = "https://politica.blackeye.id/assets/images/location.png";
            break;
    }

    try {
        const response = await fetch(url);

        const data = await response.status;

        return data;
    } catch (error) {
        return error;
    }

    // // if (result.file != "" && result.file != undefined) {
    // checkImageUrl(result.file)
    //     .then((response) => {
    //         console.log("response", response);

    //         if (response.status === 200) {
    //             data = result.file;
    //         } else {
    //             data = result.default;
    //         }
    //         console.log("data", data);

    //         return data;

    //     })
    //     .catch((error) => {
    //         return result.default;
    //     });
    // }


    // return result.file;
}

// export function capitalizeString(d) {
//     const seleksi = d.split(' ');
//     let element = '';

//     for (let index = 0; index < seleksi.length; index++) {
//         if (index != 1) {
//             element += seleksi[index].charAt(0).toLocaleUpperCase() + seleksi[index].slice(1) + ' ';
//         } else {
//             element += seleksi[index].charAt(0).toUpperCase() + seleksi[index].slice(1) + ' ';
//         }
//     }
//     return element;
// }

export function parseCitiesOfProvince(data) {
    let cities = [];
    data.forEach(element => {
        cities.push({ label: element.properties.NAME_2, hc_key: element.properties.hc_key });
    });
    return cities;
}
export function formatCitiesApi(data) {
    let city = [];
    if (data !== "-" && data.length > 0) {
        data.forEach(element => {
            let temp = element.toLowerCase();

            if (temp.indexOf("kota") < 0 && temp.indexOf("kab. ") < 0) {
                temp = "kab. " + temp;
            }
            // city.push('"' + temp + '"')
            city.push(temp)
        });
    }
    return city;
}
export function formatSubDistrictApi(data) {
    let district = [];
    if (data !== "-" && data.length > 0) {
        data.forEach(element => {
            let temp = element.toLowerCase();
            // city.push('"' + temp + '"')
            district.push(temp)
        });
    }
    return district;
}

export function formatParamsLocation(
    type_location,
    user_location = "",
    districts = "-",
    location_audience = "-",
) {
    let params = {
        locations: "-",
        location: "-",
        location_audience: "-",
    };

    let temp;
    if (type_location == "province") {
        params.location = user_location
        if (districts !== "-") {
            temp = formatCitiesApi(districts)
            params.locations = temp
        }
        if (location_audience !== "-") {
            temp = formatCitiesApi([location_audience.name])
            params.location_audience = temp
        }

    } else if (type_location == "city") {
        params.location = user_location;
        if (user_location.indexOf("kota") < 0 && user_location.indexOf("kab. ") < 0) {
            params.location = "kab. " + user_location;
        }

        if (districts !== "-") {
            temp = formatSubDistrictApi(districts)
            params.locations = temp
        }
        if (location_audience !== "-") {
            temp = formatSubDistrictApi([location_audience.name])
            params.location_audience = temp
        }
    }
    return params

}
export function parseObejctToArray(data) {
    let temp = new Array();
    Object.entries(data).forEach(([key, value], index) => {
        temp.push(
            { key: key, value: value }
        )
    });

    return temp;

}
export function sortArrayDesc(data) {
    data.sort((a, b) => a.value < b.value ? 1 : -1)

    return data;

}
export function sortArrayAsc(data) {
    data.sort((a, b) => a.value > b.value ? 1 : -1)

    return data;

}

