import React, { useEffect } from "react";
import {
  StyleSheet,
  View,
} from "react-native";
import { Container, Content } from "native-base";

import {
  WrapperContent,
  TitleSection,
  SubtitleSection,
} from "app/shared/ui/StyledComponents";
import { useNavigation } from "@react-navigation/native";

import { connect } from "react-redux";
import { setPage, getEntity } from "app/redux/ducks";

import EntityController from "app/shared/controllers/content/EntityController";
import StatementController from "app/shared/controllers/content/StatementController";
import NewsController from "app/shared/controllers/content/NewsController";
import SegmentButton from "app/shared/ui/segments/SegmentButton";
import IssueController from "app/shared/controllers/content/IssueController";
import SummaryController from "app/shared/controllers/statistic/SummaryController";
import ChartController from "app/shared/controllers/charts/ChartController";
import FabIcon from "app/shared/ui/fab/FabIcon";

function ActivityNews(props) {
  const [cPage, setCPage] = React.useState("activity-news");
  const [analyze, setAnalyze] = React.useState("activity-news");
  const [type, setType] = React.useState("person");
  const [typeIcon, setTypeIcon] = React.useState("newspaper");
  const [platform, setPlatform] = React.useState("news");
  const [color, setColor] = React.useState("#fa423b");
  const navigation = useNavigation();

  const callbackSegment = (segment) => {
    navigation.navigate(segment);
  };

  const updatePage = (type) => {
    let page = {
      name: cPage,
      type: type,
      platform: platform
    }
    props.setPage(page)
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action


      updatePage(type)
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const callbackFooter = (footer) => {
    // setPlatform(footer.navigate);
    // setColor(footer.color);
    navigation.navigate(footer.navigate);
  };

  function resetIssue() {

  }


  return (
    <Container>
      <Content>
        <SegmentButton
          analyze="activity"
          typeSegment="news"
          callback={callbackSegment}
        />
        <WrapperContent>
          <TitleSection>TOTAL NEWS</TitleSection>
          <SummaryController
            analyze={analyze + "-total"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <View>
            <TitleSection>Top Issue</TitleSection>
            <SubtitleSection
              onPress={resetIssue}
              style={styles.right}
            >
              [reset issue]
            </SubtitleSection>
            <IssueController
              analyze={analyze + "-issue"}
              type={type}
              cPage={cPage}
              platform={platform}
            />
          </View>
        </WrapperContent>

        <WrapperContent>
          <View style={styles.containerWrapper}>
            <TitleSection>NEWS</TitleSection>
            <SubtitleSection
              onPress={() => props.navigation.navigate("universe-news")}
              style={styles.right}
            >
              See other news >
            </SubtitleSection>
          </View>
          <NewsController
            analyze={analyze + "-issue"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>media share</TitleSection>
          <ChartController
            analyze={analyze + "-media-share"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>
        <WrapperContent>
          <TitleSection>sentiment</TitleSection>
          <ChartController
            analyze={analyze + "-sentiment"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>EXPOSURE</TitleSection>
          <ChartController
            analyze={analyze + "-exposure"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>RELATED INFLUENCER</TitleSection>
          {/* <Person analyze="activity-person" /> */}
          <EntityController
            analyze={analyze + "-influencer"}
            type={"influencer"}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <View
            style={styles.containerWrapper}
          >
            <TitleSection>statements</TitleSection>
            <SubtitleSection>see more</SubtitleSection>
          </View>
          <StatementController
            analyze={analyze + "-statements"}
            type="news"
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>
      </Content>
      <FabIcon
        analyze={"activity-news"}
        type={typeIcon}
        color={color}
        callback={callbackFooter}
      />
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
  },
  title: {
    padding: 10,
    fontSize: 16,
    fontWeight: "bold",
  },
  right: {
    position: "absolute",
    right: 0,
  },
  containerWrapper: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  }
});


const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  // getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityNews);
