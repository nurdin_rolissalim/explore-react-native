import React, { useEffect, useState } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { Content, Container, Icon, Button, Fab, Badge } from "native-base";
import { useNavigation } from "@react-navigation/native";

import {
  WrapperContent,
  TitleSection,
  SubtitleSection,
} from "app/shared/ui/StyledComponents";


import { connect } from "react-redux";
import { setPage, getEntity } from "app/redux/ducks";

import SegmentButton from "app/shared/ui/segments/SegmentButton";
import FabIcon from "app/shared/ui/fab/FabIcon";

import PostController from "app/shared/controllers/content/PostController";
import IssueController from "app/shared/controllers/content/IssueController";
import SummaryController from "app/shared/controllers/statistic/SummaryController";
import ChartController from "app/shared/controllers/charts/ChartController";
import StatisticController from "app/shared/controllers/statistic/StatisticController";

function ActivityResponse(props) {
  const navigation = useNavigation();
  const [cPage, setCPage] = React.useState("activity-response");
  const [analyze, setAnalyze] = React.useState("activity-response");
  const [type, setType] = React.useState("person");
  const [platform, setPlatform] = React.useState("twitter");
  const [color, setColor] = useState("#5067FF");
  const [isFabActive, setIsFabActive] = React.useState(false);

  const callbackSegment = (segment) => {
    navigation.navigate(segment);
  };

  const updatePage = (type) => {
    let page = {
      name: cPage,
      type: type,
      platform: platform
    }
    props.setPage(page)
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action


      updatePage(type)
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const callbackFooter = (footer) => {
    setPlatform(footer.navigate);
    setColor(footer.color);
  };

  function resetIssue() {

  }

  return (
    <Container>
      <Content style={{ height: "100%" }}>
        <SegmentButton
          analyze="activity"
          typeSegment="response"
          callback={callbackSegment}
        />

        <WrapperContent>
          <TitleSection>Post Count</TitleSection>
          <SummaryController
            analyze={analyze + "-total"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>
        <WrapperContent>
          <TitleSection>TOP ISSUE</TitleSection>
          <IssueController
            analyze={analyze + "-issue"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <TitleSection>TIMELINE</TitleSection>
            <SubtitleSection>see more</SubtitleSection>
          </View>
          <PostController
            analyze={analyze + "-post"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
          {/* <PostItem />
          <PostItem />
          <PostItem /> */}
        </WrapperContent>
        <WrapperContent>
          <TitleSection>sentiment</TitleSection>
          <ChartController
            analyze={"activity-sentiment-response"}
            type={type}
            cPage={cPage}
            platform={platform} />
        </WrapperContent>
        <WrapperContent>
          <TitleSection>EMOTIONS</TitleSection>
          <ChartController
            analyze={"activity-emotion-response"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>PERFORMANCE</TitleSection>
          <ChartController
            analyze={"activity-performance-response"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>post type</TitleSection>
          <StatisticController
            analyze={"activity-post-type-response"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>hashtags cloud</TitleSection>
          <ChartController
            analyze={"activity-wordcloud-response"}
            type={type}
            cPage={cPage}
            platform={platform}
          />
        </WrapperContent>

      </Content>
      <FabIcon
        analyze={"activity-response"}
        type={platform}
        color={color}
        callback={callbackFooter}
      />
      {/* <Fab
        active={isFabActive}
        direction="up"
        containerStyle={{}}
        style={{ backgroundColor: "#5067FF" }}
        position="bottomRight"
        onPress={() => setIsFabActive(!isFabActive)}
      >
        <Icon name="logo-twitter" />
        <Button style={{ backgroundColor: "#5067FF" }}>
          <Icon name="logo-twitter" />
        </Button>
        <Button style={{ backgroundColor: "#3B5998" }}>
          <Icon name="logo-facebook" />
        </Button>
        <Button style={{ backgroundColor: "#b950d9" }}>
          <Icon name="logo-instagram" />
        </Button>
      </Fab> */}
    </Container>
  );
}


const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  // getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityResponse);