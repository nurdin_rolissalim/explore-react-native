import React, { useEffect } from "react";
import {
    StyleSheet,
    View,
} from "react-native";
import { Container, Content } from "native-base";

import {
    WrapperContent,
    TitleSection,
    SubtitleSection,
} from "app/shared/ui/StyledComponents";
import { useNavigation } from "@react-navigation/native";

import { connect } from "react-redux";
import { setPage, getEntity } from "app/redux/ducks";

import NewsController from "app/shared/controllers/content/NewsController";
import SegmentButton from "app/shared/ui/segments/SegmentButton";
import FabIcon from "app/shared/ui/fab/FabIcon";

function ActivityTimeline(props) {
    const [cPage, setCPage] = React.useState("activity-timeline");
    const [analyze, setAnalyze] = React.useState("activity-timeline");
    const [type, setType] = React.useState("person");
    const [color, setColor] = React.useState("#fa423b");
    const [typeIcon, setTypeIcon] = React.useState("list");
    const [platform, setPlatform] = React.useState("news");
    const navigation = useNavigation();

    const callbackSegment = (segment) => {
        navigation.navigate(segment);
    };

    const updatePage = (type) => {
        let page = {
            name: cPage,
            type: type,
            platform: platform
        }
        props.setPage(page)
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', (e) => {
            // The screen is focused
            // Call any action


            updatePage(type)
        });

        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
    }, [navigation]);

    const callbackFooter = (footer) => {
        // setPlatform(footer.navigate);
        // setColor(footer.color);
        navigation.navigate(footer.navigate);
    };

    return (
        <Container>
            <Content>
                <SegmentButton
                    analyze="activity"
                    typeSegment="news"
                    callback={callbackSegment}
                />
                <WrapperContent>
                    <TitleSection>TOTAL NEWS</TitleSection>
                    <NewsController
                        analyze={analyze}
                        type={type}
                        cPage={cPage}
                        platform={platform}
                    />
                </WrapperContent>
            </Content>
            <FabIcon
                analyze={"activity-news"}
                type={typeIcon}
                color={color}
                callback={callbackFooter}
            />
        </Container>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 5,
    },
    title: {
        padding: 10,
        fontSize: 16,
        fontWeight: "bold",
    },
    right: {
        position: "absolute",
        right: 0,
    },
    containerWrapper: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
    }
});


const mapStateToProps = (state) => {
    const { page, type, filter, issue, entity } = state;
    return { page, type, filter, issue };
};

const mapDispatchToProps = {
    setPage,
    // getfilter,
    getEntity
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ActivityTimeline);
