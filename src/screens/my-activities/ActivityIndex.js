import React from "react";
import {
  Container,
} from "native-base";
import { createStackNavigator } from "@react-navigation/stack";
import ActivityNews from "./ActivityNews";
import ActivityMyProfile from "./ActivityMyProfile";
import ActivityResponse from "./ActivityResponse";
import ActivityTimeline from "./ActivityTimeline";

const ActivityStack = createStackNavigator();

export default function ActivityIndex(props) {
  return (
    <Container>
      <ActivityStack.Navigator initialRouteName="ActivityNews" headerMode={false}>
        <ActivityStack.Screen name="ActivityNews" component={ActivityNews} />
        <ActivityStack.Screen name="ActivityTimeline" component={ActivityTimeline} />
        <ActivityStack.Screen name="ActivityProfile" component={ActivityMyProfile} />
        <ActivityStack.Screen name="ActivityResponse" component={ActivityResponse} />
      </ActivityStack.Navigator>
    </Container>
  );
}
