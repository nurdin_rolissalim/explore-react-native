import React from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import Person from "app/shared/ui/Person";
import Issue from "app/shared/ui/Issue";
import News from "app/shared/ui/News";
import ModalComponent from "app/shared/ui/Modal";
import { Chart } from "app/shared/ui/Chart";
import { Container, Content } from "native-base";

const MyActivityScreen = () => {
  return (
    <Container>
      <Content>
        <Person analyze="activity-person" />
        <View style="margin: 10">
          <Chart />
        </View>
        {/* <ModalComponent
          // modalStyle={s.content__modal}
          alwaysOpen={60}
          handlePosition="inside"
          analyze="modal-home-news"
        /> */}
      </Content>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default MyActivityScreen;
