import React from "react";
import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  View,
  Button,
  Toast,
  Input,
  Item,
} from "native-base";
import { TextInput } from "react-native";
import { connect } from "react-redux";
import * as propTypes from "prop-types";
import { WrapperContent } from "app/shared/ui/StyledComponents";
import * as ManagementService from "app/service/ManagementService";
import { Formik } from "formik";
import globalStyle from "app/shared/styles";

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);

    this.handlerSubmit = this.handlerSubmit.bind(this);
  }

  handlerSubmit(value) {
    const { oldPassword, newPassword, confirmNewPassword } = value;

    if (newPassword !== confirmNewPassword) {
      Toast.show({ text: "Password not same", position: "bottom" });
    } else {
      const old_password = oldPassword;
      const new_password = newPassword;
      const { username } = this.props.userData;

      ManagementService.updatePassword({
        old_password,
        new_password,
        username,
      })
        .then((res) => {
          this.props.navigation.navigate("my-profile");
          Toast.show({
            text: "Update password successfully",
            position: "bottom",
          });
        })
        .catch((err) => {
          Toast.show({
            text: "Update password failed",
            position: "bottom",
          });
        });
    }
  }

  render() {
    return (
      <Container>
        <Content style={{ flex: 1, height: "100%" }}>
          <Formik
            initialValues={{
              oldPassword: "",
              newPassword: "",
              confirmNewPassword: "",
            }}
            onSubmit={this.handlerSubmit}
          >
            {({ handleChange, handleSubmit, values }) => (
              <View style={globalStyle.form}>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Old password:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      secureTextEntry
                      placeholder="Old password"
                      onChangeText={handleChange("oldPassword")}
                      value={values.oldPassword}
                      name="oldPassword"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>New Password:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      secureTextEntry
                      placeholder="New password"
                      onChangeText={handleChange("newPassword")}
                      value={values.newPassword}
                      name="newPassword"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>
                    Confirm new Password:
                  </Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      secureTextEntry
                      placeholder="Confirm new password"
                      onChangeText={handleChange("confirmNewPassword")}
                      value={values.confirmNewPassword}
                      name="confirmNewPassword"
                    />
                  </Item>
                </View>
                <View style={globalStyle.formAction}>
                  <Button full primary rounded onPress={handleSubmit}>
                    <Text>Save</Text>
                  </Button>
                </View>
              </View>
            )}
          </Formik>
        </Content>
      </Container>
    );
  }
}

ChangePassword.propTypes = {
  userData: propTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { userData } = state.auth;
  return { userData };
};

export default connect(mapStateToProps)(ChangePassword);
