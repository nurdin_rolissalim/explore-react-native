import React from "react";
import {
  Container,
  Content,
  Text,
  View,
  Button,
  Item,
  Input,
  Toast,
} from "native-base";
import { connect } from "react-redux";
import * as propTypes from "prop-types";
import { Formik } from "formik";
import globalStyle from "app/shared/styles";
import * as ManagementService from "app/service/ManagementService";

class ChangeProfileScreen extends React.Component {
  constructor(props) {
    super(props);

    this.handlerSubmit = this.handlerSubmit.bind(this);
    this.objectToFormData = this.objectToFormData.bind(this);
  }

  objectToFormData(obj) {
    const formdata = new FormData();

    Object.keys(obj).forEach((key) => {
      formdata.append(key, obj[key]);
    });

    return formdata;
  }

  handlerSubmit(value) {
    const { fullname, email } = value;
    const user = { ...this.props.userData, full_name: fullname, email };
    const formdata = this.objectToFormData(user);

    ManagementService.updateAccount(formdata)
      .then((res) => {
        this.props.navigation.navigate("my-profile");
        Toast.show({
          text: "Update account successfully",
          position: "bottom",
        });
      })
      .catch((err) => {
        Toast.show({
          text: "Update account failed",
          position: "bottom",
        });
      });
  }

  render() {
    const { fullname, email } = this.props.userData;

    return (
      <Container>
        <Content style={{ flex: 1, height: "100%" }}>
          <Formik
            initialValues={{
              fullname,
              email,
            }}
            onSubmit={this.handlerSubmit}
          >
            {({ handleChange, handleSubmit, values }) => (
              <View style={globalStyle.form}>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Fullname:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Fullname"
                      onChangeText={handleChange("fullname")}
                      value={values.fullname}
                      name="fullname"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Email:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Email"
                      onChangeText={handleChange("email")}
                      value={values.email}
                      name="email"
                    />
                  </Item>
                </View>
                <View style={globalStyle.formAction}>
                  <Button full primary rounded onPress={handleSubmit}>
                    <Text>Save</Text>
                  </Button>
                </View>
              </View>
            )}
          </Formik>
        </Content>
      </Container>
    );
  }
}

ChangeProfileScreen.propTypes = {
  userData: propTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { userData } = state.auth;
  return { userData };
};

export default connect(mapStateToProps)(ChangeProfileScreen);
