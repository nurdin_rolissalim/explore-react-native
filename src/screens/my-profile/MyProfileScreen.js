import React from "react";
import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  View,
  Button,
  Thumbnail,
} from "native-base";
import { connect } from "react-redux";
import * as propTypes from "prop-types";
import { WrapperContent } from "app/shared/ui/StyledComponents";
import { getAsset } from "../../shared/helpers/helpers";
import { signOut } from "../../redux/ducks";

class MyProfile extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { userData, navigation } = this.props;
    return (
      <Container>
        <Content style={{ flex: 1, height: "100%" }}>
          <WrapperContent>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                marginVertical: 10,
              }}
            >
              <Thumbnail
                large
                source={{ uri: getAsset(`${userData.avatar}`) }}
                style={{ marginBottom: 10 }}
              />
              <Text style={{ fontSize: 18 }}>{userData.fullname}</Text>
              <Text style={{ fontSize: 16, color: "#666" }}>
                {userData.email}
              </Text>
            </View>
          </WrapperContent>
          <List>
            <ListItem />
            <ListItem onPress={() => navigation.navigate("change-profile")}>
              <Body>
                <Text>Change my profile</Text>
                <Text note>Change name, email, etc</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem onPress={() => navigation.navigate("change-password")}>
              <Body>
                <Text>Change password</Text>
                <Text note>Change current password</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
          <View style={{ marginTop: "auto", bottom: 0 }}>
            <WrapperContent>
              <Button block danger onPress={() => this.props.signOut()}>
                <Text>Log out</Text>
              </Button>
            </WrapperContent>
          </View>
        </Content>
      </Container>
    );
  }
}

MyProfile.propTypes = {
  userData: propTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { userData } = state.auth;
  return { userData };
};

const mapDispatchToProps = { signOut };

export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);
