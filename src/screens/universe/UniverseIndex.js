import React, { Component } from "react";
import { Container } from "native-base";
import { StyleSheet } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from "@react-navigation/native";
import UniverseNews from "./UniverseNews";
import UniverseProfile from "./UniverseProfile";
import UniverseRespone from "./UniverseRespone";

const UniverseStack = createStackNavigator();

export default function UniverseIndex(props) {
  const navigation = useNavigation();

  return (
    <Container>
      <UniverseStack.Navigator initialRouteName="UniverseNews" headerMode={false}>
        <UniverseStack.Screen name="UniverseNews" component={UniverseNews} />
        <UniverseStack.Screen name="UniverseActivity" component={UniverseProfile} />
        <UniverseStack.Screen name="UniverseResponse" component={UniverseRespone} />
      </UniverseStack.Navigator>
      {/* </Content> */}
    </Container>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});
