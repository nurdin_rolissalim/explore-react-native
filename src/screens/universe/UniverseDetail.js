import React, { Component } from "react";
import { StyleSheet } from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Content,
} from "native-base";

import {
  TitleSection,
  TitleHeader,
  WrapperContent,
} from "app/shared/ui/StyledComponents";

import NewsController from "app/shared/controllers/content/NewsController";
import IssueController from "app/shared/controllers/content/IssueController";
import SummaryController from "app/shared/controllers/statistic/SummaryController";
import StatementController from "app/shared/controllers/content/StatementController";
import ProfileImageController from "app/shared/controllers/content/ProfileImageController";
import ChartController from "app/shared/controllers/charts/ChartController";
import { connect } from "react-redux";
import { setPage } from 'app/redux/ducks';

class UniverseDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "universe-news-detail",
      analyze: "universe-news-detail",
    };
  }

  UNSAFE_componentWillMount() {
    let { analyze, type, platform } = this.props.route.params;
    this.props.setPage({
      name: "universe-news-detail",
      type: type,
      platform: "news"
    })
  }

  render() {
    let { analyze, type, platform } = this.props.route.params;

    let { page } = this.state;

    return (
      <Container>
        <Header hasSegment style={styles.headerContainer}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <TitleHeader>universe detail</TitleHeader>
          </Body>
        </Header>
        <Content padder>
          <WrapperContent>
            <ProfileImageController
              analyze={analyze}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>total</TitleSection>
            <SummaryController
              analyze={analyze}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>issue</TitleSection>
            <IssueController
              analyze={analyze}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>news</TitleSection>
            <NewsController
              analyze="universe-news-issue"
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>media share</TitleSection>
            <ChartController
              analyze="universe-news-media-share"
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>sentiment</TitleSection>
            <ChartController
              analyze="universe-news-sentiment"
              type={type}
              cPage={page}
              platform={platform} />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>exposure</TitleSection>
            <ChartController
              analyze="universe-news-exposure"
              type={type}
              cPage={page}
              platform={platform}
            />
            {/* <LineChart analyze="universe-news-exposure" /> */}
          </WrapperContent>

          <WrapperContent>
            <TitleSection>related influencer</TitleSection>
            <StatementController
              analyze="universe-news-statements"
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fa423b",
  },
});



const mapStateToProps = (state) => {
  const { page, type, filter } = state;
  return { page, type, filter };
};

const mapDispatchToProps = {
  setPage,
  // getfilter,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UniverseDetail);

