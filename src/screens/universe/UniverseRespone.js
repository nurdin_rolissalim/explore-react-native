import React, { Component, useState, useEffect } from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Content,
} from "native-base";
import { connect } from "react-redux";
import SegmentButton from "app/shared/ui/segments/SegmentButton";
import SearchController from "app/shared/controllers/search/SearchController";
import EntityController from "app/shared/controllers/content/EntityController";
import { setPage } from "app/redux/ducks";

function UniverseRespone(props) {
  const navigation = useNavigation();

  const [type, setType] = useState("person");
  const [keyword, setKeyword] = useState("");
  const callbackSearch = (keyword) => {
    setKeyword(keyword);
  };
  const callbackEntity = (entity) => {
    navigation.navigate("universe-detail-sosmed", {
      analyze: "universe-response",
      page: "universe-response",
      platform: "twitter",
      type: type,
      entity,
    });
  };

  const callbackSegment = (segment) => {
    navigation.navigate(segment)
  };

  const updatePage = () => {
    let page = {
      name: "universe-activity",
      type: "person",
      platform: "twitter",
      analyze: "universe-activty"
    }
    props.changePage(page)
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      updatePage();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  return (
    <Container>
      <SegmentButton
        analyze="universe"
        typeSegment="response"
        callback={callbackSegment}
      />

      <SearchController
        analyze="universe"
        keyword={keyword}
        type={type}
        callback={callbackSearch}
      />
      <Content>
        <EntityController
          cPage="universe-response"
          analyze="universe-sosmed-response"
          type={type}
          navigation={props.navigation}
          navigate="universe-detail-sosmed"
          callback={callbackEntity}
          keyword={keyword}
        />
      </Content>
    </Container>
  );
  // }
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});

const mapStateToProps = (state) => {
  const { page, type } = state;
  return { page, type };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changePage: (page) => dispatch(setPage(page)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UniverseRespone);