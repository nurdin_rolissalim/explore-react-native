import React, { Component } from "react";
import ScrollableTabView from "react-native-scrollable-tab-view";
import { StyleSheet } from "react-native";
import {
  Header,
  Title,
  Text,
  Container,
  Content,
  View,
  Left,
  Right,
  Button,
  Body,
  Icon,
  Segment,
  Item,
  Input,
  ActionSheet,
} from "native-base";
import FooterIcon from "../../shared/ui/footer/FooterIcon";
import FooterText from "../../shared/ui/footer/FooterText";
import Person from "../../shared/ui/Person";

const BUTTONS = [
  { text: "Option 0", icon: "american-football", iconColor: "#2c8ef4" },
  { text: "Option 1", icon: "analytics", iconColor: "#f42ced" },
  { text: "Option 2", icon: "aperture", iconColor: "#ea943b" },
  { text: "Delete", icon: "trash", iconColor: "#fa213b" },
  { text: "Cancel", icon: "close", iconColor: "#25de5b" },
];
const DESTRUCTIVE_INDEX = 3;
const CANCEL_INDEX = 4;

export default class Universe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      analyze: "universe",
      type: "person",
    };
  }

  render() {
    return (
      <Container>
        <Segment style={styles.segmentContainer}>
          <Button first danger>
            <Text>News</Text>
          </Button>
          <Button dark>
            <Text>Profiling</Text>
          </Button>
          <Button last dark>
            <Text>Respone</Text>
          </Button>
        </Segment>

        <Header style={styles.headerContainer}>
          <Body style={{ width: 100 }}>
            <Item>
              <Icon name="ios-search" />
              <Input placeholder="Search" />
              {/* <Icon name="ios-people" /> */}
            </Item>
          </Body>
          <Button
            transparent
            onPress={() =>
              ActionSheet.show(
                {
                  options: BUTTONS,
                  cancelButtonIndex: CANCEL_INDEX,
                  destructiveButtonIndex: DESTRUCTIVE_INDEX,
                  title: "Filter",
                },
                (buttonIndex) => {
                  this.setState({ clicked: BUTTONS[buttonIndex] });
                }
              )
            }
          >
            <Icon name="apps" style={{ color: "#fa423b" }} />
          </Button>
        </Header>
        <Content>
          <Person
            analyze="universe"
            type={this.state.type}
            navigation={this.props.navigation}
          />
        </Content>
        <FooterText analyze="universe-news" />
        {/* <FooterIcon analyze="universe-news" active="universe-news" /> */}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});
