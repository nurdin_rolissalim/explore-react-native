import React, {
  useState,
  useEffect,
} from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Content,
} from "native-base";

import EntityController from "app/shared/controllers/content/EntityController";
import SearchController from "app/shared/controllers/search/SearchController";
import FooterIcon from "app/shared/ui/footer/FooterIcon";
import FabIcon from "app/shared/ui/fab/FabIcon";
import SegmentButton from "app/shared/ui/segments/SegmentButton";
import { connect } from "react-redux";
import { setPage, getEntity } from "app/redux/ducks";

function UniverseNews(props) {
  const [type, setType] = useState("person");
  const [typeIcon, setTypeicon] = useState("user");
  const [segment, typeSegment] = useState("user");
  const [color, setColor] = useState("#fa423b");
  const [analyze, setAnalyze] = useState("universe-person");
  const [keyword, setKeyword] = useState("");
  const navigation = useNavigation();

  const callbackFooter = (footer) => {
    setType(footer.navigate);
    setTypeicon(footer.name);
    setKeyword("");
    setAnalyze(`universe-${footer.navigate}`);
    updatePage(footer.navigate);

  };
  const callbackEntity = (entity) => {
    navigation.navigate("universe-detail", {
      analyze: analyze,
      page: "universe-news",
      platform: "news",
      type: type,
      entity,
    });
  };

  const callbackSearch = (keyword) => {
    setKeyword(keyword);
  };

  const callbackSegment = (segment) => {
    navigation.navigate(segment);

  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action


      updatePage(type)
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const updatePage = (type) => {
    let page = {
      name: "universe-news",
      type: type,
      platform: "news"
    }
    props.setPage(page)
  }


  return (
    <Container>
      <SegmentButton
        analyze="universe"
        typeSegment="news"
        callback={callbackSegment}
      />

      <Content>
        <SearchController
          analyze="universe"
          keyword={keyword}
          type={type}
          callback={callbackSearch}
        />
        <EntityController
          cPage="universe-news"
          type={type}
          platform="news"
          analyze="universe-news"
          navigation={props.navigation}
          navigate="universe-detail"
          callback={callbackEntity}
          keyword={keyword}
        />
      </Content>

      {/* <FooterIcon
        analyze="universe-news"
        type={type}
        callback={callbackFooter}
      /> */}

      <FabIcon
        analyze="universe-news"
        type={typeIcon}
        color={color}
        callback={callbackFooter}
      />
    </Container>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});

const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  // getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UniverseNews);
