import React, { useState, useEffect } from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Content,
} from "native-base";
import { connect } from "react-redux";
import SegmentButton from "app/shared/ui/segments/SegmentButton";
import SearchController from "app/shared/controllers/search/SearchController";
import EntityController from "app/shared/controllers/content/EntityController";
import { setPage } from "app/redux/ducks";

function UniverseProfile(props) {
  const navigation = useNavigation();

  const [type, setType] = useState("person");
  const [keyword, setKeyword] = useState("");

  const callbackEntity = (entity) => {
    navigation.navigate("universe-detail-sosmed", {
      analyze: "universe-activity",
      page: "universe-activity",
      platform: "twitter",
      type: type,
      entity,
    });
  };
  const callbackSearch = (keyword) => {
    setKeyword(keyword);
  };

  const callbackSegment = (segment) => {
    navigation.navigate(segment)
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // The screen is focused
      // Call any action
      updatePage();
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);

  const updatePage = () => {
    let page = {
      name: "universe-activity",
      type: "person",
      platform: "twitter",
      analyze: "universe-activty"
    }
    props.changePage(page)
  }

  return (
    <Container>
      <SegmentButton analyze="universe" typeSegment="activity" callback={callbackSegment} />
      < Content >
        <SearchController
          analyze="universe"
          keyword={keyword}
          type={type}
          callback={callbackSearch}
        />

        <EntityController
          cPage="universe-profile"
          analyze="universe-sosmed-profile"
          type={type}
          navigation={props.navigation}
          navigate="universe-detail-sosmed"
          callback={callbackEntity}
          keyword={keyword}
        />
      </Content >
    </Container >
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});


const mapStateToProps = (state) => {
  const { page, type } = state;
  return { page, type };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changePage: (page) => dispatch(setPage(page)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UniverseProfile);
