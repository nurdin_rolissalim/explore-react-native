import React, { Component } from "react";
import { StyleSheet } from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Icon,
  Content,
} from "native-base";
import FabIcon from "app/shared/ui/fab/FabIcon";

import {
  TitleSection,
  TitleHeader,
  WrapperContent,
} from "app/shared/ui/StyledComponents";

import SummaryController from "app/shared/controllers/statistic/SummaryController";
import PostController from "app/shared/controllers/content/PostController";
import IssueController from "app/shared/controllers/content/IssueController";
import ChartController from "app/shared/controllers/charts/ChartController";
import StatisticController from "app/shared/controllers/statistic/StatisticController";
import ProfileImageController from "app/shared/controllers/content/ProfileImageController";

export default class UniverseDetailSosmed extends Component {
  constructor(props) {
    super(props);

    this.state = {
      type: "twitter",
      color: "#5067FF",
      platform: "twitter",
      type: "person",
      typePage: (props.route.params.analyze == "universe-activity") ? "activity" : "response",
      analyze: props.route.params.analyze
    };

    console.log("state", this.state);
  }

  callbackType = (object) => {
    this.setState({ platform: object.name, color: object.color });
  };

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (nextState.platform != this.state.platform) {
      return true;
    }

    return false;
  }

  render() {
    let { analyze, page } = this.props.route.params;

    let { platform, type, color, typePage } = this.state;

    return (
      <Container>
        <Header hasSegment style={styles.headerContainer}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <TitleHeader>Universe Social Media</TitleHeader>
          </Body>
        </Header>
        <Content padder>
          <WrapperContent>
            <ProfileImageController
              analyze={analyze}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>total</TitleSection>
            <SummaryController
              analyze={analyze}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>issue</TitleSection>
            <IssueController
              analyze={analyze}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>timeline</TitleSection>
            <PostController
              analyze={analyze}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>
          <WrapperContent>
            <TitleSection>sentiment</TitleSection>
            <ChartController
              analyze={"universe-sentiment-" + typePage}
              type={type}
              cPage={page}
              platform={platform} />
          </WrapperContent>
          <WrapperContent>
            <TitleSection>emotion</TitleSection>
            {/* <BarChart analyze="universe-news-emotion" /> */}
            <ChartController
              analyze={"universe-emotion-" + typePage}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>

          <WrapperContent>
            <TitleSection>Performance</TitleSection>
            <ChartController
              analyze={"universe-performance-" + typePage}
              type={type}
              cPage={page}
              platform={platform}
            />
            {/* <LineChart
              analyze={"universe-performance" + typePage}
            /> */}
          </WrapperContent>
          <WrapperContent>
            <TitleSection>post type</TitleSection>
            <StatisticController
              analyze={"universe-post-type-" + typePage}
              type={type}
              cPage={page}
              platform={platform}
            />
            {/* <LineChart
              analyze={"universe-performance" + typePage}
            /> */}
          </WrapperContent>

          <WrapperContent>
            <TitleSection>hashtags cloud</TitleSection>
            <ChartController
              analyze={"universe-wordcloud-" + typePage}
              type={type}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>
        </Content>
        {/* <FooterIcon analyze="universe-activity" type={this.state.type} callback={this.callbackType} /> */}
        <FabIcon
          analyze="universe-activity"
          type={platform}
          color={color}
          callback={this.callbackType}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fa423b",
  },
});
