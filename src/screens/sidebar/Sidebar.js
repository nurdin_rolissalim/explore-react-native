import React from "react";
import { Image, Dimensions, Platform, StyleSheet } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Body,
  Badge,
  Thumbnail,
} from "native-base";
import { connect } from "react-redux";
import { getAsset } from "../../shared/helpers/helpers";

const drawerCover = require("../../assets/images/drawer-cover.png");
const drawerImage = require("../../assets/images/politica-icon-text-light.png");

const datas = [
  {
    key: 1,
    name: "Main menu",
    route: "main-menu-stack",
    icon: "home",
    bg: "#C5F442",
  },
  {
    key: 2,
    name: "Administrator",
    route: "admin",
    icon: "construct",
    bg: "#477EEA",
    types: "11",
  },
  {
    key: 3,
    name: "Catalog",
    route: "catalog",
    icon: "grid",
    bg: "#DA4437",
    types: "4",
  },
];

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
    };

    this.renderItem = this.renderItem.bind(this);
    // this.renderHeader = this.renderHeader.bind(this);
  }

  renderItem(item) {
    const { navigation } = this.props;
    return (
      <ListItem
        // key={item.key}
        button
        noBorder
        onPress={() => navigation.navigate(item.route)}
      >
        <Left>
          <Icon
            active
            type="Ionicons"
            name={item.icon}
            style={{ color: "#777", fontSize: 26, width: 30 }}
          />
          <Text style={styles.text}>{item.name}</Text>
        </Left>
        {item.types && (
          <Right style={{ flex: 1 }}>
            <Badge
              style={{
                borderRadius: 3,
                height: 25,
                width: 72,
                backgroundColor: item.bg,
              }}
            >
              <Text style={styles.badgeText}>{`${item.types} Types`}</Text>
            </Badge>
          </Right>
        )}
      </ListItem>
    );
  }

  render() {
    const avatar = getAsset(this.props.avatar);
    return (
      <>
        <Image source={drawerCover} style={styles.drawerCover} />
        <Image square style={styles.drawerImage} source={drawerImage} />
        <List
          dataArray={datas}
          renderRow={this.renderItem}
          keyExtractor={(item) => item.key.toString()}
        />
        <List style={{ backgroundColor: "#eee" }}>
          <ListItem
            style={{ marginTop: "auto", bottom: 0, padding: 0 }}
            // key={item.key}
            button
            noBorder
            onPress={() => this.props.navigation.navigate("my-profile")}
          >
            <Left>
              <Thumbnail small source={{ uri: avatar }} />
            </Left>
            <Body style={{ flex: 4 }}>
              <Text numberOfLines={1}>{this.props.fullname}</Text>
              <Text note>{this.props.email}</Text>
            </Body>
            <Right style={{ flex: 1 }}>
              <Icon
                type="MaterialCommunityIcons"
                name="arrow-right"
                fontSize="14"
              />
            </Right>
          </ListItem>
        </List>
      </>
    );
  }
}

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  drawerCover: {
    alignSelf: "stretch",
    height: deviceHeight / 4.5,
    width: null,
    position: "relative",
    marginBottom: 10,
    resizeMode: "cover",
    backgroundColor: "#eee",
    opacity: 0.8,
  },
  drawerImage: {
    position: "absolute",
    left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,
    top: Platform.OS === "android" ? deviceHeight / 13 : deviceHeight / 12,
    width: 200,
    height: 50,
    resizeMode: "contain",
    backgroundColor: "#aaa",
  },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20,
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined,
  },
});

const mapStateToProps = (state) => {
  const { fullname, avatar, email } = state.auth.userData;
  return { fullname, avatar, email };
};

export default connect(mapStateToProps)(Sidebar);
