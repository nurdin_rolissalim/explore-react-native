import React, { useEffect } from "react";
import { Container, Content, Text, Fab, Icon } from "native-base";
import CatalogListContainer from "app/shared/controllers/catalog/CatalogListContainer";
import { connect } from "react-redux";
import { setPage, getEntity, getfilter } from "app/redux/ducks";
import { useNavigation } from "@react-navigation/native";

function Catalog(props) {
  const navigation = useNavigation();
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action
      updatePage("person")
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const updatePage = (type) => {
    let page = {
      name: "catalog",
      type: type,
      platform: "news"
    }
    props.setPage(page)
  }
  return (
    <Container>
      <Content>
        <CatalogListContainer navigation={props.navigation} />
      </Content>
      <Fab
        position="bottomRight"
        style={{ backgroundColor: "rgba(250, 66, 59, 1)" }}
        onPress={() => navigation.navigate("catalog-add")}
      >
        <Icon name="add" fontSize={18} />
      </Fab>
    </Container>
  );
}

const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Catalog);