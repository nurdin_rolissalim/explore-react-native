import React from "react";
import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  View,
  Button,
  Thumbnail,
  Form,
  Item,
  Input,
} from "native-base";
import { connect } from "react-redux";
import * as propTypes from "prop-types";
import globalStyle from "app/shared/styles";
import { Formik } from "formik";

class CatalogAdd extends React.Component {
  constructor(props) {
    super(props);

    this.handlerSubmit = this.handlerSubmit.bind(this);
  }

  handlerSubmit(value) {
  }

  render() {
    return (
      <Container>
        <Content style={{ flex: 1, height: "100%" }}>
          <Formik
            initialValues={{
              catalogName,
              query,
            }}
            onSubmit={this.handlerSubmit}
          >
            {({ handleChange, handleSubmit, values }) => (
              <View style={globalStyle.form}>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Catalog name:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Catalog name"
                      onChangeText={handleChange("catalogName")}
                      value={values.catalogName}
                      name="catalogName"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Query:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Query"
                      onChangeText={handleChange("query")}
                      value={values.query}
                      name="query"
                    />
                  </Item>
                </View>
                <View style={globalStyle.formAction}>
                  <Button full primary rounded onPress={handleSubmit}>
                    <Text>Create catalog</Text>
                  </Button>
                </View>
              </View>
            )}
          </Formik>
        </Content>
      </Container>
    );
  }
}

CatalogAdd.propTypes = {
  catalog: propTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { catalog } = state;
  return { catalog };
};

export default connect(mapStateToProps)(CatalogAdd);
