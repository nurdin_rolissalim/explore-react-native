import React from "react";
import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  View,
  Button,
  Thumbnail,
  Item,
  Input,
} from "native-base";
import { connect } from "react-redux";
import * as propTypes from "prop-types";
import { Formik } from "formik";
import globalStyle from "app/shared/styles";

class CatalogEdit extends React.Component {
  constructor(props) {
    super(props);

    this.handlerSubmit = this.handlerSubmit.bind(this);
  }

  handlerSubmit(value) {
  }

  render() {
    const {
      catalog: { selectedCatalog, isFetching },
    } = this.props;

    return (
      <Container>
        <Content style={{ flex: 1, height: "100%" }}>
          <Formik
            initialValues={{
              catalogName: selectedCatalog.name,
              query: selectedCatalog.keyword,
            }}
            onSubmit={this.handlerSubmit}
          >
            {({ handleChange, handleSubmit, values }) => (
              <View style={globalStyle.form}>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Catalog name:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Catalog name"
                      onChangeText={handleChange("catalogName")}
                      value={values.catalogName}
                      name="catalogName"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Query:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Query"
                      onChangeText={handleChange("query")}
                      value={values.query}
                      name="query"
                    />
                  </Item>
                </View>
                <View style={globalStyle.formAction}>
                  <Button full primary rounded onPress={handleSubmit}>
                    <Text>Create catalog</Text>
                  </Button>
                </View>
              </View>
            )}
          </Formik>
        </Content>
      </Container>
    );
  }
}

CatalogEdit.propTypes = {
  catalog: propTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { management_catalog: catalog } = state;
  return { catalog };
};

export default connect(mapStateToProps)(CatalogEdit);
