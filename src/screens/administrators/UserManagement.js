import React, { useEffect } from "react";
import { Container, Content, Text, Fab, Icon } from "native-base";
import AccountManagementContainer from "app/shared/controllers/administrator/AccountManagementContainer";
import { connect } from "react-redux";
import { setPage, getEntity, getfilter } from "app/redux/ducks";
import { useNavigation } from "@react-navigation/native";

function UserManagement(props) {
  const navigation = useNavigation();
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action
      updatePage("person")
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const updatePage = (type) => {
    let page = {
      name: "userManagement",
      type: type,
      platform: "news"
    }
    props.setPage(page)
  }
  return (
    <Container>
      <Content>
        <AccountManagementContainer navigation={props.navigation} />
      </Content>
      <Fab
        position="bottomRight"
        style={{ backgroundColor: "rgba(250, 66, 59, 1)" }}
        onPress={() => navigation.navigate("user-add-management")}
      >
        <Icon name="add" fontSize={18} />
      </Fab>
    </Container>
  );
}

const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserManagement);
