import React from "react";
import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  View,
  Button,
  Thumbnail,
} from "native-base";
import { connect } from "react-redux";
import * as propTypes from "prop-types";
import { WrapperContent } from "app/shared/ui/StyledComponents";
import { getAsset } from "../../shared/helpers/helpers";

class UserManagementDetail extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      user: { selectedUser, isFetching },
      navigation,
    } = this.props;
    return (
      <Container>
        <Content style={{ flex: 1, height: "100%" }}>
          <WrapperContent>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                marginVertical: 10,
              }}
            >
              <Thumbnail
                large
                source={{ uri: getAsset(`${selectedUser.images}`) }}
                style={{ marginBottom: 10 }}
              />
              <Text style={{ fontSize: 18 }}>{selectedUser.fullname}</Text>
              <Text style={{ fontSize: 16, color: "#666" }}>
                {selectedUser.email}
              </Text>
            </View>
          </WrapperContent>
          <List>
            <ListItem />
            <ListItem
              onPress={() => navigation.navigate("user-edit-management")}
            >
              <Body>
                <Text>Change user data</Text>
                <Text note>Change name, email, etc</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem onPress={() => navigation.navigate("catalog")}>
              <Body>
                <Text>Catalog</Text>
                <Text note>Catalog</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

UserManagementDetail.propTypes = {
  user: propTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { management_user: user } = state;
  return { user };
};

export default connect(mapStateToProps)(UserManagementDetail);
