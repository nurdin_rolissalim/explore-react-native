import React from "react";
import {
  Container,
  Content,
  Text,
  List,
  ListItem,
  Left,
  Right,
  Icon,
  Body,
  View,
  Button,
  Thumbnail,
  Form,
  Item,
  Input,
} from "native-base";
import { connect } from "react-redux";
import * as propTypes from "prop-types";
import { WrapperContent } from "app/shared/ui/StyledComponents";
import { TextInput } from "react-native";
import globalStyle from "app/shared/styles";
import { Formik } from "formik";
import { getAsset } from "../../shared/helpers/helpers";

class UserManagementEdit extends React.Component {
  constructor(props) {
    super(props);

    this.handlerSubmit = this.handlerSubmit.bind(this);
  }

  handlerSubmit(value) {
  }

  render() {
    const {
      user: { selectedUser, isFetching },
    } = this.props;

    return (
      <Container>
        <Content style={{ flex: 1, height: "100%", marginBottom: 10 }}>
          <Formik
            initialValues={{
              username: selectedUser.username,
              full_name: selectedUser.full_name,
              email: selectedUser.email,
              period: selectedUser.period,
              role_id: selectedUser.role_id,
              keyword: selectedUser.keyword,
              type_location: selectedUser.type_location,
              location: selectedUser.location,
              workspace: selectedUser.workspace,
            }}
            onSubmit={this.handlerSubmit}
          >
            {({ handleChange, handleSubmit, values }) => (
              <View style={globalStyle.form}>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Username:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Username"
                      onChangeText={handleChange("username")}
                      value={values.username}
                      name="username"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Fullname:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Fullname"
                      onChangeText={handleChange("full_name")}
                      value={values.full_name}
                      name="full_name"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Email:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Email"
                      onChangeText={handleChange("email")}
                      value={values.email}
                      name="email"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Period:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Period"
                      onChangeText={handleChange("period")}
                      value={values.period}
                      name="period"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Role:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Role"
                      onChangeText={handleChange("role_id")}
                      value={values.role_id}
                      name="role_id"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Keyword:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Keyword"
                      onChangeText={handleChange("keyword")}
                      value={values.keyword}
                      name="keyword"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Type location:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Type location"
                      onChangeText={handleChange("type_location")}
                      value={values.type_location}
                      name="type_location"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Location:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Location"
                      onChangeText={handleChange("location")}
                      value={values.location}
                      name="location"
                    />
                  </Item>
                </View>
                <View style={globalStyle.inputGroup}>
                  <Text style={globalStyle.inputLabel}>Workspace:</Text>
                  <Item regular style={globalStyle.inputView}>
                    <Input
                      style={globalStyle.inputText}
                      placeholder="Workspace"
                      onChangeText={handleChange("workspace")}
                      value={values.workspace}
                      name="workspace"
                    />
                  </Item>
                </View>
                <View style={globalStyle.formAction}>
                  <Button full primary rounded onPress={handleSubmit}>
                    <Text>Save</Text>
                  </Button>
                </View>
              </View>
            )}
          </Formik>
        </Content>
      </Container>
    );
  }
}

UserManagementEdit.propTypes = {
  user: propTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { management_user: user } = state;
  return { user };
};

export default connect(mapStateToProps)(UserManagementEdit);
