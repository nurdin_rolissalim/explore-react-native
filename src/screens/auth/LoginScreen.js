import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Image,
  Keyboard,
  AsyncStorage,
} from "react-native";
import { connect } from "react-redux";
import * as propTypes from "prop-types";

import {
  Item,
  Input,
  Icon,
  Form,
  Toast,
  Container,
  Content,
} from "native-base";
import PlaceHolder from "app/shared/ui/utility/PlaceHolder";
import { signIn } from "../../redux/ducks";

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      hidePassword: true,
      loading: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.onToggle = this.onToggle.bind(this);
  }

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.isSignedIn) {
  //     this.props.navigation.navigate("dashboard");
  //   } else {
  //     Toast.show({
  //       text: "Failed to log in, check again username/password",
  //       position: "top",
  //       buttonText: "Okay",
  //       duration: 2000,
  //     });
  //   }
  // }

  componentDidMount() {
    if (this.props.isSignedIn) {
      this.props.navigation.navigate("dashboard");
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    const { username, password } = this.state;
    // this.setState({ loading: true })
    this.props.onLogin({ username, password });
    // this.setState({ loading: false })

    Keyboard.dismiss();
  }

  onToggle(e) {
    e.preventDefault();
    const { hidePassword } = this.state;
    this.setState({ hidePassword: !hidePassword });
  }

  render() {
    const { loading } = this.state;
    return (
      <Container>
        <Content>
          <Image
            style={styles.logo}
            source={require("assets/images/politica-icon-text.png")}
          />
          <Form style={styles.form}>
            <Item regular style={styles.inputView}>
              <Icon
                style={styles.inputIcon}
                type="MaterialCommunityIcons"
                name="account"
                size={24}
                color="rgba(0, 0, 0, 0.1)"
              />
              <Input
                style={styles.inputText}
                placeholder="Username"
                onChangeText={(text) => this.setState({ username: text })}
              />
            </Item>

            <Item regular style={styles.inputView}>
              <Icon
                style={styles.inputIcon}
                type="MaterialCommunityIcons"
                name="lock"
                size={24}
                color="rgba(0, 0, 0, 0.5)"
              />
              <Input
                secureTextEntry={this.state.hidePassword}
                style={styles.inputText}
                placeholder="Password"
                onChangeText={(text) => this.setState({ password: text })}
              />
              <Icon
                style={[styles.inputIcon, styles.pullRight]}
                type="MaterialCommunityIcons"
                name={this.state.hidePassword ? "eye-off" : "eye"}
                size={24}
                color="rgba(0, 0, 0, 0.5)"
                onPress={this.onToggle}
              />
            </Item>
            <TouchableOpacity
              style={styles.loginBtn}
              onPress={this.handleSubmit}
            >
              <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
          </Form>

          <PlaceHolder loading={loading} data={false} />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: 320,
    height: 100,
    resizeMode: "contain",
    opacity: 0.65,
    borderWidth: 1,
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 64,
    marginBottom: 24,
  },
  form: {
    paddingHorizontal: 18,
  },
  inputView: {
    backgroundColor: "rgba(0, 0, 0, 0.05)",
    marginBottom: 10,
  },
  pullRight: {
    position: "absolute",
    right: -10,
    paddingHorizontal: 0,
  },
  inputIcon: {
    width: 50,
    height: 50,
    paddingHorizontal: 10,
    paddingVertical: 12,
    color: "rgba(0,0,0,0.5)",
  },
  inputText: {
    height: 50,
    color: "#222222",
    fontSize: 16,
  },
  inputViewError: {
    borderWidth: 1,
    borderColor: "red",
    backgroundColor: "rgba(250, 198, 55, 0.15)",
  },
  loginBtn: {
    width: "100%",
    backgroundColor: "#fb5b5a",
    borderRadius: 5,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  loginText: {
    fontSize: 15,
    color: "#ffffff",
    fontWeight: "600",
  },
});

LoginScreen.propTypes = {
  isSignedIn: propTypes.bool.isRequired,
  onLogin: propTypes.func.isRequired,
  navigation: propTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { isSignedIn } = state.auth;

  return { isSignedIn };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogin: (userData) => dispatch(signIn(userData)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
