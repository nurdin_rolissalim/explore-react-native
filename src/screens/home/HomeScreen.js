import React, { Component } from "react";
import { Container, Content } from "native-base";

import { connect } from "react-redux";
import EntityController from "app/shared/controllers/content/EntityController";
import IssueController from "../../shared/controllers/content/IssueController";
import NewsController from "app/shared/controllers/content/NewsController";
import HomePersonContainer from "app/shared/controllers/home/HomePersonContainer";
import HomeIssueContainer from "app/shared/controllers/home/HomeIssueContainer";
import HomeNewsContainer from "app/shared/controllers/home/HomeNewsContainer";
import { setFilter, setPage } from "app/redux/ducks";
import { getDateTimeRangeByType } from 'app/helpers/DateHelper';
import { sub } from "react-native-reanimated";

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subscribe: null,
      analyze: "home-person"
    }

  }

  newFilter() {
    let { user } = this.props
    let period = (user.period != "") ? user.period : "m";
    let page = {
      name: "home",
      type: "person",
      analyze: "home-person",
      platform: "news"
    }

    let date = getDateTimeRangeByType(
      period
    )

    let filter = {
      page: page,
      timeframe: period,
      startDate: date.startTimeStamp,
      endDate: date.endTimeStamp,
      location: [],
      locations: [],
    }

    this.props.setFilter(filter);
    this.props.setPage(page);
  }

  UNSAFE_componentWillMount() {
    this.newFilter();
    this.setState({ subscribe: null })
  }

  componentDidMount() {
    const { navigation } = this.props;
    let subscribe = navigation.addListener('focus', () => {
      // do something
      this.updatePage("person")
      console.log("update pafe");


    });

    this.setState({ subscribe: subscribe })
  }

  updatePage(type) {
    let page = {
      name: "home",
      type: type,
      platform: "news"
    }
    this.props.setPage(page)
  }

  callbackEntity(entity) {
    console.log("entity", entity);
  }
  callbackIssue(issue) {
    console.log("issue", issue);
  }

  render() {
    const { navigation } = this.props;
    const { analyze, type } = this.state;

    console.log("home ");
    return (
      <Container>
        <Content>
          {/* <HomePersonContainer navigation={navigation} /> */}
          <EntityController
            cPage="home"
            type="person"
            platform="news"
            analyze="home-person"
            navigation={navigation}
            navigate=""
            callback={this.callbackEntity}
            keyword=""
          />
          {/* <HomeIssueContainer navigation={navigation} /> */}
          <IssueController
            analyze="home-issue"
            type="news"
            cPage="home-issue"
            platform="news"
            callback={this.callbackIssue}
          />
          {/* <HomeNewsContainer navigation={navigation} /> */}
          <NewsController
            analyze={analyze}
            type={type}
            cPage="home"
            platform="news"
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const user = state.auth.userData;
  const { filter } = state;

  return { user, filter };
};

const mapDispatchToProps = {
  setFilter,
  setPage
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
