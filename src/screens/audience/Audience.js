import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { Container, Content } from "native-base";

import { TitleSection, WrapperContent } from "app/shared/ui/StyledComponents";

import { connect } from "react-redux";
import { setPage } from 'app/redux/ducks';

import FabIcon from "app/shared/ui/fab/FabIcon";
import ChartController from "app/shared/controllers/charts/ChartController";
import StatisticController from "app/shared/controllers/statistic/StatisticController";
import PostController from "app/shared/controllers/content/PostController";

class AudienceScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "audience",
      analyze: "audience",
      type: "twitter",
      color: "#5067FF",
      platform: "twitter",
      typePage: "location"
    };
  }

  UNSAFE_componentWillMount() {
    let { page, platform, analyze } = this.state;
    this.props.setPage({
      name: page,
      analyze: "person",
      platform: platform
    })
  }

  callbackType = (object) => {
    this.setState({ platform: object.name, color: object.color });
  };

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return true;
  }

  UNSAFE_componentWillUpdate(nextProps, nextState) {
    if (this.state.platform != nextState.platform) {
      // this.changeDate(nextState.period)
    }
  }

  render() {
    let { page, analyze, platform, color, typePage } = this.state;
    return (
      <Container>
        <Content>
          <WrapperContent>
            <TitleSection>Location</TitleSection>
            <StatisticController
              analyze="audience-map"
              type={"person"}
              cPage={page}
              platform={"socmed"}
            />
          </WrapperContent>
          <WrapperContent>
            <TitleSection>trending</TitleSection>
            {/* <WordCloudChart analyze="audience" /> */}
            <ChartController
              analyze={"audience-wordcloud"}
              type={"person"}
              cPage={page}
              platform={"socmed"}
            />
          </WrapperContent>
          <WrapperContent>
            <TitleSection>{platform}</TitleSection>
            <PostController
              analyze={"audience-post-" + typePage}
              type={typePage}
              cPage={page}
              platform={platform}
            />
          </WrapperContent>
        </Content>
        <FabIcon
          analyze="audience"
          type={platform}
          color={color}
          callback={this.callbackType}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    padding: 10,
    fontSize: 16,
    fontWeight: "bold",
  },
  right: {
    position: "absolute",
    right: 0,
  },
});


const mapStateToProps = (state) => {

  const { page, type, filter, auth } = state;
  let user = auth.userData;
  return { page, type, filter, user };
};

const mapDispatchToProps = {
  setPage,
  // getfilter,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AudienceScreen);