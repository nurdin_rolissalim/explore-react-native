import React, { useState } from "react";
import { Container } from "native-base";
import { StyleSheet } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { useNavigation } from "@react-navigation/native";

import FooterIcon from "app/shared/ui/footer/FooterIcon";
import FabIcon from "app/shared/ui/fab/FabIcon";
import VolunteerRecruitment from "./VolunteerRecruitment";
import VolunteerEvent from "./VolunteerEvent";
import VolunteerCalendar from "./VolunteerCalendar";
import VolunteerIssue from "./VolunteerIssue";

const VolunteerStack = createStackNavigator();

export default function VolunteerIndex(props) {
  const navigation = useNavigation();
  const [type, setType] = useState("recruitment");
  const [typeIcon, setTypeicon] = useState("user-tie");
  const [color, setColor] = useState("#fa423b");
  const callbackFooter = (footer) => {
    setType(footer);
    setTypeicon(footer.name)
    // navigation.navigate(`volunteer-${footer}`);
    console.log("footer", footer);

    navigation.navigate(`volunteer-${footer.navigate}`);
  };
  return (
    <Container>
      <VolunteerStack.Navigator
        initialRouteName="Recruitment"
        headerMode={false}
      >
        <VolunteerStack.Screen
          name="volunteer-recruitment"
          component={VolunteerRecruitment}
        />
        <VolunteerStack.Screen
          name="volunteer-event"
          component={VolunteerEvent}
        />
        <VolunteerStack.Screen
          name="volunteer-calendar"
          component={VolunteerCalendar}
        />
        <VolunteerStack.Screen
          name="volunteer-issue"
          component={VolunteerIssue}
        />
      </VolunteerStack.Navigator>
      {/* </Content> */}
      {/* <FooterIcon analyze="volunteer" type={type} callback={callbackFooter} /> */}
      <FabIcon
        analyze="volunteer"
        type={typeIcon}
        color={color}
        callback={callbackFooter}
      />
    </Container>
  );
}

const styles = StyleSheet.create({
  headButton: {
    flexDirection: "row",
    marginHorizontal: "auto",
    marginVertical: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  headButtonItem: {
    borderColor: "#aaa",
    borderWidth: 1,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
});
