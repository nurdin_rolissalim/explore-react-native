import React, {
  useState,
  useEffect
} from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Content,
  Text
} from "native-base";
import { connect } from "react-redux";
import {
  setPage,
  getEntity,
  getfilter
} from "app/redux/ducks";
import {
  TitleSection,
  TitleHeader,
  WrapperContent,
} from "app/shared/ui/StyledComponents";

import ChartVolunteerController from "app/shared/controllers/charts/ChartVolunteerController";
import EventController from "app/shared/controllers/content/EventController";

function VolunteerCalendar(props) {
  const navigation = useNavigation();
  const [page, setPage] = useState("volunteer-calendar");
  const [type, setType] = useState("calendar");
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action


      updatePage(type)
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const updatePage = (type) => {
    let page = {
      name: "volunteer-calendar",
      type: type,
      platform: "news"
    }
    props.setPage(page)
  }
  return (
    <Container>
      <Content >
        <WrapperContent>
          <TitleSection>events</TitleSection>
          <EventController
            analyze={page}
            type={type}
            cPage={page}
          />
        </WrapperContent>
      </Content >
    </Container>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});
const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VolunteerCalendar);