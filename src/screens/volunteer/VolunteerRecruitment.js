import React, { useState, useEffect } from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Content,
} from "native-base";

import {
  TitleSection,
  WrapperContent,
} from "app/shared/ui/StyledComponents";

import { connect } from "react-redux";
import { setPage, getEntity, getfilter } from "app/redux/ducks";

import DonutChart from "app/shared/ui/charts/DonutChart";
import BarChart from "app/shared/ui/charts/BarChart";

import SummaryController from "app/shared/controllers/statistic/SummaryController";
import ChartVolunteerController from "app/shared/controllers/charts/ChartVolunteerController";

function VolunteerRecruitment(props) {
  const navigation = useNavigation();
  const [page, setPage] = useState("volunteer-recruitment");
  const [type, setType] = useState("recruitment");

  const callbackFooter = (footer) => {
    navigation.navigate(`volunteer-${footer}`);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action


      updatePage(type)
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const updatePage = (type) => {
    let page = {
      name: "volunteer-recruitment",
      type: type,
      platform: "news"
    }
    props.setPage(page)
  }

  return (
    <Container>
      <Content>
        <WrapperContent>
          <TitleSection>gender</TitleSection>
          <ChartVolunteerController
            analyze={page + "-gender"}
            type={type}
            cPage={page}
          />
        </WrapperContent>
        <WrapperContent>
          <TitleSection>maps</TitleSection>
          <ChartVolunteerController
            analyze={page + "-map"}
            type={type}
            cPage={page}
          />
        </WrapperContent>
        <WrapperContent>
          <TitleSection>religion</TitleSection>
          <ChartVolunteerController
            analyze={page + "-religion"}
            type={type}
            cPage={page}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>total volunteers</TitleSection>
          <SummaryController
            analyze={page + "-total-volunteers"}
            type={type}
            cPage={page}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>monthly growth</TitleSection>
          <SummaryController
            analyze={page + "-monthly-growth"}
            type={type}
            cPage={page}
          />
        </WrapperContent>

        <WrapperContent>
          <TitleSection>campaign</TitleSection>
          <ChartVolunteerController
            analyze={page + "-campaign"}
            type={type}
            cPage={page}
          />
        </WrapperContent>
        <WrapperContent>
          <TitleSection>education</TitleSection>
          <ChartVolunteerController
            analyze={page + "-education"}
            type={type}
            cPage={page}
          />
        </WrapperContent>
        <WrapperContent>
          <TitleSection>volunteer growth</TitleSection>
          <BarChart analyze="universe-news-emotion" />
        </WrapperContent>
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});

const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VolunteerRecruitment);
