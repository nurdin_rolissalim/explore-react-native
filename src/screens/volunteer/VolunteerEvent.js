import React, { Component, useState, useEffect } from "react";
import FooterIcon from "app/shared/ui/footer/FooterIcon";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Container, Content } from "native-base";
import { connect } from "react-redux";
import { setPage, getEntity, getfilter } from "app/redux/ducks";
import {
  TitleSection,
  TitleHeader,
  WrapperContent,
} from "app/shared/ui/StyledComponents";

import ChartVolunteerController from "app/shared/controllers/charts/ChartVolunteerController";

import DonutChart from "app/shared/ui/charts/DonutChart";

function VolunteerEvent(props) {
  const navigation = useNavigation();
  const [page, setPage] = useState("volunteer-event");
  const [type, setType] = useState("event");

  const callbackFooter = (footer) => {
    navigation.navigate(`volunteer-${footer}`);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action


      updatePage(type)
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const updatePage = (type) => {
    let page = {
      name: "volunteer-event",
      type: type,
      platform: "news"
    }
    props.setPage(page)
  }
  return (
    <Container>
      <Content>
        <WrapperContent>
          <TitleSection>maps</TitleSection>
          <ChartVolunteerController
            analyze={page + "-map"}
            type={type}
            cPage={page}
          />
        </WrapperContent>
        <WrapperContent>
          <TitleSection>events</TitleSection>
          <ChartVolunteerController
            analyze={page}
            type={type}
            cPage={page}
          />
        </WrapperContent>
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});

const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VolunteerEvent);
