import React, { Component, useState, useEffect } from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Content,
} from "native-base";
import { connect } from "react-redux";
import { setPage, getEntity, getfilter } from "app/redux/ducks";

import {
  TitleSection,
  TitleHeader,
  WrapperContent,
} from "app/shared/ui/StyledComponents";
import FabIcon from "app/shared/ui/fab/FabIcon";
import PickerCustom from "app/shared/ui/picker/PickerCustom";
import PostController from "app/shared/controllers/content/PostController";


function VolunteerIssue(props) {
  const [type, setType] = useState("volunteer");
  const [analyze, setAnalyze] = useState("volunteer-issue");
  const [category, setCategory] = useState(32);
  const [page, setPage] = useState("volunteer-issue");
  const navigation = useNavigation();

  const callbackPicker = (picker) => {
    setCategory(picker);
  };


  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', (e) => {
      // The screen is focused
      // Call any action


      updatePage(type)
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);


  const updatePage = (type) => {
    let page = {
      name: analyze,
      type: type,
      platform: ""
    }
    props.setPage(page)
  }

  return (
    <Container>
      <Content>
        {/* <WrapperContent> */}
        <PickerCustom
          analyze={analyze}
          type={"volunteer-issue"}
          cPage={page}
          callback={callbackPicker}
        />
        <PostController
          analyze={analyze + "-detail"}
          platform={category}
          type={"volunteer"}
          cPage={page}
        />
        {/* </WrapperContent> */}
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fff",
    borderColor: "#fff",
    borderTopWidth: 0,
    elevation: 0,
  },
  segmentContainer: {
    backgroundColor: "#fff",
  },
});

const mapStateToProps = (state) => {
  const { page, type, filter, issue, entity } = state;
  return { page, type, filter, issue };
};

const mapDispatchToProps = {
  setPage,
  getfilter,
  getEntity
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VolunteerIssue);
