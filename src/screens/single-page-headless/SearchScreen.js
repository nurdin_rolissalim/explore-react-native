import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";
import {
  Container,
  Content,
  Form,
  Input,
  Icon,
  Header,
  Button,
  Left,
  Body,
  Right,
} from "native-base";

import NewsItemRow from "app/shared/ui/NewsItemRow";

export default function SearchScreen(props) {
  const [query, setQuery] = useState("");
  const navigation = useNavigation();
  return (
    <Container>
      <Header style={{ backgroundColor: "#fb5b5a" }}>
        <Left style={{ flex: 1 }}>
          <Button transparent onPress={() => navigation.goBack()}>
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Right style={{ flex: 10 }}>
          <Form style={styles.form}>
            <Input
              style={styles.inputText}
              placeholder="Type keyword e.g: jokowi"
              placeholderTextColor="rgba(255,255,255,1)"
              onChangeText={(text) => setQuery(text)}
            />
          </Form>
        </Right>
      </Header>
      <Content padder>
        {query ? <ResultOfSearch query={query} /> : <FeatureSearchKeyword />}
      </Content>
    </Container>
  );
}

function ResultOfSearch({ query }) {
  return (
    <>
      <Text
        style={{
          fontSize: 22,
          fontWeight: "bold",
          letterSpacing: 2,
          marginBottom: 10,
        }}
      >
        Result search of "{query}":
      </Text>
      <NewsItemRow />
      <NewsItemRow />
      <NewsItemRow />
      <NewsItemRow />
      <NewsItemRow />
    </>
  );
}

function FeatureSearchKeyword() {
  return (
    <>
      <Text
        style={{
          fontSize: 22,
          fontWeight: "bold",
          letterSpacing: 2,
          marginBottom: 10,
        }}
      >
        Featured search:
      </Text>
      <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
        <Button bordered rounded style={{ marginRight: 5, marginBottom: 7 }}>
          <Text
            style={{ fontSize: 16, paddingVertical: 0, paddingHorizontal: 16 }}
          >
            Jokowi
          </Text>
        </Button>
        <Button bordered rounded style={{ marginRight: 5, marginBottom: 7 }}>
          <Text
            style={{ fontSize: 16, paddingVertical: 0, paddingHorizontal: 16 }}
          >
            COVID19
          </Text>
        </Button>
        <Button bordered rounded style={{ marginRight: 5, marginBottom: 7 }}>
          <Text
            style={{ fontSize: 16, paddingVertical: 0, paddingHorizontal: 16 }}
          >
            Wabah Corona
          </Text>
        </Button>
        <Button bordered rounded style={{ marginRight: 5, marginBottom: 7 }}>
          <Text
            style={{ fontSize: 16, paddingVertical: 0, paddingHorizontal: 16 }}
          >
            #TerawanOut
          </Text>
        </Button>
        <Button bordered rounded style={{ marginRight: 5, marginBottom: 7 }}>
          <Text
            style={{ fontSize: 16, paddingVertical: 0, paddingHorizontal: 16 }}
          >
            Lockdown
          </Text>
        </Button>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  form: {
    width: "100%",
    height: 50,
    alignSelf: "center",
    alignItems: "center",
    borderWidth: 0,
  },
  inputText: {
    width: "100%",
    paddingVertical: 10,
    paddingHorizontal: 32,
    borderWidth: 0,
    // backgroundColor: 'rgba(0, 0, 0, 0.05)',
    // marginBottom: 10,
    color: "#fff",
  },
});
