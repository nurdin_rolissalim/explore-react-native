import React, { Component } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Segment,
  Content,
  Text,
} from "native-base";

import NewsItemRow from "app/shared/ui/NewsItemRow";

export default function NewsScreen() {
  const navigation = useNavigation();
  const route = useRoute();

  return (
    <Container>
      <Header style={{ backgroundColor: "#fb5b5a" }}>
        <Left>
          <Button transparent onPress={() => navigation.goBack()}>
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Text style={{ color: "#fff" }}>{route.params.title}</Text>
        </Body>
      </Header>
      <Content padder>
        <NewsItemRow />
        <NewsItemRow />
        <NewsItemRow />
        <NewsItemRow />
        <NewsItemRow />
      </Content>
    </Container>
  );
}
