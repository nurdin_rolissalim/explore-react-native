import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  DatePicker,
  Text,
  Left,
  Body,
  Button,
  Icon,
  Item,
  Picker,
  View,
} from "native-base";
import { StyleSheet, ScrollView } from "react-native";
import { TitleHeader } from "app/shared/ui/StyledComponents";
import MultiSelect from "react-native-multiple-select";
import { connect } from "react-redux";
import { setFilter, getPage } from "app/redux/ducks";
import moment from "moment";
// import DeviceInfo from 'react-native-device-info';
// import 'moment/min/moment-with-locales';
import 'moment-timezone';
import { getUniqueId, getManufacturer } from 'react-native-device-info';
import { getDateTimeRangeByType } from 'app/helpers/DateHelper';

class FilterDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      endDate: new Date(),
      period: "m",
      disabled: true,
      single: false,
      datePicker: true,
      selectedLocations: [],
      locations: [
        {
          id: "jakarta",
          name: "Jakarta",
        },
        {
          id: "jawa barat",
          name: "Jawa Barat",
        },
        {
          id: "jawa tengah",
          name: "Jawa Tengah",
        },
        {
          id: "jawa timur",
          name: "Jawa Timur",
        },
        {
          id: "kalimantan timur",
          name: "Kalimantan Timur",
        },
        {
          id: "kalimantan tengah",
          name: "Kalimantan Tengah",
        },
        {
          id: "kalimantan barat",
          name: "Kalimantan Barat",
        },
        {
          id: "papua",
          name: "Paua",
        },
        {
          id: "sulawesi",
          name: "sulawesi",
        },
      ],
      location: false

    };
    this.setStartDate = this.setStartDate.bind(this);
    this.setEndDate = this.setEndDate.bind(this);
    this.applyFilter = this.applyFilter.bind(this);
  }

  setStartDate(newDate) {
    this.setState({ startDate: newDate });
  }

  setEndDate(newDate) {
    this.setState({ endDate: newDate });
  }

  changePeriod(period) {
    if (period == "adv") {
      this.setState({ period, disabled: true });
    } else {
      // this.changeDate(period)
      this.setState({ period, disabled: false });
    }
  }


  changeDate(period) {
    let disabled = true;
    let startDate = "";
    let endDate = "";
    if (period === "adv") {
      disabled = false;
      startDate = this.state.startDate;
      endDate = this.state.endDate;
    } else {
      if (period === "") {
        period = "m"
      }
      let date = getDateTimeRangeByType(
        period,
        this.startDate,
        this.endDate
      )
      startDate = date.start_date
      endDate = date.end_date

    }
    this.setState(
      {
        period: period,
        disabled: disabled,
        startDate: startDate,
        endDate: endDate,
      }
    );
  }

  onSelectedLocationsChange = (selectedLocations) => {
    this.setState({ selectedLocations });
  };

  clearSelectedCategories = () => {
    this._multiSelect._removeAllItems();
  };

  goBack() {
    this.props.navigation.goBack()
  }

  applyFilter() {
    let { page } = this.props;
    let filter = {
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      timeframe: this.state.period,
      location: this.state.selectedLocations,
      locations: this.state.selectedLocations,
      page: page,
    }
    this.props.setFilter(filter);
    // this.props.changeFilter(filter);
    this.goBack();
  }


  UNSAFE_componentWillUpdate(nextProps, nextState) {
    if (this.state.period != nextState.period) {
      // this.changeDate(nextState.period)
    }
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.period != prevState.period) {
      this.changeDate(this.state.period)
    }
  }

  getView() {
    let { page } = this.props;

    switch (page.name) {
      case "audience":
        this.setState({
          datePicker: false,
          location: true,
        })
        break;
      default:

        break;
    }
  }


  componentDidMount() {
    let { auth, filter } = this.props;
    let disabled = true;
    let startDate = "";
    let endDate = "";
    let period = filter.timeframe === "" ? filter.timeframe : auth.userData.period;
    this.getView();
    if (period === "adv") {
      disabled = false;
      startDate = filter.startDate;
      endDate = filter.endDate;
    } else {
      if (period === "") {
        period = "m"
      }
      let date = getDateTimeRangeByType(
        period,
        this.startDate,
        this.endDate
      )
      startDate = date.startTimeStamp;
      endDate = date.endTimeStamp;
    }
    this.setState(
      {
        period: period,
        disabled: disabled,
        startDate: startDate,
        endDate: endDate,
      }
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }
  UNSAFE_componentWillMount() {

  }

  render() {
    const {
      selectedLocations,
      startDate,
      endDate,
      disabled,
      locations,
      period,
      single,
      location
    } = this.state;

    return (
      <Container>
        <Header hasSegment style={styles.headerContainer}>
          <Left>
            <Button transparent onPress={() => this.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <TitleHeader>filter </TitleHeader>
          </Body>
        </Header>
        <Content>
          <ScrollView>
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined }}
                placeholder="Period"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={period}
                onValueChange={this.changePeriod.bind(this)}
              >
                <Picker.Item label="Today" value="t" />
                <Picker.Item label="Week" value="w" />
                <Picker.Item label="Month" value="m" />
                <Picker.Item label="Quarter" value="q" />
                <Picker.Item label="Year" value="y" />
                <Picker.Item label="Adv" value="adv" />
              </Picker>
            </Item>
            {disabled === false ?
              <>
                <Item disabled={false}>
                  <Text style={{ color: "#000", marginLeft: 5 }}>Start Date</Text>
                  <DatePicker
                    defaultDate={new Date(moment(startDate).format('YYYY-MM-DD'))}
                    // minimumDate={new Date(2018, 1, 1)}
                    maximumDate={new Date()}
                    locale={"id"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType="fade"
                    androidMode="default"
                    placeHolderText={null}
                    textStyle={{ color: "green" }}
                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                    onDateChange={this.setStartDate}
                    formatChosenDate={date => { return moment(date).format('DD MMM YYYY'); }}
                    disabled={disabled}
                  />
                </Item>
                <Item>
                  <Text style={{ color: "#000", marginLeft: 5 }}>End Date</Text>
                  <DatePicker
                    defaultDate={new Date(moment(endDate).format('YYYY-MM-DD'))}
                    // minimumDate={new Date(2020, 5, 1)}
                    maximumDate={new Date()}
                    locale={"id"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType="fade"
                    androidMode="default"
                    placeHolderText=""
                    textStyle={{ color: "green" }}
                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                    onDateChange={this.setEndDate}
                    formatChosenDate={date => { return moment(date).format('DD MMM YYYY'); }}
                    disabled={disabled}
                  />
                </Item>
              </>
              : null
            }

            {location === true ?
              <View style={{ marginLeft: 10 }}>
                <MultiSelect
                  hideTags
                  items={locations}
                  uniqueKey="id"
                  ref={(component) => {
                    this.multiSelect = component;
                  }}
                  onSelectedItemsChange={this.onSelectedLocationsChange}
                  selectedItems={selectedLocations}
                  selectText="Locations"
                  searchInputPlaceholderText="Search Locations ..."
                  onChangeInput={(text) => console.log(text)}
                  // altFontFamily="ProximaNova-Light"
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  displayKey="name"
                  searchInputStyle={{ color: "#CCC" }}
                  submitButtonColor="#CCC"
                  submitButtonText="Close"
                  single={single}
                  fontSize={18}
                  itemFontSize={18}
                />

                {single == false && (
                  <View>
                    {this.multiSelect &&
                      this.multiSelect.getSelectedItemsExt(selectedLocations)}
                  </View>
                )}
              </View>
              : null
            }
            <Button onPress={this.applyFilter} full primary>
              <Text>Apply</Text>
            </Button>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#fa423b",
  },
});

const mapStateToProps = (state) => {
  const { filter, page, auth } = state;

  return { filter, page, auth };
};

const mapDispatchToProps = {
  setFilter,
  getPage,
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterDate);
