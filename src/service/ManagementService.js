import { userApi } from "app/config";
import { objectParamToStringParam } from "app/shared/helpers/helpers";

export async function getRoles() {
  try {
    const url = `${userApi}role/getAll`;

    const response = await fetch(url, {
      method: "GET",
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}

export async function getUsers(params) {
  try {
    const baseUrl = `${userApi}user/getAll`;
    const strParam = objectParamToStringParam(params);
    const url = `${baseUrl}?${strParam}`;

    const response = await fetch(url, {
      method: "GET",
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}

/**
 * Update password
 * @param {*} params { username: string, old_password: string, new_password: string }
 */
export async function updatePassword(params) {
  try {
    const url = `${userApi}user/updatePassword`;

    const response = await fetch(url, {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}

/**
 * Update account
 * @param {*} params { username: string, old_password: string, new_password: string }
 */
export async function updateAccount(params) {
  try {
    const url = `${userApi}user/update`;

    const response = await fetch(url, {
      method: "PATCH",
      headers: {
        "Content-Type": "multipart/form-data",
      },
      body: params,
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}
