import { contentApi } from "app/config";

export default async function Content() { }

export async function latestPost(params) {
  try {
    const url = `${contentApi}latest-post`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}

export async function contentNews(params) {
  try {
    const url = `${contentApi}news`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
export async function postAudience(params) {
  try {
    const url = `${contentApi}post-audience`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
