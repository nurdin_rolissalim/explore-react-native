import { issueApi } from "app/config";

export default async function chart(params) {
  try {
    const url = `${issueApi}chart`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}
export async function issueChart(params) {
  try {
    const url = `${issueApi}chart`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}

export async function issueTop(params) {
  try {
    const url = `${issueApi}top`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        ContentType: "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}

export async function issueNews(params) {
  try {
    const url = `${issueApi}news`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}

export async function issueStatements(params) {
  try {
    const url = `${issueApi}statements`;
    params.time_zone = "Asia/Jakarta";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
export async function issueSentiment(params) {
  try {
    const url = `${issueApi}sentiment`;
    params.time_zone = "Asia/Jakarta";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
export async function issueInfluencer(params) {
  try {
    const url = `${issueApi}influencer`;
    params.time_zone = "Asia/Jakarta";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}

export async function socmedIssue(params) {
  try {
    const url = `${issueApi}socmed-issue`;
    params.time_zone = "Asia/Jakarta";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
