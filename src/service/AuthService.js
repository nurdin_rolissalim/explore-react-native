import { userApi } from "app/config";

export default async function Auth(params) {
  try {
    const url = `${userApi}auth/login`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const user = await response.json();

    return user;
  } catch (error) {
    return error;
  }
}

export async function login(params) {
  try {
    const url = `${userApi}auth/login`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });
    console.log("url", url);

    const user = await response.json();
    return user;
  } catch (error) {
    console.log("error", error);

    return error;
  }
}

export async function changeProfile(params) {
  try {
    const url = `${userApi}auth/login`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const user = await response.json();

    return user;
  } catch (error) {
    return error;
  }
}

export async function forgetPassword(params) {
  try {
    const url = `${userApi}auth/login`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const user = await response.json();

    return user;
  } catch (error) {
    return error;
  }
}

// export const getMessages = (updaterFn) => setListener('messages', updaterFn);
// export const postMessage = (message) => {
//     if (Boolean(message)) {
//         pushData('messages', {
//             incoming: false,
//             message
//         })
//     }
// }
// ​
// export {
//     login,
//     signup
// }
