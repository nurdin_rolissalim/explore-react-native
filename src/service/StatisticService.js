import { statisticApi } from "app/config";

export default async function statistic(params) {

    try {
        const url = `${statisticApi}total-post-type`;
        params.timezone = "Asia/Jakarta";

        const response = await fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(params),
        });

        const data = await response.json();
        return data;
    } catch (error) {
        return error;
    }
}
export async function totalPostType(params) {
    try {
        const url = `${statisticApi}total-post-type`;
        params.timezone = "Asia/Jakarta";
        console.log("url", url);

        const response = await fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(params),
        });

        const data = await response.json();
        return data;
    } catch (error) {
        return error;
    }
}
export async function totalPostSocmed(params) {
    try {
        const url = `${statisticApi}total-post-socmed`;
        params.timezone = "Asia/Jakarta";

        const response = await fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(params),
        });

        const data = await response.json();
        return data;
    } catch (error) {
        return error;
    }
}
export async function map(params) {
    try {
        const url = `${statisticApi}map`;
        params.timezone = "Asia/Jakarta";

        const response = await fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(params),
        });

        const data = await response.json();
        return data;
    } catch (error) {
        return error;
    }
}
export async function mapTotalData(params) {
    try {
        const url = `${statisticApi}map-total-data`;
        params.timezone = "Asia/Jakarta";

        const response = await fetch(url, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(params),
        });

        const data = await response.json();
        return data;
    } catch (error) {
        return error;
    }
}