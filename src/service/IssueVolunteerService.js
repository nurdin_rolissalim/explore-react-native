import { issueVolunteerApi } from "app/config";

export default async function issue(params) {

}
export async function get_categories(params) {
    try {
        const url = `${issueVolunteerApi}get_categories?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();

        return data;
    } catch (error) {
        return error;
    }
}

export async function getIssuesVolunteer(params) {
    try {
        const url = `${issueVolunteerApi}get_issues?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",

        });

        const data = await response.json();

        return data;
    } catch (error) {
        return error;
    }
}