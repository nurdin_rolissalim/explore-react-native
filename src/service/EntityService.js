import { entityApi, directoryImage, api } from "app/config";

export default async function Entity(params) { }

export async function getSubscription(params) {
  try {
    const url = `${entityApi}/get-subscription`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}

export async function checkImageUrl(value, type = "person") {
  let filename = value.replace(/ /g, "+").toLowerCase();
  let result = {
    file: `${directoryImage}` + type + `/${filename}.jpg`,
    default: api + "assets/images/person.png"
  }
  console.log("type", type);
  switch (type) {
    case "organization":
      result.default = "http://politica.ebdesk.com/assets/images/organization.png";
      break;
    case "location":
      result.default = "http://politica.ebdesk.com/assets/images/location.png";
      break;
    case "news":
      result.file = value
      result.default = "http://politica.ebdesk.com/assets/images/not_available_semiwide.png";
      break;
  }
  try {
    const response = await fetch(result.file);

    const data = await response;
    let image;
    if (data.status === 200) {
      image = result.file;
    } else {
      image = result.default;
    }

    return image;
  } catch (error) {
    return result.default;
  }
}

/**
 *
 * @param {Object} params
 *  **example**
 *  => {"keyword":"","order":"desc","order_by":"popular","page":1,"size":15,"type":"person"}
 *
 * @returns {Promise}
 */
export async function catalog(params) {
  try {
    const url = `${entityApi}/catalog`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}
