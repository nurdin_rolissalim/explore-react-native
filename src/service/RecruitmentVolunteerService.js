import { recruitmentVolunteerApi } from "app/config";

export default async function recruitment(params) {

}
export async function byEducation(params) {
    try {
        const url = `${recruitmentVolunteerApi}byEducation?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();
        let total = 0;

        for (let key in data.data) {
            total += data.data[key].total;
        }
        if (total > 0) {
            return data;
        }
        return false
    } catch (error) {
        return error;
    }
}

export async function volunteersGrowth(params) {
    try {
        const url = `${recruitmentVolunteerApi}volunteersGrowth`;
        const response = await fetch(url, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(params),
        });

        const data = await response.json();

        return data;
    } catch (error) {
        return error;
    }
}
export async function byGender(params) {
    try {
        //https://politica.blackeye.id/api/v3/recruitment/byGender?region=jawa%20barat&type_location=1&start_date=&end_date=&code_workspace=ridwan_kamil       
        const url = `${recruitmentVolunteerApi}byGender?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();
        let total = 0;

        for (let key in data.data) {
            total += data.data[key].total;
        }
        if (total > 0) {
            return data;
        }
        return false

    } catch (error) {
        return error;
    }
}
export async function byReligion(params) {
    try {
        //https://politica.blackeye.id/api/v3/recruitment/byGender?region=jawa%20barat&type_location=1&start_date=&end_date=&code_workspace=ridwan_kamil       
        const url = `${recruitmentVolunteerApi}byReligion?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();
        let total = 0;

        for (let key in data.data) {
            total += data.data[key].total;
        }
        if (total > 0) {
            return data;
        }
        return false

    } catch (error) {
        return error;
    }
}
export async function totalVolunteers(params) {
    try {
        //https://politica.blackeye.id/api/v3/recruitment/totalVolunteers?region=jawa%20barat&type_location=1&start_date=&end_date=&code_workspace=ridwan_kamil
        const url = `${recruitmentVolunteerApi}totalVolunteers?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();
        let total = 0;

        // for (let key in data.data) {
        //     total += data.data[key].total;
        // }
        // if (total > 0) {
        //     return data;
        // }
        return data

    } catch (error) {
        return error;
    }
}
export async function monthlyGrowth(params) {
    try {
        //https://politica.blackeye.id/api/v3/recruitment/monthlyGrowth?region=jawa%20barat&type_location=1&month=06&year=2020&code_workspace=ridwan_kamil
        const url = `${recruitmentVolunteerApi}monthlyGrowth?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();

        return data;

    } catch (error) {
        return error;
    }
}

