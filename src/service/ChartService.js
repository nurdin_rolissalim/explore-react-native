import { chartApi } from "app/config";

export default async function Chart(params) {
  try {
    const url = `${chartApi}chart`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response;

    return data;
  } catch (error) {
    return error;
  }
}

export async function mediaShare(params) {
  try {
    const url = `${chartApi}media-share`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}

export async function exposure(params) {
  try {
    const url = `${chartApi}exposure`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
export async function sentiment(params) {
  try {
    const url = `${chartApi}sentiment`;
    params.timezone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {
    return error;
  }
}

export async function trending(params) {
  try {
    const url = `${chartApi}trending`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}

export async function emotions(params) {
  try {
    const url = `${chartApi}emotions`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
export async function exposureSocmed(params) {
  try {
    const url = `${chartApi}exposure-socmed`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    return error;
  }
}
export async function sentimentSocmed(params) {
  try {
    const url = `${chartApi}sentiment-socmed`;
    params.time_zone = "Asia/Jakarta";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });

    const data = await response.json();

    return data;
  } catch (error) {

    return error;
  }
}

