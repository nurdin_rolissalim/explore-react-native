import { eventVolunteerApi } from "app/config";

export default async function event(params) {

}
export async function count(params) {
    try {
        //https://politica.blackeye.id/api/v3/event/count?region=jawa%20barat&type_location=1&code_workspace=ridwan_kamil&start_date=&end_date=
        const url = `${eventVolunteerApi}count?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();
        let total = 0;

        for (let key in data.data) {
            total += data.data[key].total;
        }
        if (total > 0) {
            return data;
        }
        return false
    } catch (error) {
        return error;
    }
}
export async function eventsCalendar(params) {
    try {
        // https://politica.blackeye.id/api/v3/event/events_calendar?region=jawa%20barat&type_location=1&code_workspace=ridwan_kamil&start_date=&end_date=
        const url = `${eventVolunteerApi}events_calendar?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();

        return data
    } catch (error) {
        return error;
    }
}


