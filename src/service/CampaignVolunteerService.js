import { campaignVolunteerApi } from "app/config";

export default async function campaign(params) {

}
export async function countCategory(params) {
    try {
        //https://politica.blackeye.id/api/v3/campaign/countCategory?region=jawa%20barat&type_location=1&code_workspace=ridwan_kamil&start_date=&end_date=
        const url = `${campaignVolunteerApi}countCategory?` + params.join('&');

        const response = await fetch(url, {
            method: "GET",
        });

        const data = await response.json();
        let total = 0;

        for (let key in data.data) {
            total += data.data[key].total;
        }
        if (total > 0) {
            return data;
        }
        return false
        return data;
    } catch (error) {
        return error;
    }
}

