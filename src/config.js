export const api = "http://politica.ebdesk.com/";

export const userApi = `${api}api/pol-account/`;

/* api chart */
export const chartApi = `${api}api/v1/chart/`;

export const entityApi = `${api}api/v1/entity/`;

export const issueApi = `${api}api/v1/issue/`;

export const contentApi = `${api}api/v1/content/`;

export const statisticApi = `${api}api/v1/statistic/`;

/* api volunteer  */
export const recruitmentVolunteerApi = `${api}api/v3/recruitment/`;

export const eventVolunteerApi = `${api}api/v3/event/`;

export const issueVolunteerApi = `${api}api/v3/issue/`;

export const campaignVolunteerApi = `${api}api/v3/campaign/`;

/* cdn/{type}
    type example : person,organization,location
 */
export const directoryImage = `${api}cdn/`;

export const apiGoogleMapAndroid = "AIzaSyBUfrpF4pXsZy3dSDjTXphZOuNV71rW2js"
