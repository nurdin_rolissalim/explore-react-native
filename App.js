/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useEffect, useRef, useState} from 'react';
import {Animated, Dimensions, StyleSheet, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RNBootSplash from 'react-native-bootsplash';
import BootSplash from 'react-native-bootsplash';

import TabScreen from './screen/TabScreen';
import LoginScreen from './screen/LoginScreen';
import MainScreen from './screen/MainScreen';
import NewsScreen from './screen/NewsScreen';
import Header from './components/Header';

let bootSplashLogo = require('./icon.png');


function App() {
  let [bootSplashIsVisible, setBootSplashIsVisible] = useState(true);
  let [bootSplashLogoIsLoaded, setBootSplashLogoIsLoaded] = useState(false);
  let opacity = useRef(new Animated.Value(1));
  let translateY = useRef(new Animated.Value(0));
  const screenOptions = { headerShown: false };
  const Stack = createStackNavigator();

  const Wrapper = props => (
    <>
      <Header />
      <MainScreen {...props} />
    </>
  );

  let init = async () => {
    BootSplash.hide();

    // You can uncomment this line to add a delay on app startup
    // let data = await fakeApiCallWithoutBadNetwork(3000);

    let useNativeDriver = true;

    Animated.stagger(250, [
      Animated.spring(translateY.current, {useNativeDriver, toValue: -50}),
      Animated.spring(translateY.current, {
        useNativeDriver,
        toValue: Dimensions.get('window').height,
      }),
    ]).start();

    Animated.timing(opacity.current, {
      useNativeDriver,
      toValue: 0,
      duration: 150,
      delay: 350,
    }).start(() => {
      setBootSplashIsVisible(false);
    });
  };

  useEffect(() => {
    bootSplashLogoIsLoaded && init();
  }, [bootSplashLogoIsLoaded]);

  return (
    <>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Home"
            options={screenOptions}
            component={Wrapper}
          />
          <Stack.Screen
            name="detail-news"
            options={screenOptions}
            component={NewsScreen}
          />
          <Stack.Screen
            name="Login"
            options={screenOptions}
            component={LoginScreen}
          />
        </Stack.Navigator>
        {bootSplashIsVisible && (
          <Animated.View
            style={[
              StyleSheet.absoluteFill,
              styles.bootsplash,
              {opacity: opacity.current},
            ]}>
            <Animated.Image
              source={bootSplashLogo}
              fadeDuration={0}
              onLoadEnd={() => setBootSplashLogoIsLoaded(true)}
              style={[
                styles.logo,
                {transform: [{translateY: translateY.current}]},
              ]}
            />
          </Animated.View>
        )}
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 24,
    fontWeight: '700',
    margin: 20,
    lineHeight: 30,
    color: '#333',
    textAlign: 'center',
  },
  bootsplash: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  logo: {
    height: 100,
    width: 100,
  },
});

export default App;
