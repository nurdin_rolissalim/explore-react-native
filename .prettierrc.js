module.exports = {
  bracketSpacing: true,
  // jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: true,
  tabWidth: 2,
};